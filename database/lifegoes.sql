-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 19, 2021 at 03:44 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lifegoes`
--

-- --------------------------------------------------------

--
-- Table structure for table `appendix_d5_s`
--

CREATE TABLE `appendix_d5_s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `provider_id` tinyint(4) DEFAULT NULL,
  `member_name` varchar(255) DEFAULT NULL,
  `medicaid_id` varchar(255) DEFAULT NULL,
  `date_completed` varchar(255) DEFAULT NULL,
  `current_street_address` varchar(255) DEFAULT NULL,
  `current_appointment_no` varchar(255) DEFAULT NULL,
  `current_city` varchar(255) DEFAULT NULL,
  `current_state` varchar(255) DEFAULT NULL,
  `current_zip` varchar(255) DEFAULT NULL,
  `current_country` varchar(255) DEFAULT NULL,
  `previous_street_address` varchar(255) DEFAULT NULL,
  `previous_appointment_no` varchar(255) DEFAULT NULL,
  `previous_city` varchar(255) DEFAULT NULL,
  `previous_state` varchar(255) DEFAULT NULL,
  `previous_zip` varchar(255) DEFAULT NULL,
  `previous_country` varchar(255) DEFAULT NULL,
  `living_setting` varchar(255) DEFAULT NULL,
  `relative_name` varchar(255) DEFAULT NULL,
  `relative_relationship_to_member` varchar(255) DEFAULT NULL,
  `relative_phone` varchar(255) DEFAULT NULL,
  `relative_alternate_phone` varchar(255) DEFAULT NULL,
  `relative_street_address` varchar(255) DEFAULT NULL,
  `relative_city` varchar(255) DEFAULT NULL,
  `relative_zip` varchar(255) DEFAULT NULL,
  `relative_country` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `appendix_d5_s`
--

INSERT INTO `appendix_d5_s` (`id`, `client_id`, `case_manager_id`, `provider_id`, `member_name`, `medicaid_id`, `date_completed`, `current_street_address`, `current_appointment_no`, `current_city`, `current_state`, `current_zip`, `current_country`, `previous_street_address`, `previous_appointment_no`, `previous_city`, `previous_state`, `previous_zip`, `previous_country`, `living_setting`, `relative_name`, `relative_relationship_to_member`, `relative_phone`, `relative_alternate_phone`, `relative_street_address`, `relative_city`, `relative_zip`, `relative_country`, `created_at`, `updated_at`) VALUES
(7, 7, NULL, NULL, 'mmmqwe', 'ttPC9NwpzB', NULL, 'ZxZrMJBiEY', 'LsIzHnVJSQ', 'LO5bvSwsV7', 'Fq47ZZqdb6', 'VDckfAWbZw', 'O9DZTdRbzW', 'Nwb27chUP3', 'Bg9dyv7YTR', 'BVqfHuWyuq', '05QIQ4FF5O', 'kmsloFrAxQ', 'bQVzhsgZO3', '6', 'A6nAX1SO7v', 'TcDHfUsGIy', '224730', '860688', 'nbZG5VRlST', '9eplN8XzRz', 'jTE5llm50p', 'tEN0Qt6WY9', '2021-05-15 06:38:08', '2021-05-15 06:38:08'),
(8, 8, NULL, NULL, 'CareForm', 'nc82Vkjym4', NULL, 'giGusXEgvg', 'Zr2pljJid8', 'RMWar10Mjq', 'TqL21zp5x2', 'Z4k4z4V4QL', 'aX6vWPP8bz', 'BfLxcz8p3X', '20ImSeJQuE', 'SQR6E0NCyd', 'tsHwps76Ln', 'HlTsTMy7SL', 'TsJ1uPtfKa', '6', 'lLsCZpgHhN', 'vvwrmSnXeh', '747152', '265807', 'IJ2aUhaPqM', 'd1nfqCkRDP', 'Mlrf0TpXNe', 'Kgw98ZutNP', '2021-05-15 06:46:03', '2021-05-15 06:46:03');

-- --------------------------------------------------------

--
-- Table structure for table `appendix_k1_s`
--

CREATE TABLE `appendix_k1_s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `provider_id` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `case_manager_id` varchar(255) DEFAULT NULL,
  `provider_name` varchar(255) DEFAULT NULL,
  `provider_number` varchar(255) DEFAULT NULL,
  `provider_address` varchar(255) DEFAULT NULL,
  `member_name` varchar(255) DEFAULT NULL,
  `medicaid_number` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `effective_date` varchar(255) DEFAULT NULL,
  `discharge_reasons` longtext DEFAULT NULL,
  `transfer_reasons` varchar(255) DEFAULT NULL,
  `provider_signature` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `appendix_k1_s`
--

INSERT INTO `appendix_k1_s` (`id`, `provider_id`, `client_id`, `case_manager_id`, `provider_name`, `provider_number`, `provider_address`, `member_name`, `medicaid_number`, `status`, `effective_date`, `discharge_reasons`, `transfer_reasons`, `provider_signature`, `date`, `created_at`, `updated_at`) VALUES
(2, NULL, '8', NULL, 'zmGKHDWTyf', '026516', '5EDV3Fi08U', 'CareForm', '8112444328', '', NULL, 'grV1ACBs0l', '', 'WHQ1RHzVhL', 'asdasdsa', '2021-05-15 06:46:29', '2021-05-15 06:46:29'),
(3, NULL, '8', NULL, 'zmGKHDWTyf', '026516', '5EDV3Fi08U', 'CareForm', '8112444328', '1,2', NULL, 'grV1ACBs0l,2,4,6', '1,2,3', 'WHQ1RHzVhL', 'asdasdsa', '2021-05-15 06:46:58', '2021-05-15 06:46:58');

-- --------------------------------------------------------

--
-- Table structure for table `careforms`
--

CREATE TABLE `careforms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `provider_id` tinyint(4) DEFAULT NULL,
  `member_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medicaid_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_completed` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_street_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_appointment_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_zip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previous_street_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previous_appointment_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previous_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previous_state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previous_zip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previous_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `living_setting` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relative_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relative_relationship_to_member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relative_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relative_alternate_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relative_street_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relative_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relative_zip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relative_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `careform_form1s`
--

CREATE TABLE `careform_form1s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `careform_form1_equipment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `careform_form1_equipment_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `careform_form1_miscellaneous` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `careform_form1_wheelchair` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `careform_form1_assistive` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `careform_form1_respiratory` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `careform_form1_mobility` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `careform_form1_bathroom` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `careform_form1_beds` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `careform_form1_transfer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `careform_form2s`
--

CREATE TABLE `careform_form2s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `icwp_supply_list` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gloves_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gloves_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gloves_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gloves_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adult_briefs_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adult_briefs_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adult_briefs_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adult_briefs_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bladder_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bladder_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bladder_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bladder_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chux_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chux_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chux_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chux_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wipes_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wipes_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wipes_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wipes_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skin_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skin_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skin_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skin_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reusable_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reusable_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reusable_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reusable_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urinal_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urinal_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urinal_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urinal_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bisacodyl_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bisacodyl_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bisacodyl_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bisacodyl_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `glycerin_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `glycerin_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `glycerin_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `glycerin_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uagic_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uagic_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uagic_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uagic_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fleet_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fleet_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fleet_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fleet_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lubricant_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lubricant_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lubricant_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lubricant_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urinary_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urinary_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urinary_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urinary_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tf_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tf_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tf_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tf_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `glycerin_swabs_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `glycerin_swabs_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `glycerin_swabs_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `glycerin_swabs_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `toothettes_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `toothettes_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `toothettes_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `toothettes_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slipAndpuff_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slipAndpuff_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slipAndpuff_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slipAndpuff_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bibs_disposable_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bibs_disposable_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bibs_disposable_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bibs_disposable_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bibs_reusable_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bibs_reusable_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bibs_reusable_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bibs_reusable_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `inner_cannula_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `inner_cannula_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `inner_cannula_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `inner_cannula_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trach_care_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trach_care_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trach_care_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trach_care_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trach_collar_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trach_collar_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trach_collar_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trach_collar_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `drain_sponge_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `drain_sponge_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `drain_sponge_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `drain_sponge_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `multidose_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `multidose_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `multidose_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `multidose_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barrier_spray_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barrier_spray_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barrier_spray_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barrier_spray_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `periwash_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `periwash_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `periwash_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `periwash_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `heel_protector_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `heel_protector_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `heel_protector_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `heel_protector_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `elbow_protectors_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `elbow_protectors_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `elbow_protectors_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `elbow_protectors_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skin_barrier_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skin_barrier_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skin_barrier_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skin_barrier_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `geomat_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `geomat_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `geomat_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `geomat_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reacher_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reacher_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reacher_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reacher_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `careform_form3s`
--

CREATE TABLE `careform_form3s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `icwp_financial_summary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `frequency_intensity_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `responsible_provider_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `per_unit_cost_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yearly_cast_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `frequency_intensity_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `responsible_provider_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `per_unit_cost_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yearly_cast_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `frequency_intensity_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `responsible_provider_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `per_unit_cost_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yearly_cast_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `frequency_intensity_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `responsible_provider_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `per_unit_cost_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yearly_cast_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `frequency_intensity_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `responsible_provider_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `per_unit_cost_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yearly_cast_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description_6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `frequency_intensity_6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `responsible_provider_6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `per_unit_cost_6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yearly_cast_6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description_7` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `frequency_intensity_7` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `responsible_provider_7` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `per_unit_cost_7` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yearly_cast_7` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description_8` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `frequency_intensity_8` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `responsible_provider_8` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `per_unit_cost_8` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yearly_cast_8` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `careform_form4s`
--

CREATE TABLE `careform_form4s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plan_of_care_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `your_icwp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `your_icwp_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `your_emergency_contact_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `your_emergency_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_a_provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_a_provider_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `your_primary_care_physician` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `your_primary_care_physician_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_e` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_e_sp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_e_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_e_equipment_provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_e_equipment_provider_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hospital_for_emergencies` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `h_f_e_phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `h_f_e_address1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `h_f_e_address2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `behaviour_managment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note_6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `careform_form5s`
--

CREATE TABLE `careform_form5s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `name_of_member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `if_available` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `request_and_authoriz` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_obtain_from` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `following_types1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `following_types2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `for_the_perpose1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `for_the_perpose2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `all_information_authorize` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `i_understand_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_of_member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_of_witness` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_relation_member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sign_parent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sign_parent_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `withdrawn_member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_member_2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `careform_form6s`
--

CREATE TABLE `careform_form6s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `name_of_member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medicaid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_quarter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_quarter_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_quarter_cm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_quarter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_quarter_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_quarter_cm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `third_quarter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `third_quarter_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `third_quarter_cm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forth_quarter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forth_quarter_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forth_quarter_cm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `careform_form7s`
--

CREATE TABLE `careform_form7s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `member` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medicaid_number` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `careform_form8s`
--

CREATE TABLE `careform_form8s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `careform_form9s`
--

CREATE TABLE `careform_form9s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `careform_form10s`
--

CREATE TABLE `careform_form10s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `this` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day_of` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year20` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `print_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_member_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `print_name_cm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_cm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `careform_form11s`
--

CREATE TABLE `careform_form11s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `careform_form12s`
--

CREATE TABLE `careform_form12s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `this` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day_of` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year20` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `print_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_member_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `print_name_cm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_cm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `careform_form13s`
--

CREATE TABLE `careform_form13s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `case_manager` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `participant` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `participant_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `authorized_representative` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `authorized_representative_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `witness` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `witness_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refusal_participant` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refusal_participant_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refusal_authorized_representative` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refusal_authorized_representative_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refusal_witness` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refusal_witness_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `careform_form14s`
--

CREATE TABLE `careform_form14s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `member_name_last` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Care_plan_due_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Member_Name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_name_First` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_name_Middle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_name_Suffix` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `City` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `State` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Zip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Cell` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Date_of_Birth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Age` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `past_year` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DFCS` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Living_Arrangements` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `compared_to_1_year_ago` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Current_Diagnosis_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Current_Diagnosis_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Current_Diagnosis_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Current_Diagnosis_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Current_Diagnosis_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Current_Diagnosis_6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Current_Diagnosis_7` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Current_Diagnosis_8` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Current_Diagnosis_9` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Current_Diagnosis_10` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Current_Diagnosis_11` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Current_Diagnosis_12` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `careform_form15s`
--

CREATE TABLE `careform_form15s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `member_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Medical_List_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Medical_List_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Medical_List_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Medical_Lis5_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Medical_List_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Medical_List_6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Medical_List_7` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Medical_List_8` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Medical_List_9` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Medical_List_10` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Medical_List_11` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Medical_List_12` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Skilled` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Evaluation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Potential` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ICWP_member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Comments` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `careform_form16s`
--

CREATE TABLE `careform_form16s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `member_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `physician_visit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `annual_period` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `describe` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hospitalizations_visit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `diagnosis_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `variance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facility_car` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facility_placement` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facility_placement_reasons` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `progressive_disease` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disease_state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_past_year` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `safety_implemented` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contributing_factors` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urinary_incontinence` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bowel_incontinence` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `devices` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Comments` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `careform_form17s`
--

CREATE TABLE `careform_form17s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `member_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_wound` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `where_is_located` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `where_the_wound` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `separate_attachment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comments1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primary_modes` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comments2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `careform_form18s`
--

CREATE TABLE `careform_form18s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `member_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `based_on_your_personal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `classify_memory` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comments1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `behavior_issues_yes_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `behavior_issues_yes` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smoking_without` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smoking_without_yes` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suspected_alcohol` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suspected_alcohol_yes` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suspected_drug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suspected_drug_yes` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comments2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `personal_assistance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comments3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `careform_form19s`
--

CREATE TABLE `careform_form19s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `member_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `safety_hazards` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sanitation_hazards` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_functioning` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isolation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `community_activities` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `careform_form20s`
--

CREATE TABLE `careform_form20s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `member_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `support_system` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `renewal_visit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attestation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `explain` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Caregiver` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assistance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `support` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lives_Member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Relationship_to_Member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Lives_with_Member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Relationship_to_memberd` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Signature` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `careform_form21s`
--

CREATE TABLE `careform_form21s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `member_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Summary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Signature` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `careform_form22s`
--

CREATE TABLE `careform_form22s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `member_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `i_agree_to_provide` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skills` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sign1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relationship1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sign2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relationship2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sign3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `witness` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cases_assignments`
--

CREATE TABLE `cases_assignments` (
  `id` int(11) NOT NULL,
  `staff_id` tinyint(4) DEFAULT NULL,
  `manager_id` tinyint(4) DEFAULT NULL,
  `staff_name` varchar(255) DEFAULT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `client_name` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cases_assignments`
--

INSERT INTO `cases_assignments` (`id`, `staff_id`, `manager_id`, `staff_name`, `client_id`, `client_name`, `status`, `created_at`, `updated_at`) VALUES
(15, 6, 6, NULL, 5, NULL, NULL, '2021-04-21 09:23:28', '2021-04-21 09:23:28'),
(17, 6, 6, NULL, 6, NULL, NULL, '2021-04-21 05:49:17', '2021-04-21 05:49:17'),
(18, 8, 8, NULL, 7, NULL, NULL, '2021-05-15 06:36:58', '2021-05-15 06:36:58'),
(19, 9, 9, NULL, 8, NULL, NULL, '2021-05-15 06:44:56', '2021-05-15 06:44:56'),
(20, 10, 10, NULL, 9, NULL, NULL, '2021-05-17 06:39:19', '2021-05-17 06:39:19'),
(21, 10, 10, NULL, 9, NULL, NULL, '2021-05-17 06:39:50', '2021-05-17 06:39:50');

-- --------------------------------------------------------

--
-- Table structure for table `case_managers`
--

CREATE TABLE `case_managers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `no_of_cases` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `case_managers`
--

INSERT INTO `case_managers` (`id`, `staff_id`, `no_of_cases`, `status`, `date`, `created_at`, `updated_at`) VALUES
(16, 6, NULL, '1', NULL, '2021-04-21 05:39:11', '2021-04-21 05:39:11'),
(17, 8, NULL, '1', NULL, '2021-05-15 06:36:26', '2021-05-15 06:36:26'),
(18, 9, NULL, '1', NULL, '2021-05-15 06:43:56', '2021-05-15 06:43:56'),
(19, 10, NULL, '1', NULL, '2021-05-17 06:32:42', '2021-05-17 06:32:42');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `county` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `user_id`, `name`, `contact`, `age`, `country`, `state`, `city`, `county`, `area`, `zip_code`, `status`, `created_at`, `updated_at`) VALUES
(5, 38, 'Client1', '123213', NULL, 'USA', 'Alaska', 'New York', 'Washington', 'abc', '78551', '1', '2021-04-21 09:17:19', '2021-04-21 09:17:19'),
(6, 41, 'Finch', '12313123123', NULL, 'USA', 'Alkian', 'New York', 'Washington', 'abcd', '12312312', '1', '2021-04-21 05:48:23', '2021-04-21 05:48:23'),
(7, 42, 'Client3', '0098098908', NULL, 'USA', 'Alaska', 'New York', 'Washington', 'abc', '1234', '1', '2021-05-15 06:34:03', '2021-05-15 06:34:03'),
(8, 44, 'client4', '1212312', NULL, 'USA', 'Alaska', 'New York', 'Washingtone', 'abc', '1221212', '1', '2021-05-15 06:41:19', '2021-05-15 06:41:19'),
(9, 47, 'testclient', '4534543', NULL, 'usa', 'washington', 'newyork', 'testclient', 'testclient', 'testclient', '1', '2021-05-17 06:35:47', '2021-05-17 06:35:47');

-- --------------------------------------------------------

--
-- Table structure for table `client_cases`
--

CREATE TABLE `client_cases` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `documents` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `client_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `county` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_height` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_case_status` tinyint(4) DEFAULT NULL,
  `assigned_status` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `case_manager_name` tinyint(4) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `client_change_forms`
--

CREATE TABLE `client_change_forms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_change` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `change_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_new_case_manager` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved_hours` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remaining_hours` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `additionals_notes_documentation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_signature` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_signature` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `client_change_forms`
--

INSERT INTO `client_change_forms` (`id`, `provider_id`, `client_id`, `case_manager_id`, `member_name`, `date_of_change`, `change_type`, `name_new_case_manager`, `approved_hours`, `remaining_hours`, `additionals_notes_documentation`, `case_manager_signature`, `case_manager_date`, `owner_signature`, `owner_date`, `created_at`, `updated_at`) VALUES
(0, NULL, '8', NULL, 'CareForm', NULL, '1,2,4', 'su0jWztNJS', 'cobEc0nOxF', 'iEd29X52D9', 'hmbSGafz3A', 'xsltdMwov8', 'qwe', '9l0AyILSt7', NULL, '2021-05-15 06:47:32', '2021-05-15 06:47:32'),
(0, NULL, '8', NULL, 'CareForm', NULL, '1,2,4', 'su0jWztNJS', 'cobEc0nOxF', 'iEd29X52D9', 'hmbSGafz3A', 'xsltdMwov8', 'qwe', '9l0AyILSt7', NULL, '2021-05-15 06:47:32', '2021-05-15 06:47:32');

-- --------------------------------------------------------

--
-- Table structure for table `diseases`
--

CREATE TABLE `diseases` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `followups`
--

CREATE TABLE `followups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `st_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `et_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plan_fu1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doc_hrs1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `st_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `et_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plan_fu2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doc_hrs2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `st_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `et_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plan_fu3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doc_hrs3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `st_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `et_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plan_fu4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doc_hrs4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_signature` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_signature` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `followups`
--

INSERT INTO `followups` (`id`, `provider_id`, `client_id`, `case_manager_id`, `member_name`, `date_1`, `st_1`, `et_1`, `plan_fu1`, `notes1`, `doc_hrs1`, `date_2`, `st_2`, `et_2`, `plan_fu2`, `notes2`, `doc_hrs2`, `date_3`, `st_3`, `et_3`, `plan_fu3`, `notes3`, `doc_hrs3`, `date_4`, `st_4`, `et_4`, `plan_fu4`, `notes4`, `doc_hrs4`, `case_manager_signature`, `case_manager_date`, `owner_signature`, `owner_date`, `created_at`, `updated_at`) VALUES
(4, NULL, '6', NULL, 'Josiah Gaines', '14-Nov-1970', 'Qui eos aliquip quas', 'Obcaecati consectetu', 'Quis aliquid sit ve', 'Quasi dolores odit s', 'Voluptas est officii', '28-Sep-1973', 'In qui libero simili', 'Minus enim sunt nihi', 'Sit dolore iure exc', 'Enim iure eos perspi', 'Omnis aut enim in pr', '07-Jun-2007', 'Esse voluptatum vel', 'Consequat Numquam i', 'Quia sed necessitati', 'Tempore magnam tene', 'Tempora quas labore', '25-Feb-1985', 'Nemo est consequatur', 'Tempore dolor volup', 'Incidunt et elit n', 'Nostrum officia susc', 'In accusantium sed q', 'Fugiat consectetur', '01-Nov-1970', 'Dolore quidem accusa', '11-Apr-1989', '2021-05-04 01:53:17', '2021-05-04 01:53:17'),
(5, NULL, '8', NULL, 'CareForm', NULL, '4TWVAzv6bT', 'Wqp8u4ow7r', 'vENhifIV0V', 'LtYTDrlKKD', 'reiPgQWpi3', NULL, 'aadInj2HlB', 'hvN1nYDR90', 'm7gJ7BSGYN', '4StrAsvPiP', 'YkPpRLrOUA', NULL, 'VfCZyAMaW8', '1vNc3whMFz', 'yV43LdktcM', 'FNrMdpIoI4', 'mIJIng40Sp', NULL, 'PXHqVN4AIi', 'UlpiYhblmv', 'mLA38duFHp', 'AirQDjCFMD', 'cLLdafieTT', 'zdvPU7WYsf', 'qwe', '7ijuJ101TQ', NULL, '2021-05-15 06:48:41', '2021-05-15 06:48:41');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `monthly_contact_forms`
--

CREATE TABLE `monthly_contact_forms` (
  `id` bigint(4) NOT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cal_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_of_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `worker` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cal_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `topic_of_discussion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `narraitive_requirement` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `curent_source` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `curent_icwp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_relationship` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contacts_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contacts_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contacts_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contacts_relationship` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reviewed_population` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `varified` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_signature` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_signature` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `monthly_visit_forms`
--

CREATE TABLE `monthly_visit_forms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `monthly_contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `st_et` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_hm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `documentation_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permision_for_tele` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verbal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nonverbal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_present` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appearence_of_emotional` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `self_reported_mood` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `monthly_health_provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral_needed` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_hygiene` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smoke_cigarettes` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `per_day` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appearence_home_envoirnment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qr` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cp_renewal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `initial_cp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pss_agency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_attendence` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `schedule` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `services_report` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comments_problems` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medical` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `detail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_pcp_medical_visit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `next_pcp_medical_visit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pcp_medication_conditions` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accu_checks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trach` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ventilator` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oxygen` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skin_integrity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active_wound_care` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `decubitus_stage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `how_offen_are_turned` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uti` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bowel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bladder` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ambulation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transfer_assistance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_hrs` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fall_past` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `identify_risk_factor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meal_plan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_activities` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `summar_followup` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_authorized` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_authorized` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `providers`
--

CREATE TABLE `providers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `county` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `client_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `providers`
--

INSERT INTO `providers` (`id`, `user_id`, `name`, `email`, `contact`, `country`, `state`, `city`, `county`, `area`, `zip_code`, `status`, `created_at`, `updated_at`, `client_id`, `client_name`, `password`) VALUES
(6, 39, 'provider1', 'Provider1@gmail.com', '12312312', 'USA', 'Alaska', 'New York', 'Washington', 'abv', '12312312', '1', '2021-04-21 09:45:00', '2021-04-21 09:45:00', NULL, NULL, 'provider1');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `county` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_manager` tinyint(4) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `user_id`, `name`, `contact`, `country`, `state`, `city`, `county`, `area`, `zip_code`, `status`, `is_manager`, `created_at`, `updated_at`) VALUES
(6, 37, 'staff1', '213213123', 'USA', 'Alaska', 'New York', 'California', 'ABC', '78854', NULL, 1, '2021-04-21 09:14:14', '2021-04-21 05:39:11'),
(7, 40, 'Bravo', '21312312', 'USA', 'Alaska', 'New York', 'Washington', 'abcd', '112312', NULL, 0, '2021-04-21 05:47:05', '2021-04-21 05:47:05'),
(8, 43, 'newStaff', '00000000000', 'USA', 'Alaska', 'New York', 'Washington', 'abc', '1223221', NULL, 1, '2021-05-15 06:36:13', '2021-05-15 06:36:26'),
(9, 45, 'staff4', '123123123', 'USA', 'Alaska', 'New York', 'Washingtone', 'abc', '123123', NULL, 1, '2021-05-15 06:43:07', '2021-05-15 06:43:56'),
(10, 46, 'test', '3432423', 'usa', 'washington', 'newyork', 'test', 'test', 'test', NULL, 1, '2021-05-17 06:31:53', '2021-05-17 06:32:42');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `roles` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `county` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assigned_disease` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `roles`, `country`, `city`, `county`, `contact`, `assigned_disease`) VALUES
(36, 'admin', 'admin@gmail.com', NULL, '$2y$10$KypKV8JtxGltQhmDc4ZYq.uqbduHcJ2AqA6K3bVv887yB3ttYFNtq', NULL, NULL, NULL, 'admin', 'USA', 'New York', 'Washington', '123123123', NULL),
(37, 'staff1', 'staff1@gmail.com', NULL, '$2y$10$ef.e1Lmkyeg.xe9KmOZUDu7a720Wf0o/GJ3RYTPNbtJ/n6hwogYq.', NULL, '2021-04-21 09:14:14', '2021-04-21 09:14:14', 'staff', NULL, NULL, NULL, NULL, NULL),
(38, 'Client1', 'client1@gmail.com', NULL, '$2y$10$KypKV8JtxGltQhmDc4ZYq.uqbduHcJ2AqA6K3bVv887yB3ttYFNtq', NULL, '2021-04-21 09:17:19', '2021-04-21 09:17:19', 'client', NULL, NULL, NULL, NULL, NULL),
(39, 'provider1', 'Provider1@gmail.com', NULL, '$2y$10$90gYwxaYHfLdBmgXC2NpoemWqWKDKJfyTf6OCuY2eRn6k9yI6W8p2', NULL, '2021-04-21 09:45:00', '2021-04-21 09:45:00', 'provider', NULL, NULL, NULL, NULL, NULL),
(40, 'Bravo', 'bravo@gmail.com', NULL, '$2y$10$t2t3wX0BdN7Z4eg7npkUJeRmUvcaNGPtPpubeI8ZzVLN00VWpc4wW', NULL, '2021-04-21 05:47:05', '2021-04-21 05:47:05', 'staff', NULL, NULL, NULL, NULL, NULL),
(41, 'Finch', 'finch@gmail.com', NULL, '$2y$10$47OlDy50/879H/BIXmrsweslTp1dVPwMICQL.1NUvY..TkDzgmAei', NULL, '2021-04-21 05:48:22', '2021-04-21 05:48:22', 'client', NULL, NULL, NULL, NULL, NULL),
(42, 'Client3', 'Client3@gmail.com', NULL, '$2y$10$AsSpLC0te61nl/uRrPa/8.pLEVKTLKT4KzMvp4UBsDh999p0JA3N.', NULL, '2021-05-15 06:34:03', '2021-05-15 06:34:03', 'client', NULL, NULL, NULL, NULL, NULL),
(43, 'newStaff', 'newStaff@gmail.com', NULL, '$2y$10$9Baz7ZezB3jfXRLST0r0DuM9PEbIOrSr1nxkiHweC5SUPd4F/BNAC', NULL, '2021-05-15 06:36:13', '2021-05-15 06:36:13', 'staff', NULL, NULL, NULL, NULL, NULL),
(44, 'client4', 'client4@gmail.com', NULL, '$2y$10$5kSqPdAd0j9bcaMe3Orga.u9E94AlTJX2WsrQT2/m6rSIzelq9mz6', NULL, '2021-05-15 06:41:19', '2021-05-15 06:41:19', 'client', NULL, NULL, NULL, NULL, NULL),
(45, 'staff4', 'staff4@gmail.com', NULL, '$2y$10$RDbfn44R31K0rdie.Q09NONUnQEEcND/pYTMXrAEsYE0tW29VksYW', NULL, '2021-05-15 06:43:07', '2021-05-15 06:43:07', 'staff', NULL, NULL, NULL, NULL, NULL),
(46, 'test', 'test@gmail.com', NULL, '$2y$10$jIg93b84OexJ6Uqt55jWHOsAXlOv7ZUPngCec11KyMhME1aFijpV6', NULL, '2021-05-17 06:31:53', '2021-05-17 06:31:53', 'staff', NULL, NULL, NULL, NULL, NULL),
(47, 'testclient', 'testclient@gmail.com', NULL, '$2y$10$tyg7uN0keeaA4GfI8nZ.9utLlE2OyXxi28PAKIq/Lj4LfTbCVaj5C', NULL, '2021-05-17 06:35:47', '2021-05-17 06:35:47', 'client', NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appendix_d5_s`
--
ALTER TABLE `appendix_d5_s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `appendix_k1_s`
--
ALTER TABLE `appendix_k1_s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careforms`
--
ALTER TABLE `careforms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form1s`
--
ALTER TABLE `careform_form1s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form2s`
--
ALTER TABLE `careform_form2s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form3s`
--
ALTER TABLE `careform_form3s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form4s`
--
ALTER TABLE `careform_form4s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form5s`
--
ALTER TABLE `careform_form5s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form6s`
--
ALTER TABLE `careform_form6s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form7s`
--
ALTER TABLE `careform_form7s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form8s`
--
ALTER TABLE `careform_form8s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form9s`
--
ALTER TABLE `careform_form9s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form10s`
--
ALTER TABLE `careform_form10s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form11s`
--
ALTER TABLE `careform_form11s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form12s`
--
ALTER TABLE `careform_form12s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form13s`
--
ALTER TABLE `careform_form13s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form14s`
--
ALTER TABLE `careform_form14s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form15s`
--
ALTER TABLE `careform_form15s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form16s`
--
ALTER TABLE `careform_form16s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form17s`
--
ALTER TABLE `careform_form17s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form18s`
--
ALTER TABLE `careform_form18s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form19s`
--
ALTER TABLE `careform_form19s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form20s`
--
ALTER TABLE `careform_form20s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form21s`
--
ALTER TABLE `careform_form21s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form22s`
--
ALTER TABLE `careform_form22s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cases_assignments`
--
ALTER TABLE `cases_assignments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `case_managers`
--
ALTER TABLE `case_managers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_cases`
--
ALTER TABLE `client_cases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diseases`
--
ALTER TABLE `diseases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `followups`
--
ALTER TABLE `followups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `monthly_contact_forms`
--
ALTER TABLE `monthly_contact_forms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `providers`
--
ALTER TABLE `providers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appendix_d5_s`
--
ALTER TABLE `appendix_d5_s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `appendix_k1_s`
--
ALTER TABLE `appendix_k1_s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `careforms`
--
ALTER TABLE `careforms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `careform_form1s`
--
ALTER TABLE `careform_form1s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `careform_form2s`
--
ALTER TABLE `careform_form2s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `careform_form3s`
--
ALTER TABLE `careform_form3s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `careform_form4s`
--
ALTER TABLE `careform_form4s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `careform_form5s`
--
ALTER TABLE `careform_form5s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `careform_form6s`
--
ALTER TABLE `careform_form6s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `careform_form7s`
--
ALTER TABLE `careform_form7s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `careform_form8s`
--
ALTER TABLE `careform_form8s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `careform_form9s`
--
ALTER TABLE `careform_form9s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `careform_form10s`
--
ALTER TABLE `careform_form10s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `careform_form11s`
--
ALTER TABLE `careform_form11s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `careform_form12s`
--
ALTER TABLE `careform_form12s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `careform_form13s`
--
ALTER TABLE `careform_form13s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `careform_form14s`
--
ALTER TABLE `careform_form14s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `careform_form15s`
--
ALTER TABLE `careform_form15s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `careform_form16s`
--
ALTER TABLE `careform_form16s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `careform_form17s`
--
ALTER TABLE `careform_form17s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `careform_form18s`
--
ALTER TABLE `careform_form18s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `careform_form19s`
--
ALTER TABLE `careform_form19s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `careform_form20s`
--
ALTER TABLE `careform_form20s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `careform_form21s`
--
ALTER TABLE `careform_form21s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `careform_form22s`
--
ALTER TABLE `careform_form22s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cases_assignments`
--
ALTER TABLE `cases_assignments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `case_managers`
--
ALTER TABLE `case_managers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `client_cases`
--
ALTER TABLE `client_cases`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `diseases`
--
ALTER TABLE `diseases`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `followups`
--
ALTER TABLE `followups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `monthly_contact_forms`
--
ALTER TABLE `monthly_contact_forms`
  MODIFY `id` bigint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `providers`
--
ALTER TABLE `providers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
