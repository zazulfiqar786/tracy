<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFollowupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('followups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('provider_id')->nullable();
            $table->string('client_id')->nullable();
            $table->string('case_manager_id')->nullable();

            $table->string('member_name')->nullable();
            $table->string('date_1')->nullable();
            $table->string('st_1')->nullable();
            $table->string('et_1')->nullable();
            $table->string('plan_fu1')->nullable();
            $table->string('notes1')->nullable();
            $table->string('doc_hrs1')->nullable();

            $table->string('date_2')->nullable();
            $table->string('st_2')->nullable();
            $table->string('et_2')->nullable();
            $table->string('plan_fu2')->nullable();
            $table->string('notes2')->nullable();
            $table->string('doc_hrs2')->nullable();

            $table->string('date_3')->nullable();
            $table->string('st_3')->nullable();
            $table->string('et_3')->nullable();
            $table->string('plan_fu3')->nullable();
            $table->string('notes3')->nullable();
            $table->string('doc_hrs3')->nullable();

            $table->string('date_4')->nullable();
            $table->string('st_4')->nullable();
            $table->string('et_4')->nullable();
            $table->string('plan_fu4')->nullable();
            $table->string('notes4')->nullable();
            $table->string('doc_hrs4')->nullable();

            $table->string('case_manager_signature')->nullable();
            $table->string('case_manager_date')->nullable();
            $table->string('owner_signature')->nullable();
            $table->string('owner_date')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('followups');
    }
}
