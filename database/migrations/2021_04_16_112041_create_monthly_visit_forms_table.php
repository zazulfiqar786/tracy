<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonthlyVisitFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monthly_visit_forms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('provider_id')->nullable();
            $table->string('client_id')->nullable();
            $table->string('case_manager_id')->nullable();
            $table->string('monthly_contact')->nullable();
            $table->string('Date')->nullable();
            $table->string('st_et')->nullable();
            $table->string('total_hm')->nullable();
            $table->string('documentation_time')->nullable();
            $table->string('permision_for_tele')->nullable();
            $table->string('member')->nullable();
            $table->string('verbal')->nullable();
            $table->string('nonverbal')->nullable();
            $table->string('other_present')->nullable();
            $table->string('appearence_of_emotional')->nullable();
            $table->string('self_reported_mood')->nullable();


            $table->string('monthly_health_provider')->nullable();
            $table->string('name_contact')->nullable();
            $table->string('referral_needed')->nullable();
            $table->string('member_hygiene')->nullable();
            $table->string('smoke_cigarettes')->nullable();
            $table->string('appearence_home_envoirnment')->nullable();
            $table->string('qr')->nullable();
            $table->string('cp_renewal')->nullable();
            $table->string('initial_cp')->nullable();
            $table->string('pss_agency')->nullable();
            $table->string('no_of_attendence')->nullable();
            $table->string('schedule')->nullable();
            $table->string('services_report')->nullable();

            $table->string('comments_problems')->nullable();
            $table->string('medical')->nullable();
            $table->string('detail')->nullable();
            $table->string('last_pcp_medical_visit')->nullable();
            $table->string('next_pcp_medical_visit')->nullable();
            $table->string('pcp_medication_conditions')->nullable();
            $table->string('accu_checks')->nullable();
            $table->string('trach')->nullable();
            $table->string('ventilator')->nullable();
            $table->string('oxygen')->nullable();
            $table->string('other')->nullable();

            $table->string('skin_integrity')->nullable();
            $table->string('active_wound_care')->nullable();
            $table->string('decubitus_stage')->nullable();
            $table->string('how_offen_are_turned')->nullable();
            $table->string('uti')->nullable();
            $table->string('bowel')->nullable();
            $table->string('bladder')->nullable();
            $table->string('ambulation')->nullable();

            $table->string('transfer_assistance')->nullable();
            $table->string('no_of_hrs')->nullable();
            $table->string('fall_past')->nullable();
            $table->string('identify_risk_factor')->nullable();

            $table->string('meal_plan')->nullable();
            $table->string('social_activities')->nullable();
            $table->string('summar_followup')->nullable();
            $table->string('member_authorized')->nullable();
            $table->string('date_authorized')->nullable();
            $table->string('case_manager_date')->nullable();
            $table->string('owner_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monthly_visit_forms');
    }
}
