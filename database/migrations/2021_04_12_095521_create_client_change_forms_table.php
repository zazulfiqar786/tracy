<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientChangeFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_change_forms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('provider_id')->nullable();
            $table->string('client_id')->nullable();
            $table->string('case_manager_id')->nullable();
            $table->string('member_name')->nullable();
            $table->string('date_of_change')->nullable();
            $table->string('change_type')->nullable();
            $table->string('name_new_case_manager')->nullable();
            $table->string('approved_hours')->nullable();
            $table->string('remaining_hours')->nullable();
            $table->string('additionals_notes_documentation')->nullable();
            $table->string('case_manager_signature')->nullable();
            $table->string('case_manager_date')->nullable();
            $table->string('owner_signature')->nullable();
            $table->string('owner_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_change_forms');
    }
}
