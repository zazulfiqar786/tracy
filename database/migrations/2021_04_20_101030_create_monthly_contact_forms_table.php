<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonthlyContactFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monthly_contact_forms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('provider_id')->nullable();
            $table->string('client_id')->nullable();
            $table->string('case_manager_id')->nullable();
            $table->string('cal_date')->nullable();
            $table->string('time_of_date')->nullable();
            $table->string('worker')->nullable();
            $table->string('cal_type')->nullable();
            $table->string('contact_name')->nullable();
            $table->string('relation')->nullable();
            $table->string('topic_of_discussion')->nullable();
            $table->string('narraitive_requirement')->nullable();
            $table->string('curent_source')->nullable();
            $table->string('curent_icwp')->nullable();
            $table->string('file_name')->nullable();
            $table->string('file_address')->nullable();
            $table->string('file_phone')->nullable();
            $table->string('file_relationship')->nullable();
            $table->string('contacts_name')->nullable();
            $table->string('contacts_address')->nullable();
            $table->string('contacts_phone')->nullable();
            $table->string('contacts_relationship')->nullable();
            $table->string('reviewed_population')->nullable();
            $table->string('varified')->nullable();
            $table->string('case_manager_signature')->nullable();
            $table->string('case_manager_date')->nullable();
            $table->string('owner_signature')->nullable();
            $table->string('owner_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monthly_contact_forms');
    }
}
