<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareformForm20sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careform_form20s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('client_id')->nullable();
            $table->tinyInteger('case_manager_id')->nullable();
            $table->tinyInteger('careform_id')->nullable();
            $table->string('member_name')->nullable();
            $table->string('support_system')->nullable();
            $table->string('renewal_visit')->nullable();
            $table->string('attestation')->nullable();
            $table->string('explain')->nullable();
            $table->string('Caregiver')->nullable();
            $table->string('assistance')->nullable();
            $table->string('support')->nullable();
            $table->string('lives_Member')->nullable();
            $table->string('Relationship_to_Member')->nullable();
            $table->string('Lives_with_Member')->nullable();
            $table->string('Relationship_to_memberd')->nullable();
            $table->string('Signature')->nullable();
            $table->string('Date')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careform_form20s');
    }
}
