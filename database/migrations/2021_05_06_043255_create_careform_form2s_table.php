<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareformForm2sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careform_form2s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('client_id')->nullable();
            $table->tinyInteger('case_manager_id')->nullable();
            $table->tinyInteger('careform_id')->nullable();

            $table->string('icwp_supply_list')->nullable();
            $table->string('date')->nullable();


            $table->string('gloves_permonth')->nullable();
            $table->string('gloves_peryear')->nullable();
            $table->string('gloves_perunit')->nullable();
            $table->string('gloves_totalyear')->nullable();

            $table->string('adult_briefs_permonth')->nullable();
            $table->string('adult_briefs_peryear')->nullable();
            $table->string('adult_briefs_perunit')->nullable();
            $table->string('adult_briefs_totalyear')->nullable();

            $table->string('bladder_permonth')->nullable();
            $table->string('bladder_peryear')->nullable();
            $table->string('bladder_perunit')->nullable();
            $table->string('bladder_totalyear')->nullable();

            $table->string('chux_permonth')->nullable();
            $table->string('chux_peryear')->nullable();
            $table->string('chux_perunit')->nullable();
            $table->string('chux_totalyear')->nullable();

            $table->string('wipes_permonth')->nullable();
            $table->string('wipes_peryear')->nullable();
            $table->string('wipes_perunit')->nullable();
            $table->string('wipes_totalyear')->nullable();

            $table->string('skin_permonth')->nullable();
            $table->string('skin_peryear')->nullable();
            $table->string('skin_perunit')->nullable();
            $table->string('skin_totalyear')->nullable();

            $table->string('reusable_permonth')->nullable();
            $table->string('reusable_peryear')->nullable();
            $table->string('reusable_perunit')->nullable();
            $table->string('reusable_totalyear')->nullable();

            $table->string('urinal_permonth')->nullable();
            $table->string('urinal_peryear')->nullable();
            $table->string('urinal_perunit')->nullable();
            $table->string('urinal_totalyear')->nullable();

            $table->string('bisacodyl_permonth')->nullable();
            $table->string('bisacodyl_peryear')->nullable();
            $table->string('bisacodyl_perunit')->nullable();
            $table->string('bisacodyl_totalyear')->nullable();

            $table->string('glycerin_permonth')->nullable();
            $table->string('glycerin_peryear')->nullable();
            $table->string('glycerin_perunit')->nullable();
            $table->string('glycerin_totalyear')->nullable();

            $table->string('uagic_permonth')->nullable();
            $table->string('uagic_peryear')->nullable();
            $table->string('uagic_perunit')->nullable();
            $table->string('uagic_totalyear')->nullable();

            $table->string('fleet_permonth')->nullable();
            $table->string('fleet_peryear')->nullable();
            $table->string('fleet_perunit')->nullable();
            $table->string('fleet_totalyear')->nullable();

            $table->string('lubricant_permonth')->nullable();
            $table->string('lubricant_peryear')->nullable();
            $table->string('lubricant_perunit')->nullable();
            $table->string('lubricant_totalyear')->nullable();

            $table->string('urinary_permonth')->nullable();
            $table->string('urinary_peryear')->nullable();
            $table->string('urinary_perunit')->nullable();
            $table->string('urinary_totalyear')->nullable();

            $table->string('tf_permonth')->nullable();
            $table->string('tf_peryear')->nullable();
            $table->string('tf_perunit')->nullable();
            $table->string('tf_totalyear')->nullable();

            $table->string('glycerin_swabs_permonth')->nullable();
            $table->string('glycerin_swabs_peryear')->nullable();
            $table->string('glycerin_swabs_perunit')->nullable();
            $table->string('glycerin_swabs_totalyear')->nullable();

            $table->string('toothettes_permonth')->nullable();
            $table->string('toothettes_peryear')->nullable();
            $table->string('toothettes_perunit')->nullable();
            $table->string('toothettes_totalyear')->nullable();

            $table->string('slipAndpuff_permonth')->nullable();
            $table->string('slipAndpuff_peryear')->nullable();
            $table->string('slipAndpuff_perunit')->nullable();
            $table->string('slipAndpuff_totalyear')->nullable();

            $table->string('bibs_disposable_permonth')->nullable();
            $table->string('bibs_disposable_peryear')->nullable();
            $table->string('bibs_disposable_perunit')->nullable();
            $table->string('bibs_disposable_totalyear')->nullable();

            $table->string('bibs_reusable_permonth')->nullable();
            $table->string('bibs_reusable_peryear')->nullable();
            $table->string('bibs_reusable_perunit')->nullable();
            $table->string('bibs_reusable_totalyear')->nullable();

            $table->string('inner_cannula_permonth')->nullable();
            $table->string('inner_cannula_peryear')->nullable();
            $table->string('inner_cannula_perunit')->nullable();
            $table->string('inner_cannula_totalyear')->nullable();

            $table->string('trach_care_permonth')->nullable();
            $table->string('trach_care_peryear')->nullable();
            $table->string('trach_care_perunit')->nullable();
            $table->string('trach_care_totalyear')->nullable();

            $table->string('trach_collar_permonth')->nullable();
            $table->string('trach_collar_peryear')->nullable();
            $table->string('trach_collar_perunit')->nullable();
            $table->string('trach_collar_totalyear')->nullable();

            $table->string('drain_sponge_permonth')->nullable();
            $table->string('drain_sponge_peryear')->nullable();
            $table->string('drain_sponge_perunit')->nullable();
            $table->string('drain_sponge_totalyear')->nullable();

            $table->string('multidose_permonth')->nullable();
            $table->string('multidose_peryear')->nullable();
            $table->string('multidose_perunit')->nullable();
            $table->string('multidose_totalyear')->nullable();

            $table->string('barrier_spray_permonth')->nullable();
            $table->string('barrier_spray_peryear')->nullable();
            $table->string('barrier_spray_perunit')->nullable();
            $table->string('barrier_spray_totalyear')->nullable();

            $table->string('periwash_permonth')->nullable();
            $table->string('periwash_peryear')->nullable();
            $table->string('periwash_perunit')->nullable();
            $table->string('periwash_totalyear')->nullable();

            $table->string('heel_protector_permonth')->nullable();
            $table->string('heel_protector_peryear')->nullable();
            $table->string('heel_protector_perunit')->nullable();
            $table->string('heel_protector_totalyear')->nullable();

            $table->string('elbow_protectors_permonth')->nullable();
            $table->string('elbow_protectors_peryear')->nullable();
            $table->string('elbow_protectors_perunit')->nullable();
            $table->string('elbow_protectors_totalyear')->nullable();

            $table->string('skin_barrier_permonth')->nullable();
            $table->string('skin_barrier_peryear')->nullable();
            $table->string('skin_barrier_perunit')->nullable();
            $table->string('skin_barrier_totalyear')->nullable();

            $table->string('geomat_permonth')->nullable();
            $table->string('geomat_peryear')->nullable();
            $table->string('geomat_perunit')->nullable();
            $table->string('geomat_totalyear')->nullable();

            $table->string('reacher_permonth')->nullable();
            $table->string('reacher_peryear')->nullable();
            $table->string('reacher_perunit')->nullable();
            $table->string('reacher_totalyear')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careform_form2s');
    }
}
