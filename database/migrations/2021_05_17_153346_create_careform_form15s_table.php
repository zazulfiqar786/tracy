<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareformForm15sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careform_form15s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('client_id')->nullable();
            $table->tinyInteger('case_manager_id')->nullable();
            $table->tinyInteger('careform_id')->nullable();
            $table->string('member_name')->nullable();
            $table->string('Medical_List_1')->nullable();
            $table->string('Medical_List_2')->nullable();
            $table->string('Medical_List_3')->nullable();
            $table->string('Medical_Lis5_4')->nullable();
            $table->string('Medical_List_5')->nullable();
            $table->string('Medical_List_6')->nullable();
            $table->string('Medical_List_7')->nullable();
            $table->string('Medical_List_8')->nullable();
            $table->string('Medical_List_9')->nullable();
            $table->string('Medical_List_10')->nullable();
            $table->string('Medical_List_11')->nullable();
            $table->string('Medical_List_12')->nullable();
            $table->string('Skilled')->nullable();
            $table->string('Evaluation')->nullable();
            $table->string('Potential')->nullable();
            $table->string('ICWP_member')->nullable();
            $table->string('Comments')->nullable();

            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careform_form15s');
    }
}
