<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareformForm4sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careform_form4s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('client_id')->nullable();
            $table->tinyInteger('case_manager_id')->nullable();
            $table->tinyInteger('careform_id')->nullable();

            $table->string('name')->nullable();
            $table->string('plan_of_care_date')->nullable();
            $table->string('your_icwp')->nullable();
            $table->string('your_icwp_phone')->nullable();
            $table->string('your_emergency_contact_name')->nullable();
            $table->string('your_emergency_phone')->nullable();
            $table->string('p_a_provider')->nullable();
            $table->string('p_a_provider_phone')->nullable();
            $table->string('your_primary_care_physician')->nullable();
            $table->string('your_primary_care_physician_phone')->nullable();
            $table->string('s_e')->nullable();
            $table->string('s_e_sp')->nullable();
            $table->string('s_e_phone')->nullable();
            $table->string('s_e_equipment_provider')->nullable();
            $table->string('s_e_equipment_provider_phone')->nullable();
            $table->string('hospital_for_emergencies')->nullable();
            $table->string('h_f_e_phone_number')->nullable();
            $table->string('h_f_e_address1')->nullable();
            $table->string('h_f_e_address2')->nullable();
            $table->string('behaviour_managment')->nullable();
            $table->string('note_1')->nullable();
            $table->string('note_2')->nullable();
            $table->string('note_3')->nullable();
            $table->string('note_4')->nullable();
            $table->string('note_5')->nullable();
            $table->string('note_6')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careform_form4s');
    }
}
