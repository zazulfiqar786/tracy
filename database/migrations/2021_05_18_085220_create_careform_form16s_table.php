<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareformForm16sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careform_form16s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('client_id')->nullable();
            $table->tinyInteger('case_manager_id')->nullable();
            $table->tinyInteger('careform_id')->nullable();
            $table->string('member_name')->nullable();
            $table->string('physician_visit')->nullable();
            $table->string('annual_period')->nullable();
            $table->string('describe')->nullable();
            $table->string('hospitalizations_visit')->nullable();
            $table->string('diagnosis_date')->nullable();
            $table->string('variance')->nullable();
            $table->string('facility_car')->nullable();
            $table->string('facility_placement')->nullable();
            $table->string('facility_placement_reasons')->nullable();
            $table->string('progressive_disease')->nullable();
            $table->string('disease_state')->nullable();
            $table->string('member_past_year')->nullable();
            $table->string('safety_implemented')->nullable();
            $table->string('contributing_factors')->nullable();
            $table->string('urinary_incontinence')->nullable();
            $table->string('bowel_incontinence')->nullable();
            $table->string('devices')->nullable();
            $table->string('Comments')->nullable();

            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careform_form16s');
    }
}
