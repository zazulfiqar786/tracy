<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareformForm22sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careform_form22s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('client_id')->nullable();
            $table->tinyInteger('case_manager_id')->nullable();
            $table->tinyInteger('careform_id')->nullable();
            $table->string('member_name')->nullable();
            $table->string('i_agree_to_provide')->nullable();
            $table->string('skills')->nullable();
            $table->string('sign1')->nullable();
            $table->string('relationship1')->nullable();
            $table->string('date1')->nullable();
            $table->string('sign2')->nullable();
            $table->string('relationship2')->nullable();
            $table->string('date2')->nullable();
            $table->string('sign3')->nullable();
            $table->string('date3')->nullable();
            $table->string('witness')->nullable();
            $table->string('date4')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careform_form22s');
    }
}
