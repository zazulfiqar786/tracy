<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareformForm14sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careform_form14s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('client_id')->nullable();
            $table->tinyInteger('case_manager_id')->nullable();
            $table->tinyInteger('careform_id')->nullable();

            $table->string('member_name_last')->nullable();
            $table->string('Care_plan_due_date')->nullable();
            $table->string('Member_Name')->nullable();
            $table->string('member_name_First')->nullable();
            $table->string('member_name_Middle')->nullable();
            $table->string('member_name_Suffix')->nullable();
            $table->string('Address')->nullable();
            $table->string('City')->nullable();
            $table->string('State')->nullable();
            $table->string('Zip')->nullable();
            $table->string('Country')->nullable();
            $table->string('Phone')->nullable();
            $table->string('Cell')->nullable();
            $table->string('Date_of_Birth')->nullable();
            $table->string('Age')->nullable();
            $table->string('gender')->nullable();
            $table->string('past_year')->nullable();
            $table->string('DFCS')->nullable();
            $table->string('Living_Arrangements')->nullable();
            $table->string('compared_to_1_year_ago')->nullable();
            $table->string('details')->nullable();
            $table->string('Current_Diagnosis_1')->nullable();
            $table->string('Current_Diagnosis_2')->nullable();
            $table->string('Current_Diagnosis_3')->nullable();
            $table->string('Current_Diagnosis_4')->nullable();
            $table->string('Current_Diagnosis_5')->nullable();
            $table->string('Current_Diagnosis_6')->nullable();
            $table->string('Current_Diagnosis_7')->nullable();
            $table->string('Current_Diagnosis_8')->nullable();
            $table->string('Current_Diagnosis_9')->nullable();
            $table->string('Current_Diagnosis_10')->nullable();
            $table->string('Current_Diagnosis_11')->nullable();
            $table->string('Current_Diagnosis_12')->nullable();
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careform_form14s');
    }
}
