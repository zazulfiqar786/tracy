<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_cases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('description')->nullable();
            $table->string('documents')->nullable();
            $table->string('case_no')->nullable();
            $table->string('case_date')->nullable();
            $table->integer('client_id')->nullable();
            $table->string('client_name')->nullable();
            $table->string('client_height')->nullable();
            $table->string('client_weight')->nullable();
            $table->tinyInteger('client_case_status')->nullable();
            $table->tinyInteger('assigned_status')->nullable();
            $table->tinyInteger('case_manager_id')->nullable();
            $table->tinyInteger('case_manager_name')->nullable();
            $table->string('country')->nullable();
            $table->string('county')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('area')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_cases');
    }
}
