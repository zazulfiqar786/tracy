<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareformForm1sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careform_form1s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('client_id')->nullable();
            $table->tinyInteger('case_manager_id')->nullable();
            $table->tinyInteger('careform_id')->nullable();
            $table->string('careform_form1_equipment')->nullable();
            $table->string('careform_form1_equipment_date')->nullable();
            $table->string('careform_form1_miscellaneous')->nullable();
            $table->string('careform_form1_wheelchair')->nullable();
            $table->string('careform_form1_assistive')->nullable();
            $table->string('careform_form1_respiratory')->nullable();
            $table->string('careform_form1_mobility')->nullable();
            $table->string('careform_form1_beds')->nullable();
            $table->string('careform_form1_transfer')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careform_form1s');
    }
}
