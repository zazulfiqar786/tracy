<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareformForm5sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careform_form5s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('client_id')->nullable();
            $table->tinyInteger('case_manager_id')->nullable();
            $table->tinyInteger('careform_id')->nullable();

            $table->string('name_of_member')->nullable();
            $table->string('dob')->nullable();
            $table->string('if_available')->nullable();
            
            $table->string('request_and_authoriz')->nullable();
            $table->string('to_obtain_from')->nullable();
            $table->string('following_types1')->nullable();
            $table->string('following_types2')->nullable();
            $table->string('for_the_perpose1')->nullable();
            $table->string('for_the_perpose2')->nullable();
            $table->string('all_information_authorize')->nullable();
            $table->string('i_understand_date')->nullable();
            $table->string('signature_of_member')->nullable();
            $table->string('signature_of_witness')->nullable();
            $table->string('title_relation_member')->nullable();
            $table->string('sign_parent')->nullable();
            $table->string('sign_parent_date')->nullable();
            $table->string('withdrawn_member')->nullable();  
            $table->string('signature_member_2')->nullable(); 
             
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careform_form5s');
    }
}
