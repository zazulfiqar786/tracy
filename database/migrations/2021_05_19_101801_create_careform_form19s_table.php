<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareformForm19sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careform_form19s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('client_id')->nullable();
            $table->tinyInteger('case_manager_id')->nullable();
            $table->tinyInteger('careform_id')->nullable();
            $table->string('member_name')->nullable();
            $table->string('safety_hazards')->nullable();
            $table->string('comment1')->nullable();
            $table->string('sanitation_hazards')->nullable();
            $table->string('comment2')->nullable();
            $table->string('social_functioning')->nullable();
            $table->string('isolation')->nullable();
            $table->string('community_activities')->nullable();
            $table->string('comment3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careform_form19s');
    }
}
