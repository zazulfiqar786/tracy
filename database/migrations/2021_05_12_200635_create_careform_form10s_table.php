<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareformForm10sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careform_form10s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('client_id')->nullable();
            $table->tinyInteger('case_manager_id')->nullable();
            $table->tinyInteger('careform_id')->nullable();
            $table->string('this')->nullable();
            $table->string('day_of')->nullable();
            $table->string('year20')->nullable();
            $table->string('print_name')->nullable();
            $table->string('signature_member')->nullable();
            $table->string('signature_member_date')->nullable();
            $table->string('print_name_cm')->nullable();
            $table->string('signature_cm')->nullable();
            $table->string('signature_date')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careform_form10s');
    }
}
