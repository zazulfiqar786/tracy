<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareformForm18sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careform_form18s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('client_id')->nullable();
            $table->tinyInteger('case_manager_id')->nullable();
            $table->tinyInteger('careform_id')->nullable();

            $table->string('member_name')->nullable();
            $table->string('based_on_your_personal')->nullable();
            $table->string('classify_memory')->nullable();
            $table->string('comments1')->nullable();
            $table->string('behavior_issues_yes_no')->nullable();
            $table->string('behavior_issues_yes')->nullable();
            $table->string('smoking_without')->nullable();
            $table->string('smoking_without_yes')->nullable();
            $table->string('suspected_alcohol')->nullable();
            $table->string('suspected_alcohol_yes')->nullable();
            $table->string('suspected_drug')->nullable();
            $table->string('suspected_drug_yes')->nullable();
            $table->string('comments2')->nullable();
            $table->string('personal_assistance')->nullable();
            $table->string('comments3')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careform_form18s');
    }
}
