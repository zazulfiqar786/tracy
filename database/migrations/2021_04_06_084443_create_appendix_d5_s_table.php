<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppendixD5STable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appendix_d5_s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('client_id')->nullable();
            $table->tinyInteger('case_manager_id')->nullable();
            $table->tinyInteger('provider_id')->nullable();
            $table->string('member_name')->nullable();
            $table->string('medicaid_id')->nullable();
            $table->string('date_completed')->nullable();
            $table->string('current_street_address')->nullable();
            $table->string('current_appointment_no')->nullable();
            $table->string('current_city')->nullable();
            $table->string('current_state')->nullable();
            $table->string('current_zip')->nullable();
            $table->string('current_country')->nullable();
            $table->string('previous_street_address')->nullable();
            $table->string('previous_appointment_no')->nullable();
            $table->string('previous_city')->nullable();
            $table->string('previous_state')->nullable();
            $table->string('previous_zip')->nullable();
            $table->string('previous_country')->nullable();
            $table->string('living_setting')->nullable();
            $table->string('relative_name')->nullable();
            $table->string('relative_relationship_to_member')->nullable();
            $table->string('relative_phone')->nullable();
            $table->string('relative_alternate_phone')->nullable();
            $table->string('relative_street_address')->nullable();
            $table->string('relative_city')->nullable();
            $table->string('relative_zip')->nullable();
            $table->string('relative_country')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appendix_d5_s');
    }
}
