<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppendixK1STable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appendix_k1_s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('provider_id')->nullable();
            $table->string('client_id')->nullable();
            $table->string('case_manager_id')->nullable();
            $table->string('provider_name')->nullable();
            $table->string('provider_number')->nullable();
            $table->string('provider_address')->nullable();
            $table->string('member_name')->nullable();
            $table->string('medicaid_number')->nullable();
            $table->string('status')->nullable();
            $table->string('effective_date')->nullable();
            $table->longText('discharge_reasons')->nullable();
            $table->string('transfer_reasons')->nullable();
            $table->string('provider_signature')->nullable();
            $table->string('date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appendix_k1_s');
    }
}
