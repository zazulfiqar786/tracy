<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareformForm6sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careform_form6s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('client_id')->nullable();
            $table->tinyInteger('case_manager_id')->nullable();
            $table->tinyInteger('careform_id')->nullable();

            $table->string('name_of_member')->nullable();
            $table->string('dob')->nullable();
            $table->string('medicaid')->nullable();
            $table->string('date')->nullable();
            $table->string('case_manager_name')->nullable();
            $table->string('signature')->nullable();
            $table->string('first_quarter')->nullable();
            $table->string('first_quarter_date')->nullable();
            $table->string('first_quarter_cm')->nullable();
            $table->string('second_quarter')->nullable();
            $table->string('second_quarter_date')->nullable();
            $table->string('second_quarter_cm')->nullable();
            $table->string('third_quarter')->nullable();
            $table->string('third_quarter_date')->nullable();
            $table->string('third_quarter_cm')->nullable();
            $table->string('forth_quarter')->nullable();
            $table->string('forth_quarter_date')->nullable();
            $table->string('forth_quarter_cm')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careform_form6s');
    }
}
