<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareformForm13sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careform_form13s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('client_id')->nullable();
            $table->tinyInteger('case_manager_id')->nullable();
            $table->tinyInteger('careform_id')->nullable();

            $table->string('case_manager')->nullable();
            $table->string('case_manager_date')->nullable();
            $table->string('participant')->nullable();
            $table->string('participant_date')->nullable();
            $table->string('authorized_representative')->nullable();
            $table->string('authorized_representative_date')->nullable();
            $table->string('witness')->nullable();
            $table->string('witness_date')->nullable();
            $table->string('refusal_participant')->nullable();
            $table->string('refusal_participant_date')->nullable();
            $table->string('refusal_authorized_representative')->nullable();
            $table->string('refusal_authorized_representative_date')->nullable();
            $table->string('refusal_witness')->nullable();
            $table->string('refusal_witness_date')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careform_form13s');
    }
}
