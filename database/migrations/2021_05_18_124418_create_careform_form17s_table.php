<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareformForm17sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careform_form17s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('client_id')->nullable();
            $table->tinyInteger('case_manager_id')->nullable();
            $table->tinyInteger('careform_id')->nullable();

            $table->string('member_name')->nullable();
            $table->string('where_is_located')->nullable();
            $table->string('where_the_wound')->nullable();
            $table->string('member_wound')->nullable();
            $table->string('separate_attachment')->nullable();
            $table->string('comments1')->nullable();
            $table->string('primary_modes')->nullable();
            $table->string('comments2')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careform_form17s');
    }
}
