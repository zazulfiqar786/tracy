<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareformForm3sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careform_form3s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('client_id')->nullable();
            $table->tinyInteger('case_manager_id')->nullable();
            $table->tinyInteger('careform_id')->nullable();

            $table->string('icwp_financial_summary')->nullable();
            $table->string('date')->nullable();

            $table->string('service_description_1')->nullable();
            $table->string('frequency_intensity_1')->nullable();
            $table->string('responsible_provider_1')->nullable();
            $table->string('per_unit_cost_1')->nullable();
            $table->string('yearly_cast_1')->nullable();

            $table->string('service_description_2')->nullable();
            $table->string('frequency_intensity_2')->nullable();
            $table->string('responsible_provider_2')->nullable();
            $table->string('per_unit_cost_2')->nullable();
            $table->string('yearly_cast_2')->nullable();

            $table->string('service_description_3')->nullable();
            $table->string('frequency_intensity_3')->nullable();
            $table->string('responsible_provider_3')->nullable();
            $table->string('per_unit_cost_3')->nullable();
            $table->string('yearly_cast_3')->nullable();

            $table->string('service_description_4')->nullable();
            $table->string('frequency_intensity_4')->nullable();
            $table->string('responsible_provider_4')->nullable();
            $table->string('per_unit_cost_4')->nullable();
            $table->string('yearly_cast_4')->nullable();

            $table->string('service_description_5')->nullable();
            $table->string('frequency_intensity_5')->nullable();
            $table->string('responsible_provider_5')->nullable();
            $table->string('per_unit_cost_5')->nullable();
            $table->string('yearly_cast_5')->nullable();

            $table->string('service_description_6')->nullable();
            $table->string('frequency_intensity_6')->nullable();
            $table->string('responsible_provider_6')->nullable();
            $table->string('per_unit_cost_6')->nullable();
            $table->string('yearly_cast_6')->nullable();

            $table->string('service_description_7')->nullable();
            $table->string('frequency_intensity_7')->nullable();
            $table->string('responsible_provider_7')->nullable();
            $table->string('per_unit_cost_7')->nullable();
            $table->string('yearly_cast_7')->nullable();

            $table->string('service_description_8')->nullable();
            $table->string('frequency_intensity_8')->nullable();
            $table->string('responsible_provider_8')->nullable();
            $table->string('per_unit_cost_8')->nullable();
            $table->string('yearly_cast_8')->nullable();



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careform_form3s');
    }
}
