-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2021 at 01:59 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lifegoes`
--

-- --------------------------------------------------------

--
-- Table structure for table `appendix_d5_s`
--

CREATE TABLE `appendix_d5_s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `provider_id` tinyint(4) DEFAULT NULL,
  `member_name` varchar(255) DEFAULT NULL,
  `medicaid_id` varchar(255) DEFAULT NULL,
  `date_completed` varchar(255) DEFAULT NULL,
  `current_street_address` varchar(255) DEFAULT NULL,
  `current_appointment_no` varchar(255) DEFAULT NULL,
  `current_city` varchar(255) DEFAULT NULL,
  `current_state` varchar(255) DEFAULT NULL,
  `current_zip` varchar(255) DEFAULT NULL,
  `current_country` varchar(255) DEFAULT NULL,
  `previous_street_address` varchar(255) DEFAULT NULL,
  `previous_appointment_no` varchar(255) DEFAULT NULL,
  `previous_city` varchar(255) DEFAULT NULL,
  `previous_state` varchar(255) DEFAULT NULL,
  `previous_zip` varchar(255) DEFAULT NULL,
  `previous_country` varchar(255) DEFAULT NULL,
  `living_setting` varchar(255) DEFAULT NULL,
  `relative_name` varchar(255) DEFAULT NULL,
  `relative_relationship_to_member` varchar(255) DEFAULT NULL,
  `relative_phone` varchar(255) DEFAULT NULL,
  `relative_alternate_phone` varchar(255) DEFAULT NULL,
  `relative_street_address` varchar(255) DEFAULT NULL,
  `relative_city` varchar(255) DEFAULT NULL,
  `relative_zip` varchar(255) DEFAULT NULL,
  `relative_country` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `appendix_d5_s`
--

INSERT INTO `appendix_d5_s` (`id`, `client_id`, `case_manager_id`, `provider_id`, `member_name`, `medicaid_id`, `date_completed`, `current_street_address`, `current_appointment_no`, `current_city`, `current_state`, `current_zip`, `current_country`, `previous_street_address`, `previous_appointment_no`, `previous_city`, `previous_state`, `previous_zip`, `previous_country`, `living_setting`, `relative_name`, `relative_relationship_to_member`, `relative_phone`, `relative_alternate_phone`, `relative_street_address`, `relative_city`, `relative_zip`, `relative_country`, `created_at`, `updated_at`) VALUES
(6, 5, NULL, NULL, 'Cherokee Bauer', 'Fugiat nihil autem', '12-Mar-2012', 'Voluptate sed obcaec', 'Maiores odit digniss', 'Laborum Consequat', 'Ipsum nulla fuga A', '44861', 'Ut aliquam excepteur', 'Rerum illo omnis qua', 'Ipsam tenetur quis c', 'Sed magni rerum aute', 'Est officiis et cill', '62892', 'Inventore placeat q', '4', 'Travis Wooten', 'Est asperiores quo c', '100', '28', 'Maxime debitis omnis', 'Et quia amet eos i', '37385', 'Eiusmod iusto sed ve', '2021-04-21 09:25:15', '2021-04-21 05:50:28'),
(7, 7, NULL, NULL, 'mmmqwe', 'ttPC9NwpzB', NULL, 'ZxZrMJBiEY', 'LsIzHnVJSQ', 'LO5bvSwsV7', 'Fq47ZZqdb6', 'VDckfAWbZw', 'O9DZTdRbzW', 'Nwb27chUP3', 'Bg9dyv7YTR', 'BVqfHuWyuq', '05QIQ4FF5O', 'kmsloFrAxQ', 'bQVzhsgZO3', '6', 'A6nAX1SO7v', 'TcDHfUsGIy', '224730', '860688', 'nbZG5VRlST', '9eplN8XzRz', 'jTE5llm50p', 'tEN0Qt6WY9', '2021-05-15 06:38:08', '2021-05-15 06:38:08'),
(8, 8, NULL, NULL, 'CareForm', 'nc82Vkjym4', NULL, 'giGusXEgvg', 'Zr2pljJid8', 'RMWar10Mjq', 'TqL21zp5x2', 'Z4k4z4V4QL', 'aX6vWPP8bz', 'BfLxcz8p3X', '20ImSeJQuE', 'SQR6E0NCyd', 'tsHwps76Ln', 'HlTsTMy7SL', 'TsJ1uPtfKa', '6', 'lLsCZpgHhN', 'vvwrmSnXeh', '747152', '265807', 'IJ2aUhaPqM', 'd1nfqCkRDP', 'Mlrf0TpXNe', 'Kgw98ZutNP', '2021-05-15 06:46:03', '2021-05-15 06:46:03');

-- --------------------------------------------------------

--
-- Table structure for table `appendix_k1_s`
--

CREATE TABLE `appendix_k1_s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `provider_id` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `case_manager_id` varchar(255) DEFAULT NULL,
  `provider_name` varchar(255) DEFAULT NULL,
  `provider_number` varchar(255) DEFAULT NULL,
  `provider_address` varchar(255) DEFAULT NULL,
  `member_name` varchar(255) DEFAULT NULL,
  `medicaid_number` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `effective_date` varchar(255) DEFAULT NULL,
  `discharge_reasons` longtext DEFAULT NULL,
  `transfer_reasons` varchar(255) DEFAULT NULL,
  `provider_signature` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `appendix_k1_s`
--

INSERT INTO `appendix_k1_s` (`id`, `provider_id`, `client_id`, `case_manager_id`, `provider_name`, `provider_number`, `provider_address`, `member_name`, `medicaid_number`, `status`, `effective_date`, `discharge_reasons`, `transfer_reasons`, `provider_signature`, `date`, `created_at`, `updated_at`) VALUES
(2, NULL, '8', NULL, 'zmGKHDWTyf', '026516', '5EDV3Fi08U', 'CareForm', '8112444328', '', NULL, 'grV1ACBs0l', '', 'WHQ1RHzVhL', 'asdasdsa', '2021-05-15 06:46:29', '2021-05-15 06:46:29'),
(3, NULL, '8', NULL, 'zmGKHDWTyf', '026516', '5EDV3Fi08U', 'CareForm', '8112444328', '1,2', NULL, 'grV1ACBs0l,2,4,6', '1,2,3', 'WHQ1RHzVhL', 'asdasdsa', '2021-05-15 06:46:58', '2021-05-15 06:46:58');

-- --------------------------------------------------------

--
-- Table structure for table `careforms`
--

CREATE TABLE `careforms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `provider_id` tinyint(4) DEFAULT NULL,
  `member_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medicaid_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_completed` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_street_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_appointment_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_zip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previous_street_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previous_appointment_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previous_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previous_state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previous_zip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previous_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `living_setting` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relative_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relative_relationship_to_member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relative_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relative_alternate_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relative_street_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relative_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relative_zip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relative_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `careforms`
--

INSERT INTO `careforms` (`id`, `client_id`, `case_manager_id`, `provider_id`, `member_name`, `medicaid_id`, `date_completed`, `current_street_address`, `current_appointment_no`, `current_city`, `current_state`, `current_zip`, `current_country`, `previous_street_address`, `previous_appointment_no`, `previous_city`, `previous_state`, `previous_zip`, `previous_country`, `living_setting`, `relative_name`, `relative_relationship_to_member`, `relative_phone`, `relative_alternate_phone`, `relative_street_address`, `relative_city`, `relative_zip`, `relative_country`, `created_at`, `updated_at`) VALUES
(52, 6, NULL, NULL, 'mmmqwe', 'HIswfaotGY', NULL, '77D0Tu6LYL', 'mFKwzyj7dz', 'X06JSa4LiG', 'AydFalvd9X', 'AKGdpoT4FN', '9OniFgYF4D', 'ETChNK2xoV', 'P8M51adbNZ', '3WO3Sq7UwS', 'NKvJE6ivud', 'XYV2cnLOJJ', 'bEySn5ISLT', '6', 'znFXoyevIf', 'SrQIjTP7x1', '502827', '053783', '6lAOSqiJVN', 'm3MQkYCnk3', 'RRAJ8kgmiB', 'tfXELemxmp', '2021-05-13 15:49:06', '2021-05-13 15:49:06'),
(53, 7, NULL, NULL, 'CareForm', 'eySWmqGqxW', NULL, '850uPixxf4', 'zFLNbxTnUF', 'I1diUZoylU', 'qqKnTTIGp5', 'UizrKji77M', 'sf27t0V4cf', '2hUlGxODi0', 'SqgzhU5iWi', '47QMVYVMRe', '1TYJX1xvRf', 'zjvKNCRsNt', 'wzcuo3CMCp', '6', 'VDZ7nbvjCS', 'gCkBv9eClU', '903040', '192743', 'ka5u0UHnAW', 'XjEnKUmNTt', 'rTh09AOX9p', 'Hu4wcJq0f9', '2021-05-15 06:38:47', '2021-05-15 06:38:47'),
(54, 8, NULL, NULL, 'CareForm Fulll2222', 'Hz0zDHu7e2', 'asd', 'IhTeuMvPqr', 'hb43SBhbHo', 'HccoaUZlBd', 'e51shhMWZ9', 'yLtxv9C0Hp', '5IVxK4WkpT', 'T2JQIs0qDI', 'EcmZujJDI3', 'G2md2xK25r', 'rQO8EU3l7x', 'tKxj8KVvcM', 'oi0VLkY7oI', '2', 'l4KHFOa9xE', '1jGT3NGBaQ', '016577', '202687', 'tVuMkGjQ3C', 'DGyEMUTXbU', 'xzWycoYR6r', 'Cu9p2LKIYk', '2021-05-15 06:49:15', '2021-05-15 06:49:37'),
(57, 9, NULL, NULL, 'yyyy', 'sss', 'ss', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-17 09:01:56', '2021-05-17 09:01:56'),
(58, 9, NULL, NULL, 'ccccc', 'cc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-17 11:47:07', '2021-05-17 11:47:07'),
(59, 56, NULL, NULL, 'Member’s Name', 'id', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-18 03:23:31', '2021-05-18 03:23:31');

-- --------------------------------------------------------

--
-- Table structure for table `careform_form1s`
--

CREATE TABLE `careform_form1s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `careform_form1_equipment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `careform_form1_equipment_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `careform_form1_miscellaneous` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `careform_form1_wheelchair` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `careform_form1_assistive` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `careform_form1_respiratory` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `careform_form1_mobility` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `careform_form1_bathroom` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `careform_form1_beds` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `careform_form1_transfer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `careform_form1s`
--

INSERT INTO `careform_form1s` (`id`, `client_id`, `case_manager_id`, `careform_id`, `careform_form1_equipment`, `careform_form1_equipment_date`, `careform_form1_miscellaneous`, `careform_form1_wheelchair`, `careform_form1_assistive`, `careform_form1_respiratory`, `careform_form1_mobility`, `careform_form1_bathroom`, `careform_form1_beds`, `careform_form1_transfer`, `created_at`, `updated_at`) VALUES
(34, NULL, NULL, 52, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, NULL, NULL, 53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, NULL, NULL, 54, 'weeqwe', 'asdasd', '1,4,5,6,8', '4,6', '', '', '5,8,9', '1,4,6,7,9', '', '1,2,3', NULL, '2021-05-15 06:50:12'),
(39, NULL, NULL, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, NULL, NULL, 58, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, NULL, NULL, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `careform_form2s`
--

CREATE TABLE `careform_form2s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `icwp_supply_list` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gloves_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gloves_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gloves_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gloves_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adult_briefs_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adult_briefs_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adult_briefs_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adult_briefs_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bladder_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bladder_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bladder_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bladder_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chux_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chux_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chux_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chux_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wipes_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wipes_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wipes_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wipes_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skin_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skin_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skin_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skin_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reusable_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reusable_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reusable_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reusable_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urinal_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urinal_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urinal_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urinal_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bisacodyl_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bisacodyl_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bisacodyl_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bisacodyl_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `glycerin_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `glycerin_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `glycerin_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `glycerin_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uagic_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uagic_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uagic_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uagic_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fleet_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fleet_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fleet_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fleet_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lubricant_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lubricant_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lubricant_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lubricant_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urinary_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urinary_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urinary_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urinary_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tf_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tf_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tf_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tf_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `glycerin_swabs_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `glycerin_swabs_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `glycerin_swabs_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `glycerin_swabs_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `toothettes_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `toothettes_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `toothettes_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `toothettes_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slipAndpuff_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slipAndpuff_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slipAndpuff_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slipAndpuff_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bibs_disposable_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bibs_disposable_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bibs_disposable_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bibs_disposable_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bibs_reusable_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bibs_reusable_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bibs_reusable_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bibs_reusable_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `inner_cannula_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `inner_cannula_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `inner_cannula_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `inner_cannula_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trach_care_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trach_care_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trach_care_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trach_care_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trach_collar_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trach_collar_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trach_collar_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trach_collar_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `drain_sponge_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `drain_sponge_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `drain_sponge_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `drain_sponge_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `multidose_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `multidose_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `multidose_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `multidose_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barrier_spray_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barrier_spray_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barrier_spray_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barrier_spray_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `periwash_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `periwash_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `periwash_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `periwash_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `heel_protector_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `heel_protector_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `heel_protector_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `heel_protector_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `elbow_protectors_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `elbow_protectors_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `elbow_protectors_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `elbow_protectors_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skin_barrier_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skin_barrier_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skin_barrier_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skin_barrier_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `geomat_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `geomat_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `geomat_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `geomat_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reacher_permonth` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reacher_peryear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reacher_perunit` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reacher_totalyear` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `careform_form2s`
--

INSERT INTO `careform_form2s` (`id`, `client_id`, `case_manager_id`, `careform_id`, `icwp_supply_list`, `date`, `gloves_permonth`, `gloves_peryear`, `gloves_perunit`, `gloves_totalyear`, `adult_briefs_permonth`, `adult_briefs_peryear`, `adult_briefs_perunit`, `adult_briefs_totalyear`, `bladder_permonth`, `bladder_peryear`, `bladder_perunit`, `bladder_totalyear`, `chux_permonth`, `chux_peryear`, `chux_perunit`, `chux_totalyear`, `wipes_permonth`, `wipes_peryear`, `wipes_perunit`, `wipes_totalyear`, `skin_permonth`, `skin_peryear`, `skin_perunit`, `skin_totalyear`, `reusable_permonth`, `reusable_peryear`, `reusable_perunit`, `reusable_totalyear`, `urinal_permonth`, `urinal_peryear`, `urinal_perunit`, `urinal_totalyear`, `bisacodyl_permonth`, `bisacodyl_peryear`, `bisacodyl_perunit`, `bisacodyl_totalyear`, `glycerin_permonth`, `glycerin_peryear`, `glycerin_perunit`, `glycerin_totalyear`, `uagic_permonth`, `uagic_peryear`, `uagic_perunit`, `uagic_totalyear`, `fleet_permonth`, `fleet_peryear`, `fleet_perunit`, `fleet_totalyear`, `lubricant_permonth`, `lubricant_peryear`, `lubricant_perunit`, `lubricant_totalyear`, `urinary_permonth`, `urinary_peryear`, `urinary_perunit`, `urinary_totalyear`, `tf_permonth`, `tf_peryear`, `tf_perunit`, `tf_totalyear`, `glycerin_swabs_permonth`, `glycerin_swabs_peryear`, `glycerin_swabs_perunit`, `glycerin_swabs_totalyear`, `toothettes_permonth`, `toothettes_peryear`, `toothettes_perunit`, `toothettes_totalyear`, `slipAndpuff_permonth`, `slipAndpuff_peryear`, `slipAndpuff_perunit`, `slipAndpuff_totalyear`, `bibs_disposable_permonth`, `bibs_disposable_peryear`, `bibs_disposable_perunit`, `bibs_disposable_totalyear`, `bibs_reusable_permonth`, `bibs_reusable_peryear`, `bibs_reusable_perunit`, `bibs_reusable_totalyear`, `inner_cannula_permonth`, `inner_cannula_peryear`, `inner_cannula_perunit`, `inner_cannula_totalyear`, `trach_care_permonth`, `trach_care_peryear`, `trach_care_perunit`, `trach_care_totalyear`, `trach_collar_permonth`, `trach_collar_peryear`, `trach_collar_perunit`, `trach_collar_totalyear`, `drain_sponge_permonth`, `drain_sponge_peryear`, `drain_sponge_perunit`, `drain_sponge_totalyear`, `multidose_permonth`, `multidose_peryear`, `multidose_perunit`, `multidose_totalyear`, `barrier_spray_permonth`, `barrier_spray_peryear`, `barrier_spray_perunit`, `barrier_spray_totalyear`, `periwash_permonth`, `periwash_peryear`, `periwash_perunit`, `periwash_totalyear`, `heel_protector_permonth`, `heel_protector_peryear`, `heel_protector_perunit`, `heel_protector_totalyear`, `elbow_protectors_permonth`, `elbow_protectors_peryear`, `elbow_protectors_perunit`, `elbow_protectors_totalyear`, `skin_barrier_permonth`, `skin_barrier_peryear`, `skin_barrier_perunit`, `skin_barrier_totalyear`, `geomat_permonth`, `geomat_peryear`, `geomat_perunit`, `geomat_totalyear`, `reacher_permonth`, `reacher_peryear`, `reacher_perunit`, `reacher_totalyear`, `created_at`, `updated_at`) VALUES
(26, NULL, NULL, 52, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, NULL, NULL, 53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, NULL, NULL, 54, 'mJhhjyUm6P', 'asdasdsa', 'Working?', 'B1gpVXsEHh', 'W7FrcYvH0e', 'RwLNWLnJnh', 'iHpCR2Vnme', '1sAwyFI5sO', 'DxjZ6dUcIz', '2aEmNNPvkM', 'Tq8rHvinqD', '07o25DLjqe', 'shyFleKfg6', 'booA225Q0g', 'vsHTj9Xyfx', 'MV53Cm8bE7', NULL, 'l7tMW1tAQd', 'sTyr9tIXoI', 'lQC1TKiE3h', '3a3aDA2vrG', 'OlKHV699vY', '5HlWdjrlm3', 'CZo1gqInxv', 'ASYXXdHqGt', 'a3WFET9Y9J', 'c6207sU3mx', 'ALz75codV2', 'UYfXrq29g2', 'RLwUbLqBv9', 'myeF56Iwbh', 'omdeBh8oAx', 'iGDdQ0fgFJ', 'hvTGrHUIOu', 'nUDOiJ8xnJ', '0yOO4RCqbO', '7AtnmPH1AB', 'fGvSn0WGkR', 'v4EWgJvOUY', '01Phv4v5EC', '1UQ87JWmjb', '95semU7YaN', 'PfhzTt2kNu', '7wpLHoHpqY', 'WXIKdRso4M', 'HG5RQL7p03', 'Y3XJMD7NmP', 'EcEqe0Xknh', 'maWk1xWv2T', 'eTZokNmWX7', 'aqbQBIVQko', 'j2tfMGMU33', 'Q2osqct87K', 'IAP6tyW05E', 'OgXlzzj4RT', '8RDOXn8vzm', 'XMnfvX1VbY', 'dBmuH6h1k3', 'D9MshRFXTV', 'jnfhPKUO2p', 'FT8wo02luG', 'WV7Jo6Fcmp', 'rM9xNMqlmE', 'c9w9NoncvA', 'coagCfV3x4', 'BUN7unirf6', '7OsDuhJ2g3', 'Wrw6uVdKQJ', 'yojlTMqLO5', 'WcfCwKli1Q', 'hWt4MnFGLk', 'eeQveYy6EG', 'W5D1nGMk5L', 'siooC1VlX6', 'WYJQN0vfS5', 'SYQlHBltaC', '1JugCPondJ', 'rmMBPLSkvB', 'nIDp4d9uUW', 'C5ZBdD4WHG', '5uLHxARorC', 'ApShqrlBS2', 'UJlQpuWYRO', 'ZkEqLlhkWy', 'JtgOmh56Qg', 'CadMTkwexx', 't3A2ScET8l', 'gNiwigonEw', 'mOcMFwuRmh', '6YK7uveqau', 'Yxtur5ovmd', '1FF7pCueHs', 'Y1n1mMzTem', 'Sud8gtOZ0C', 'GiocKPs0oN', '4VSHjWBKIl', 'dCoxwMZXia', 'sHKcTSSoba', '8c55Oi4UGX', 'l0SYUGWW65', 'HlOSxaqNKO', 'Uopkq2ZhK5', 'vlfbst5bgB', 'lGtdjjYoqR', 'OZQhllpN9o', 'f8kYGNGhNd', 'CWexQ4IffX', 'TmHilc0dhe', 'wvMV8ZnySi', 'SC9PHpn7lT', 'YTyCSaSYw9', 'XbnYZyXSdE', 'Wo1tZYKhUg', 'v2f6nba0Ka', 'NKK6FdTQzb', '7LSNObQB0c', 'Si72heQaRR', 'kj9dJSyIr5', 'TBpfth6bUm', 'LFN81pw2VQ', 'sw62byQM7T', 'YqwwUe6TLj', 'xnYML6XSF2', 'ssv6Hofz9r', 'VS35VVohVI', 'JG8FY4mMJ5', 'OKfxbImkCn', 'axiCAuWgzd', 'taof9HLxZ3', 'zzzzzzzzzzz', NULL, '2021-05-15 06:51:13'),
(31, NULL, NULL, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, NULL, NULL, 58, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, NULL, NULL, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `careform_form3s`
--

CREATE TABLE `careform_form3s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `icwp_financial_summary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `frequency_intensity_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `responsible_provider_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `per_unit_cost_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yearly_cast_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `frequency_intensity_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `responsible_provider_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `per_unit_cost_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yearly_cast_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `frequency_intensity_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `responsible_provider_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `per_unit_cost_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yearly_cast_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `frequency_intensity_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `responsible_provider_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `per_unit_cost_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yearly_cast_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `frequency_intensity_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `responsible_provider_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `per_unit_cost_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yearly_cast_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description_6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `frequency_intensity_6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `responsible_provider_6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `per_unit_cost_6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yearly_cast_6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description_7` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `frequency_intensity_7` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `responsible_provider_7` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `per_unit_cost_7` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yearly_cast_7` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description_8` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `frequency_intensity_8` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `responsible_provider_8` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `per_unit_cost_8` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yearly_cast_8` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `careform_form3s`
--

INSERT INTO `careform_form3s` (`id`, `client_id`, `case_manager_id`, `careform_id`, `icwp_financial_summary`, `date`, `service_description_1`, `frequency_intensity_1`, `responsible_provider_1`, `per_unit_cost_1`, `yearly_cast_1`, `service_description_2`, `frequency_intensity_2`, `responsible_provider_2`, `per_unit_cost_2`, `yearly_cast_2`, `service_description_3`, `frequency_intensity_3`, `responsible_provider_3`, `per_unit_cost_3`, `yearly_cast_3`, `service_description_4`, `frequency_intensity_4`, `responsible_provider_4`, `per_unit_cost_4`, `yearly_cast_4`, `service_description_5`, `frequency_intensity_5`, `responsible_provider_5`, `per_unit_cost_5`, `yearly_cast_5`, `service_description_6`, `frequency_intensity_6`, `responsible_provider_6`, `per_unit_cost_6`, `yearly_cast_6`, `service_description_7`, `frequency_intensity_7`, `responsible_provider_7`, `per_unit_cost_7`, `yearly_cast_7`, `service_description_8`, `frequency_intensity_8`, `responsible_provider_8`, `per_unit_cost_8`, `yearly_cast_8`, `created_at`, `updated_at`) VALUES
(22, NULL, NULL, 52, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, NULL, NULL, 53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, NULL, NULL, 54, 'Tester Form', '12', 'D0WiAUu35O', 'h07mNOKi9B', '3pAdq3Nm4w', 'hRzM0HGWbo', 'f3vbnBqjes', 'PLf7hXyUtj', 'SZgjJgCVjH', '0y0ITM8ouv', 'pEwr8E1Upt', 'Gc2Hr23lL1', 'LfPS4q64rO', 'Oq8vZPXfuU', 'I1m9a27nLh', 'ARuYWvJvGS', 'tSFBqOnx8k', 'JjGbIA7a73', 'sb8QEm7pPP', 'iMW4dgbhPv', 'zdVgxTX88I', 'MnDj9zq6vR', 'ubSa7HGQrL', '0BU9MRJSdL', 'HZdjY7OM5S', 'gASuFctkOX', 'BmAnXV6p7k', 'ECDcf6MI54', 'NM7hLXJx2Q', 'lKYCJuY39p', 'vI8FfiYIhd', 'vxzNoF5yaP', 'Tk3J78Rjte', '3DBV33DBBj', 'V5on0btA5v', 'dDyhDBfS5x', 'rbTObTP2Y0', 'IDMEaYjVuP', 'pwE1eqAFCg', 'egeBCCGdca', 'o8tNEc6iOa', 'Teeessst', NULL, '2021-05-15 06:51:45'),
(27, NULL, NULL, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, NULL, NULL, 58, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, NULL, NULL, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `careform_form4s`
--

CREATE TABLE `careform_form4s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plan_of_care_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `your_icwp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `your_icwp_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `your_emergency_contact_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `your_emergency_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_a_provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_a_provider_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `your_primary_care_physician` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `your_primary_care_physician_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_e` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_e_sp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_e_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_e_equipment_provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_e_equipment_provider_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hospital_for_emergencies` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `h_f_e_phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `h_f_e_address1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `h_f_e_address2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `behaviour_managment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note_6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `careform_form4s`
--

INSERT INTO `careform_form4s` (`id`, `client_id`, `case_manager_id`, `careform_id`, `name`, `plan_of_care_date`, `your_icwp`, `your_icwp_phone`, `your_emergency_contact_name`, `your_emergency_phone`, `p_a_provider`, `p_a_provider_phone`, `your_primary_care_physician`, `your_primary_care_physician_phone`, `s_e`, `s_e_sp`, `s_e_phone`, `s_e_equipment_provider`, `s_e_equipment_provider_phone`, `hospital_for_emergencies`, `h_f_e_phone_number`, `h_f_e_address1`, `h_f_e_address2`, `behaviour_managment`, `note_1`, `note_2`, `note_3`, `note_4`, `note_5`, `note_6`, `created_at`, `updated_at`) VALUES
(21, NULL, NULL, 52, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, NULL, NULL, 53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, NULL, NULL, 54, 'Tester', '12', '1ptltJVxkg', '2947627236', '9269313959', '8288195498', 'SxoPgvAqLB', '4343799889', 'ndDgD4IIBz', '6723521212', '4Ltdz51eVJ', 'oc9uAWXDES', '7489463417', 'rzPfgPoFYf', '7608915353', 'qcz8prpOlq', '5876083834', 'wsMeNxXmV8', 'BWBwQjF7jp', 'Yes', 'jD58g214wh', 'Nkz6eKgmgD', '8eVXCaDrP7', '2rbfPiKuXE', '1Lh3gG6s4v', 'y9hRxW4hLs', NULL, '2021-05-15 06:52:13'),
(26, NULL, NULL, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, NULL, NULL, 58, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, NULL, NULL, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `careform_form5s`
--

CREATE TABLE `careform_form5s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `name_of_member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `if_available` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `request_and_authoriz` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_obtain_from` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `following_types1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `following_types2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `for_the_perpose1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `for_the_perpose2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `all_information_authorize` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `i_understand_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_of_member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_of_witness` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_relation_member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sign_parent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sign_parent_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `withdrawn_member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_member_2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `careform_form5s`
--

INSERT INTO `careform_form5s` (`id`, `client_id`, `case_manager_id`, `careform_id`, `name_of_member`, `dob`, `if_available`, `request_and_authoriz`, `to_obtain_from`, `following_types1`, `following_types2`, `for_the_perpose1`, `for_the_perpose2`, `all_information_authorize`, `i_understand_date`, `signature_of_member`, `signature_of_witness`, `title_relation_member`, `sign_parent`, `sign_parent_date`, `withdrawn_member`, `signature_member_2`, `created_at`, `updated_at`) VALUES
(20, NULL, NULL, 52, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, NULL, NULL, 53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, NULL, NULL, 54, 'Tester', '2021-04-14', 'ZHaSDjKyT3', 'sOV9kAolMj', 'Qv2ho7hT8Z', 'MrLlWHwKCi', '72EUup0vVP', 'q6GYoDt2zU', 'VrnZK9Dmfl', '3', '12', 'uXKLFYPHgg', 'ZEP984vQoA', 'pxrhnVt8OW', 'DZJ56C1PAK', NULL, 'KIdYpF5VNw', 'kUQm0ezddu', NULL, '2021-05-15 06:52:40'),
(25, NULL, NULL, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, NULL, NULL, 58, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, NULL, NULL, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `careform_form6s`
--

CREATE TABLE `careform_form6s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `name_of_member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medicaid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_quarter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_quarter_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_quarter_cm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_quarter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_quarter_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_quarter_cm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `third_quarter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `third_quarter_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `third_quarter_cm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forth_quarter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forth_quarter_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forth_quarter_cm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `careform_form6s`
--

INSERT INTO `careform_form6s` (`id`, `client_id`, `case_manager_id`, `careform_id`, `name_of_member`, `dob`, `medicaid`, `date`, `case_manager_name`, `signature`, `first_quarter`, `first_quarter_date`, `first_quarter_cm`, `second_quarter`, `second_quarter_date`, `second_quarter_cm`, `third_quarter`, `third_quarter_date`, `third_quarter_cm`, `forth_quarter`, `forth_quarter_date`, `forth_quarter_cm`, `created_at`, `updated_at`) VALUES
(20, NULL, NULL, 52, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, NULL, NULL, 53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, NULL, NULL, 54, 'Tester333', '2021-04-14', '0g9p8v2WvO', '12', '397QXOnk5V', '0YM4XIxyrW', 'khoUT8Oqxu', NULL, 'VUH0nRmAYz', 'DOR6ywgWSZ', NULL, 'j1pdRr2aOd', 'WwP3k9LGk4', NULL, 't3TDpiNcMo', 'KbwHYKHvFa', '12', 'docbO1WLf4', NULL, '2021-05-15 06:53:38'),
(25, NULL, NULL, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, NULL, NULL, 58, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, NULL, NULL, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `careform_form7s`
--

CREATE TABLE `careform_form7s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `member` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medicaid_number` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `careform_form7s`
--

INSERT INTO `careform_form7s` (`id`, `client_id`, `case_manager_id`, `careform_id`, `member`, `medicaid_number`, `created_at`, `updated_at`) VALUES
(15, NULL, NULL, 52, NULL, NULL, NULL, NULL),
(16, NULL, NULL, 53, NULL, NULL, NULL, NULL),
(17, NULL, NULL, 54, 'Tesssssttttttt', '3849733702', NULL, '2021-05-15 06:53:57'),
(20, NULL, NULL, 57, NULL, NULL, NULL, NULL),
(21, NULL, NULL, 58, NULL, NULL, NULL, NULL),
(22, NULL, NULL, 59, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `careform_form8s`
--

CREATE TABLE `careform_form8s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `careform_form9s`
--

CREATE TABLE `careform_form9s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `careform_form10s`
--

CREATE TABLE `careform_form10s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `this` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day_of` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year20` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `print_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_member_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `print_name_cm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_cm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `careform_form10s`
--

INSERT INTO `careform_form10s` (`id`, `client_id`, `case_manager_id`, `careform_id`, `this`, `day_of`, `year20`, `print_name`, `signature_member`, `signature_member_date`, `print_name_cm`, `signature_cm`, `signature_date`, `created_at`, `updated_at`) VALUES
(9, NULL, NULL, 52, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, NULL, NULL, 53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, NULL, NULL, 54, 'Testtttttt', 'Dn7Hkdbzzj', 'QCHqfnKRkW', 'EuA5SpVyhC', 'cbPwq2UA0c', NULL, 'g7rlvGlF8s', 'RuwbaexbcA', NULL, NULL, '2021-05-15 06:54:21'),
(14, NULL, NULL, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, NULL, NULL, 58, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, NULL, NULL, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `careform_form11s`
--

CREATE TABLE `careform_form11s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `careform_form12s`
--

CREATE TABLE `careform_form12s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `this` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day_of` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year20` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `print_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_member_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `print_name_cm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_cm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `careform_form12s`
--

INSERT INTO `careform_form12s` (`id`, `client_id`, `case_manager_id`, `careform_id`, `this`, `day_of`, `year20`, `print_name`, `signature_member`, `signature_member_date`, `print_name_cm`, `signature_cm`, `signature_date`, `created_at`, `updated_at`) VALUES
(7, NULL, NULL, 52, 'kmRI8VcDFi', 'HCDWoOrSY3', 'rZe7NYjkJa', 'yPbyXtMtIF', 'unTX9ccQ80', NULL, 'dwmUhXeU0D', 'JPKKfixmxt', NULL, NULL, '2021-05-13 15:49:23'),
(8, NULL, NULL, 53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, NULL, NULL, 54, 'Testttttttqeqweqweqwe', 'CCAWfv3wLY', '5sq5rDOIOs', 'AfhENqobBX', 'v30yrX7lxc', NULL, '1NGkUuitVL', 's2ttfWXtQG', NULL, NULL, '2021-05-15 06:54:44'),
(12, NULL, NULL, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, NULL, NULL, 58, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, NULL, NULL, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `careform_form13s`
--

CREATE TABLE `careform_form13s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `case_manager` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `participant` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `participant_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `authorized_representative` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `authorized_representative_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `witness` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `witness_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refusal_participant` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refusal_participant_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refusal_authorized_representative` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refusal_authorized_representative_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refusal_witness` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refusal_witness_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `careform_form13s`
--

INSERT INTO `careform_form13s` (`id`, `client_id`, `case_manager_id`, `careform_id`, `case_manager`, `case_manager_date`, `participant`, `participant_date`, `authorized_representative`, `authorized_representative_date`, `witness`, `witness_date`, `refusal_participant`, `refusal_participant_date`, `refusal_authorized_representative`, `refusal_authorized_representative_date`, `refusal_witness`, `refusal_witness_date`, `created_at`, `updated_at`) VALUES
(2, NULL, NULL, 52, 'M6d2B7bd9H', 'qwe', 'Vy4bBri7rN', 'wqe', 'Kvs6kUEsph', 'qwe', '59nR5mQpCx', 'qwe', 'LhnLMinpkd', 'qwe', 'BdR77efKsm', 'qwe', 'P5FK4067kl', 'qwe', NULL, '2021-05-13 15:49:34'),
(3, NULL, NULL, 53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, NULL, NULL, 54, 'zzzzzzzzzzzzzzzzzzzzzz', 'qwe', 'hwLR350mUJ', 'wqe', 'gWbEwwbuRq', 'qwe', 'w9ixcXysNo', 'qwe', 'XIkf5l2cD7', 'qwe', 'FfYYtxz7fo', 'qwe', 'nOnN0kAKjc', 'qwe', NULL, '2021-05-15 06:55:05'),
(7, NULL, NULL, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, NULL, NULL, 58, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, NULL, NULL, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `careform_form14s`
--

CREATE TABLE `careform_form14s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `member_name_last` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Care_plan_due_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Member_Name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_name_First` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_name_Middle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_name_Suffix` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `City` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `State` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Zip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Cell` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Date_of_Birth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Age` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `past_year` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DFCS` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Living_Arrangements` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `compared_to_1_year_ago` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Current_Diagnosis_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Current_Diagnosis_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Current_Diagnosis_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Current_Diagnosis_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Current_Diagnosis_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Current_Diagnosis_6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Current_Diagnosis_7` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Current_Diagnosis_8` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Current_Diagnosis_9` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Current_Diagnosis_10` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Current_Diagnosis_11` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Current_Diagnosis_12` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `careform_form14s`
--

INSERT INTO `careform_form14s` (`id`, `client_id`, `case_manager_id`, `careform_id`, `member_name_last`, `Care_plan_due_date`, `Member_Name`, `member_name_First`, `member_name_Middle`, `member_name_Suffix`, `Address`, `City`, `State`, `Zip`, `Country`, `Phone`, `Cell`, `Date_of_Birth`, `Age`, `gender`, `past_year`, `DFCS`, `Living_Arrangements`, `compared_to_1_year_ago`, `details`, `Current_Diagnosis_1`, `Current_Diagnosis_2`, `Current_Diagnosis_3`, `Current_Diagnosis_4`, `Current_Diagnosis_5`, `Current_Diagnosis_6`, `Current_Diagnosis_7`, `Current_Diagnosis_8`, `Current_Diagnosis_9`, `Current_Diagnosis_10`, `Current_Diagnosis_11`, `Current_Diagnosis_12`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 57, 'Member name', 'due date', 'Last', 'First', 'Middle Initial', 'Suffix', 'Address', 'City', 'State', 'Zip', 'Country', 'Phone', 'Cell', 'Date of Birth', '22', 'Male', 'No', 'DFCS', '1,6', 'Yes', 'detailsss', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', NULL, '2021-05-17 10:28:42'),
(2, NULL, NULL, 58, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, NULL, NULL, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `careform_form15s`
--

CREATE TABLE `careform_form15s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `member_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Medical_List_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Medical_List_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Medical_List_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Medical_Lis5_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Medical_List_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Medical_List_6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Medical_List_7` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Medical_List_8` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Medical_List_9` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Medical_List_10` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Medical_List_11` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Medical_List_12` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Skilled` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Evaluation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Potential` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ICWP_member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Comments` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `careform_form15s`
--

INSERT INTO `careform_form15s` (`id`, `client_id`, `case_manager_id`, `careform_id`, `member_name`, `Medical_List_1`, `Medical_List_2`, `Medical_List_3`, `Medical_Lis5_4`, `Medical_List_5`, `Medical_List_6`, `Medical_List_7`, `Medical_List_8`, `Medical_List_9`, `Medical_List_10`, `Medical_List_11`, `Medical_List_12`, `Skilled`, `Evaluation`, `Potential`, `ICWP_member`, `Comments`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 58, 'Member name', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '', '', '', '', NULL, NULL, '2021-05-17 11:54:07'),
(2, NULL, NULL, 59, 'Member name', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '1,2,3,4,5,6,7,8,9', '1,2,3', '1,2,3,4', '1,2,3,4,5,6', NULL, NULL, '2021-05-18 03:35:52');

-- --------------------------------------------------------

--
-- Table structure for table `careform_form16s`
--

CREATE TABLE `careform_form16s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `careform_id` tinyint(4) DEFAULT NULL,
  `member_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `physician_visit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `annual_period` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `describe` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hospitalizations_visit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `diagnosis_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `variance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facility_car` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facility_placement` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facility_placement_reasons` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `progressive_disease` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disease_state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_past_year` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `safety_implemented` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contributing_factors` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urinary_incontinence` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bowel_incontinence` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `devices` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Comments` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `careform_form16s`
--

INSERT INTO `careform_form16s` (`id`, `client_id`, `case_manager_id`, `careform_id`, `member_name`, `physician_visit`, `annual_period`, `describe`, `hospitalizations_visit`, `diagnosis_date`, `variance`, `facility_car`, `facility_placement`, `facility_placement_reasons`, `progressive_disease`, `disease_state`, `member_past_year`, `safety_implemented`, `contributing_factors`, `urinary_incontinence`, `bowel_incontinence`, `devices`, `Comments`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 59, 'Member name', 'physician', 'Yes', NULL, 'Yes', NULL, 'No', 'Yes', 'No', NULL, 'Yes', NULL, 'No', 'Yes', NULL, 'No', 'No', '3,4,11,12', NULL, NULL, '2021-05-18 06:20:31');

-- --------------------------------------------------------

--
-- Table structure for table `cases_assignments`
--

CREATE TABLE `cases_assignments` (
  `id` int(11) NOT NULL,
  `staff_id` tinyint(4) DEFAULT NULL,
  `manager_id` tinyint(4) DEFAULT NULL,
  `staff_name` varchar(255) DEFAULT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `client_name` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cases_assignments`
--

INSERT INTO `cases_assignments` (`id`, `staff_id`, `manager_id`, `staff_name`, `client_id`, `client_name`, `status`, `created_at`, `updated_at`) VALUES
(15, 6, 6, NULL, 5, NULL, NULL, '2021-04-21 09:23:28', '2021-04-21 09:23:28'),
(17, 6, 6, NULL, 6, NULL, NULL, '2021-04-21 05:49:17', '2021-04-21 05:49:17'),
(18, 8, 8, NULL, 7, NULL, NULL, '2021-05-15 06:36:58', '2021-05-15 06:36:58'),
(19, 9, 9, NULL, 8, NULL, NULL, '2021-05-15 06:44:56', '2021-05-15 06:44:56'),
(20, 10, 10, NULL, 9, NULL, NULL, '2021-05-17 06:39:19', '2021-05-17 06:39:19'),
(21, 10, 10, NULL, 9, NULL, NULL, '2021-05-17 06:39:50', '2021-05-17 06:39:50');

-- --------------------------------------------------------

--
-- Table structure for table `case_managers`
--

CREATE TABLE `case_managers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `no_of_cases` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `case_managers`
--

INSERT INTO `case_managers` (`id`, `staff_id`, `no_of_cases`, `status`, `date`, `created_at`, `updated_at`) VALUES
(16, 6, NULL, '1', NULL, '2021-04-21 05:39:11', '2021-04-21 05:39:11'),
(17, 8, NULL, '1', NULL, '2021-05-15 06:36:26', '2021-05-15 06:36:26'),
(18, 9, NULL, '1', NULL, '2021-05-15 06:43:56', '2021-05-15 06:43:56'),
(19, 10, NULL, '1', NULL, '2021-05-17 06:32:42', '2021-05-17 06:32:42');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `county` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `user_id`, `name`, `contact`, `age`, `country`, `state`, `city`, `county`, `area`, `zip_code`, `status`, `created_at`, `updated_at`) VALUES
(5, 38, 'Client1', '123213', NULL, 'USA', 'Alaska', 'New York', 'Washington', 'abc', '78551', '1', '2021-04-21 09:17:19', '2021-04-21 09:17:19'),
(6, 41, 'Finch', '12313123123', NULL, 'USA', 'Alkian', 'New York', 'Washington', 'abcd', '12312312', '1', '2021-04-21 05:48:23', '2021-04-21 05:48:23'),
(7, 42, 'Client3', '0098098908', NULL, 'USA', 'Alaska', 'New York', 'Washington', 'abc', '1234', '1', '2021-05-15 06:34:03', '2021-05-15 06:34:03'),
(8, 44, 'client4', '1212312', NULL, 'USA', 'Alaska', 'New York', 'Washingtone', 'abc', '1221212', '1', '2021-05-15 06:41:19', '2021-05-15 06:41:19'),
(9, 47, 'testclient', '4534543', NULL, 'usa', 'washington', 'newyork', 'testclient', 'testclient', 'testclient', '1', '2021-05-17 06:35:47', '2021-05-17 06:35:47');

-- --------------------------------------------------------

--
-- Table structure for table `client_cases`
--

CREATE TABLE `client_cases` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `documents` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `client_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `county` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_height` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_case_status` tinyint(4) DEFAULT NULL,
  `assigned_status` tinyint(4) DEFAULT NULL,
  `case_manager_id` tinyint(4) DEFAULT NULL,
  `case_manager_name` tinyint(4) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `client_change_forms`
--

CREATE TABLE `client_change_forms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_change` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `change_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_new_case_manager` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved_hours` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remaining_hours` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `additionals_notes_documentation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_signature` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_signature` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `client_change_forms`
--

INSERT INTO `client_change_forms` (`id`, `provider_id`, `client_id`, `case_manager_id`, `member_name`, `date_of_change`, `change_type`, `name_new_case_manager`, `approved_hours`, `remaining_hours`, `additionals_notes_documentation`, `case_manager_signature`, `case_manager_date`, `owner_signature`, `owner_date`, `created_at`, `updated_at`) VALUES
(0, NULL, '8', NULL, 'CareForm', NULL, '1,2,4', 'su0jWztNJS', 'cobEc0nOxF', 'iEd29X52D9', 'hmbSGafz3A', 'xsltdMwov8', 'qwe', '9l0AyILSt7', NULL, '2021-05-15 06:47:32', '2021-05-15 06:47:32'),
(0, NULL, '8', NULL, 'CareForm', NULL, '1,2,4', 'su0jWztNJS', 'cobEc0nOxF', 'iEd29X52D9', 'hmbSGafz3A', 'xsltdMwov8', 'qwe', '9l0AyILSt7', NULL, '2021-05-15 06:47:32', '2021-05-15 06:47:32');

-- --------------------------------------------------------

--
-- Table structure for table `diseases`
--

CREATE TABLE `diseases` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `followups`
--

CREATE TABLE `followups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `st_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `et_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plan_fu1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doc_hrs1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `st_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `et_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plan_fu2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doc_hrs2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `st_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `et_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plan_fu3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doc_hrs3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `st_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `et_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plan_fu4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doc_hrs4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_signature` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_signature` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `followups`
--

INSERT INTO `followups` (`id`, `provider_id`, `client_id`, `case_manager_id`, `member_name`, `date_1`, `st_1`, `et_1`, `plan_fu1`, `notes1`, `doc_hrs1`, `date_2`, `st_2`, `et_2`, `plan_fu2`, `notes2`, `doc_hrs2`, `date_3`, `st_3`, `et_3`, `plan_fu3`, `notes3`, `doc_hrs3`, `date_4`, `st_4`, `et_4`, `plan_fu4`, `notes4`, `doc_hrs4`, `case_manager_signature`, `case_manager_date`, `owner_signature`, `owner_date`, `created_at`, `updated_at`) VALUES
(1, NULL, '5', NULL, 'Azalia Castaneda', '28-Nov-1978', 'Sunt nulla molestia', 'Ea error nulla et no', 'Amet quia est offi', 'Neque vero consequun', 'Nulla ipsam sequi es', '23-Jul-2000', 'Laboris nisi nesciun', 'Fugiat molestiae a d', 'Ullam in officiis an', 'Elit dolore corpori', 'Veniam ea qui quod', '03-Oct-2018', 'Tenetur ut iusto eve', 'Assumenda in ipsum i', 'In numquam quia exer', 'Est eum animi cillu', 'Qui omnis quos alias', '27-Jan-1977', 'Eveniet in optio p', 'Aut ut nostrud offic', 'Minim nostrum praese', 'Eiusmod id voluptate', 'Beatae consequat Mo', 'A et nemo maxime in', '09-Jan-1985', 'Aperiam consequat Q', '28-Jun-1979', '2021-05-04 00:22:48', '2021-05-04 01:40:30'),
(3, NULL, '6', NULL, 'Alexa Berry', '27-Dec-1976', 'Cum odio et enim dol', 'Fugiat proident ex', 'Enim quam qui quam e', 'Repellendus Eiusmod', 'Et incidunt enim la', '26-May-1994', 'Praesentium minima p', 'Neque cum eu in aliq', 'Quibusdam vitae cons', 'Ut consequatur esse', 'Nostrud voluptas ali', '17-Jul-2001', 'Dolore atque invento', 'Asperiores saepe est', 'Vero doloribus vero', 'Sit non placeat nos', 'Rerum enim dolorem s', '21-Jun-1985', 'Odit doloremque lore', 'Et voluptatem eum mi', 'Aperiam totam accusa', 'Deleniti sed et in e', 'Excepteur qui ut rei', 'Aut in deserunt blan', '15-Feb-2012', 'Repudiandae ipsum re', '26-Dec-1976', '2021-05-04 01:48:58', '2021-05-04 01:48:58'),
(4, NULL, '6', NULL, 'Josiah Gaines', '14-Nov-1970', 'Qui eos aliquip quas', 'Obcaecati consectetu', 'Quis aliquid sit ve', 'Quasi dolores odit s', 'Voluptas est officii', '28-Sep-1973', 'In qui libero simili', 'Minus enim sunt nihi', 'Sit dolore iure exc', 'Enim iure eos perspi', 'Omnis aut enim in pr', '07-Jun-2007', 'Esse voluptatum vel', 'Consequat Numquam i', 'Quia sed necessitati', 'Tempore magnam tene', 'Tempora quas labore', '25-Feb-1985', 'Nemo est consequatur', 'Tempore dolor volup', 'Incidunt et elit n', 'Nostrum officia susc', 'In accusantium sed q', 'Fugiat consectetur', '01-Nov-1970', 'Dolore quidem accusa', '11-Apr-1989', '2021-05-04 01:53:17', '2021-05-04 01:53:17'),
(5, NULL, '8', NULL, 'CareForm', NULL, '4TWVAzv6bT', 'Wqp8u4ow7r', 'vENhifIV0V', 'LtYTDrlKKD', 'reiPgQWpi3', NULL, 'aadInj2HlB', 'hvN1nYDR90', 'm7gJ7BSGYN', '4StrAsvPiP', 'YkPpRLrOUA', NULL, 'VfCZyAMaW8', '1vNc3whMFz', 'yV43LdktcM', 'FNrMdpIoI4', 'mIJIng40Sp', NULL, 'PXHqVN4AIi', 'UlpiYhblmv', 'mLA38duFHp', 'AirQDjCFMD', 'cLLdafieTT', 'zdvPU7WYsf', 'qwe', '7ijuJ101TQ', NULL, '2021-05-15 06:48:41', '2021-05-15 06:48:41');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `monthly_contact_forms`
--

CREATE TABLE `monthly_contact_forms` (
  `id` bigint(4) NOT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cal_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_of_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `worker` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cal_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `topic_of_discussion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `narraitive_requirement` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `curent_source` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `curent_icwp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_relationship` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contacts_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contacts_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contacts_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contacts_relationship` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reviewed_population` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `varified` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_signature` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_signature` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `monthly_contact_forms`
--

INSERT INTO `monthly_contact_forms` (`id`, `provider_id`, `client_id`, `case_manager_id`, `cal_date`, `time_of_date`, `worker`, `cal_type`, `contact_name`, `relation`, `topic_of_discussion`, `narraitive_requirement`, `curent_source`, `curent_icwp`, `file_name`, `file_address`, `file_phone`, `file_relationship`, `contacts_name`, `contacts_address`, `contacts_phone`, `contacts_relationship`, `reviewed_population`, `varified`, `case_manager_signature`, `case_manager_date`, `owner_signature`, `owner_date`, `created_at`, `updated_at`) VALUES
(1, NULL, '5', NULL, 'z', 'z', 'z', '3', 'z', 'z', '16', 'za', '7,8,9,10', '8', 'z', 'za', 'z', 'z', 'z', 'z', 'z', 'z', '2', '2', 'z', 'z', 'z', 'z', '2021-04-26 00:57:54', '2021-05-03 23:57:17');

-- --------------------------------------------------------

--
-- Table structure for table `monthly_visit_forms`
--

CREATE TABLE `monthly_visit_forms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `monthly_contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `st_et` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_hm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `documentation_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permision_for_tele` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verbal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nonverbal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_present` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appearence_of_emotional` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `self_reported_mood` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `monthly_health_provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral_needed` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_hygiene` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smoke_cigarettes` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `per_day` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appearence_home_envoirnment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qr` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cp_renewal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `initial_cp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pss_agency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_attendence` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `schedule` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `services_report` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comments_problems` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medical` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `detail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_pcp_medical_visit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `next_pcp_medical_visit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pcp_medication_conditions` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accu_checks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trach` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ventilator` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oxygen` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skin_integrity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active_wound_care` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `decubitus_stage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `how_offen_are_turned` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uti` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bowel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bladder` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ambulation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transfer_assistance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_hrs` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fall_past` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `identify_risk_factor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meal_plan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_activities` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `summar_followup` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_authorized` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_authorized` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_manager_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `monthly_visit_forms`
--

INSERT INTO `monthly_visit_forms` (`id`, `provider_id`, `client_id`, `case_manager_id`, `monthly_contact`, `Date`, `st_et`, `total_hm`, `documentation_time`, `permision_for_tele`, `member`, `verbal`, `nonverbal`, `other_present`, `appearence_of_emotional`, `self_reported_mood`, `monthly_health_provider`, `name_contact`, `referral_needed`, `member_hygiene`, `smoke_cigarettes`, `per_day`, `appearence_home_envoirnment`, `qr`, `cp_renewal`, `initial_cp`, `pss_agency`, `no_of_attendence`, `schedule`, `services_report`, `comments_problems`, `medical`, `detail`, `last_pcp_medical_visit`, `next_pcp_medical_visit`, `pcp_medication_conditions`, `accu_checks`, `trach`, `ventilator`, `oxygen`, `other`, `skin_integrity`, `active_wound_care`, `decubitus_stage`, `how_offen_are_turned`, `uti`, `bowel`, `bladder`, `ambulation`, `transfer_assistance`, `no_of_hrs`, `fall_past`, `identify_risk_factor`, `meal_plan`, `social_activities`, `summar_followup`, `member_authorized`, `date_authorized`, `case_manager_date`, `owner_date`, `created_at`, `updated_at`) VALUES
(0, NULL, '5', NULL, '1', '01-Aug-1976', 'Ut culpa cum ab lore', 'Eius ipsum facilis', 'Sit quia facere quos', '', 'Aperiam dolore aliqu', 'Quia harum quo assum', 'Incidunt in id omni', 'Ut quisquam cupidata', '1,5,6,7,8,11,12', 'Quibusdam incidunt', '', 'Kaitlin Carney', '2', 'Eu commodi consequun', 'Incididunt repudiand', '5', 'Nisi consectetur oc', 'Aliquid aut eum corr', 'Illo elit hic quis', 'Aut nisi velit eos a', 'Et praesentium velit', 'Ut optio minima non', 'Porro natus dolor au', '2,3,4,6,7,8,9', 'Libero molestiae aut', '2', 'Magnam voluptates al', 'Ut quis eveniet est', 'Incididunt ipsum te', 'Nostrum voluptatum c', 'Veniam sapiente et', 'Distinctio Maxime e', 'Lorem in voluptatem', 'Quisquam quod volupt', 'Velit est dolore fug', '3', '1,2', '1', 'Ad omnis cum dolorum', '', '2,6', '1,3,5', '6', '1,2', 'Mollitia esse quasi', '', 'Quod aliquid laboris', '4,6', 'Est enim dolor eos', 'Esse eum ad quo simi', 'Cillum omnis ipsum', '01-Jan-2009', '29-Apr-1994', '12-May-1986', '2021-04-21 05:45:14', '2021-04-21 05:45:14'),
(0, NULL, '8', NULL, '1,2', 'bbju0Lp8El', 'dpK6waUkpN', 'chB21iiSTb', 'eyDg4BZILF', '', 'yv2LU7WhEt', '1CkuQzxcPE', 'v0U52zK6xC', 'bjrFEZlvJ9', '', 'IKvqc9qTGp', '', '3631637219', '', 'TLoZommJ6Z', 'iZPt68txCx', 'hOAYxKc9as', 'qw4cZHZZHQ', 'RezBFLRSeQ', 'orJmiLPUT4', 'SBYxgzMD0J', '4dhVbugbw0', 'u5b52eAAQk', '16j6PszfXa', '10,11', 'mFgXFqUvbp', '', 'R38Oen27gp', 'Tm74QTniTb', 'vpwmFbeUty', 'xeumj7sRvO', 'eYLv9D7YYp', 'taPu5CCwnh', 'mn9WwDTwMh', 'Ibbs05ultH', 'wcKWoexu9C', '', '', '', 'dRJiu0fv4h', '', '', '', '', '', '8cKuB8gnxP', '', 'nqPHUmkGVJ', '5,6', '15Mr6dtcvN', 'b3GFOSaT1x', '2fZRMhyJ7C', NULL, 'qwe', NULL, '2021-05-15 06:48:13', '2021-05-15 06:48:13');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `providers`
--

CREATE TABLE `providers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `county` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `client_id` tinyint(4) DEFAULT NULL,
  `client_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `providers`
--

INSERT INTO `providers` (`id`, `user_id`, `name`, `email`, `contact`, `country`, `state`, `city`, `county`, `area`, `zip_code`, `status`, `created_at`, `updated_at`, `client_id`, `client_name`, `password`) VALUES
(6, 39, 'provider1', 'Provider1@gmail.com', '12312312', 'USA', 'Alaska', 'New York', 'Washington', 'abv', '12312312', '1', '2021-04-21 09:45:00', '2021-04-21 09:45:00', NULL, NULL, 'provider1');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `county` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_manager` tinyint(4) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `user_id`, `name`, `contact`, `country`, `state`, `city`, `county`, `area`, `zip_code`, `status`, `is_manager`, `created_at`, `updated_at`) VALUES
(6, 37, 'staff1', '213213123', 'USA', 'Alaska', 'New York', 'California', 'ABC', '78854', NULL, 1, '2021-04-21 09:14:14', '2021-04-21 05:39:11'),
(7, 40, 'Bravo', '21312312', 'USA', 'Alaska', 'New York', 'Washington', 'abcd', '112312', NULL, 0, '2021-04-21 05:47:05', '2021-04-21 05:47:05'),
(8, 43, 'newStaff', '00000000000', 'USA', 'Alaska', 'New York', 'Washington', 'abc', '1223221', NULL, 1, '2021-05-15 06:36:13', '2021-05-15 06:36:26'),
(9, 45, 'staff4', '123123123', 'USA', 'Alaska', 'New York', 'Washingtone', 'abc', '123123', NULL, 1, '2021-05-15 06:43:07', '2021-05-15 06:43:56'),
(10, 46, 'test', '3432423', 'usa', 'washington', 'newyork', 'test', 'test', 'test', NULL, 1, '2021-05-17 06:31:53', '2021-05-17 06:32:42');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `roles` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `county` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assigned_disease` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `roles`, `country`, `city`, `county`, `contact`, `assigned_disease`) VALUES
(36, 'admin', 'admin@gmail.com', NULL, '$2y$10$KypKV8JtxGltQhmDc4ZYq.uqbduHcJ2AqA6K3bVv887yB3ttYFNtq', NULL, NULL, NULL, 'admin', 'USA', 'New York', 'Washington', '123123123', NULL),
(37, 'staff1', 'staff1@gmail.com', NULL, '$2y$10$ef.e1Lmkyeg.xe9KmOZUDu7a720Wf0o/GJ3RYTPNbtJ/n6hwogYq.', NULL, '2021-04-21 09:14:14', '2021-04-21 09:14:14', 'staff', NULL, NULL, NULL, NULL, NULL),
(38, 'Client1', 'client1@gmail.com', NULL, '$2y$10$KypKV8JtxGltQhmDc4ZYq.uqbduHcJ2AqA6K3bVv887yB3ttYFNtq', NULL, '2021-04-21 09:17:19', '2021-04-21 09:17:19', 'client', NULL, NULL, NULL, NULL, NULL),
(39, 'provider1', 'Provider1@gmail.com', NULL, '$2y$10$90gYwxaYHfLdBmgXC2NpoemWqWKDKJfyTf6OCuY2eRn6k9yI6W8p2', NULL, '2021-04-21 09:45:00', '2021-04-21 09:45:00', 'provider', NULL, NULL, NULL, NULL, NULL),
(40, 'Bravo', 'bravo@gmail.com', NULL, '$2y$10$t2t3wX0BdN7Z4eg7npkUJeRmUvcaNGPtPpubeI8ZzVLN00VWpc4wW', NULL, '2021-04-21 05:47:05', '2021-04-21 05:47:05', 'staff', NULL, NULL, NULL, NULL, NULL),
(41, 'Finch', 'finch@gmail.com', NULL, '$2y$10$47OlDy50/879H/BIXmrsweslTp1dVPwMICQL.1NUvY..TkDzgmAei', NULL, '2021-04-21 05:48:22', '2021-04-21 05:48:22', 'client', NULL, NULL, NULL, NULL, NULL),
(42, 'Client3', 'Client3@gmail.com', NULL, '$2y$10$AsSpLC0te61nl/uRrPa/8.pLEVKTLKT4KzMvp4UBsDh999p0JA3N.', NULL, '2021-05-15 06:34:03', '2021-05-15 06:34:03', 'client', NULL, NULL, NULL, NULL, NULL),
(43, 'newStaff', 'newStaff@gmail.com', NULL, '$2y$10$9Baz7ZezB3jfXRLST0r0DuM9PEbIOrSr1nxkiHweC5SUPd4F/BNAC', NULL, '2021-05-15 06:36:13', '2021-05-15 06:36:13', 'staff', NULL, NULL, NULL, NULL, NULL),
(44, 'client4', 'client4@gmail.com', NULL, '$2y$10$5kSqPdAd0j9bcaMe3Orga.u9E94AlTJX2WsrQT2/m6rSIzelq9mz6', NULL, '2021-05-15 06:41:19', '2021-05-15 06:41:19', 'client', NULL, NULL, NULL, NULL, NULL),
(45, 'staff4', 'staff4@gmail.com', NULL, '$2y$10$RDbfn44R31K0rdie.Q09NONUnQEEcND/pYTMXrAEsYE0tW29VksYW', NULL, '2021-05-15 06:43:07', '2021-05-15 06:43:07', 'staff', NULL, NULL, NULL, NULL, NULL),
(46, 'test', 'test@gmail.com', NULL, '$2y$10$jIg93b84OexJ6Uqt55jWHOsAXlOv7ZUPngCec11KyMhME1aFijpV6', NULL, '2021-05-17 06:31:53', '2021-05-17 06:31:53', 'staff', NULL, NULL, NULL, NULL, NULL),
(47, 'testclient', 'testclient@gmail.com', NULL, '$2y$10$tyg7uN0keeaA4GfI8nZ.9utLlE2OyXxi28PAKIq/Lj4LfTbCVaj5C', NULL, '2021-05-17 06:35:47', '2021-05-17 06:35:47', 'client', NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appendix_d5_s`
--
ALTER TABLE `appendix_d5_s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `appendix_k1_s`
--
ALTER TABLE `appendix_k1_s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careforms`
--
ALTER TABLE `careforms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form1s`
--
ALTER TABLE `careform_form1s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form2s`
--
ALTER TABLE `careform_form2s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form3s`
--
ALTER TABLE `careform_form3s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form4s`
--
ALTER TABLE `careform_form4s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form5s`
--
ALTER TABLE `careform_form5s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form6s`
--
ALTER TABLE `careform_form6s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form7s`
--
ALTER TABLE `careform_form7s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form8s`
--
ALTER TABLE `careform_form8s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form9s`
--
ALTER TABLE `careform_form9s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form10s`
--
ALTER TABLE `careform_form10s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form11s`
--
ALTER TABLE `careform_form11s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form12s`
--
ALTER TABLE `careform_form12s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form13s`
--
ALTER TABLE `careform_form13s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form14s`
--
ALTER TABLE `careform_form14s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form15s`
--
ALTER TABLE `careform_form15s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careform_form16s`
--
ALTER TABLE `careform_form16s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cases_assignments`
--
ALTER TABLE `cases_assignments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `case_managers`
--
ALTER TABLE `case_managers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_cases`
--
ALTER TABLE `client_cases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diseases`
--
ALTER TABLE `diseases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `followups`
--
ALTER TABLE `followups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `monthly_contact_forms`
--
ALTER TABLE `monthly_contact_forms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `providers`
--
ALTER TABLE `providers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appendix_d5_s`
--
ALTER TABLE `appendix_d5_s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `appendix_k1_s`
--
ALTER TABLE `appendix_k1_s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `careforms`
--
ALTER TABLE `careforms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `careform_form1s`
--
ALTER TABLE `careform_form1s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `careform_form2s`
--
ALTER TABLE `careform_form2s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `careform_form3s`
--
ALTER TABLE `careform_form3s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `careform_form4s`
--
ALTER TABLE `careform_form4s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `careform_form5s`
--
ALTER TABLE `careform_form5s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `careform_form6s`
--
ALTER TABLE `careform_form6s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `careform_form7s`
--
ALTER TABLE `careform_form7s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `careform_form8s`
--
ALTER TABLE `careform_form8s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `careform_form9s`
--
ALTER TABLE `careform_form9s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `careform_form10s`
--
ALTER TABLE `careform_form10s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `careform_form11s`
--
ALTER TABLE `careform_form11s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `careform_form12s`
--
ALTER TABLE `careform_form12s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `careform_form13s`
--
ALTER TABLE `careform_form13s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `careform_form14s`
--
ALTER TABLE `careform_form14s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `careform_form15s`
--
ALTER TABLE `careform_form15s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `careform_form16s`
--
ALTER TABLE `careform_form16s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cases_assignments`
--
ALTER TABLE `cases_assignments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `case_managers`
--
ALTER TABLE `case_managers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `client_cases`
--
ALTER TABLE `client_cases`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `diseases`
--
ALTER TABLE `diseases`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `followups`
--
ALTER TABLE `followups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `monthly_contact_forms`
--
ALTER TABLE `monthly_contact_forms`
  MODIFY `id` bigint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `providers`
--
ALTER TABLE `providers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
