<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', 'HomeController@index')->name('home1');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('user/logout', 'HomeController@logout')->name('user.logout');
Route::group([ 'middleware' => ['auth']] ,function () {
    Route::group(['middleware' => ['IsAdmin']] ,function () {
        Route::get('/create/', 'StaffController@create')->name('staff.create');
        Route::post('/store/', 'StaffController@store')->name('staff.store');
        Route::get('/showAll/', 'StaffController@showAllStaff')->name('staff.showAll');
        Route::get('/edit/{id}', 'StaffController@edit')->name('staff.edit');
        Route::post('/update/', 'StaffController@update')->name('staff.update');
        Route::get('/delete/{id}', 'StaffController@destroy')->name('staff.delete');
    });
    Route::group(['prefix' => 'client', 'middleware' => ['IsAdmin']], function () {
        Route::post('/caseManager/', 'StaffController@caseManager')->name('staff.caseManager');
        Route::post('/UnmarkCaseManager/', 'StaffController@unMarkaseManager')->name('staff.unMarkCaseManager');
    });
    Route::get('/ShowAll/', 'ClientController@showAllClient')->name('client.showAll');
    Route::get('/staff_client/', 'StaffController@staffClient')->name('staff.client');
    Route::get('/all_forms/{id}', 'HomeController@allForms')->name('all.forms');
    Route::group(['prefix' =>'client' ] ,function () {
        Route::get('/Show/', 'ClientController@show')->name('client.show')->middleware('IsClient');
        Route::get('/delete/{id}', 'ClientController@destroy')->name('client.details');
        Route::get('/edit/{id}', 'ClientController@edit')->name('client.edit');
        Route::post('/update/', 'ClientController@update')->name('client.update');
        Route::get('/delete/{id}', 'ClientController@destroy')->name('client.delete');
        Route::get('/details/{id}', 'ClientController@details')->name('client.details');
        Route::get('/profile/', 'ClientController@clientProfile')->name('client.profile');
});
    Route::group(['prefix' => 'staff', 'middleware' => ['IsStaff']], function () {
    Route::get('/show/', 'StaffController@show')->name('staff.show');
    Route::get('/view/', 'StaffController@staffDetails')->name('staff.view');
});
Route::prefix('disease')->group(function () {
    Route::get('/show/', 'DiseaseController@show')->name('disease.show');
    Route::get('/create/', 'DiseaseController@create')->name('disease.create');
    Route::post('/store/', 'DiseaseController@store')->name('disease.store');
    Route::get('/edit/{id}', 'DiseaseController@edit')->name('disease.edit');
    Route::post('/update/', 'DiseaseController@update')->name('disease.update');
    Route::get('/delete/{id}', 'DiseaseController@destroy')->name('disease.delete');
});
Route::prefix('cases')->group(function () {
    Route::get('/clientcases/{id}', 'ClientCaseController@ShowClientCases')->name('cases.clientcases');
    Route::get('/allcases/', 'ClientCaseController@ShowAllCases')->name('cases.allcases');
    Route::get('/create/{id}', 'ClientCaseController@create')->name('cases.create');
    Route::post('/store/', 'ClientCaseController@store')->name('cases.store');
    Route::get('/edit/{id}', 'ClientCaseController@edit')->name('cases.edit');
    Route::post('/update/', 'ClientCaseController@update')->name('cases.update');
    Route::get('/delete/{id}', 'ClientCaseController@destroy')->name('cases.delete');
    Route::get('/details/{id}', 'ClientCaseController@details')->name('cases.details');
});
    Route::prefix('casesassignmemt')->group(function () {
    Route::get('/show/{id}', 'CasesAssignmentController@show')->name('assignment.show');
    Route::post('/assign-casemanager/', 'CasesAssignmentController@store')->name('assignment.store');
    Route::get('/edit/{id}', 'CasesAssignmentController@edit')->name('assignment.edit');
    Route::get('/update/', 'CasesAssignmentController@update')->name('assignment.update');
    Route::get('/client-And-Managers/', 'CasesAssignmentController@viewAll')->name('assigned.managers');
    Route::get('/delete/{id}', 'CasesAssignmentController@destroy')->name('assignment.destroy');
});
    Route::prefix('provider')->group(function () {
    Route::get('/show/', 'ProviderController@show')->name('provider.show');
    Route::get('/showAll/', 'ProviderController@showAll')->name('provider.showAll');
    Route::get('/create', 'ProviderController@create')->name('provider.create');
    Route::post('/store', 'ProviderController@store')->name('provider.store');
    Route::get('/edit/{id}', 'ProviderController@edit')->name('provider.edit');
    Route::post('/update/{id}', 'ProviderController@update')->name('provider.update');
    Route::get('/delete/{id}', 'ProviderController@destroy')->name('provider.delete');
    });

    Route::group(['middleware' => ['IsAdmin']] ,function () {
    Route::resource('casemanager', 'CaseManagersController');
    });

    Route::get('/my_client_forms/{id}', 'AppendixD5Controller@myClientForms')->name('my.client.forms');
    Route::get('/create_form/{id}', 'AppendixD5Controller@createForm')->name('AppendixD5.create_form');
    Route::resource('AppendixD5', 'AppendixD5Controller');



    Route::get('/ClientChangeForm/{id}', 'ClientChangeFormController@myClientForms')->name('client.change.form');
    Route::get('/ClientChangeForm_create/{id}', 'ClientChangeFormController@createForm')->name('ClientChangeForm.create_form');
    Route::get('ClientChangeForm_show/{id}', 'ClientChangeFormController@show')->name('ClientChangeForm_show');
    Route::get('ClientChangeForm_edit/{id}', 'ClientChangeFormController@editForm')->name('ClientChangeForm_edit');




    Route::get('/appendixkData/{id}', 'AppendixK1Controller@myClientForms')->name('Appendixmy.client.forms');
    Route::get('/create_appendixk1/{id}', 'AppendixK1Controller@createForm')->name('AppendixK1.create_form');
    Route::resource('AppendixK1', 'AppendixK1Controller');
    Route::resource('ClientChangeForm', 'ClientChangeFormController');



    Route::get('/monthlyVisitReport/{id}', 'MonthlyVisitFormController@myClientForms')->name('monthlyVisitReport.forms');
    Route::get('/createmonthlyVisitReport/{id}', 'MonthlyVisitFormController@createForm')->name('monthlyVisitReport.create_form');
    Route::resource('monthlyVisitReport', 'MonthlyVisitFormController');
    Route::get('monthlyVisitReport_show/{id}', 'MonthlyVisitFormController@show')->name('monthlyVisitReport_show');
    Route::get('monthlyVisitReport_edit/{id}', 'MonthlyVisitFormController@editForm')->name('monthlyVisitReport_edit');
    // Route::get('temp', 'TemperatureController@index');



    Route::get('/monthlyContactReport/{id}', 'MonthlyContactFormController@myClientForms')->name('monthlyContactReport.forms');
    Route::get('/createmonthlyContactReport/{id}', 'MonthlyContactFormController@createForm')->name('monthlyContactReport.create_form');
    Route::resource('monthlyContactReport', 'MonthlyContactFormController');
    Route::get('monthlyContactReport_show/{id}', 'MonthlyContactFormController@show')->name('monthlyContactReport_show');
    Route::get('monthlyContactReport_edit/{id}', 'MonthlyContactFormController@editForm')->name('monthlyContactReport_edit');

    Route::get('/followup/{id}', 'FollowupController@myClientForms')->name('followup.forms');
    Route::get('/createfollowup/{id}', 'FollowupController@createForm')->name('followup.create_form');
    Route::resource('followup', 'FollowupController');
    Route::get('followup_show/{id}', 'FollowupController@show')->name('followup_show');
    Route::get('followup_edit/{id}', 'FollowupController@editForm')->name('followup_edit');

    Route::get('/careform/{id}', 'CareformController@myClientForms')->name('careform.forms');
    Route::get('/createcareform/{id}', 'CareformController@createForm')->name('careform.create_form');
    Route::resource('careform', 'CareformController');
    Route::get('careform_show/{id}', 'CareformController@show')->name('careform_show');
    Route::get('careform_edit/{id}', 'CareformController@editForm')->name('careform_edit');
    Route::get('careform_forms/{id}', 'CareformController@careform_forms')->name('careform_forms');

// Care Form --- Form 1 part -------------- Start
    Route::get('careform_form1Update/{id}', 'CareformForm1Controller@careform_form1Update')->name('careform_form1Update');
    Route::get('careform_form1index/{id}', 'CareformForm1Controller@index')->name('careform_form1index');
// Care Form --- Form 1 part -------------- Over

// Care Form --- Form 2 part -------------- Start
    Route::get('careform_form2index/{id}', 'CareformForm2Controller@index')->name('careform_form2index');
    Route::get('careform_form2Update/{id}', 'CareformForm2Controller@careform_form2Update')->name('careform_form2Update');
// Care Form --- Form 2 part -------------- Over

    // Care Form --- Form 3 part -------------- Start
    Route::get('careform_form3index/{id}', 'CareformForm3Controller@index')->name('careform_form3index');
    Route::get('careform_form3Update/{id}', 'CareformForm3Controller@careform_form3Update')->name('careform_form3Update');
    // Care Form --- Form 3 part -------------- Over

// Care Form --- Form 4 part -------------- Start
Route::get('careform_form4index/{id}', 'CareformForm4Controller@index')->name('careform_form4index');
Route::get('careform_form4Update/{id}', 'CareformForm4Controller@careform_form4Update')->name('careform_form4Update');
// Care Form --- Form 4 part -------------- Over

// Care Form --- Form 5 part -------------- Start
Route::get('careform_form5index/{id}', 'CareformForm5Controller@index')->name('careform_form5index');
Route::get('careform_form5Update/{id}', 'CareformForm5Controller@careform_form5Update')->name('careform_form5Update');
// Care Form --- Form 5 part -------------- Over

// Care Form --- Form 6 part -------------- Start
Route::get('careform_form6index/{id}', 'CareformForm6Controller@index')->name('careform_form6index');
Route::get('careform_form6Update/{id}', 'CareformForm6Controller@careform_form6Update')->name('careform_form6Update');
// Care Form --- Form 6 part -------------- Over


// Care Form --- Form 7 part -------------- Start
Route::get('careform_form7index/{id}', 'CareformForm7Controller@index')->name('careform_form7index');
Route::get('careform_form7Update/{id}', 'CareformForm7Controller@careform_form7Update')->name('careform_form7Update');
// Care Form --- Form 7 part -------------- Over

// Care Form --- Form 7 part -------------- Start
Route::get('careform_form8index/{id}', 'CareformForm8Controller@index')->name('careform_form8index');
Route::get('careform_form8Update/{id}', 'CareformForm8Controller@careform_form8Update')->name('careform_form8Update');
// Care Form --- Form 7 part -------------- Over

// Care Form --- Form 7 part -------------- Start
Route::get('careform_form9index/{id}', 'CareformForm9Controller@index')->name('careform_form9index');
Route::get('careform_form9Update/{id}', 'CareformForm9Controller@careform_form9Update')->name('careform_form9Update');
// Care Form --- Form 7 part -------------- Over

// Care Form --- Form 6 part -------------- Start
Route::get('careform_form10index/{id}', 'CareformForm10Controller@index')->name('careform_form10index');
Route::get('careform_form10Update/{id}', 'CareformForm10Controller@careform_form10Update')->name('careform_form10Update');
// Care Form --- Form 6 part -------------- Over

// Care Form --- Form 6 part -------------- Start
Route::get('careform_form11index/{id}', 'CareformForm11Controller@index')->name('careform_form11index');
Route::get('careform_form11Update/{id}', 'CareformForm11Controller@careform_form11Update')->name('careform_form11Update');
// Care Form --- Form 6 part -------------- Over

// Care Form --- Form 6 part -------------- Start
Route::get('careform_form12index/{id}', 'CareformForm12Controller@index')->name('careform_form12index');
Route::get('careform_form12Update/{id}', 'CareformForm12Controller@careform_form12Update')->name('careform_form12Update');
// Care Form --- Form 6 part -------------- Over

// Care Form --- Form 6 part -------------- Start
Route::get('careform_form13index/{id}', 'CareformForm13Controller@index')->name('careform_form13index');
Route::get('careform_form13Update/{id}', 'CareformForm13Controller@careform_form13Update')->name('careform_form13Update');
// Care Form --- Form 6 part -------------- Over


// Care Form --- Form 6 part -------------- Start
Route::get('careform_form14index/{id}', 'CareformForm14Controller@index')->name('careform_form14index');
Route::get('careform_form14Update/{id}', 'CareformForm14Controller@careform_form14Update')->name('careform_form14Update');
// Care Form --- Form 6 part -------------- Over

// Care Form --- Form 6 part -------------- Start
Route::get('careform_form15index/{id}', 'CareformForm15Controller@index')->name('careform_form15index');
Route::get('careform_form15Update/{id}', 'CareformForm15Controller@careform_form15Update')->name('careform_form15Update');
// Care Form --- Form 6 part -------------- Over

// Care Form --- Form 6 part -------------- Start
Route::get('careform_form16index/{id}', 'CareformForm16Controller@index')->name('careform_form16index');
Route::get('careform_form16Update/{id}', 'CareformForm16Controller@careform_form16Update')->name('careform_form16Update');
// Care Form --- Form 6 part -------------- Over

// Care Form --- Form 6 part -------------- Start
Route::get('careform_form17index/{id}', 'CareformForm17Controller@index')->name('careform_form17index');
Route::get('careform_form17Update/{id}', 'CareformForm17Controller@careform_form17Update')->name('careform_form17Update');
// Care Form --- Form 6 part -------------- Over

// Care Form --- Form 6 part -------------- Start
Route::get('careform_form18index/{id}', 'CareformForm18Controller@index')->name('careform_form18index');
Route::get('careform_form18Update/{id}', 'CareformForm18Controller@careform_form18Update')->name('careform_form18Update');
// Care Form --- Form 6 part -------------- Over

// Care Form --- Form 6 part -------------- Start
Route::get('careform_form19index/{id}', 'CareformForm19Controller@index')->name('careform_form19index');
Route::get('careform_form19Update/{id}', 'CareformForm19Controller@careform_form19Update')->name('careform_form19Update');
// Care Form --- Form 6 part -------------- Over

// Care Form --- Form 6 part -------------- Start
Route::get('careform_form20index/{id}', 'CareformForm20Controller@index')->name('careform_form20index');
Route::get('careform_form20Update/{id}', 'CareformForm20Controller@careform_form20Update')->name('careform_form20Update');
// Care Form --- Form 6 part -------------- Over


// Care Form --- Form 6 part -------------- Start
Route::get('careform_form21index/{id}', 'CareformForm21Controller@index')->name('careform_form21index');
Route::get('careform_form21Update/{id}', 'CareformForm21Controller@careform_form21Update')->name('careform_form21Update');
// Care Form --- Form 6 part -------------- Over

// Care Form --- Form 6 part -------------- Start
Route::get('careform_form22index/{id}', 'CareformForm22Controller@index')->name('careform_form22index');
Route::get('careform_form22Update/{id}', 'CareformForm22Controller@careform_form22Update')->name('careform_form22Update');
// Care Form --- Form 6 part -------------- Over






    Route::resource('ClientChangeForm', 'ClientChangeFormController');
    Route::get('/clear_cache', 'LiveCmdController@clear_cache')->name('clear_cache');
    Route::get('/view_clear', 'LiveCmdController@view_clear')->name('view_clear');
    Route::get('/optimize', 'LiveCmdController@optimize')->name('optimize');
    Route::get('/storage_link', 'LiveCmdController@storage_link')->name('storage_link');
    Route::get('/route_clear', 'LiveCmdController@route_clear')->name('route_clear');
    Route::get('/route_cache', 'LiveCmdController@route_cache')->name('route_cache');
});

