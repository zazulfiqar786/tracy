@extends('layouts.masterlayout')

@section('content')
<h2 style="margin-left: 10px;"><b>All Cases</b></h2>
<section id="widget-grid" class="" style="border-top:2px solid #CCC; margin-left: 10px; margin-right: 10px">
	<!-- row -->
	<div class="row">
		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
				<div>
					<!-- widget content -->
					<div class="widget-body no-padding">
						<div class="widget-body-toolbar" style="overflow-x:auto;">
						</div >
						<table id="datatable_tabletools" class="table table-striped table-hover table-bordered">
							<thead>
								<tr>
									<th nowrap><center>S#</center></th>
									<th nowrap><center>Case Name</center></th>
									<th nowrap><center>Case No</center></th>
									<th nowrap><center>CLient Name</center></th>
									<th nowrap><center>Case Status</center></th>
									<th nowrap><center>Assigned Status</center></th>
									<th nowrap><center>Action</center></th>
								</tr>
							</thead>
							<tbody>
								@foreach($cases as $case)
                                <tr>
                                    <td nowrap><center>{{$case->id}}</center></td>
                                    <td nowrap><center>{{$case->case_name}}</center></td>
                                    <td nowrap><center>{{$case->case_no}}</center></td>
                                    <td nowrap>{{$case->client_name}}</td>
                                    @if($case->client_case_status == 1)
                                    <td nowrap><center>PENDING</center></td>
                                    @else
                                    <td nowrap><center>{{$case->client_case_status}}</center></td>
                                    @endif

                                    @if($case->assigned_status == 1)
                                    <td style="color: green;"  nowrap><center>ASSIGNED</center></td>
                                    @else
                                    <td style="color: red;" nowrap><center>NOT ASSIGNED</center></td>
                                    @endif

                                    <td nowrap> <center>


                                    <a href="{{route('assignment.show',$case->id)}}" class="btn btn-sm btn-primary"  title="Assign Case to Staff" ><i class="fa fa-tasks" aria-hidden="true"></i>
                                    </a>
{{--                                    <a href="{{route('cases.details',$case->id)}}" class="btn btn-sm btn-primary"  title="View" ><i class="fa fa-eye" aria-hidden="true"></i>--}}
{{--                                    </a>--}}

                                    <a href="{{route('cases.edit',$case->id)}}" class="btn btn-sm btn-primary"  title="Edit">
                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                    </a>

                                    <a href="{{route('cases.delete',$case->id)}}" class="btn btn-sm btn-primary"  title="Delete"  >
                                    <i class="fa fa-user-minus" aria-hidden="true"></i>
                                    </a>
                                    </center>
                                    </td>
                                </tr>
							@endforeach
							</tbody>
							<tfoot>

							</tfoot>
						</table>
					</div>
					<!-- end widget content -->
				</div>
				<!-- end widget div -->
			</div>
			<!-- end widget -->
		</article>
		</div>
		</section>

@endsection


<script>
$('#datatable_tabletools').DataTable({
			dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
			buttons: [
		                'copy', 'csv', 'excel', 'pdf', 'print'
		            ],
			initComplete : function(oSettings, json) {
			$(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
				$(this).addClass('btn-sm btn-default');
			});
			},
			"language": {
    			"search": "Search: ",
				"searchPlaceholder": "Search records"
  			},
			"pageLength": 50,
			"bDestroy": true,

			"order": []
		});
		/* END TABLE TOOLS */

		function ReportError(res)
		{
		}// Filter placeholder dataTable
		$(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>
