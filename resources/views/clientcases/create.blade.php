@extends('layouts.masterlayout')
@section('content')
<div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4 ">Add Case</h1>
                                    </div>


                    <form method="POST" action="{{ route('cases.store') }}">
                        @csrf
                        <input type="hidden" name="client_id" value="{{$clientData->id}}">
                        <input type="hidden" name="client_name" value="{{$clientData->name}}">

                        <div class="form-group row">
                            <label for="case_name" class="col-md-4 col-form-label text-md-right">{{ __('Case Name') }}</label>

                            <div class="col-md-6">
                                <input id="case_name" type="text" class="form-control @error('case_name') is-invalid @enderror" name="case_name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('case_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Case Description') }}</label>

                            <div class="col-md-6">
                                <input id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ old('name') }}" required autocomplete="description">

                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="documents" class="col-md-4 col-form-label text-md-right">{{ __('Case Documents') }}</label>

                            <div class="col-md-6">
                                <input id="documents" type="text" class="form-control @error('documents') is-invalid @enderror" name="documents" required autocomplete="documents">

                                @error('documents')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="case_no" class="col-md-4 col-form-label text-md-right">{{ __('Case Number') }}</label>

                            <div class="col-md-6">
                                <input id="case_no" type="text" class="form-control @error('case_no') is-invalid @enderror" name="case_no" value="{{ old('name') }}" required autocomplete="case_no" autofocus>

                                @error('case_no')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="city" class="col-md-4 col-form-label text-md-right">{{ __('Case Date') }}</label>

                            <div class="col-md-6">
                                <input id="case_date" type="date" class="form-control @error('case_date') is-invalid @enderror" name="case_date" value="{{ old('name') }}" required autocomplete="case_date" autofocus>

                                @error('case_date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="height" class="col-md-4 col-form-label text-md-right">{{ __('Height') }}</label>

                            <div class="col-md-6">
                                <input id="height" type="number" class="form-control @error('height') is-invalid @enderror" name="client_height" value="{{ old('name') }}" required autocomplete="height" autofocus>

                                @error('height')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="weight" class="col-md-4 col-form-label text-md-right">{{ __('Weight') }}</label>

                            <div class="col-md-6">
                                <input id="client_weight" type="number" class="form-control @error('client_weight') is-invalid @enderror" name="client_weight" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('weight')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
{{--                        <div class="form-group row">--}}
{{--                            <label for="country" class="col-md-4 col-form-label text-md-right">{{ __('Asssigned Disease') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <select class="form-control" name="assignedDisease" required id="assignedDisease">--}}
{{--                                    <option class=" form-control" value="option_select" disabled selected>Select Specific Disease</option>--}}

{{--                                    @foreach($diseases as $disease)--}}
{{--                                        <option class="form-control" value="{{ $disease->name}}"> {{ $disease->name}}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}

{{--                                @error('country')--}}
{{--                                <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

                        <div class="form-group row">
                            <label for="country" class="col-md-4 col-form-label text-md-right">{{ __('Country') }}</label>

                            <div class="col-md-6">
                                <input id="country" type="text" class="form-control @error('country') is-invalid @enderror" name="country" value="{{ $clientData->country }}" required autocomplete="name" autofocus>

                                @error('country')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="state" class="col-md-4 col-form-label text-md-right">{{ __('State') }}</label>

                            <div class="col-md-6">
                                <input id="state" type="text" class="form-control @error('state') is-invalid @enderror" name="state" value="{{ $clientData->state }}" required autocomplete="state" autofocus>

                                @error('country')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="city" class="col-md-4 col-form-label text-md-right">{{ __('City') }}</label>

                            <div class="col-md-6">
                                <input id="city" type="text" class="form-control @error('city') is-invalid @enderror" name="city" value="{{ $clientData->city }}" required autocomplete="name" autofocus>

                                @error('city')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="county" class="col-md-4 col-form-label text-md-right">{{ __('County') }}</label>

                            <div class="col-md-6">
                                <input id="county" type="text" class="form-control @error('county') is-invalid @enderror" name="county" value="{{ $clientData->county }}" required autocomplete="county" autofocus>

                                @error('county')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="area" class="col-md-4 col-form-label text-md-right">{{ __('Area') }}</label>

                            <div class="col-md-6">
                                <input id="area" type="text" class="form-control @error('area') is-invalid @enderror" name="area" value="{{ $clientData->area }}" required autocomplete="area" autofocus>

                                @error('area')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="zip_code" class="col-md-4 col-form-label text-md-right">{{ __('ZIP Code') }}</label>

                            <div class="col-md-6">
                                <input id="zip_code" type="text" class="form-control @error('zip_code') is-invalid @enderror" name="zip_code" value="{{ $clientData->zip_code }}" required autocomplete="zip_code" autofocus>

                                @error('zip_code')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Add Case') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
