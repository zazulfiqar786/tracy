@extends('layouts.masterlayout')

@section('content')
<div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4 ">Edit Case</h1>
                                    </div>


                    <form method="POST" action="{{ route('cases.update') }}">
                        @csrf
                        <input type="hidden" name="caseId" value="{{$caseData->id}}">
                        <input type="hidden" name="clientId" value="{{$caseData->client_id}}">
                        <div class="form-group row">
                            <label for="caseName" class="col-md-4 col-form-label text-md-right">{{ __('Case Name') }}</label>

                            <div class="col-md-6">
                                <input id="caseName" type="text" class="form-control @error('caseName') is-invalid @enderror" name="caseName" value="{{$caseData->name}}" required autocomplete="name" autofocus>

                                @error('caseName')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="caseDescription" class="col-md-4 col-form-label text-md-right">{{ __('Case Description') }}</label>

                            <div class="col-md-6">
                                <input id="caseDescription" type="text" class="form-control @error('caseDescription') is-invalid @enderror" name="caseDescription" value="{{$caseData->description}}" required autocomplete="caseDescription">

                                @error('caseDescription')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="caseDocuments" class="col-md-4 col-form-label text-md-right">{{ __('Case Documents') }}</label>

                            <div class="col-md-6">
                                <input id="caseDocuments" type="text" value="{{$caseData->documents}}" class="form-control @error('caseDocuments') is-invalid @enderror" name="caseDocuments" required autocomplete="caseDocuments">

                                @error('caseDocuments')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="caseNo" class="col-md-4 col-form-label text-md-right">{{ __('Case Number') }}</label>

                            <div class="col-md-6">
                                <input id="caseNo" type="text" class="form-control @error('caseNo') is-invalid @enderror" name="caseNo" value="{{$caseData->case_no}}" required autocomplete="caseNo" autofocus>

                                @error('caseNo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="city" class="col-md-4 col-form-label text-md-right">{{ __('Case Date') }}</label>

                            <div class="col-md-6">
                                <input id="case_date" type="date" class="form-control @error('caseDate') is-invalid @enderror" name="caseDate" value="{{$caseData->case_date}}" required autocomplete="caseDate" autofocus>

                                @error('caseDate')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="height" class="col-md-4 col-form-label text-md-right">{{ __('Height') }}</label>

                            <div class="col-md-6">
                                <input id="height" type="number" class="form-control @error('height') is-invalid @enderror" name="clientHeight" value="{{$caseData->client_height}}" required autocomplete="height" autofocus>

                                @error('height')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="weight" class="col-md-4 col-form-label text-md-right">{{ __('Weight') }}</label>

                            <div class="col-md-6">
                                <input id="clientWeight" type="number" class="form-control @error('clientWeight') is-invalid @enderror" name="clientWeight" value="{{$caseData->client_weight}}" required autocomplete="name" autofocus>

                                @error('weight')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update Case') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
