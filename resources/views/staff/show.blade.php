@extends('layouts.masterlayout')

@section('content')

<section id="widget-grid" class="" style="border-top:2px solid #CCC; margin-left: 10px; margin-right: 10px">
	<!-- row -->
	<div class="row">
		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
				<div>
					<!-- widget content -->
					<div class="widget-body no-padding">
						<div class="widget-body-toolbar" style="overflow-x:auto;">
						</div >
						<table id="datatable_tabletools" class="table table-striped table-hover table-bordered">
							<thead>
								<tr>
									<th nowrap><center>S#</center></th>
									<th nowrap><center>User Name</center></th>
									<th nowrap><center>Email</center></th>
									<th nowrap><center>Contact</center></th>
									<th nowrap><center>Role</center></th>
									<th nowrap><center>Action</center></th>
								</tr>
							</thead>
							<tbody>
								@foreach($userData as $staff)
                                <tr>
                                    <td nowrap><center>{{$staff->id}}</center></td>
                                    <td nowrap><center>{{$staff->name}}</center></td>
                                    <td nowrap><center>{{$staff->email}}</center></td>
                                    <td nowrap>{{$staff->contact}}</td>
                                    <td nowrap><center>{{$staff->roles}}</center></td>

                                    <td nowrap> <center>


{{--                                    <a class="btn btn-sm btn-primary"  title="View" ><i class="fa fa-eye" aria-hidden="true"></i>	--}}
{{--                                    </a>--}}

                                    </center>
                                    </td>
                                </tr>
							@endforeach
							</tbody>
							<tfoot>

							</tfoot>
						</table>
					</div>
					<!-- end widget content -->
				</div>
				<!-- end widget div -->
			</div>
			<!-- end widget -->
		</article>
		</div>
		</section>

@endsection


<script>
$('#datatable_tabletools').DataTable({
			dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
			buttons: [
		                'copy', 'csv', 'excel', 'pdf', 'print'
		            ],
			initComplete : function(oSettings, json) {
			$(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
				$(this).addClass('btn-sm btn-default');
			});
			},
			"language": {
    			"search": "Search: ",
				"searchPlaceholder": "Search records"
  			},
			"pageLength": 50,
			"bDestroy": true,

			"order": []
		});
		/* END TABLE TOOLS */

		function ReportError(res)
		{
		}// Filter placeholder dataTable
		$(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>
