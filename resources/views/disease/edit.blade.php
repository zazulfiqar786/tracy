@extends('layouts.masterlayout')

@section('content')
<div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4 ">Edit Disease Here</h1>
                                    </div>


                                    {{--<h1>number of candidates registered here {{$events}} </h1>--}}
                    <form method="POST" action="{{ route('disease.update') }}">
                        @csrf
                        <input type="hidden" name="diseaseId" value="{{$diseases->id}}">
                        <div class="form-group row">
                            <label for="diseaseName" class="col-md-4 col-form-label text-md-right">{{ __('Disease Name') }}</label>

                            <div class="col-md-6">
                                <input id="diseaseName" type="text" class="form-control @error('diseaseName') is-invalid @enderror" name="diseaseName" value="{{$diseases->name}}" required autocomplete="diseaseName" autofocus>

                                @error('diseaseName')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Edit Disease') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
