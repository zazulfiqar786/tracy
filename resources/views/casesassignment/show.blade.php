@extends('layouts.masterlayout')
@section('content')

    {{--    {{dd($caseManagers)}}--}}
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4 ">Assign Case Manager on Case
                                            No: {{ $clientDetail->case_no }}</h1>
                                    </div>
                                    {{--                                    <form method="POST" action="{{ route('client.update') }}">--}}
                                    <form method="POST" action="{{route('assignment.store')}}">
                                        @csrf
                                        <input type="hidden" name="client_id" value="{{ $clientDetail->id }}">
                                        <input type="hidden" name="user_id" value="{{ $clientDetail->user_id }}">
                                        <div class="form-group row">
                                            <label for="name"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Client Name') }}</label>
                                            <div class="col-md-6">
                                                <input id="cLient_name" type="text" readonly
                                                       class="form-control @error('cLient_name') is-invalid @enderror"
                                                       name="cLient_name" value="{{$clientDetail->name}}" required
                                                       autocomplete="name" autofocus>
                                                @error('cLient_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="country"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Country') }}</label>
                                            <div class="col-md-6">
                                                <input id="country" type="text" name="" readonly
                                                       class="form-control @error('country') is-invalid @enderror"
                                                       value="{{ $clientDetail->country }}" required
                                                       autocomplete="country" autofocus>
                                                @error('country')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="state"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('State') }}</label>
                                            <div class="col-md-6">
                                                <input id="state" type="text" name="" readonly
                                                       class="form-control @error('state') is-invalid @enderror"
                                                       value="{{ $clientDetail->state }}" required
                                                       autocomplete="state" autofocus>
                                                @error('state')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="city"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('City') }}</label>
                                            <div class="col-md-6">
                                                <input id="city" type="text" name="" readonly
                                                       class="form-control @error('city') is-invalid @enderror"
                                                       value="{{ $clientDetail->city }}" required
                                                       autocomplete="city" autofocus>
                                                @error('city')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="county"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('County') }}</label>
                                            <div class="col-md-6">
                                                <input id="county" name="" type="text" readonly
                                                       class="form-control @error('county') is-invalid @enderror"
                                                       value="{{ $clientDetail->county }}" required
                                                       autocomplete="county" autofocus>
                                                @error('county')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                 </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="area"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Area') }}</label>
                                            <div class="col-md-6">
                                                <input id="area" type="text" name="" readonly
                                                       class="form-control @error('area') is-invalid @enderror"
                                                       value="{{ $clientDetail->area }}" required
                                                       autocomplete="area" autofocus>
                                                @error('area')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="zip_code"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Zip Code') }}</label>
                                            <div class="col-md-6">
                                                <input type="text" name="" readonly
                                                       class="form-control @error('zip_code') is-invalid @enderror"
                                                       value="{{ $clientDetail->zip_code }}" required
                                                       autocomplete="zip_code" autofocus>
                                                @error('zip_code')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        @php
                                            $clientCountry = $clientDetail->country;
                                            $clientState = $clientDetail->state;
                                            $clientCity = $clientDetail->city;
                                            $clientCounty = $clientDetail->county;
                                            $clientArea = $clientDetail->area;
                                            $clientZipCode = $clientDetail->zip_code;
                                        @endphp

                                        {{--                                        ZIP Code Wise--}}
                                        <table class="table table-bordered table-striped table-hover">
                                            <tr>

                                                <td>
                                                    <center>Zip Code</center>
                                                </td>
                                                <td>
                                                    <center> Area</center>
                                                </td>
                                                <td>
                                                    <center> County</center>
                                                </td>
                                                <td>
                                                    <center>City</center>
                                                </td>
                                                <td>
                                                    <center>State</center>
                                                </td>
                                                <td>
                                                    <center>Country</center>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    <center><input type="radio" id="zip_code_wise" name="btnRadio"></center>
                                                </td>
                                                <td>
                                                    <center><input type="radio" id="area_wise" name="btnRadio"></center>
                                                </td>
                                                <td>
                                                    <center><input type="radio" id="county_wise" name="btnRadio"></center>
                                                </td>
                                                <td>
                                                    <center><input type="radio" id="city_wise" name="btnRadio"></center>
                                                </td>
                                                <td>
                                                    <center><input type="radio" id="state_wise" name="btnRadio"></center>
                                                </td>
                                                <td>
                                                    <center><input type="radio" id="country_wise" name="btnRadio"></center>
                                                </td>
                                            </tr>

                                        </table>
                                        <div class="form-group row">
                                            <label for="zip_code"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Zip Code Wise Case Managers') }}</label>
                                            <div class="col-md-6">
                                                <select readonly="" id="manager_zip_code"
                                                        class="form-control @error('manager_id') is-invalid @enderror"
                                                        autocomplete="zip_code" autofocus>
                                                    <option disabled selected>Zip Code Wise Case Manager</option>
                                                    @foreach($caseManagers as $caseManager)
                                                        @php
                                                            $countryWiseManagers = App\Staff::where('id',$caseManager->staff_id)
                                                              ->where('zip_code',$clientZipCode)->select('name','country','id')->get();
                                                        @endphp
                                                        @foreach($countryWiseManagers as $countryWiseManager )
                                                            <option
                                                                value="{{$countryWiseManager->id}}">{{$countryWiseManager->name}}</option>
                                                        @endforeach

                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        {{--                                        Area Wise --}}
                                        <div class="form-group row">
                                            <label for="Area"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Area Wise Case Managers') }}</label>
                                            <div class="col-md-6">
                                                <select readonly="" id="manager_area"
                                                        class="form-control @error('manager_id') is-invalid @enderror"
                                                        autocomplete="area" autofocus>
                                                    <option disabled selected>Area Wise Case Manager</option>
                                                    @foreach($caseManagers as $caseManager)
                                                        @php
                                                            $countryWiseManagers = App\Staff::where('id',$caseManager->staff_id)
                                                              ->where('area',$clientArea)->select('name','country','id')->get();
                                                        @endphp
                                                        @foreach($countryWiseManagers as $countryWiseManager )
                                                            <option
                                                                value="{{$countryWiseManager->id}}">{{$countryWiseManager->name}}</option>
                                                        @endforeach

                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        {{--                                        county Wise--}}
                                        <div class="form-group row">
                                            <label for="County"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('County Wise Case Managers') }}</label>
                                            <div class="col-md-6">
                                                <select readonly="" id="manager_county"
                                                        class="form-control @error('manager_id') is-invalid @enderror"
                                                        autocomplete="county" autofocus>
                                                    <option disabled selected>County Wise Case Manager</option>
                                                    @foreach($caseManagers as $caseManager)
                                                        @php
                                                            $countryWiseManagers = App\Staff::where('id',$caseManager->staff_id)
                                                              ->where('county',$clientCounty)->select('name','country','id')->get();
                                                        @endphp
                                                        @foreach($countryWiseManagers as $countryWiseManager )
                                                            <option
                                                                value="{{$countryWiseManager->id}}">{{$countryWiseManager->name}}</option>
                                                        @endforeach

                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        {{--                                        City Wise--}}
                                        <div class="form-group row">
                                            <label for="City"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('City Wise Case Managers') }}</label>
                                            <div class="col-md-6">
                                                <select readonly="" id="manager_city"
                                                        class="form-control @error('manager_id') is-invalid @enderror"
                                                        autocomplete="city" autofocus>
                                                    <option disabled selected>City Wise Case Manager</option>
                                                    @foreach($caseManagers as $caseManager)
                                                        @php
                                                            $countryWiseManagers = App\Staff::where('id',$caseManager->staff_id)
                                                              ->where('city',$clientCity)->select('name','country','id')->get();
                                                        @endphp
                                                        @foreach($countryWiseManagers as $countryWiseManager )
                                                            <option
                                                                value="{{$countryWiseManager->id}}">{{$countryWiseManager->name}}</option>
                                                        @endforeach

                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        {{--                                        State Wise--}}
                                        <div class="form-group row">
                                            <label for="State"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('State Wise Case Managers') }}</label>
                                            <div class="col-md-6">
                                                <select readonly="" id="manager_state"
                                                        class="form-control @error('manager_id') is-invalid @enderror"
                                                        autocomplete="zip_code" autofocus>
                                                    <option disabled selected>State Wise Case Manager</option>
                                                    @foreach($caseManagers as $caseManager)
                                                        @php
                                                            $countryWiseManagers = App\Staff::where('id',$caseManager->staff_id)
                                                              ->where('state',$clientState)->select('name','country','id')->get();
                                                        @endphp
                                                        @foreach($countryWiseManagers as $countryWiseManager )
                                                            <option
                                                                value="{{$countryWiseManager->id}}">{{$countryWiseManager->name}}</option>
                                                        @endforeach

                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        {{--                                        Country Wise--}}
                                        <div class="form-group row">
                                            <label for="Country"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Country Wise Case Managers') }}</label>
                                            <div class="col-md-6">
                                                <select readonly="" id="manager_country"
                                                        class="form-control @error('manager_id') is-invalid @enderror"
                                                        autocomplete="zip_code" autofocus>
                                                    <option disabled selected>Country Wise Case Manager</option>
                                                    @foreach($caseManagers as $caseManager)
                                                        @php
                                                            $countryWiseManagers = App\Staff::where('id',$caseManager->staff_id)
                                                              ->where('country',$clientCountry)->select('name','country','id')->get();
                                                        @endphp
                                                        @foreach($countryWiseManagers as $countryWiseManager )
                                                            <option
                                                                value="{{$countryWiseManager->id}}">{{$countryWiseManager->name}}</option>
                                                        @endforeach

                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row mb-0">
                                            <div class="col-md-6 offset-md-4">
                                                <button type="submit" class="btn btn-primary">
                                                    {{ __('Assign') }}
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
    $(document).ready(function () {
        $('#zip_code_wise').change(function (){
            var ischecked= $(this).is(':radio');
            if(ischecked)
            {
                $('#manager_zip_code').removeAttr('readonly');
                $('#manager_zip_code').attr('name','manager_id');

                // $('#manager_zip_code').attr('required','required');

                $('#manager_county').attr('readonly','readonly');
                $('#manager_state').attr('readonly','readonly');
                $('#manager_area').attr('readonly','readonly');
                $('#manager_country').attr('readonly','readonly');


            }
            else if(!ischecked)
            {
                $('#manager_zip_code').attr('readonly','readonly');
            }
        });

    $('#area_wise').change(function (){
        var ischecked= $(this).is(':radio');
        if(ischecked)
        {
            $('#manager_area').removeAttr('readonly');
            $('#manager_area').attr('name','manager_id');

            $('#manager_zip_code').attr('readonly','readonly');
            $('#manager_county').attr('readonly','readonly');
            $('#manager_state').attr('readonly','readonly');
            $('#manager_city').attr('readonly','readonly');
            $('#manager_country').attr('readonly','readonly');

            // $('#manager_area').attr('required','required');
        }
        else if(!ischecked)
        {
            $('#manager_zip_code').attr('readonly','readonly');
        }
    });

    $('#county_wise').change(function (){
        var ischecked= $(this).is(':checked');
        if(ischecked)
        {
            $('#manager_county').removeAttr('readonly');
            $('#manager_county').attr('name','manager_id');
            // $('#manager_county').attr('required','required');

            $('#manager_zip_code').attr('readonly','readonly');
            $('#manager_area').attr('readonly','readonly');
            $('#manager_state').attr('readonly','readonly');
            $('#manager_city').attr('readonly','readonly');
            $('#manager_country').attr('readonly','readonly');

        }
        else
        {
            $('#manager_county').attr('readonly','readonly');
        }
    });

    $('#city_wise').change(function (){
        var ischecked= $(this).is(':checked');
        if(ischecked)
        {
            $('#manager_city').removeAttr('readonly');
            $('#manager_city').attr('name','manager_id');
            // $('#manager_city').attr('required','required');

            $('#manager_zip_code').attr('readonly','readonly');
            $('#manager_area').attr('readonly','readonly');
            $('#manager_state').attr('readonly','readonly');
            $('#manager_county').attr('readonly','readonly');
            $('#manager_country').attr('readonly','readonly');
        }
        else
        {
            $('#manager_city').attr('readonly','readonly');
        }
    });

    $('#state_wise').change(function (){
        var ischecked= $(this).is(':checked');
        if(ischecked)
        {
            $('#manager_state').removeAttr('readonly');
            $('#manager_state').attr('name','manager_id');
            // $('#manager_state').attr('required','required');

            $('#manager_zip_code').attr('readonly','readonly');
            $('#manager_area').attr('readonly','readonly');
            $('#manager_county').attr('readonly','readonly');
            $('#manager_city').attr('readonly','readonly');
            $('#manager_country').attr('readonly','readonly');
        }
        else
        {
            $('#manager_state').attr('readonly','readonly');
        }
    });

    $('#country_wise').change(function (){
        var ischecked= $(this).is(':checked');
        if(ischecked)
        {
            $('#manager_country').removeAttr('readonly');
            $('#manager_country').attr('name','manager_id');
            // $('#manager_country').attr('required');

            $('#manager_zip_code').attr('readonly','readonly');
            $('#manager_area').attr('readonly','readonly');
            $('#manager_county').attr('readonly','readonly');
            $('#manager_state').attr('readonly','readonly');
            $('#manager_city').attr('readonly','readonly');
        }
        else
        {
            $('#manager_country').attr('readonly','readonly');
        }
    });
});


</script>
