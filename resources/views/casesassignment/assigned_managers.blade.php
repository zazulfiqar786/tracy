@extends('layouts.masterlayout')
@section('content')

    <section id="widget-grid" class="" style="border-top:2px solid #CCC; margin-left: 10px; margin-right: 10px">
        <!-- row -->
        <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">

                    <!-- widget div-->
                    <div>
                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                        </div>
                        <!-- end widget edit box -->
                        <!-- widget content -->
                        <div class="widget-body no-padding">
                            <div class="widget-body-toolbar" style="overflow-x:auto;">
                            </div >
                            <table id="datatable_tabletools" class="table table-striped table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th nowrap><center>S#</center></th>
                                    <th nowrap><center>Manager Name</center></th>
                                    <th nowrap><center>Client Name</center></th>
                                    <th nowrap><center>Client Contact</center></th>
                                    <th nowrap><center>Client Email</center></th>
{{--                                    <th nowrap><center>Action</center></th>--}}
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($clientManagers as $clientManager)
                                    <tr>
                                        <td nowrap><center>{{$loop->iteration}}</center></td>
                                        <td nowrap><center>{{$clientManager->staff->name}}</center></td>

                                        @if(isset($clientManager->client->users->name))
                                            <td nowrap>{{$clientManager->client->name}}</td>
                                        @else
                                            <td>NA</td>
                                        @endif

                                        @if(isset($clientManager->client->users->contact))
                                            <td nowrap>{{$clientManager->client->contact}}</td>
                                        @else
                                            <td>NA</td>
                                        @endif


                                        @if(isset($clientManager->client->users->email))
                                            <td nowrap>{{$clientManager->client->users->email}}</td>
                                        @else
                                            <td>NA</td>
                                        @endif
                                        <td nowrap> <center>
{{--                                            <a href="{{route('staff.view',$clientManager->id)}}" class="btn btn-sm btn-primary"  title="View" ><i class="fa fa-eye" aria-hidden="true"></i>--}}
{{--                                            </a>--}}
{{--                                            <a href="{{route('staff.edit',$clientManager->id)}}" class="btn btn-sm btn-primary"  title="Edit">--}}
{{--                                                <i class="fa fa-edit" aria-hidden="true"></i>--}}
{{--                                            </a>--}}
{{--                                            <a href="{{route('staff.delete',$clientManager->id)}}" class="btn btn-sm btn-primary"  title="Delete"  >--}}
{{--                                                <i class="fa fa-user-minus" aria-hidden="true"></i>--}}
{{--                                            </a>--}}
                                        </center>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>

                                </tfoot>
                            </table>
                        </div>
                        <!-- end widget content -->
                    </div>
                    <!-- end widget div -->
                </div>
                <!-- end widget -->
            </article>
        </div>
    </section>

@endsection


<script>
    $('#datatable_tabletools').DataTable({
        dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete : function(oSettings, json) {
            $(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
                $(this).addClass('btn-sm btn-default');
            });
        },
        "language": {
            "search": "Search: ",
            "searchPlaceholder": "Search records"
        },
        "pageLength": 50,
        "bDestroy": true,

        "order": []
    });
    /* END TABLE TOOLS */

    function ReportError(res)
    {
    }// Filter placeholder dataTable
    $(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>
