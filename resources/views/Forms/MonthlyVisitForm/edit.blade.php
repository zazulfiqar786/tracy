@extends('layouts.masterlayout')

@section('content')

    <!-- All CSS -->
    {{--    <link href="{{asset('css/all.css')}}" rel="stylesheet">--}}
    {{--    <!-- slicknav CSS -->--}}
    {{--    <link href="{{asset('css/slicknav.css')}}" rel="stylesheet">--}}
    <!-- Style CSS -->
    <link href="{{asset('css/custom.css')}}" rel="stylesheet">
    <!-- responsive css -->
    <link href="{{asset('css/responsive.css')}}" rel="stylesheet">
    <!-- Google Fonts CSS -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">




    <body class="responsive">

            <form method="POST" action="{{route('monthlyVisitReport.update',$form->id)}}">
                @csrf
                @method('put')

                <input type="hidden" name="client_id" value="{{$form->client_id}}">
                    @php
                    $monthly_contact = $form->monthly_contact;
                    $monthly_contact2 = explode(',', $monthly_contact);
                    @endphp

                    @php
                    $permision_for_tele = $form->permision_for_tele;
                    $permision_for_tele2 = explode(',', $permision_for_tele);
                    @endphp

                    @php
                    $appearence_of_emotional = $form->appearence_of_emotional;
                    $appearence_of_emotional2 = explode(',', $appearence_of_emotional);
                    @endphp

                    @php
                    $monthly_health_provider = $form->monthly_health_provider;
                    $monthly_health_provider2 = explode(',', $monthly_health_provider);
                    @endphp

                    @php
                    $referral_needed = $form->referral_needed;
                    $referral_needed2 = explode(',', $referral_needed);
                    @endphp

                    @php
                    $services_report = $form->services_report;
                    $services_report2 = explode(',', $services_report);
                    @endphp

                    @php
                    $medical = $form->medical;
                    $medical2 = explode(',', $medical);
                    @endphp

                    @php
                    $skin_integrity = $form->skin_integrity;
                    $skin_integrity2 = explode(',', $skin_integrity);
                    @endphp

                    @php
                    $active_wound_care = $form->active_wound_care;
                    $active_wound_care2 = explode(',', $active_wound_care);
                    @endphp

                    @php
                    $decubitus_stage = $form->decubitus_stage;
                    $decubitus_stage2 = explode(',', $decubitus_stage);
                    @endphp

                    @php
                    $uti = $form->uti;
                    $uti2 = explode(',', $uti);
                    @endphp

                    @php
                    $bowel = $form->bowel;
                    $bowel2 = explode(',', $bowel);
                    @endphp

                    @php
                    $bladder = $form->bladder;
                    $bladder2 = explode(',', $bladder);
                    @endphp

                    @php
                    $ambulation = $form->ambulation;
                    $ambulation2 = explode(',', $ambulation);
                    @endphp

                    @php
                    $transfer_assistance = $form->transfer_assistance;
                    $transfer_assistance2 = explode(',', $transfer_assistance);
                    @endphp

                    @php
                    $fall_past = $form->fall_past;
                    $fall_past2 = explode(',', $fall_past);
                    @endphp

                    @php
                    $meal_plan = $form->meal_plan;
                    $meal_plan2 = explode(',', $meal_plan);
                    @endphp



        <div class="allPadding monthlyFrom">
          <div class="topSecform">
            <div class="container">
              <div class="formHead">
                <div class="row">
                  <div class="col-md-8 col-sm-8 col-xs-12 centerCol">
                    <h2>Monthly Visit Form</h2>
                  </div>
                </div>
              </div>
              <div class="formTable">
                <div class="row flexRow">
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Monthly Contact:</h4>
                    </div>
                  </div>
                  <div class="col-md-10 col-sm-10 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox"  @php echo (in_array("1", $monthly_contact2)?'checked':''); @endphp name="monthly_contact[]" value="1">
                        <label for="vehicle1"> In Home</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("2", $monthly_contact2)?'checked':''); @endphp name="monthly_contact[]" value="2">
                        <label for="vehicle2"> Tele Visit</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Date:</h4>
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="Date" value="{{ $form->Date }}">
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>ST/ET:</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="st_et" value="{{ $form->st_et }}">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Total H/M</h4>
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="total_hm" value="{{ $form->total_hm }}">
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Documentation Time:</h4>
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="documentation_time"  value="{{$form->documentation_time}}">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Permission for Tele-Visit obtained from member:</h4>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox"  @php echo (in_array("1", $permision_for_tele2)?'checked':''); @endphp name="permision_for_tele[]" value="1">
                        <label for="vehicle1"> Yes</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("2", $permision_for_tele2)?'checked':''); @endphp name="permision_for_tele[]" value="2">
                        <label for="vehicle2"> No</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Member:</h4>
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="member"  value="{{ $form->member }}">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Verbal:</h4>
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="verbal"  value="{{ $form->verbal }}">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Non Verbal:</h4>
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="nonverbal"   value="{{ $form->nonverbal }}">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Other(s) Present:</h4>
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="other_present"  value="{{ $form->other_present }}">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Appearance of Emotional Health/Mood:</h4>
                    </div>
                  </div>
                  <div class="col-md-9 col-sm-9 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox"  @php echo (in_array("1", $appearence_of_emotional2)?'checked':''); @endphp name="appearence_of_emotional[]" value="1">
                        <label for="vehicle1"> Cooperative</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("2", $appearence_of_emotional2)?'checked':''); @endphp name="appearence_of_emotional[]" value="2">
                        <label for="vehicle2"> Defensive</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("3", $appearence_of_emotional2)?'checked':''); @endphp name="appearence_of_emotional[]" value="3">
                        <label for="vehicle2"> Evasive</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("4", $appearence_of_emotional2)?'checked':''); @endphp name="appearence_of_emotional[]" value="4">
                        <label for="vehicle2"> Sad</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("5", $appearence_of_emotional2)?'checked':''); @endphp name="appearence_of_emotional[]" value="5">
                        <label for="vehicle2"> Easily Distracted</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("6", $appearence_of_emotional2)?'checked':''); @endphp name="appearence_of_emotional[]" value="6">
                        <label for="vehicle2"> Alert</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("7", $appearence_of_emotional2)?'checked':''); @endphp name="appearence_of_emotional[]" value="7">
                        <label for="vehicle2"> Anxiouse</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("8", $appearence_of_emotional2)?'checked':''); @endphp name="appearence_of_emotional[]" value="8">
                        <label for="vehicle2"> Irritable</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("9", $appearence_of_emotional2)?'checked':''); @endphp name="appearence_of_emotional[]" value="9">
                        <label for="vehicle2"> Euphoric</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("10", $appearence_of_emotional2)?'checked':''); @endphp name="appearence_of_emotional[]" value="10">
                        <label for="vehicle2"> Blunted</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("11", $appearence_of_emotional2)?'checked':''); @endphp name="appearence_of_emotional[]" value="11">
                        <label for="vehicle2"> Flat</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("12", $appearence_of_emotional2)?'checked':''); @endphp name="appearence_of_emotional[]" value="12">
                        <label for="vehicle2"> Even</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Self-Reported mood: </h4>
                    </div>
                  </div>
                  <div class="col-md-9 col-sm-9 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="self_reported_mood"  value="{{ $form->self_reported_mood }}">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Monthly Health Provider:  </h4>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox"   @php echo (in_array("1", $monthly_health_provider2)?'checked':''); @endphp  name="monthly_health_provider[]" value="1">
                        <label for="vehicle1"> Yes</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("2", $monthly_health_provider2)?'checked':''); @endphp  name="monthly_health_provider[]" value="2">
                        <label for="vehicle2"> No</label>
                      </li>
                    </ul>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Name/Contact:</h4>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="name_contact"  value="{{ $form->name_contact }}">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Referral Needed:</h4>
                    </div>
                  </div>
                  <div class="col-md-9 col-sm-9 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox"  @php echo (in_array("1", $referral_needed2)?'checked':''); @endphp  name="referral_needed[]" value="1">
                        <label for="vehicle1"> Yes</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("2", $referral_needed2)?'checked':''); @endphp name="referral_needed[]" value="2">
                        <label for="vehicle2"> No</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Member Hygiene/Dress:</h4>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="member_hygiene"  value="{{ $form->member_hygiene }}">
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Smoke Cigarettes?</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="smoke_cigarettes"  value="{{ $form->smoke_cigarettes }}">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Per Day:</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="per_day"  value="{{ $form->per_day }}">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Appearance/Home Enviroment: </h4>
                    </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="appearence_home_envoirnment"  value="{{ $form->appearence_home_envoirnment }}">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-12 col-sm-12 col-xs-12 noPadding">
                    <div class="heading services">
                      <h4><b>Services</b></h4>
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>QR</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="qr"  value="{{ $form->qr }}">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>CP Renewal</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="cp_renewal"   value="{{ $form->cp_renewal }}">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Initial CP:</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="initial_cp"  value="{{ $form->initial_cp }}">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>PSS Agency or CDC:</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="pss_agency"  value="{{ $form->pss_agency }}">
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>No. of attendants:</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="no_of_attendence"  value="{{ $form->no_of_attendence }}">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Schedule:</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="schedule"  value="{{ $form->schedule }}">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-12 col-sm-12 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes monthly">
                      <li>
                        <input type="checkbox"  @php echo (in_array("1", $services_report2)?'checked':''); @endphp  name="services_report[]" value="1">
                        <label for="vehicle1"> CDC Monthly Budget</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("2", $services_report2)?'checked':''); @endphp  name="services_report[]" value="2">
                        <label for="vehicle2"> ERS tested this month?</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("3", $services_report2)?'checked':''); @endphp  name="services_report[]" value="3">
                        <label for="vehicle2"> Medical supplies received?</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("4", $services_report2)?'checked':''); @endphp  name="services_report[]" value="4">
                        <label for="vehicle2"> Day Program</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("5", $services_report2)?'checked':''); @endphp  name="services_report[]" value="5">
                        <label for="vehicle2"> CM</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("6", $services_report2)?'checked':''); @endphp  name="services_report[]" value="6">
                        <label for="vehicle2"> Enhanced CM</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("7", $services_report2)?'checked':''); @endphp  name="services_report[]" value="7">
                        <label for="vehicle2"> PT/OT</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("8", $services_report2)?'checked':''); @endphp  name="services_report[]" value="8">
                        <label for="vehicle2"> Dialysis</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("9", $services_report2)?'checked':''); @endphp  name="services_report[]" value="9">
                        <label for="vehicle2"> Home Modification</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("10", $services_report2)?'checked':''); @endphp  name="services_report[]" value="10">
                        <label for="vehicle2">Respite</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("11", $services_report2)?'checked':''); @endphp  name="services_report[]" value="11">
                        <label for="vehicle2"> DME Supplies Adequate?</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Comments/Problems w/services or providers:</h4>
                    </div>
                  </div>
                  <div class="col-md-9 col-sm-9 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="comments_problems"  value="{{ $form->comments_problems }}">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4><b>Medical:</b></h4>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox"  @php echo (in_array("1", $medical2)?'checked':''); @endphp  name="medical[]" value="1">
                        <label for="vehicle1"> ER</label>
                      </li>
                      <li>
                        <input type="checkbox"   @php echo (in_array("2", $medical2)?'checked':''); @endphp  name="medical[]" value="2">
                        <label for="vehicle2"> Hospital</label>
                      </li>
                    </ul>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Detail:</h4>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="detail"  value="{{ $form->detail }}">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Last PCP/Medical visit(s):</h4>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="last_pcp_medical_visit"  value="{{ $form->last_pcp_medical_visit }}">
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Next PCP/Medical visit(s):</h4>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="next_pcp_medical_visit"   value="{{ $form->next_pcp_medical_visit }}">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                    <div class="heading">
                      <h4><b>Changes:</b> PCP, Medications, Medical Conditions</h4>
                    </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="pcp_medication_conditions"  value="{{ $form->pcp_medication_conditions }}">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Accu Checks</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="accu_checks" value="{{ $form->accu_checks }}">
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Trach</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="trach"  value="{{ $form->trach }}">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Ventilator</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="ventilator"   value="{{ $form->ventilator }}">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Oxygen</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="oxygen"    value="{{ $form->oxygen }}">
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Other</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="other"   value="{{ $form->oxygen }}">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4><b>Skin Integrity</b></h4>
                    </div>
                  </div>
                  <div class="col-md-10 col-sm-10 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox"   @php echo (in_array("1", $skin_integrity2)?'checked':''); @endphp name="skin_integrity[]" value="1">
                        <label for="vehicle1"> Rash</label>
                      </li>
                      <li>
                        <input type="checkbox"   @php echo (in_array("2", $skin_integrity2)?'checked':''); @endphp  name="skin_integrity[]" value="2">
                        <label for="vehicle2"> Wound</label>
                      </li>
                      <li>
                        <input type="checkbox"   @php echo (in_array("3", $skin_integrity2)?'checked':''); @endphp  name="skin_integrity[]" value="3">
                        <label for="vehicle2"> Breakdown</label>
                      </li>
                      <li>
                        <input type="checkbox"   @php echo (in_array("4", $skin_integrity2)?'checked':''); @endphp  name="skin_integrity[]" value="4">
                        <label for="vehicle2"> Redness</label>
                      </li>
                      <li>
                        <input type="checkbox"   @php echo (in_array("5", $skin_integrity2)?'checked':''); @endphp  name="skin_integrity[]" value="5">
                        <label for="vehicle2"> Itching</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4><b>Active Wound Care ?</b></h4>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox"   @php echo (in_array("1", $active_wound_care2)?'checked':''); @endphp  name="active_wound_care[]" value="1">
                        <label for="vehicle1"> Yes</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("2", $active_wound_care2)?'checked':''); @endphp  name="active_wound_care[]" value="2">
                        <label for="vehicle2"> No</label>
                      </li>
                    </ul>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4><b>Decubitus Stage</b></h4>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox"   @php echo (in_array("1", $decubitus_stage2)?'checked':''); @endphp  name="decubitus_stage[]" value="1">
                        <label for="vehicle1"> 1</label>
                      </li>
                      <li>
                        <input type="checkbox"   @php echo (in_array("2", $decubitus_stage2)?'checked':''); @endphp  name="decubitus_stage[]" value="2">
                        <label for="vehicle2"> 2</label>
                      </li>
                      <li>
                        <input type="checkbox"    @php echo (in_array("3", $decubitus_stage2)?'checked':''); @endphp  name="decubitus_stage[]" value="3">
                        <label for="vehicle2"> 3</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-5 col-sm-5 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>How often are you turned/Repositioned?</h4>
                    </div>
                  </div>
                  <div class="col-md-7 col-sm-7 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="how_offen_are_turned" value="{{ $form->how_offen_are_turned }}">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-12 col-sm-12 col-xs-12 noPadding">
                    <div class="heading services">
                      <h4><b>Bowel & Bladder Maintenance:</b></h4>
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>UTI</h4>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox"    @php echo (in_array("1", $uti2)?'checked':''); @endphp  name="uti[]" value="1">
                        <label for="vehicle1"> Yes</label>
                      </li>
                      <li>
                        <input type="checkbox"    @php echo (in_array("2", $uti2)?'checked':''); @endphp   name="uti[]" value="2">
                        <label for="vehicle2"> No</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="heading bowel">
                      <h4>Bowel:</h4>
                    </div>
                  </div>
                  <div class="col-md-11 col-sm-11 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox"  @php echo (in_array("1", $bowel2)?'checked':''); @endphp name="bowel[]" value="1">
                        <label for="vehicle1"> Continent</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("2", $bowel2)?'checked':''); @endphp  name="bowel[]" value="2">
                        <label for="vehicle2"> Incontinent</label>
                      </li>
                      <li>
                        <input type="checkbox"   @php echo (in_array("3", $bowel2)?'checked':''); @endphp  name="bowel[]" value="3">
                        <label for="vehicle1"> Colostomy</label>
                      </li>
                      <li>
                        <input type="checkbox"   @php echo (in_array("4", $bowel2)?'checked':''); @endphp  name="bowel[]" value="4">
                        <label for="vehicle2"> Bowel Program Stimulation</label>
                      </li>
                      <li>
                        <input type="checkbox"   @php echo (in_array("5", $bowel2)?'checked':''); @endphp  name="bowel[]" value="5">
                        <label for="vehicle2"> Suppository</label>
                      </li>
                      <li>
                        <input type="checkbox"   @php echo (in_array("6", $bowel2)?'checked':''); @endphp  name="bowel[]" value="6">
                        <label for="vehicle2">Constipated</label>
                      </li>
                      <li>
                        <input type="checkbox"   @php echo (in_array("7", $bowel2)?'checked':''); @endphp  name="bowel[]" value="7">
                        <label for="vehicle2">Diarrhea</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Bladder:</h4>
                    </div>
                  </div>
                  <div class="col-md-10 col-sm-10 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox"   @php echo (in_array("1", $bladder2)?'checked':''); @endphp  name="bladder[]" value="1">
                        <label for="vehicle1"> Continent</label>
                      </li>
                      <li>
                        <input type="checkbox"    @php echo (in_array("2", $bladder2)?'checked':''); @endphp name="bladder[]" value="2">
                        <label for="vehicle2"> Incontinent</label>
                      </li>
                      <li>
                        <input type="checkbox"    @php echo (in_array("3", $bladder2)?'checked':''); @endphp  name="bladder[]" value="3">
                        <label for="vehicle1"> Catheter</label>
                      </li>
                      <li>
                        <input type="checkbox"    @php echo (in_array("4", $bladder2)?'checked':''); @endphp  name="bladder[]" value="4">
                        <label for="vehicle2"> Excessive Flow</label>
                      </li>
                      <li>
                        <input type="checkbox"    @php echo (in_array("5", $bladder2)?'checked':''); @endphp  name="bladder[]" value="5">
                        <label for="vehicle2"> Weak Stream</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Ambulation:</h4>
                    </div>
                  </div>
                  <div class="col-md-10 col-sm-10 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox"   @php echo (in_array("1", $ambulation2)?'checked':''); @endphp  name="ambulation[]" value="1">
                        <label for="vehicle1"> None</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("2", $ambulation2)?'checked':''); @endphp  name="ambulation[]" value="2">
                        <label for="vehicle2"> Supervision</label>
                      </li>
                      <li>
                        <input type="checkbox"  @php echo (in_array("3", $ambulation2)?'checked':''); @endphp name="ambulation[]" value="3">
                        <label for="vehicle1"> Cane</label>
                      </li>
                      <li>
                        <input type="checkbox"   @php echo (in_array("4", $ambulation2)?'checked':''); @endphp  name="ambulation[]" value="4">
                        <label for="vehicle2"> Walker</label>
                      </li>
                      <li>
                        <input type="checkbox"   @php echo (in_array("5", $ambulation2)?'checked':''); @endphp  name="ambulation[]" value="5">
                        <label for="vehicle2"> Wheelchair</label>
                      </li>
                      <li>
                        <input type="checkbox"   @php echo (in_array("6", $ambulation2)?'checked':''); @endphp  name="ambulation[]" value="6">
                        <label for="vehicle2">Electric Wheelchair</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Transfer Assistance:</h4>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox"   @php echo (in_array("1", $transfer_assistance2)?'checked':''); @endphp  name="transfer_assistance[]" value="1">
                        <label for="vehicle1"> Yes</label>
                      </li>
                      <li>
                        <input type="checkbox"   @php echo (in_array("2", $transfer_assistance2)?'checked':''); @endphp  name="transfer_assistance[]" value="2">
                        <label for="vehicle1"> No</label>
                      </li>
                      <li>
                        <input type="checkbox"   @php echo (in_array("3", $transfer_assistance2)?'checked':''); @endphp  name="transfer_assistance[]" value="3">
                        <label for="vehicle2"> Supervision</label>
                      </li>
                    </ul>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>No. of hrs. out of bed daily:</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="no_of_hrs" value="{{ $form->no_of_hrs }}">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Fall Past 30 Days:</h4>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox"   @php echo (in_array("1", $fall_past2)?'checked':''); @endphp  name="fall_past[]" value="1">
                        <label for="vehicle1"> Yes</label>
                      </li>
                      <li>
                        <input type="checkbox"    @php echo (in_array("2", $fall_past2)?'checked':''); @endphp   name="fall_past[]" value="2">
                        <label for="vehicle1"> No</label>
                      </li>
                    </ul>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Identify risk factor(s):</h4>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="identify_risk_factor"  value="{{ $form->identify_risk_factor }}">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-12 col-sm-12 col-xs-12 noPadding">
                    <div class="heading services">
                      <h4><b>Meals Per Day - Meal plan:</b></h4>
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-12 col-sm-12 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes mealList">
                      <li>
                        <input type="checkbox"    @php echo (in_array("1", $meal_plan2)?'checked':''); @endphp  name="meal_plan[]" value="1">
                        <label for="vehicle1"> None</label>
                      </li>
                      <li>
                        <input type="checkbox"    @php echo (in_array("2", $meal_plan2)?'checked':''); @endphp name="meal_plan[]" value="2">
                        <label for="vehicle1"> Diabetic</label>
                      </li>
                      <li>
                        <input type="checkbox"    @php echo (in_array("3", $meal_plan2)?'checked':''); @endphp  name="meal_plan[]" value="3">
                        <label for="vehicle1"> No/Low Salt</label>
                      </li>
                      <li>
                        <input type="checkbox"    @php echo (in_array("4", $meal_plan2)?'checked':''); @endphp  name="meal_plan[]" value="4">
                        <label for="vehicle1"> No/Low Fat</label>
                      </li>
                      <li>
                        <input type="checkbox"    @php echo (in_array("5", $meal_plan2)?'checked':''); @endphp  name="meal_plan[]" value="5">
                        <label for="vehicle1">Liquid</label>
                      </li>
                      <li>
                        <input type="checkbox"    @php echo (in_array("6", $meal_plan2)?'checked':''); @endphp  name="meal_plan[]" value="6">
                        <label for="vehicle1">G-Tube Choking Risk</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Social Activities/Work:</h4>
                    </div>
                  </div>
                  <div class="col-md-9 col-sm-9 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="social_activities"  value="{{ $form->social_activities }}">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading summary">
                      <h4>Summar/Follow Up</h4>
                    </div>
                  </div>
                  <div class="col-md-9 col-sm-9 col-xs-12 noPadding">
                    <div class="putData">
                      <textarea rows="5" name="summar_followup"></textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="signSec">
                <div class="row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="heading">
                      <h4>Member/Authorized Representative:</h4>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="putData">
                      <input type="text"  name="member_authorized"   value="{{ $form->member_authorized }}">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12">
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12">
                    <div class="heading">
                      <h4>Date:</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12">
                    <div class="putData">
                      <input type="text"  name="date_authorized"  value="{{ $form->date_authorized }}">
                    </div>
                  </div>
                </div>
              </div>
              <div class="signSec">
                <div class="row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="heading">
                      <h4>Case Maneger/Date:</h4>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="putData">
                      <input type="text"  name="case_manager_date"  value="{{ $form->case_manager_date }}">
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12">
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12">
                    <div class="heading">
                      <h4>Owner/Date:</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12">
                    <div class="putData">
                      <input type="text"  name="owner_date"  value="{{ $form->owner_date }}">
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>

        </div>
        <center> <input type="submit" value="Save" name="btnSubmit" class="btn btn-success"></center>

    </form>
      </body>


    <!-- All JS -->
    <script src="{{asset('js/all.js')}}"></script>
    <!-- jquery.slicknav JS -->
    <script src="{{asset('js/jquery.slicknav.js')}}"></script>
    <!-- Custom JS -->
    <script src="{{asset('js/custom.js')}}"></script>

@endsection
