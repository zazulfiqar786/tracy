@extends('layouts.masterlayout')

@section('content')

    <!-- All CSS -->
    {{--    <link href="{{asset('css/all.css')}}" rel="stylesheet">--}}
    {{--    <!-- slicknav CSS -->--}}
    {{--    <link href="{{asset('css/slicknav.css')}}" rel="stylesheet">--}}
    <!-- Style CSS -->
    <link href="{{asset('css/custom.css')}}" rel="stylesheet">
    <!-- responsive css -->
    <link href="{{asset('css/responsive.css')}}" rel="stylesheet">
    <!-- Google Fonts CSS -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

    <body class="responsive">
        <form method="POST" action="{{route('monthlyVisitReport.store')}}">
            @csrf
            @method('post')
            <input type="hidden" name="client_id" value="{{$clientId}}">
        <div class="allPadding monthlyFrom">
          <div class="topSecform">
            <div class="container">
              <div class="formHead">
                <div class="row">
                  <div class="col-md-8 col-sm-8 col-xs-12 centerCol">
                    <h2>Monthly Visit Form</h2>
                  </div>
                </div>
              </div>
              <div class="formTable">
                <div class="row flexRow">
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Monthly Contact:</h4>
                    </div>
                  </div>
                  <div class="col-md-10 col-sm-10 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox" name="monthly_contact[]" value="1">
                        <label for="vehicle1"> In Home</label>
                      </li>
                      <li>
                        <input type="checkbox" name="monthly_contact[]" value="2">
                        <label for="vehicle2"> Tele Visit</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Date:</h4>
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="Date">
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>ST/ET:</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="st_et">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Total H/M</h4>
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="total_hm">
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Documentation Time:</h4>
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="documentation_time">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Permission for Tele-Visit obtained from member:</h4>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox" name="permision_for_tele[]" value="1">
                        <label for="vehicle1"> Yes</label>
                      </li>
                      <li>
                        <input type="checkbox" name="permision_for_tele[]" value="2">
                        <label for="vehicle2"> No</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Member:</h4>
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="member">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Verbal:</h4>
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="verbal">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Non Verbal:</h4>
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="nonverbal">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Other(s) Present:</h4>
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="other_present">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Appearance of Emotional Health/Mood:</h4>
                    </div>
                  </div>
                  <div class="col-md-9 col-sm-9 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox" name="appearence_of_emotional[]" value="1">
                        <label for="vehicle1"> Cooperative</label>
                      </li>
                      <li>
                        <input type="checkbox" name="appearence_of_emotional[]" value="2">
                        <label for="vehicle2"> Defensive</label>
                      </li>
                      <li>
                        <input type="checkbox" name="appearence_of_emotional[]" value="3">
                        <label for="vehicle2"> Evasive</label>
                      </li>
                      <li>
                        <input type="checkbox" name="appearence_of_emotional[]" value="4">
                        <label for="vehicle2"> Sad</label>
                      </li>
                      <li>
                        <input type="checkbox" name="appearence_of_emotional[]" value="5">
                        <label for="vehicle2"> Easily Distracted</label>
                      </li>
                      <li>
                        <input type="checkbox" name="appearence_of_emotional[]" value="6">
                        <label for="vehicle2"> Alert</label>
                      </li>
                      <li>
                        <input type="checkbox" name="appearence_of_emotional[]" value="7">
                        <label for="vehicle2"> Anxiouse</label>
                      </li>
                      <li>
                        <input type="checkbox" name="appearence_of_emotional[]" value="8">
                        <label for="vehicle2"> Irritable</label>
                      </li>
                      <li>
                        <input type="checkbox" name="appearence_of_emotional[]" value="9">
                        <label for="vehicle2"> Euphoric</label>
                      </li>
                      <li>
                        <input type="checkbox" name="appearence_of_emotional[]" value="10">
                        <label for="vehicle2"> Blunted</label>
                      </li>
                      <li>
                        <input type="checkbox" name="appearence_of_emotional[]" value="11">
                        <label for="vehicle2"> Flat</label>
                      </li>
                      <li>
                        <input type="checkbox" name="appearence_of_emotional[]" value="12">
                        <label for="vehicle2"> Even</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Self-Reported mood: </h4>
                    </div>
                  </div>
                  <div class="col-md-9 col-sm-9 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="self_reported_mood">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Monthly Health Provider:  </h4>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox" name="monthly_health_provider[]" value="1">
                        <label for="vehicle1"> Yes</label>
                      </li>
                      <li>
                        <input type="checkbox" name="monthly_health_provider[]" value="2">
                        <label for="vehicle2"> No</label>
                      </li>
                    </ul>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Name/Contact:</h4>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="name_contact">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Referral Needed:</h4>
                    </div>
                  </div>
                  <div class="col-md-9 col-sm-9 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox" name="referral_needed[]" value="1">
                        <label for="vehicle1"> Yes</label>
                      </li>
                      <li>
                        <input type="checkbox" name="referral_needed[]" value="2">
                        <label for="vehicle2"> No</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Member Hygiene/Dress:</h4>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="member_hygiene">
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Smoke Cigarettes?</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="smoke_cigarettes">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Per Day:</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="per_day">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Appearance/Home Enviroment: </h4>
                    </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="appearence_home_envoirnment">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-12 col-sm-12 col-xs-12 noPadding">
                    <div class="heading services">
                      <h4><b>Services</b></h4>
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>QR</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="qr">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>CP Renewal</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="cp_renewal">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Initial CP:</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="initial_cp">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>PSS Agency or CDC:</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="pss_agency">
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>No. of attendants:</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="no_of_attendence">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Schedule:</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="schedule">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-12 col-sm-12 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes monthly">
                      <li>
                        <input type="checkbox" name="services_report[]" value="1">
                        <label for="vehicle1"> CDC Monthly Budget</label>
                      </li>
                      <li>
                        <input type="checkbox" name="services_report[]" value="2">
                        <label for="vehicle2"> ERS tested this month?</label>
                      </li>
                      <li>
                        <input type="checkbox" name="services_report[]" value="3">
                        <label for="vehicle2"> Medical supplies received?</label>
                      </li>
                      <li>
                        <input type="checkbox" name="services_report[]" value="4">
                        <label for="vehicle2"> Day Program</label>
                      </li>
                      <li>
                        <input type="checkbox" name="services_report[]" value="5">
                        <label for="vehicle2"> CM</label>
                      </li>
                      <li>
                        <input type="checkbox" name="services_report[]" value="6">
                        <label for="vehicle2"> Enhanced CM</label>
                      </li>
                      <li>
                        <input type="checkbox" name="services_report[]" value="7">
                        <label for="vehicle2"> PT/OT</label>
                      </li>
                      <li>
                        <input type="checkbox" name="services_report[]" value="8">
                        <label for="vehicle2"> Dialysis</label>
                      </li>
                      <li>
                        <input type="checkbox" name="services_report[]" value="9">
                        <label for="vehicle2"> Home Modification</label>
                      </li>
                      <li>
                        <input type="checkbox" name="services_report[]" value="10">
                        <label for="vehicle2">Respite</label>
                      </li>
                      <li>
                        <input type="checkbox" name="services_report[]" value="11">
                        <label for="vehicle2"> DME Supplies Adequate?</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Comments/Problems w/services or providers:</h4>
                    </div>
                  </div>
                  <div class="col-md-9 col-sm-9 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="comments_problems">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4><b>Medical:</b></h4>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox" name="medical[]" value="1">
                        <label for="vehicle1"> ER</label>
                      </li>
                      <li>
                        <input type="checkbox" name="medical[]" value="2">
                        <label for="vehicle2"> Hospital</label>
                      </li>
                    </ul>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Detail:</h4>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="detail">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Last PCP/Medical visit(s):</h4>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="last_pcp_medical_visit">
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Next PCP/Medical visit(s):</h4>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="next_pcp_medical_visit">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                    <div class="heading">
                      <h4><b>Changes:</b> PCP, Medications, Medical Conditions</h4>
                    </div>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="pcp_medication_conditions">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Accu Checks</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="accu_checks">
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Trach</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="trach">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Ventilator</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="ventilator">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Oxygen</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="oxygen">
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Other</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="other">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4><b>Skin Integrity</b></h4>
                    </div>
                  </div>
                  <div class="col-md-10 col-sm-10 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox" name="skin_integrity[]" value="1">
                        <label for="vehicle1"> Rash</label>
                      </li>
                      <li>
                        <input type="checkbox" name="skin_integrity[]" value="2">
                        <label for="vehicle2"> Wound</label>
                      </li>
                      <li>
                        <input type="checkbox"  name="skin_integrity[]" value="3">
                        <label for="vehicle2"> Breakdown</label>
                      </li>
                      <li>
                        <input type="checkbox"  name="skin_integrity[]" value="4">
                        <label for="vehicle2"> Redness</label>
                      </li>
                      <li>
                        <input type="checkbox"  name="skin_integrity[]" value="5">
                        <label for="vehicle2"> Itching</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4><b>Active Wound Care ?</b></h4>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox" name="active_wound_care[]" value="1">
                        <label for="vehicle1"> Yes</label>
                      </li>
                      <li>
                        <input type="checkbox" name="active_wound_care[]" value="2">
                        <label for="vehicle2"> No</label>
                      </li>
                    </ul>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4><b>Decubitus Stage</b></h4>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox" name="decubitus_stage[]" value="1">
                        <label for="vehicle1"> 1</label>
                      </li>
                      <li>
                        <input type="checkbox" name="decubitus_stage[]" value="2">
                        <label for="vehicle2"> 2</label>
                      </li>
                      <li>
                        <input type="checkbox" name="decubitus_stage[]" value="3">
                        <label for="vehicle2"> 3</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-5 col-sm-5 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>How often are you turned/Repositioned?</h4>
                    </div>
                  </div>
                  <div class="col-md-7 col-sm-7 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="how_offen_are_turned">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-12 col-sm-12 col-xs-12 noPadding">
                    <div class="heading services">
                      <h4><b>Bowel & Bladder Maintenance:</b></h4>
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>UTI</h4>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox" name="uti[]" value="1">
                        <label for="vehicle1"> Yes</label>
                      </li>
                      <li>
                        <input type="checkbox" name="uti[]" value="2">
                        <label for="vehicle2"> No</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="heading bowel">
                      <h4>Bowel:</h4>
                    </div>
                  </div>
                  <div class="col-md-11 col-sm-11 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox" name="bowel[]" value="1">
                        <label for="vehicle1"> Continent</label>
                      </li>
                      <li>
                        <input type="checkbox" name="bowel[]" value="2">
                        <label for="vehicle2"> Incontinent</label>
                      </li>
                      <li>
                        <input type="checkbox" name="bowel[]" value="3">
                        <label for="vehicle1"> Colostomy</label>
                      </li>
                      <li>
                        <input type="checkbox" name="bowel[]" value="4">
                        <label for="vehicle2"> Bowel Program Stimulation</label>
                      </li>
                      <li>
                        <input type="checkbox" name="bowel[]" value="5">
                        <label for="vehicle2"> Suppository</label>
                      </li>
                      <li>
                        <input type="checkbox" name="bowel[]" value="6">
                        <label for="vehicle2">Constipated</label>
                      </li>
                      <li>
                        <input type="checkbox" name="bowel[]" value="7">
                        <label for="vehicle2">Diarrhea</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Bladder:</h4>
                    </div>
                  </div>
                  <div class="col-md-10 col-sm-10 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox" name="bladder[]" value="1">
                        <label for="vehicle1"> Continent</label>
                      </li>
                      <li>
                        <input type="checkbox" name="bladder[]" value="2">
                        <label for="vehicle2"> Incontinent</label>
                      </li>
                      <li>
                        <input type="checkbox" name="bladder[]" value="3">
                        <label for="vehicle1"> Catheter</label>
                      </li>
                      <li>
                        <input type="checkbox" name="bladder[]" value="4">
                        <label for="vehicle2"> Excessive Flow</label>
                      </li>
                      <li>
                        <input type="checkbox" name="bladder[]" value="5">
                        <label for="vehicle2"> Weak Stream</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Ambulation:</h4>
                    </div>
                  </div>
                  <div class="col-md-10 col-sm-10 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox" name="ambulation[]" value="1">
                        <label for="vehicle1"> None</label>
                      </li>
                      <li>
                        <input type="checkbox" name="ambulation[]" value="2">
                        <label for="vehicle2"> Supervision</label>
                      </li>
                      <li>
                        <input type="checkbox" name="ambulation[]" value="3">
                        <label for="vehicle1"> Cane</label>
                      </li>
                      <li>
                        <input type="checkbox" name="ambulation[]" value="4">
                        <label for="vehicle2"> Walker</label>
                      </li>
                      <li>
                        <input type="checkbox" name="ambulation[]" value="5">
                        <label for="vehicle2"> Wheelchair</label>
                      </li>
                      <li>
                        <input type="checkbox" name="ambulation[]" value="6">
                        <label for="vehicle2">Electric Wheelchair</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Transfer Assistance:</h4>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox" name="transfer_assistance[]" value="1">
                        <label for="vehicle1"> Yes</label>
                      </li>
                      <li>
                        <input type="checkbox" name="transfer_assistance[]" value="2">
                        <label for="vehicle1"> No</label>
                      </li>
                      <li>
                        <input type="checkbox" name="transfer_assistance[]" value="3">
                        <label for="vehicle2"> Supervision</label>
                      </li>
                    </ul>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>No. of hrs. out of bed daily:</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="no_of_hrs">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Fall Past 30 Days:</h4>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox" name="fall_past[]" value="1">
                        <label for="vehicle1"> Yes</label>
                      </li>
                      <li>
                        <input type="checkbox" name="fall_past[]" value="2">
                        <label for="vehicle1"> No</label>
                      </li>
                    </ul>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Identify risk factor(s):</h4>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="identify_risk_factor">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-12 col-sm-12 col-xs-12 noPadding">
                    <div class="heading services">
                      <h4><b>Meals Per Day - Meal plan:</b></h4>
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-12 col-sm-12 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes mealList">
                      <li>
                        <input type="checkbox" name="meal_plan[]" value="1">
                        <label for="vehicle1"> None</label>
                      </li>
                      <li>
                        <input type="checkbox" name="meal_plan[]" value="2">
                        <label for="vehicle1"> Diabetic</label>
                      </li>
                      <li>
                        <input type="checkbox" name="meal_plan[]" value="3">
                        <label for="vehicle1"> No/Low Salt</label>
                      </li>
                      <li>
                        <input type="checkbox" name="meal_plan[]" value="4">
                        <label for="vehicle1"> No/Low Fat</label>
                      </li>
                      <li>
                        <input type="checkbox" name="meal_plan[]" value="5">
                        <label for="vehicle1">Liquid</label>
                      </li>
                      <li>
                        <input type="checkbox" name="meal_plan[]" value="6">
                        <label for="vehicle1">G-Tube Choking Risk</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Social Activities/Work:</h4>
                    </div>
                  </div>
                  <div class="col-md-9 col-sm-9 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="social_activities">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading summary">
                      <h4>Summar/Follow Up</h4>
                    </div>
                  </div>
                  <div class="col-md-9 col-sm-9 col-xs-12 noPadding">
                    <div class="putData">
                      <textarea rows="5" name="summar_followup"></textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="signSec">
                <div class="row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="heading">
                      <h4>Member/Authorized Representative:</h4>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="putData">
                      <input type="text" name="member_authorized">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12">
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12">
                    <div class="heading">
                      <h4>Date:</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12">
                    <div class="putData">
                      <input type="text" name="date_authorized">
                    </div>
                  </div>
                </div>
              </div>
              <div class="signSec">
                <div class="row">
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="heading">
                      <h4>Case Maneger/Date:</h4>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="putData">
                      <input type="text" name="case_manager_date">
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12">
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12">
                    <div class="heading">
                      <h4>Owner/Date:</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12">
                    <div class="putData">
                      <input type="text" name="owner_date">
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>

        </div>
        <center> <input type="submit" value="Save" name="btnSubmit" class="btn btn-success"></center>
    </form>
      </body>


    <!-- All JS -->
    <script src="{{asset('js/all.js')}}"></script>
    <!-- jquery.slicknav JS -->
    <script src="{{asset('js/jquery.slicknav.js')}}"></script>
    <!-- Custom JS -->
    <script src="{{asset('js/custom.js')}}"></script>

@endsection
