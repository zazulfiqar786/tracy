@extends('layouts.masterlayout')
@section('content')
<style>
    .tiles {
    margin-top: 20px;

}
@media (max-width: 992px) and (min-width: 768px) {
    .tiles {
        margin-top: 50px;
   }
}
@media (max-width: 768px) {
    .tiles {
        padding: 0 15px;
        margin-top: 60px;
   }
}
.tiles h2 {
    margin-bottom: 50px;
}
@media (max-width: 992px) {
    .tiles h2 {
        margin-bottom: 40px;
   }
}
.tiles .link-tiles {
    margin-bottom: 20px;
    background: #385fcf30;
    height: 137px;
}
@media (max-width: 992px) and (min-width: 768px) {
    .tiles .link-tiles {
        height: 100px;
   }
}
@media (max-width: 768px) {
    .tiles .link-tiles {
        height: 90px;
   }
}
.tiles .link-tiles a {
    display: inline-flex;
    width: 100%;
    height: 100%;
    align-items: center;
    position: relative;
    color: #111;
    font-weight: 500;
    font-size: 17px;
    line-height: 20px;
    padding-left: 40px;
    text-transform: uppercase;
}
@media (max-width: 992px) and (min-width: 768px) {
    .tiles .link-tiles a {
        padding-left: 15px;
        font-size: 15px;
        line-height: 18px;
   }
}
@media (max-width: 768px) {
    .tiles .link-tiles a {
        font-size: 14px;
        line-height: 18px;
        padding-left: 15px;
   }
}
@media (max-width: 350px) {
    .tiles .link-tiles a {
        font-size: 12px;
        line-height: 14px;
   }
}
.link-tiles a:after {

}
.link-tiles:hover {
    background: #3f65d4;
    transition: all, 0.4s, ease;
}
.link-tiles:hover a {
    color: #f1f1f1;
}
@media (max-width: 992px) and (min-width: 768px) {
    .tiles .link-tiles a:after {
        right: 15px;
        font-size: 30px;
   }
}
@media (max-width: 768px) {
    .link-tiles a:after {
        right: 10px;
        font-size: 30px;
   }
}
.link-tiles:hover a {
    color: #f1f1f1;
}
</style>

<section class="tiles">
	<div class="container">
		<div class="row">
			<h2>Forms</h2>
			<div class="row">
				<div class="col-md-3 col-sm-4">
					<div class="link-tiles"><a href="{{route('my.client.forms',$clientId)}}">Appendix-D-5<br>
                            </a>
					</div>
					<!--.link-tiles-->
				</div>
				<div class="col-md-3 col-sm-4">
					<div class="link-tiles"><a href="{{route('Appendixmy.client.forms',$clientId)}}">Appendix-K-1<br>
                            </a>
					</div>
					<!--.link-tiles-->
				</div>
				<div class="col-md-3 col-sm-4">
					<div class="link-tiles"><a href="{{route('client.change.form',$clientId)}}">Client Change <br>
                        Form  </a>
					</div>
					<!--.link-tiles-->
				</div>
				<div class="col-md-3 col-sm-4">
					<div class="link-tiles"><a href="{{route('monthlyVisitReport.forms',$clientId)}}">Monthly <br>
                        Visit Form</a>
					</div>
					<!--.link-tiles-->
				</div>
				<div class="col-md-3 col-sm-4">
					<div class="link-tiles"><a href="{{route('monthlyContactReport.forms',$clientId)}}">Monthly <br>
                        Contact Form</a>
					</div>
					<!--.link-tiles-->
				</div>
				<div class="col-md-3 col-sm-4">
					<div class="link-tiles"><a href="{{route('followup.forms',$clientId)}}">Follow-<br>
                        Up<br>
                        </a>
					</div>
					<!--.link-tiles-->
				</div>

                <div class="col-md-3 col-sm-4">
					<div class="link-tiles">
                        <a href="{{route('careform.forms',$clientId)}}">

                        Care Form<br>
                        <br>
                        </a>
					</div>
					<!--.link-tiles-->
				</div>


			</div>
		</div>
		<!--.row-->
	</div>
	<!--.container-->


@endsection
