@extends('layouts.masterlayout')
@section('content')

    <!-- All CSS -->
{{--    <link href="{{asset('css/all.css')}}" rel="stylesheet">--}}
{{--    <!-- slicknav CSS -->--}}
{{--    <link href="{{asset('css/slicknav.css')}}" rel="stylesheet">--}}
    <!-- Style CSS -->
    <link href="{{asset('css/custom.css')}}" rel="stylesheet">
    <!-- responsive css -->
    <link href="{{asset('css/responsive.css')}}" rel="stylesheet">
    <!-- Google Fonts CSS -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">


<div class="allPadding">
    <div class="topSecform">
        <div class="container">
            <div class="formHead">
                <div class="row">
                    <div class="col-md-8 col-sm-8 col-xs-12 centerCol">
                        <h2>Appendix D-5</h2>
                        <h6>INDEPENDENT CARE WAIVER ADDRESS STATUS FORM <br> COMPLETE WITH ANNUAL REVIEW AND/OR CHANGE OF ADDRESS</h6>
                    </div>
                </div>
            </div>
            <form method="POST" action="{{route('AppendixD5.store')}}">
                @csrf
                @method('post')
                <input type="hidden" name="client_id" value="{{$clientId}}">
            <div class="formTable">
                <div class="row flexRow">
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="heading">
                            <h4>Member’s Name:</h4>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                        <div class="putData">
                            <input type="text" name="member_name">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="heading">
                            <h4>Medicaid ID:  </h4>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                        <div class="putData">
                            <input type="text" name="medicaid_id">
                        </div>
                    </div>
                </div>
                <div class="row flexRow">
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="heading">
                            <h4>Date Completed: </h4>
                        </div>
                    </div>
                    <div class="col-md-10 col-sm-9 col-xs-12 noPadding">
                        <div class="putData">
                            <input type="text" name="date_completed">
                        </div>
                    </div>
                </div>
                <div class="row flexRow">
                    <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                        <div class="heading">
                            <h4>Current Street Address: </h4>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 noPadding">
                        <div class="putData">
                            <input type="text" name="current_street_address">
                        </div>
                    </div>
                </div>
                <div class="row flexRow">
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="heading">
                            <h4>Apt. No:  </h4>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 noPadding">
                        <div class="putData">
                            <input type="text" name="current_appointment_no">
                        </div>
                    </div>
                </div>
                <div class="row flexRow">
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="heading">
                            <h4>City: </h4>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="putData">
                            <input type="text" name="current_city">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="heading">
                            <h4>State:  </h4>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="putData">
                            <input type="text" name="current_state">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="heading">
                            <h4>Zip: </h4>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="putData">
                            <input type="text" name="current_zip">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="heading">
                            <h4>Country:</h4>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="putData">
                            <input type="text" name="current_country">
                        </div>
                    </div>
                </div>
                <div class="row flexRow">
                    <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                        <div class="heading">
                            <h4>Previous Street Address: </h4>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 noPadding">
                        <div class="putData">
                            <input type="text" name="previous_street_address">
                        </div>
                    </div>
                </div>
                <div class="row flexRow">
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="heading">
                            <h4>Apt. No:  </h4>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 noPadding">
                        <div class="putData">
                            <input type="text" name="previous_appointment_no">
                        </div>
                    </div>
                </div>
                <div class="row flexRow">
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="heading">
                            <h4>City: </h4>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="putData">
                            <input type="text" name="previous_city">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="heading">
                            <h4>State:  </h4>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="putData">
                            <input type="text" name="previous_state">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="heading">
                            <h4>Zip: </h4>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="putData">
                            <input type="text" name="previous_zip">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="heading">
                            <h4>Country:</h4>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="putData">
                            <input type="text" name="previous_country">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="midSecForm">
        <div class="container">
            <div class="formHead">
                <div class="row">
                    <div class="col-md-8 col-sm-8 col-xs-12 centerCol">
                        <h2>Living Setting</h2>
                    </div>
                </div>
            </div>
            <div class="formRadioBtn">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                        <div class="radioBtn">
                            <input type="radio" id="private" name="living_setting" value="1">
                            <label for="male">Private Home:</label>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                        <div class="radioBtn">
                            <input type="radio" id="assistedLiving" name="living_setting" value="2">
                            <label for="male">Assisted Living:</label>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                        <div class="radioBtn">
                            <input type="radio" id="male" name="living_setting" value="3">
                            <label for="male">Licensed PCH:</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                        <div class="radioBtn">
                            <input type="radio" id="male" name="living_setting" value="4">
                            <label for="male">ALS:</label>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                        <div class="radioBtn">
                            <input type="radio" id="male" name="living_setting" value="5">
                            <label for="male">Group Home:</label>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                        <div class="radioBtn">
                            <input type="radio" id="male" name="living_setting" value="6">
                            <label for="male">Other:</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottomSecform">
        <div class="container">
            <div class="formHead">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 centerCol">
                        <h6>Member’s support system, emergency contact or responsible party information</h6>
                    </div>
                </div>
            </div>
            <div class="formTable">
                <div class="row flexRow">
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="heading">
                            <h4>Name:</h4>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                        <div class="putData">
                            <input type="text" name="relative_name">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                        <div class="heading">
                            <h4>Relationship to Member:</h4>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="putData">
                            <input type="text" name="relative_relationship_to_member">
                        </div>
                    </div>
                </div>
                <div class="row flexRow">
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="heading">
                            <h4>Phone:</h4>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                        <div class="putData">
                            <input type="number" name="relative_phone">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="heading">
                            <h4>Alternate Phone:</h4>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                        <div class="putData">
                            <input type="number" name="relative_alternate_phone">
                        </div>
                    </div>
                </div>
                <div class="row flexRow">
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="heading">
                            <h4>Street address:</h4>
                        </div>
                    </div>
                    <div class="col-md-10 col-sm-10 col-xs-12 noPadding">
                        <div class="putData">
                            <input type="text" name="relative_street_address">
                        </div>
                    </div>
                </div>
                <div class="row flexRow">
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="heading">
                            <h4>City:</h4>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="putData">
                            <input type="text" name="relative_city">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="heading">
                            <h4>Zip:</h4>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="putData">
                            <input type="text" name="relative_zip">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="heading">
                            <h4>Country:</h4>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                        <div class="putData">
                            <input type="text" name="relative_country">
                        </div>
                    </div>

                </div>
            </div>
                  <center> <input type="submit" value="Save" name="btnSubmit" class="btn btn-success"></center>
            </form>
        </div>
    </div>
</div>


<!-- All JS -->
<script src="{{asset('js/all.js')}}"></script>
<!-- jquery.slicknav JS -->
<script src="{{asset('js/jquery.slicknav.js')}}"></script>
<!-- Custom JS -->
<script src="{{asset('js/custom.js')}}"></script>

@endsection
