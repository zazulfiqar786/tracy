@extends('layouts.masterlayout')

@section('content')

    <!-- All CSS -->
    {{--    <link href="{{asset('css/all.css')}}" rel="stylesheet">--}}
    {{--    <!-- slicknav CSS -->--}}
    {{--    <link href="{{asset('css/slicknav.css')}}" rel="stylesheet">--}}
    <!-- Style CSS -->
    <link href="{{asset('css/custom.css')}}" rel="stylesheet">
    <!-- responsive css -->
    <link href="{{asset('css/responsive.css')}}" rel="stylesheet">
    <!-- Google Fonts CSS -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">





   @php
        $myString = $form->status;
        $status = explode(',', $myString);
    @endphp

@php
    $discharge_reasons1 = $form->discharge_reasons;
    $discharge_reasons = explode(',', $discharge_reasons1);
@endphp

@php
    $transfer_reasons1 = $form->transfer_reasons;
    $transfer_reasons = explode(',', $transfer_reasons1);
@endphp





    <div class="allPadding">
        <div class="topSecform">
            <div class="container">
                <div class="formHead">
                    <div class="row">
                        <div class="col-md-8 col-sm-8 col-xs-12 centerCol">
                            <h2>Appendix K-1</h2>
                            <h2>DISCHARGE NOTICE</h2>
                            <h6>DISCHARGE/TRANSFER NOTICE <br> INDEPENDENT CARE WAIVER PROGRAM</h6>
                        </div>
                    </div>
                </div>
                {{-- <form method="POST" action="{{route('AppendixK1.store',$clientId)}}"> --}}
                    @csrf
                    @method('post')

                    {{-- <input type="hidden" name="client_id" value="{{$clientId}}"> --}}
                <div class="formTable">
                    <div class="row flexRow">
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                            <div class="heading">
                                <h4>Provider Name:</h4>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                            <div class="putData">
                                <input type="text"  name="provider_name" disabled value="{{$form->provider_name}}">
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                            <div class="heading">
                                <h4>Provider Number:  </h4>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                            <div class="putData">
                                <input type="number" name="provider_number" disabled value="{{$form->provider_number}}">
                            </div>
                        </div>
                    </div>
                    <div class="row flexRow">
                        <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                            <div class="heading">
                                <h4>Provider Address: </h4>
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-9 col-xs-12 noPadding">
                            <div class="putData">
                                <input type="text" name="provider_address" disabled value="{{$form->provider_address}}">
                            </div>
                        </div>
                    </div>
                    <div class="row flexRow">
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                            <div class="heading">
                                <h4>Member Name:</h4>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                            <div class="putData">
                                <input type="text" name="member_name" disabled value="{{$form->member_name}}">
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                            <div class="heading">
                                <h4>MEDICAID Number:</h4>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                            <div class="putData">
                                <input type="text" name="medicaid_number" disabled value="{{$form->medicaid_number}}">
                            </div>
                        </div>
                    </div>
                    <div class="row flexRow">
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                            <div class="heading">
                                <h4>STATUS:</h4>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                            <div class="putData">
                                <ul class="list-inline checkboxes">
                                    <li>
                                        <input type="checkbox" name="status[]" disabled  @php echo (in_array("1", $status)?'checked':''); @endphp value="1">
                                        <label for="discharge"> Discharge</label>
                                    </li>
                                    <li>
                                        <input type="checkbox" name="status[]" disabled @php echo (in_array("2", $status)?'checked':''); @endphp  value="2">
                                        <label for="transfer"> Transfer</label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                            <div class="heading">
                                <h4>EFFECTIVE DATE:</h4>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                            <div class="putData">
                                <input type="text" name="effective_date" disabled value="{{$form->effective_date}}">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="dischargeSec">
            <div class="container">
                <div class="row">
                    <h6>REASON FOR DISCHARGE: </h6>
                    <ul class="checkboxes">
                        <li>
                            <input type="checkbox" name="discharge_reasons[]" disabled @php echo (in_array("1", $discharge_reasons)?'checked':''); @endphp value="1">
                            <label for="1"> Member or member’s caregiver/legal guardian requests member be discharged or transferred (please check appropriate status). If transfer is indicated, please specify reason:</label>
                        </li>
                        <li>
                            <div class="putData">
                                <textarea name="discharge_reasons[] " disabled></textarea>
                            </div>
                        </li>
                        <li>
                            <input type="checkbox" name="discharge_reasons[]" disabled @php echo (in_array("2", $discharge_reasons)?'checked':''); @endphp value="2">
                            <label for="discharge_reasons">Member no longer requires services. </label>
                        </li>
                        <li>
                            <input type="checkbox" name="discharge_reasons[]" disabled @php echo (in_array("3", $discharge_reasons)?'checked':''); @endphp value="3">
                            <label for="discharge_reasons">Member expired. </label>
                        </li>
                        <li>
                            <input type="checkbox" name="discharge_reasons[]" disabled @php echo (in_array("4", $discharge_reasons)?'checked':''); @endphp value="4">
                            <label for="discharge_reasons">Member no longer Medicaid eligible. </label>
                        </li>
                        <li>
                            <input type="checkbox" name="discharge_reasons[]" disabled @php echo (in_array("5", $discharge_reasons)?'checked':''); @endphp value="5">
                            <label for="discharge_reasons">Utilization & Compliance Team recommends discharge.</label>
                        </li>
                        <li>
                            <input type="checkbox" name="discharge_reasons[]" disabled @php echo (in_array("6", $discharge_reasons)?'checked':''); @endphp value="6">
                            <label for="discharge_reasons">Member has entered a Long Term Care Facility.</label>
                        </li>
                        <li>
                            <input type="checkbox" name="discharge_reasons[]" disabled @php echo (in_array("7", $discharge_reasons)?'checked':''); @endphp value="7">
                            <label for="discharge_reasons">Agency issues. Please provide explanation: </label>
                        </li>
                        <li>
                            <div class="putData">
                                <textarea name="" disabled></textarea>
                            </div>
                        </li>
                        <li>
                            <input type="checkbox" name="discharge_reasons[]" disabled @php echo (in_array("8", $discharge_reasons)?'checked':''); @endphp value="8">
                            <label for="discharge_reasons">Member, member’s caregiver/legal guardian engages in/or allows illegal activities in the home or member, member’s caregiver or others living in the home have inflicted or threatened bodily harm to another person within past 30 calendar days. (Must provide completed Sentinel Event Report). </label>
                        </li>
                        <li>
                            <input type="checkbox" name="discharge_reasons[]" disabled @php echo (in_array("9", $discharge_reasons)?'checked':''); @endphp value="9">
                            <label for="discharge_reasons">Member or representative fails to adhere to the conditions of the “Member Rights and Responsibilities.”</label>
                        </li>
                        <li>
                            <input type="checkbox" name="discharge_reasons[]" disabled @php echo (in_array("10", $discharge_reasons)?'checked':''); @endphp value="10">
                            <label for="discharge_reasons">Member or representative refuses to comply with “Memorandum of Understanding.”</label>
                        </li>
                        <li>
                            <input type="checkbox" name="discharge_reasons[]" value="11" disabled @php echo (in_array("11", $discharge_reasons)?'checked':''); @endphp>
                            <label for="discharge_reasons">Provider is no longer able to provide appropriate staff to render services to the member. </label>
                        </li>
                        <li>
                            <input type="checkbox" name="discharge_reasons[]" value="12" disabled @php echo (in_array("12", $discharge_reasons)?'checked':''); @endphp>
                            <label for="discharge_reasons">The enrolled member has not received ICWP Personal Support Services, Behavioral Management, and/or Case Management for sixty (60) consecutive days.</label>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="transferSec">
            <div class="container">
                <h6>REASON FOR TRANSFER </h6>
                <p>The member is transitioning to another waiver program. </p>
                <ul class="list-inline checkboxes">
                    <li>
                        <input type="checkbox" name="transfer_reasons[]" disabled @php echo (in_array("1", $transfer_reasons)?'checked':''); @endphp value="1">
                        <label for="vehicle1"> CCSP</label>
                    </li>
                    <li>
                        <input type="checkbox" name="transfer_reasons[]" disabled @php echo (in_array("2", $transfer_reasons)?'checked':''); @endphp value="2">
                        <label for="vehicle2"> SOURCE</label>
                    </li>
                    <li>
                        <input type="checkbox" name="transfer_reasons[]" disabled @php echo (in_array("3", $transfer_reasons)?'checked':''); @endphp value="3">
                        <label for="vehicle2"> NOW/COMP</label>
                    </li>
                </ul>
            </div>
        </div>
        <div class="signSec">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="heading">
                            <h4>PROVIDER SIGNATURE:</h4>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="putData">
                            <input type="text" name="provider_signature" disabled value="{{$form->provider_signature}}">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-12">
                        <div class="heading">
                            <h4>DATE:</h4>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="putData">
                            <input type="text" name="date" disabled value="{{$form->date}}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- <center> <input type="submit" value="Save" name="btnSubmit" class="btn btn-success"></center> --}}
    </form>
    <!-- All JS -->
    <script src="{{asset('js/all.js')}}"></script>
    <!-- jquery.slicknav JS -->
    <script src="{{asset('js/jquery.slicknav.js')}}"></script>
    <!-- Custom JS -->
    <script src="{{asset('js/custom.js')}}"></script>

@endsection
