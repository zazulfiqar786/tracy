


@extends('layouts.masterlayout')

@section('content')
    {{-- <a href="{{route('careform.create_form',$clientId)}}"" class="btn btn-success fa-pull-right" style="margin-right: 20px;">Create Form</a> --}}
    <br>
    <br>


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>New Care form</title>
        <!-- All CSS -->
        {{-- <link href="{{ asset('css/all.css') }}" rel="stylesheet"> --}}
        <!-- slicknav CSS -->
        <link href="{{ asset('css/slicknav.css') }}" rel="stylesheet">
        <!-- Style CSS -->
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <!-- responsive css -->
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
        <!-- Google Fonts CSS -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

        </head>
<style>
    .topbar .navbar-search input {
    /* font-size: .85rem; */
    height: 62px;
    margin-top: 10px;
}
</style>

        <body class="responsive">




            @foreach ($forms as $form)




            <form method="POST" action="{{route('careform_form10Update',$careform_id)}}">
                @csrf
                @method('get')
                <input type="hidden" name="careform_id" value=" {{ $careform_id }}">
                <div class="allPadding newCareForm">
    <div class="topSecform">
      <div class="container">
        <div class="formHead ">
          <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12 centerCol">
              <h2>APPENDIX C-1</h2>
              <h2>CONSUMER DIRECETD CARE OPTION<br> MEMORANDUM OF UNDERSTANDING</h2>
            </div>
          </div>
        </div>
        <div class="appenCMain">
          <h6>Mutual Responsibilities:</h6>
          <p>The term of this agreement is for one year from the year of approval date, and must be renewed annually or as warranted by mutual written consent of the member and the case manager.</p>
          <p>As a recipient of the ICWP, you may be assessed intermittently, but at least annually to determine if your care can be managed under the policy and fiscal limitations of the ICWP.Independent Care Waiver Services C1-6. As a member of the Consumer Directed option, I understand that I, or my documented representative, assume the responsibility of Employer and all the related duties required of my Member-Directed Providers (MEPs) or atten</p>
          <div class="appenCMainCntnt">
            <div class="row">
              <div class="col-md-1 col-sm-1 col-xs-12">
                <div class="heading">
                  <h4>This</h4>
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="putData">
                  <input type="text" name="this" value="{{$form->this}}">
                </div>
              </div>
              <div class="col-md-1 col-sm-1 col-xs-12">
                <div class="heading">
                  <h4>day of</h4>
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="putData">
                  <input type="text" name="day_of" value="{{$form->day_of}}">
                </div>
              </div>
              <div class="col-md-1 col-sm-1 col-xs-12">
                <div class="heading">
                  <h4>20</h4>
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="putData">
                  <input type="text" name="year20" value="{{$form->year20}}">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-5 col-sm-5 col-xs-12">
                <div class="putData">
                  <input type="text" name="print_name" value="{{$form->print_name}}">
                  <h5><i>Print Name of Member/Legal Guardian</i></h5>
                </div>
              </div>
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="putData">
                  <input type="text" name="signature_member" value="{{$form->signature_member}}">
                  <h5><i>Signature of Member Legal Representative</i></h5>
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="putData">
                  <input type="text" name="signature_member_date" value="{{$form->signature_member_date}}">
                  <h5><i>Date</i></h5>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-5 col-sm-5 col-xs-12">
                <div class="putData">
                  <input type="text" name="print_name_cm" value="{{$form->print_name_cm}}">
                  <h5><i>Print Name of Case Manager</i></h5>
                </div>
              </div>
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="putData">
                  <input type="text" name="signature_cm" value="{{$form->signature_cm}}">
                  <h5><i>Signature of Case Manager</i></h5>
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="putData">
                  <input type="text" name="signature_date" value="{{$form->signature_date}}">
                  <h5><i>Date</i></h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="pages">
      <div class="container">
        <ul class="list-inline">
            <input type="Submit" value="Next" class="submitbtnsave">
        <li><a href="{{route('careform_form9index',$careform_id)}}">Back</a></li>

        </ul>
      </div>
    </div>
  </div>
            </form>
            @endforeach

          </body>


@endsection


<script>
    $('#datatable_tabletools').DataTable({
        dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete : function(oSettings, json) {
            $(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
                $(this).addClass('btn-sm btn-default');
            });
        },
        "language": {
            "search": "Search: ",
            "searchPlaceholder": "Search records"
        },
        "pageLength": 50,
        "bDestroy": true,

        "order": []
    });
    /* END TABLE TOOLS */

    function ReportError(res)
    {
    }// Filter placeholder dataTable
    $(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>

<script src="{{ asset('js/all.js') }}""></script>
<!-- jquery.slicknav JS -->
<script src="{{ asset('js/jquery.slicknav.js') }}""></script>
<!-- Custom JS -->
<script src="{{ asset('js/custom.js') }}""></script>
