


@extends('layouts.masterlayout')

@section('content')
    {{-- <a href="{{route('careform.create_form',$clientId)}}"" class="btn btn-success fa-pull-right" style="margin-right: 20px;">Create Form</a> --}}
    <br>
    <br>


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>New Care form</title>
        <!-- All CSS -->
        {{-- <link href="{{ asset('css/all.css') }}" rel="stylesheet"> --}}
        <!-- slicknav CSS -->
        <link href="{{ asset('css/slicknav.css') }}" rel="stylesheet">
        <!-- Style CSS -->
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <!-- responsive css -->
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
        <!-- Google Fonts CSS -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

        </head>
<style>
    .topbar .navbar-search input {
    /* font-size: .85rem; */
    height: 62px;
    margin-top: 10px;
}
</style>

        <body class="responsive">




            @foreach ($forms as $form)


            @php
$Living_Arrangements = $form->Living_Arrangements;
$Living_Arrangements2 = explode(',', $Living_Arrangements);
@endphp


            <form method="POST" action="{{route('careform_form14Update',$careform_id)}}">
                @csrf
                @method('get')
                <input type="hidden" name="careform_id" value=" {{ $careform_id }}">
                <div class="allPadding newCareForm">
    <div class="topSecform">
      <div class="container">
        <div class="formHead ">
          <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12 centerCol">
              <h2>Appendix D-4 </h2>
              <h2>RENEWAL PARTICIPANT ASSESSMENT FORM</h2>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="heading">
                <h4>Member name:</h4>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="putData">
                <input type="text" name="member_name_last" value="{{$form->member_name_last}}">
              </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12"></div>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="heading">
                <h4>Care plan due date:</h4>
              </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="putData">
                <input type="text" name="Care_plan_due_date" value="{{$form->Care_plan_due_date}}">
              </div>
            </div>
          </div>
        </div>
        <div class="appenCMain">
          <h6>Identifying Information </h6>
          <div class="appenCMainCntnt">
            <div class="row">
              <div class="col-md-2 col-sm-2 col-xs-12">
                <div class="heading">
                  <h4>Member Name:</h4>
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="putData">
                  <input type="text" name="Member_Name" value="{{$form->Member_Name}}">
                  <h5><i>Last</i></h5>
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="putData">
                  <input type="text" name="member_name_First" value="{{$form->member_name_First}}">
                  <h5><i>First</i></h5>
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="putData">
                  <input type="text" name="member_name_Middle" value="{{$form->member_name_Middle}}">
                  <h5><i>Middle Initial</i></h5>
                </div>
              </div>
              <div class="col-md-1 col-sm-1 col-xs-12">
                <div class="putData">
                  <input type="text" name="member_name_Suffix" value="{{$form->member_name_Suffix}}">
                  <h5><i>Suffix</i></h5>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-2 col-sm-2 col-xs-12">
                <div class="heading">
                  <h4>Address:</h4>
                </div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-12">
                <div class="putData">
                  <input type="text" name="Address" value="{{$form->Address}}">
                  <h5><i>Appartment #</i></h5>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="putData">
                  <input type="text" name="City" value="{{$form->City}}">
                  <h5><i>City</i></h5>
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="putData">
                  <input type="text" name="State" value="{{$form->State}}">
                  <h5><i>State</i></h5>
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="putData">
                  <input type="text" name="Zip" value="{{$form->Zip}}">
                  <h5><i>Zip</i></h5>
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="putData">
                  <input type="text" name="Country" value="{{$form->Country}}">
                  <h5><i>Country</i></h5>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-2 col-sm-2 col-xs-12">
                <div class="heading">
                  <h4>Phone #:</h4>
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="putData">
                  <input type="text" name="Phone" value="{{$form->Phone}}" >
                </div>
              </div>
              <div class="col-md-4 col-sm-4 col-xs-12"></div>
              <div class="col-md-1 col-sm-1 col-xs-12">
                <div class="heading">
                  <h4>Cell #:</h4>
                </div>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-12">
                <div class="putData">
                  <input type="text" name="Cell" value="{{$form->Cell}}">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-2 col-sm-2 col-xs-12">
                <div class="heading">
                  <h4>Date of Birth:</h4>
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="putData">
                  <input type="text" name="Date_of_Birth" value="{{$form->Date_of_Birth}}">
                </div>
              </div>
              <div class="col-md-1 col-sm-1 col-xs-12">
                <div class="heading">
                  <h4>Age:</h4>
                </div>
              </div>
              <div class="col-md-1 col-sm-1 col-xs-12">
                <div class="putData">
                  <input type="text" name="Age" value="{{$form->Age}}">
                </div>
              </div>
              <div class="col-md-5 col-sm-5 col-xs-12">
                <ul class="list-inline checkboxes">
                  <li>
                    <input type="radio" name="gender" value="Male" @php echo ($form->gender == 'Male'?'checked':''); @endphp >
                    <label for="vehicle1">Male </label>
                  </li>
                  <li>
                    <input type="radio" name="gender" value="Female" @php echo ($form->gender == 'Female'?'checked':''); @endphp>
                    <label for="vehicle2">Female</label>
                  </li>
                </ul>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="heading">
                  <h4>Have you moved in the past year?</h4>
                </div>
              </div>
              <div class="col-md-5 col-sm-5 col-xs-12">
                <ul class="list-inline checkboxes">
                  <li>
                    <input type="radio" name="past_year" value="Yes" @php echo ($form->past_year == 'Yes'?'checked':''); @endphp>
                    <label for="vehicle1">Yes</label>
                  </li>
                  <li>
                    <input type="radio" name="past_year" value="No" @php echo ($form->past_year == 'No'?'checked':''); @endphp>
                    <label for="vehicle2">No </label>
                  </li>
                </ul>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="heading">
                  <h4>If yes; when did you notify DFCS?</h4>
                </div>
              </div>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="putData">
                  <input type="text" name="DFCS" value="{{$form->DFCS}}">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="heading">
                  <h4>Living Arrangements at the time of the Renewal Assessment: </h4>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <ul class="list-inline checkboxes">
                  <li>
                    <input type="checkbox" name="Living_Arrangements[]" value="1" @php echo (in_array("1", $Living_Arrangements2)?'checked':''); @endphp>
                    <label for="vehicle1">Lives Alone </label>
                  </li>
                  <li>
                    <input type="checkbox" name="Living_Arrangements[]" value="2" @php echo (in_array("2", $Living_Arrangements2)?'checked':''); @endphp>
                    <label for="vehicle2">With Other Family Member  </label>
                  </li>
                  <li>
                    <input type="checkbox" name="Living_Arrangements[]" value="3" @php echo (in_array("3", $Living_Arrangements2)?'checked':''); @endphp>
                    <label for="vehicle1">With Paid Help (not ICWP PSS) </label>
                  </li>
                  <li>
                    <input type="checkbox" name="Living_Arrangements[]" value="4" @php echo (in_array("4", $Living_Arrangements2)?'checked':''); @endphp>
                    <label for="vehicle2">With Spouse/Significant other</label>
                  </li>
                  <li>
                    <input type="checkbox" name="Living_Arrangements[]" value="5" @php echo (in_array("5", $Living_Arrangements2)?'checked':''); @endphp>
                    <label for="vehicle1">With a Friend</label>
                  </li>
                  <li>
                    <input type="checkbox" name="Living_Arrangements[]" value="6" @php echo (in_array("6", $Living_Arrangements2)?'checked':''); @endphp>
                    <label for="vehicle2">ALS (Alternative Living Services) </label>
                  </li>
                </ul>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="heading">
                  <h4>As compared to 1 year ago, Member now lives with other persons (e.g. moved in with another or other person moved in with individual).</h4>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <ul class="list-inline checkboxes">
                  <li>
                    <input type="radio" name="compared_to_1_year_ago" value="Yes" @php echo ($form->compared_to_1_year_ago == 'Yes'?'checked':''); @endphp>
                    <label for="vehicle1">Yes</label>
                  </li>
                  <li>
                    <input type="radio" name="compared_to_1_year_ago" value="No" @php echo ($form->compared_to_1_year_ago == 'No'?'checked':''); @endphp>
                    <label for="vehicle2">No</label>
                  </li>
                </ul>
                <textarea rows="5" name="details" value="{{$form->details}}"></textarea>
              </div>
            </div>
          </div>
          <h6>Medical Information</h6>
          <p>Current Diagnosis:</p>
          <table class="table table-bordered">
            <tbody>
              <tr>
                <td>
                  <input type="text" name="Current_Diagnosis_1" value="{{$form->Current_Diagnosis_1}}">
                </td>
                <td>
                  <input type="text" name="Current_Diagnosis_2" value="{{$form->Current_Diagnosis_2}}">
                </td>
              </tr>
              <tr>
                <td>
                  <input type="text" name="Current_Diagnosis_3" value="{{$form->Current_Diagnosis_3}}">
                </td>
                <td>
                  <input type="text" name="Current_Diagnosis_4" value="{{$form->Current_Diagnosis_4}}">
                </td>
              </tr>
              <tr>
                <td>
                  <input type="text" name="Current_Diagnosis_5" value="{{$form->Current_Diagnosis_5}}">
                </td>
                <td>
                  <input type="text" name="Current_Diagnosis_6" value="{{$form->Current_Diagnosis_6}}">
                </td>
              </tr>
              <tr>
                <td>
                  <input type="text" name="Current_Diagnosis_7" value="{{$form->Current_Diagnosis_7}}">
                </td>
                <td>
                  <input type="text" name="Current_Diagnosis_8" value="{{$form->Current_Diagnosis_8}}">
                </td>
              </tr>
              <tr>
                <td>
                  <input type="text" name="Current_Diagnosis_9" value="{{$form->Current_Diagnosis_9}}">
                </td>
                <td>
                  <input type="text" name="Current_Diagnosis_10" value="{{$form->Current_Diagnosis_10}}">
                </td>
              </tr>
              <tr>
                <td>
                  <input type="text" name="Current_Diagnosis_11" value="{{$form->Current_Diagnosis_11}}">
                </td>
                <td>
                  <input type="text" name="Current_Diagnosis_12" value="{{$form->Current_Diagnosis_12}}">
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="pages">
      <div class="container">
        <ul class="list-inline">
            <input type="Submit" value="Next" class="submitbtnsave">
        <li><a href="{{route('careform_form13index',$careform_id)}}">Back</a></li>

        </ul>
      </div>
    </div>
  </div>
            </form>
            @endforeach

          </body>


@endsection


<script>
    $('#datatable_tabletools').DataTable({
        dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete : function(oSettings, json) {
            $(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
                $(this).addClass('btn-sm btn-default');
            });
        },
        "language": {
            "search": "Search: ",
            "searchPlaceholder": "Search records"
        },
        "pageLength": 50,
        "bDestroy": true,

        "order": []
    });
    /* END TABLE TOOLS */

    function ReportError(res)
    {
    }// Filter placeholder dataTable
    $(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>

<script src="{{ asset('js/all.js') }}""></script>
<!-- jquery.slicknav JS -->
<script src="{{ asset('js/jquery.slicknav.js') }}""></script>
<!-- Custom JS -->
<script src="{{ asset('js/custom.js') }}""></script>
