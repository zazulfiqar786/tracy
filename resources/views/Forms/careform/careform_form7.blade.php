


@extends('layouts.masterlayout')

@section('content')
    {{-- <a href="{{route('careform.create_form',$clientId)}}"" class="btn btn-success fa-pull-right" style="margin-right: 20px;">Create Form</a> --}}
    <br>
    <br>


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>New Care form</title>
        <!-- All CSS -->
        {{-- <link href="{{ asset('css/all.css') }}" rel="stylesheet"> --}}
        <!-- slicknav CSS -->
        <link href="{{ asset('css/slicknav.css') }}" rel="stylesheet">
        <!-- Style CSS -->
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <!-- responsive css -->
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
        <!-- Google Fonts CSS -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

        </head>
<style>
    .topbar .navbar-search input {
    /* font-size: .85rem; */
    height: 62px;
    margin-top: 10px;
}
</style>

        <body class="responsive">




            @foreach ($forms as $form)




            <form method="POST" action="{{route('careform_form7Update',$careform_id)}}">
                @csrf
                @method('get')
                <input type="hidden" name="careform_id" value=" {{ $careform_id }}">
                <div class="allPadding newCareForm">
    <div class="topSecform">
      <div class="container">
        <div class="formHead ">
          <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12 centerCol">
              <h2>APPENDIX C-1</h2>
              <h2>CONSUMER DIRECETD CARE OPTION<br> MEMORANDUM OF UNDERSTANDING</h2>
            </div>
          </div>
        </div>
        <div class="appenCHead">
          <div class="row">
            <div class="col-md-2 col-md-2 col-xs-12">
              <div class="heading">
                <h4>Member:</h4>
              </div>
            </div>
            <div class="col-md-2 col-md-2 col-xs-12">
              <div class="putData">
                <input type="text" name="member" value="{{ $form->member }}">
              </div>
            </div>
            <div class="col-md-3 col-md-3 col-xs-12"></div>
            <div class="col-md-3 col-md-3 col-xs-12">
              <div class="heading">
                <h4>Medicaid Number:</h4>
              </div>
            </div>
            <div class="col-md-2 col-md-2 col-xs-12">
              <div class="putData">
                <input type="text" name="medicaid_number" value="{{ $form->medicaid_number }}">
              </div>
            </div>
          </div>
        </div>
        <div class="appenCMain">
          <h6>Purpose:</h6>
          <p>It is the intent of this agreement to provide an assurance that members understand the expectations and guidelines of the Consumer Directed Service option under the Independent Care Waiver Program (ICWP). The members must follow specific guidelines in order to ensure compliance with the Plan of Care. Failure to follow these guidelines may result in a member returning to the traditional option or termination from the program. Signing this agreement indicates that you understand and will abide by the terms and conditions for participation in the Consumer Directed Care option as implemented by the Georgia Division of Medical Assistance. The following does not intend to address all of the conditions of participation, therefore please discuss with your case manager if you have any questions or concerns.</p>
          <p>In order to be eligible for the Consumer Directed Option service under ICWP the member must meet the following criteria:</p>
          <ul>
            <li>Medicaid eligible or potentially Medicaid eligible;</li>
            <li> Be between twenty-one (21) and sixty-four (64) years of age;</li>
            <li>. Have a severe physical disability and/or traumatic brain injury that substantially limits one or more activities of daily living and requires help from another person;</li>
            <li>Mentally capable to direct own care and demonstrate control over daily schedule and decisions. This does not apply to those with a traumatic brain injury. A member’s representative may make decisions for the TBI member. Mentally capable means must be cognitively alert and capable of directing their own services. Cognitively alert means the member displays or exhibits reasoning and perception abilities in their thought processes at the time of assessment and thereafter. Directing their own services means the member must be able to make independent decisions and judgements regarding their care, care choices and overall wellbeing. In addition, members must be able to communicate their decisions and care choices through commonly accepted forms of communication including verbal communication, sign language, or through acceptable communication assistive devices such as communication boards mediums. Access to and the use of such devices is the sole responsibility of the member</li>
            <li> Be mentally stable but in or at risk of hospital or nursing home placement;</li>
            <li> Does not have a primary diagnosis of a mental disorder including mental illness or mental retardation;</li>
            <li>Certified for a level of care appropriate for placement in a hospital or nursing home;</li>
            <li> Be motivated to self-direct care;</li>
            <li>Be willing to assume responsibility for cost effective use of Personal Support Services;</li>
            <li>Does not have a history of behavior that is problematic that places self or others at risk;</li>
            <li> Must have a Primary Care Physician;</li>
            <li>Must successfully fulfill the training requirements of the Consumer Directed Option by review of the training manual and successfully demonstration of understanding of the training sessions; and</li>
            <li>Must be willing to sign a Memorandum of Understanding (MOU) which outlines the roles and responsibilities of a Self Directed Consumer.</li>
          </ul>
        </div>
      </div>
    </div>
    <div class="pages">
      <div class="container">
        <ul class="list-inline">
            <input type="Submit" value="Next" class="submitbtnsave">
        <li><a href="{{route('careform_form6index',$careform_id)}}">Back</a></li>

        </ul>
      </div>
    </div>
  </div>
            </form>
            @endforeach

          </body>


@endsection


<script>
    $('#datatable_tabletools').DataTable({
        dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete : function(oSettings, json) {
            $(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
                $(this).addClass('btn-sm btn-default');
            });
        },
        "language": {
            "search": "Search: ",
            "searchPlaceholder": "Search records"
        },
        "pageLength": 50,
        "bDestroy": true,

        "order": []
    });
    /* END TABLE TOOLS */

    function ReportError(res)
    {
    }// Filter placeholder dataTable
    $(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>

<script src="{{ asset('js/all.js') }}""></script>
<!-- jquery.slicknav JS -->
<script src="{{ asset('js/jquery.slicknav.js') }}""></script>
<!-- Custom JS -->
<script src="{{ asset('js/custom.js') }}""></script>
