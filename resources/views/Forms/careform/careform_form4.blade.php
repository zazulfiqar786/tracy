


@extends('layouts.masterlayout')

@section('content')
    {{-- <a href="{{route('careform.create_form',$clientId)}}"" class="btn btn-success fa-pull-right" style="margin-right: 20px;">Create Form</a> --}}
    <br>
    <br>


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>New Care form</title>
        <!-- All CSS -->
        {{-- <link href="{{ asset('css/all.css') }}" rel="stylesheet"> --}}
        <!-- slicknav CSS -->
        <link href="{{ asset('css/slicknav.css') }}" rel="stylesheet">
        <!-- Style CSS -->
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <!-- responsive css -->
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
        <!-- Google Fonts CSS -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

        </head>
<style>
    .topbar .navbar-search input {
    /* font-size: .85rem; */
    height: 62px;
    margin-top: 10px;
}
</style>
        <body class="responsive">




            @foreach ($forms as $form)

            @php
        $behaviour_managment2 = $form->behaviour_managment;

        @endphp


            <form method="POST" action="{{route('careform_form4Update',$careform_id)}}">
                @csrf
                @method('get')
                <input type="hidden" name="careform_id" value=" {{ $careform_id }}">
                <div class="allPadding newCareForm">
    <div class="topSecform">
      <div class="container">
        <div class="formHead ">
          <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12 centerCol">
              <h2>Reference Sheet</h2>
            </div>
          </div>
          <div class="row flexRow">
            <div class="col-md-1 col-sm-1 col-xs-12">
              <div class="heading">
                <h4><b>Name:</b></h4>
              </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="putData">
                <input type="text" name="name" value="{{ $form->name }}">
              </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12"></div>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="heading">
                <h4><b>Plan of Care Date:</b></h4>
              </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="putData">
                <input type="text" name="plan_of_care_date" value="{{ $form->plan_of_care_date }}">
              </div>
            </div>
          </div>
        </div>
        <div class="refSheetSec">
          <div class="container">
            <p>This reference sheet will assist you and your provider in addressing your needs. </p>
          </div>
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="refCntnt">
                <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Your ICWP case manager is:</h4>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="your_icwp" value="{{ $form->your_icwp }}">
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="heading">
                      <h6>Phone number:</h6>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="your_icwp_phone" value="{{ $form->your_icwp_phone }}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="refCntnt">
                <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Your Emergency Contact Name:</h4>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="your_emergency_contact_name" value="{{ $form->your_emergency_contact_name }}">
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="heading">
                      <h6>Phone number:</h6>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="your_emergency_phone" value="{{ $form->your_emergency_phone }}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="refCntnt">
                <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Personal Assistance Provider:</h4>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="p_a_provider" value="{{ $form->p_a_provider }}">
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="heading">
                      <h6>Phone number:</h6>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="p_a_provider_phone" value="{{ $form->p_a_provider_phone }}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="refCntnt">
                <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Your Primary Care Physician:</h4>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="your_primary_care_physician" value="{{ $form->your_primary_care_physician }}">
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="heading">
                      <h6>Phone number:</h6>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="your_primary_care_physician_phone" value="{{ $form->your_primary_care_physician_phone }}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="refCntnt">
                <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Supplies and Equipment:</h4>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="s_e" value="{{ $form->s_e }}">
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="heading">
                      <h6>Supplies Provider</h6>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="s_e_sp" value="{{ $form->s_e_sp }}">
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="heading">
                      <h6>Phone number</h6>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="s_e_phone" value="{{ $form->s_e_phone }}">
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="heading">
                      <h6>Equipment Provider</h6>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="s_e_equipment_provider" value="{{ $form->s_e_equipment_provider }}">
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="heading">
                      <h6>Phone number</h6>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="s_e_equipment_provider_phone" value="{{ $form->s_e_equipment_provider_phone }}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="refCntnt">
                <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Hospital for Emergencies:</h4>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="hospital_for_emergencies" value="{{ $form->hospital_for_emergencies }}">
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="heading">
                      <h6>Phone number:</h6>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="h_f_e_phone_number" value="{{ $form->h_f_e_phone_number }}">
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="heading">
                      <h6>Address:</h6>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="h_f_e_address1" value="{{ $form->h_f_e_address1 }}">
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="heading">
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="h_f_e_address2" value="{{ $form->h_f_e_address2 }}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="behavior">
            <div class="row">
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="heading ">
                  <h4>Behavior Management:</h4>
                  <h6>(if yes, see specific plan)</h6>
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12">
                <ul class="list-inline checkboxes">
                  <li>
                    <input type="Radio" name="behaviour_managment" {{ ($form->behaviour_managment=="Yes")? "checked" : "" }}  value="Yes">
                    <label for="vehicle1">Yes</label>
                  </li>
                  <li>
                    <input type="Radio" name="behaviour_managment"  {{ ($form->behaviour_managment=="No")? "checked" : "" }}  value="No">
                    <label for="vehicle2">No</label>

                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="noteSec">
            <h4>Note:</h4>
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <td>
                    <input type="text" name="note_1" value="{{ $form->note_1 }}">
                  </td>
                </tr>
                <tr>
                  <td>
                    <input type="text" name="note_2" value="{{ $form->note_2 }}">
                  </td>
                </tr>
                <tr>
                  <td>
                    <input type="text" name="note_3" value="{{ $form->note_3 }}">
                  </td>
                </tr>
                <tr>
                  <td>
                    <input type="text" name="note_4" value="{{ $form->note_4 }}">
                  </td>
                </tr>
                <tr>
                  <td>
                    <input type="text" name="note_5" value="{{ $form->note_1 }}">
                  </td>
                </tr>
                <tr>
                  <td>
                    <input type="text" name="note_6" value="{{ $form->note_6 }}">
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="pages">
      <div class="container">
        <ul class="list-inline">
            <input type="Submit" value="Next" class="submitbtnsave">
        <li><a href="{{route('careform_form3index',$careform_id)}}">Back</a></li>

        </ul>
      </div>
    </div>
  </div>
            </form>
            @endforeach

          </body>


@endsection


<script>
    $('#datatable_tabletools').DataTable({
        dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete : function(oSettings, json) {
            $(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
                $(this).addClass('btn-sm btn-default');
            });
        },
        "language": {
            "search": "Search: ",
            "searchPlaceholder": "Search records"
        },
        "pageLength": 50,
        "bDestroy": true,

        "order": []
    });
    /* END TABLE TOOLS */

    function ReportError(res)
    {
    }// Filter placeholder dataTable
    $(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>

<script src="{{ asset('js/all.js') }}""></script>
<!-- jquery.slicknav JS -->
<script src="{{ asset('js/jquery.slicknav.js') }}""></script>
<!-- Custom JS -->
<script src="{{ asset('js/custom.js') }}""></script>
