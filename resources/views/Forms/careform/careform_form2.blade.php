


@extends('layouts.masterlayout')

@section('content')
    {{-- <a href="{{route('careform.create_form',$clientId)}}"" class="btn btn-success fa-pull-right" style="margin-right: 20px;">Create Form</a> --}}
    <br>
    <br>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>New Care form</title>
        <!-- All CSS -->
        {{-- <link href="{{ asset('css/all.css') }}" rel="stylesheet"> --}}
        <!-- slicknav CSS -->
        <link href="{{ asset('css/slicknav.css') }}" rel="stylesheet">
        <!-- Style CSS -->
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <!-- responsive css -->
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
        <!-- Google Fonts CSS -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

        </head>
<style>
    .topbar .navbar-search input {
    /* font-size: .85rem; */
    height: 62px;
    margin-top: 10px;
}
</style>
        <body class="responsive">
            @foreach ($forms as $form)
            <form method="POST" action="{{route('careform_form2Update',$careform_id)}}">
                @csrf
                @method('get')
                <input type="hidden" name="careform_id" value=" {{ $careform_id }}">
            <div class="allPadding newCareForm">
              <div class="topSecform">
                <div class="container">
                  <div class="formHead ">
                    <div class="row flexRow">
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="heading">
                          <h4><b>ICWP SUPPLY LIST FOR:</b></h4>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="putData">
                          <input type="text" name="icwp_supply_list" value="{{ $form->icwp_supply_list }}">
                        </div>
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12"></div>
                      <div class="col-md-1 col-sm-1 col-xs-12">
                        <div class="heading">
                          <h4><b>DATE:</b></h4>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="putData">
                          <input type="text"  name="date" value="{{ $form->date }}">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th scope="col">SUPPLY ITEM</th>
                          <th scope="col">PER MONTH</th>
                          <th scope="col">PER YEAR</th>
                          <th scope="col">COST PER UNIT</th>
                          <th scope="col">TOTAL COST PER YEAR</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Gloves</td>
                          <td>
                            <input type="text" name="gloves_permonth" value="{{ $form->gloves_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="gloves_peryear" value="{{ $form->gloves_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="gloves_perunit" id="gloves_perunit" placeholder="$0.00" value="{{ $form->gloves_perunit }}">
                          </td>
                          <td>
                            <input type="text" name="gloves_totalyear" placeholder="$0.00" value="{{ $form->gloves_totalyear }}">
                          </td>
                        </tr>
                        <tr>
                          <td>Adult Briefs/ pullups</td>
                          <td>
                            <input type="text" name="adult_briefs_permonth" value="{{ $form->adult_briefs_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="adult_briefs_peryear" value="{{ $form->adult_briefs_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="adult_briefs_perunit" id="adult_briefs_perunit" placeholder="$0.00" value="{{ $form->adult_briefs_perunit }}">
                          </td>
                          <td>
                            <input type="text" name="adult_briefs_totalyear" placeholder="$0.00" value="{{ $form->adult_briefs_totalyear }}">
                          </td>
                        </tr>
                        <tr>
                          <td>Bladder control pads cs</td>
                          <td>
                            <input type="text" name="bladder_permonth" value="{{ $form->bladder_permonth }}" >
                          </td>
                          <td>
                            <input type="text" name="bladder_peryear" value="{{ $form->bladder_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="bladder_perunit" placeholder="$0.00" value="{{ $form->bladder_perunit }}">
                          </td>
                          <td>
                            <input type="text" name="bladder_totalyear" placeholder="$0.00" value="{{ $form->bladder_totalyear }}">
                          </td>
                        </tr>
                        <tr>
                          <td>Chux</td>
                          <td>
                            <input type="text" name="chux_permonth" value="{{ $form->chux_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="chux_peryear" value="{{ $form->chux_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="chux_perunit" value="{{ $form->chux_perunit }} "placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="chux_totalyear" value="{{ $form->chux_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Wipes- disposable</td>
                          <td>
                            <input type="text" name="wipes_permonth" value="{{ $form->wipes_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="wipes_peryear" value="{{ $form->wipes_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="wipes_perunit" value="{{ $form->wipes_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="wipes_totalyear" value="{{ $form->wipes_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Skin Cream</td>
                          <td>
                            <input type="text" name="skin_permonth" value="{{ $form->skin_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="skin_peryear" value="{{ $form->skin_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="skin_perunit" value="{{ $form->skin_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="skin_totalyear" value="{{ $form->skin_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Reusable underpads</td>
                          <td>
                            <input type="text" name="reusable_permonth" value="{{ $form->reusable_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="reusable_peryear" value="{{ $form->reusable_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="reusable_perunit" value="{{ $form->reusable_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="reusable_totalyear" value="{{ $form->reusable_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Urinal</td>
                          <td>
                            <input type="text" name="urinal_permonth" value="{{ $form->urinal_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="urinal_peryear" value="{{ $form->urinal_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="urinal_perunit" value="{{ $form->urinal_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="urinal_totalyear" value="{{ $form->urinal_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Bisacodyl box of 10</td>
                          <td>
                            <input type="text" name="bisacodyl_permonth" value="{{ $form->bisacodyl_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="bisacodyl_peryear" value="{{ $form->bisacodyl_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="bisacodyl_perunit" value="{{ $form->bisacodyl_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="bisacodyl_totalyear" value="{{ $form->bisacodyl_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Glycerin box of 4</td>
                          <td>
                            <input type="text" name="glycerin_permonth" value="{{ $form->glycerin_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="glycerin_peryear" value="{{ $form->glycerin_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="glycerin_perunit" value="{{ $form->glycerin_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="glycerin_totalyear" value="{{ $form->glycerin_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Magic bullets box 100</td>
                          <td>
                            <input type="text" name="uagic_permonth" value="{{ $form->uagic_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="uagic_peryear" value="{{ $form->uagic_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="uagic_perunit" value="{{ $form->uagic_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="uagic_totalyear" value="{{ $form->uagic_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Fleet enema</td>
                          <td>
                            <input type="text" name="fleet_permonth" value="{{ $form->fleet_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="fleet_peryear" value="{{ $form->fleet_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="fleet_perunit" value="{{ $form->fleet_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="fleet_totalyear" value="{{ $form->fleet_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Lubricant *M pays 4ea/mo</td>
                          <td>
                            <input type="text" name="lubricant_permonth" value="{{ $form->lubricant_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="lubricant_peryear" value="{{ $form->lubricant_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="lubricant_perunit" value="{{ $form->lubricant_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="lubricant_totalyear" value="{{ $form->lubricant_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Urinary leg bag</td>
                          <td>
                            <input type="text" name="urinary_permonth" value="{{ $form->urinary_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="urinary_peryear" value="{{ $form->urinary_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="urinary_perunit" value="{{ $form->urinary_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="urinary_totalyear" value="{{ $form->urinary_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>TF formula case</td>
                          <td>
                            <input type="text" name="tf_permonth" value="{{ $form->tf_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="tf_peryear" value="{{ $form->tf_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="tf_perunit" value="{{ $form->tf_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="tf_totalyear" value="{{ $form->tf_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Glycerin swabs</td>
                          <td>
                            <input type="text" name="glycerin_swabs_permonth" value="{{ $form->glycerin_swabs_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="glycerin_swabs_peryear" value="{{ $form->glycerin_swabs_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="glycerin_swabs_perunit" value="{{ $form->glycerin_swabs_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="glycerin_swabs_totalyear" value="{{ $form->glycerin_swabs_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Toothettes- bx 250</td>
                          <td>
                            <input type="text" name="toothettes_permonth" value="{{ $form->toothettes_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="toothettes_peryear" value="{{ $form->toothettes_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="toothettes_perunit" value="{{ $form->toothettes_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="toothettes_totalyear" value="{{ $form->toothettes_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Sip and puff straws bx 100</td>
                          <td>
                            <input type="text" name="slipAndpuff_permonth" value="{{ $form->slipAndpuff_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="slipAndpuff_peryear" value="{{ $form->slipAndpuff_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="slipAndpuff_perunit" value="{{ $form->slipAndpuff_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="slipAndpuff_totalyear" value="{{ $form->slipAndpuff_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Bibs- disposable cs500</td>
                          <td>
                            <input type="text" name="bibs_disposable_permonth" value="{{ $form->bibs_disposable_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="bibs_disposable_peryear" value="{{ $form->bibs_disposable_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="bibs_disposable_perunit" value="{{ $form->bibs_disposable_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="bibs_disposable_totalyear" value="{{ $form->bibs_disposable_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Bibs- reusable</td>
                          <td>
                            <input type="text" name="bibs_reusable_permonth" value="{{ $form->bibs_reusable_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="bibs_reusable_peryear" value="{{ $form->bibs_reusable_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="bibs_reusable_perunit" value="{{ $form->bibs_reusable_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="bibs_reusable_totalyear" value="{{ $form->bibs_reusable_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Inner cannula bx10</td>
                          <td>
                            <input type="text" name="inner_cannula_permonth" value="{{ $form->inner_cannula_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="inner_cannula_peryear" value="{{ $form->inner_cannula_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="inner_cannula_perunit" value="{{ $form->inner_cannula_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="inner_cannula_totalyear" value="{{ $form->inner_cannula_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Trach care kit</td>
                          <td>
                            <input type="text" name="trach_care_permonth" value="{{ $form->trach_care_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="trach_care_peryear" value="{{ $form->trach_care_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="trach_care_perunit" value="{{ $form->trach_care_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="trach_care_totalyear" value="{{ $form->trach_care_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Trach collar tie bx12</td>
                          <td>
                            <input type="text" name="trach_collar_permonth" value="{{ $form->trach_collar_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="trach_collar_peryear" value="{{ $form->trach_collar_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="trach_collar_perunit" value="{{ $form->trach_collar_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="trach_collar_totalyear" value="{{ $form->trach_collar_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Drain sponge pk50</td>
                          <td>
                            <input type="text" name="drain_sponge_permonth" value="{{ $form->drain_sponge_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="drain_sponge_peryear" value="{{ $form->drain_sponge_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="drain_sponge_perunit" value="{{ $form->drain_sponge_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="drain_sponge_totalyear" value="{{ $form->drain_sponge_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Multidose saline bx100</td>
                          <td>
                            <input type="text" name="multidose_permonth" value="{{ $form->multidose_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="multidose_peryear" value="{{ $form->multidose_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="multidose_perunit" value="{{ $form->multidose_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="multidose_totalyear" value="{{ $form->multidose_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Barrier spray</td>
                          <td>
                            <input type="text" name="barrier_spray_permonth" value="{{ $form->barrier_spray_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="barrier_spray_peryear" value="{{ $form->barrier_spray_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="barrier_spray_perunit" value="{{ $form->barrier_spray_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="barrier_spray_totalyear" value="{{ $form->barrier_spray_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Periwash</td>
                          <td>
                            <input type="text" name="periwash_permonth" value="{{ $form->periwash_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="periwash_peryear" value="{{ $form->periwash_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="periwash_perunit" value="{{ $form->periwash_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="periwash_totalyear" value="{{ $form->periwash_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Heel protector</td>
                          <td>
                            <input type="text" name="heel_protector_permonth" value="{{ $form->heel_protector_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="heel_protector_peryear" value="{{ $form->heel_protector_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="heel_protector_perunit" value="{{ $form->heel_protector_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="heel_protector_totalyear" value="{{ $form->heel_protector_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Elbow protectors</td>
                          <td>
                            <input type="text" name="elbow_protectors_permonth" value="{{ $form->elbow_protectors_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="elbow_protectors_peryear" value="{{ $form->elbow_protectors_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="elbow_protectors_perunit" value="{{ $form->elbow_protectors_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="elbow_protectors_totalyear" value="{{ $form->elbow_protectors_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Skin barrier wipes bx50</td>
                          <td>
                            <input type="text" name="skin_barrier_permonth" value="{{ $form->skin_barrier_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="skin_barrier_peryear" value="{{ $form->skin_barrier_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="skin_barrier_perunit" value="{{ $form->skin_barrier_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="skin_barrier_totalyear" value="{{ $form->skin_barrier_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Geomat</td>
                          <td>
                            <input type="text" name="geomat_permonth" value="{{ $form->geomat_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="geomat_peryear" value="{{ $form->geomat_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="geomat_perunit" value="{{ $form->geomat_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="geomat_totalyear" value="{{ $form->geomat_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td>Reacher</td>
                          <td>
                            <input type="text" name="reacher_permonth" value="{{ $form->reacher_permonth }}">
                          </td>
                          <td>
                            <input type="text" name="reacher_peryear" value="{{ $form->reacher_peryear }}">
                          </td>
                          <td>
                            <input type="text" name="reacher_perunit" value="{{ $form->reacher_perunit }}" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="reacher_totalyear" value="{{ $form->reacher_totalyear }}" placeholder="$0.00">
                          </td>
                        </tr>
                        <tr>
                          <td><b>Grand Total:</b></td>
                          <td>
                            <input type="text" name="" >
                          </td>
                          <td>
                            <input type="text" name="" >
                          </td>
                          <td>
                            <input type="text" name="sum" id="sum" placeholder="$0.00">
                          </td>
                          <td>
                            <input type="text" name="" placeholder="$0.00">
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="pages">
                <div class="container">
                  <ul class="list-inline">
                    <input type="Submit" value="Next" class="submitbtnsave">
                    <li><a href="{{route('careform_form1index',$careform_id)}}">Back</a></li>
                  </ul>
                </div>
              </div>
            </div>
            </form>
            @endforeach
          </body>


@endsection





<script>
    $(function() {
        $('#value1, #value2').keyup(function() {
            var value1 = parseFloat($('#gloves_perunit').val()) || 0;
            var value2 = parseFloat($('#adult_briefs_perunit').val()) || 0;
            $('#sum').val(value1 + value2);
        });
    });
</script>



<script>
    $('#datatable_tabletools').DataTable({
        dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete : function(oSettings, json) {
            $(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
                $(this).addClass('btn-sm btn-default');
            });
        },
        "language": {
            "search": "Search: ",
            "searchPlaceholder": "Search records"
        },
        "pageLength": 50,
        "bDestroy": true,

        "order": []
    });
    /* END TABLE TOOLS */

    function ReportError(res)
    {
    }// Filter placeholder dataTable
    $(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>

<script src="{{ asset('js/all.js') }}""></script>
<!-- jquery.slicknav JS -->
<script src="{{ asset('js/jquery.slicknav.js') }}""></script>
<!-- Custom JS -->
<script src="{{ asset('js/custom.js') }}""></script>
