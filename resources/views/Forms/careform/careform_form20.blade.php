


@extends('layouts.masterlayout')

@section('content')
    {{-- <a href="{{route('careform.create_form',$clientId)}}"" class="btn btn-success fa-pull-right" style="margin-right: 20px;">Create Form</a> --}}
    <br>
    <br>


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>New Care form</title>
        <!-- All CSS -->
        {{-- <link href="{{ asset('css/all.css') }}" rel="stylesheet"> --}}
        <!-- slicknav CSS -->
        <link href="{{ asset('css/slicknav.css') }}" rel="stylesheet">
        <!-- Style CSS -->
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <!-- responsive css -->
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
        <!-- Google Fonts CSS -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

        </head>
<style>
    .topbar .navbar-search input {
    /* font-size: .85rem; */
    height: 62px;
    margin-top: 10px;
}
</style>

        <body class="responsive">




            @foreach ($forms as $form)




           @php
$Caregiver = $form->Caregiver;
$Caregiver2 = explode(',', $Caregiver);
@endphp

           @php
$assistance = $form->assistance;
$assistance2 = explode(',', $assistance);
@endphp




           @php
$Relationship_to_Member = $form->Relationship_to_Member;
$Relationship_to_Member2 = explode(',', $Relationship_to_Member);
@endphp

           @php
$Relationship_to_memberd = $form->Relationship_to_memberd;
$Relationship_to_memberd2 = explode(',', $Relationship_to_memberd);
@endphp





            <form method="POST" action="{{route('careform_form20Update',$careform_id)}}">
                @csrf
                @method('get')
                <input type="hidden" name="careform_id" value=" {{ $careform_id }}">
                <div class="allPadding newCareForm">
             <div class="topSecform">
      <div class="container">
        <div class="formHead ">
          <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12 centerCol">
              <h2>Appendix D-4 </h2>
              <h2>RENEWAL PARTICIPANT ASSESSMENT FORM</h2>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="heading">
                <h4>Member name:</h4>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="putData">
                <input type="text" name="member_name" value="{{$form->member_name}}">
              </div>
            </div>
          </div>
        </div>
        <div class="appenCMain statusSec">
          <h6>Informal Caregiver:</h6>
          <div class="informalTop">
            <div class="row">
              <div class="col-md-7 col-sm-7 col-xs-12">
                <div class="heading">
                  <h4>Does the member have an informal support system? </h4>
                </div>
              </div>
              <div class="col-md-5 col-sm-5 col-xs-12">
                <ul class="list-inline checkboxes">
                  <li>
                    <input type="radio" name="support_system" value="Yes" @php echo ($form->support_system == 'Yes'?'checked':''); @endphp>
                    <label for="vehicle1">Yes</label>
                  </li>
                  <li>
                    <input type="radio" name="support_system" value="No" @php echo ($form->support_system == 'No'?'checked':''); @endphp>
                    <label for="vehicle2">No</label>
                  </li>
                </ul>
              </div>
            </div>
            <div class="row">
              <div class="col-md-7 col-sm-7 col-xs-12">
                <div class="heading">
                  <h4>If yes, was the informal support person present for your renewal visit? </h4>
                </div>
              </div>
              <div class="col-md-5 col-sm-5 col-xs-12">
                <ul class="list-inline checkboxes">
                  <li>
                    <input type="radio" name="renewal_visit" value="Yes" @php echo ($form->renewal_visit == 'Yes'?'checked':''); @endphp>
                    <label for="vehicle1">Yes</label>
                  </li>
                  <li>
                    <input type="radio" name="renewal_visit" value="No" @php echo ($form->renewal_visit == 'No'?'checked':''); @endphp>
                    <label for="vehicle2">No</label>
                  </li>
                </ul>
              </div>
            </div>
            <div class="row">
              <div class="col-md-7 col-sm-7 col-xs-12">
                <div class="heading">
                  <h4>If yes, did the person sign the support attestation form? </h4>
                </div>
              </div>
              <div class="col-md-5 col-sm-5 col-xs-12">
                <ul class="list-inline checkboxes">
                  <li>
                    <input type="radio" name="attestation" value="Yes" @php echo ($form->attestation == 'Yes'?'checked':''); @endphp>
                    <label for="vehicle1">Yes</label>
                  </li>
                  <li>
                    <input type="radio" name="attestation" value="No" @php echo ($form->attestation == 'No'?'checked':''); @endphp>
                    <label for="vehicle2">No</label>
                  </li>
                </ul>
              </div>
            </div>
            <p>If you answered “no” to any of the above questions, then explain: </p>
            <textarea rows="3" name="explain"></textarea>
          </div>
          <div class="informal">
            <p><i>Informal Caregiver Status (check all that apply):</i></p>
            <div class="chkBxsList">
              <ul class="list-inline checkboxes">
                <li>
                  <input type="checkbox" name="Caregiver[]" value="1" @php echo (in_array("1", $Caregiver2)?'checked':''); @endphp>
                  <label for="vehicle1">Caregiver is unable to continue in caring activities (e.g. decline in health of care giver makes it difficult to continue). </label>
                </li>
                <li>
                  <input type="checkbox" name="Caregiver[]" value="2" @php echo (in_array("2", $Caregiver2)?'checked':''); @endphp>
                  <label for="vehicle2">Primary caregiver is not satisfied with support received from family and friends (e.g. other children of individual).</label>
                </li>
                <li>
                  <input type="checkbox" name="Caregiver[]" value="3" @php echo (in_array("3", $Caregiver2)?'checked':''); @endphp>
                  <label for="vehicle2">Primary caregiver expresses feelings of distress, anger or depression</label>
                </li>
                <li>
                  <input type="checkbox" name="Caregiver[]" value="4" @php echo (in_array("4", $Caregiver2)?'checked':''); @endphp>
                  <label for="vehicle2">Caregiver expresses feelings of frustration with current PSS staffing</label>
                </li>
                <li>
                  <input type="checkbox" name="Caregiver[]" value="5" @php echo (in_array("5", $Caregiver2)?'checked':''); @endphp>
                  <label for="vehicle1">No issues</label>
                </li>
                <li>
                  <input type="checkbox" name="Caregiver[]" value="6" @php echo (in_array("6", $Caregiver2)?'checked':''); @endphp>
                  <label for="vehicle2">Other</label>
                </li>
              </ul>
            </div>
          </div>
          <div class="informalCare">
            <p>How often does the Member receive assistance from the Primary Informal caregiver?</p>
            <div class="chkBxsList incontinenceSec">
              <ul class="list-inline checkboxes">
                <li>
                  <input type="checkbox" name="assistance[]" value="1" @php echo (in_array("1", $assistance2)?'checked':''); @endphp>
                  <label for="vehicle1">Several time during the day and night </label>
                </li>
                <li>
                  <input type="checkbox" name="assistance[]" value="2" @php echo (in_array("2", $assistance2)?'checked':''); @endphp>
                  <label for="vehicle2">Once daily </label>
                </li>
                <li>
                  <input type="checkbox" name="assistance[]" value="3" @php echo (in_array("3", $assistance2)?'checked':''); @endphp>
                  <label for="vehicle2">1-2 times per week</label>
                </li>
                <li>
                  <input type="checkbox" name="assistance[]" value="4" @php echo (in_array("4", $assistance2)?'checked':''); @endphp>
                  <label for="vehicle2">Several times during the day</label>
                </li>
                <li>
                  <input type="checkbox" name="assistance[]" value="5" @php echo (in_array("5", $assistance2)?'checked':''); @endphp>
                  <label for="vehicle1">3 or more times per week </label>
                </li>
                <li>
                  <input type="checkbox" name="assistance[]" value="6" @php echo (in_array("6", $assistance2)?'checked':''); @endphp>
                  <label for="vehicle2">Less often than weekly</label>
                </li>
              </ul>
            </div>
          </div>

          <div class="informalSupport">
            <h6>LIST MEMBER’S INFORMAL SUPPORT: </h6>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="putData">
                  <input type="text" name="support" value="{{$form->support}}">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-7 col-sm-7 col-xs-12">
                <div class="heading">
                  <h4> Lives with Member?</h4>
                </div>
              </div>
              <div class="col-md-5 col-sm-5 col-xs-12">
                <ul class="list-inline checkboxes">
                  <li>
                    <input type="radio" name="lives_Member" value="Yes" @php echo ($form->lives_Member == 'Yes'?'checked':''); @endphp>
                    <label for="vehicle1">Yes</label>
                  </li>
                  <li>
                    <input type="radio" name="lives_Member" value="No" @php echo ($form->lives_Member == 'No'?'checked':''); @endphp>
                    <label for="vehicle2">No</label>
                  </li>
                </ul>
              </div>
            </div>
            <div class="row">
              <div class="col-md-5 col-sm-5 col-xs-12">
                <div class="heading">
                  <h4> Relationship to Member:</h4>
                </div>
              </div>
              <div class="col-md-7 col-sm-7 col-xs-12">
                <ul class="list-inline checkboxes">
                  <li>
                    <input type="checkbox" name="Relationship_to_Member[]" value="1" @php echo (in_array("1", $Relationship_to_Member2)?'checked':''); @endphp>
                    <label for="vehicle1">Child</label>
                  </li>
                  <li>
                    <input type="checkbox" name="Relationship_to_Member[]" value="2" @php echo (in_array("2", $Relationship_to_Member2)?'checked':''); @endphp>
                    <label for="vehicle2">Spouse</label>
                  </li>
                  <li>
                    <input type="checkbox" name="Relationship_to_Member[]" value="3" @php echo (in_array("3", $Relationship_to_Member2)?'checked':''); @endphp>
                    <label for="vehicle2">Friend/Neighbour</label>
                  </li>
                  <li>
                    <input type="checkbox" name="Relationship_to_Member[]" value="4" @php echo (in_array("4", $Relationship_to_Member2)?'checked':''); @endphp>
                    <label for="vehicle2">Parent</label>
                  </li>
                </ul>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="putData">
                  <input type="text" name="">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-7 col-sm-7 col-xs-12">
                <div class="heading">
                  <h4> Lives with Member?</h4>
                </div>
              </div>
              <div class="col-md-5 col-sm-5 col-xs-12">
                <ul class="list-inline checkboxes">
                  <li>
                    <input type="radio" name="Lives_with_Member" value="Yes" @php echo ($form->Lives_with_Member == 'Yes'?'checked':''); @endphp>
                    <label for="vehicle1">Yes</label>
                  </li>
                  <li>
                    <input type="radio" name="Lives_with_Member" value="No" @php echo ($form->Lives_with_Member == 'No'?'checked':''); @endphp>
                    <label for="vehicle2">No</label>
                  </li>
                </ul>
              </div>
            </div>
            <div class="row">
              <div class="col-md-5 col-sm-5 col-xs-12">
                <div class="heading">
                  <h4> Relationship to Member:</h4>
                </div>
              </div>
              <div class="col-md-7 col-sm-7 col-xs-12">
                <ul class="list-inline checkboxes">
                  <li>
                    <input type="checkbox" name="Relationship_to_memberd[]" value="1" @php echo (in_array("1", $Relationship_to_memberd2)?'checked':''); @endphp>
                    <label for="vehicle1">Child</label>
                  </li>
                  <li>
                    <input type="checkbox" name="Relationship_to_memberd[]" value="2" @php echo (in_array("2", $Relationship_to_memberd2)?'checked':''); @endphp>
                    <label for="vehicle2">Spouse</label>
                  </li>
                  <li>
                    <input type="checkbox" name="Relationship_to_memberd[]" value="3" @php echo (in_array("3", $Relationship_to_memberd2)?'checked':''); @endphp>
                    <label for="vehicle2">Friend/Neighbour</label>
                  </li>
                  <li>
                    <input type="checkbox" name="Relationship_to_memberd[]" value="4" @php echo (in_array("4", $Relationship_to_memberd2)?'checked':''); @endphp>
                    <label for="vehicle2">Parent</label>
                  </li>
                </ul>
              </div>
            </div>
            <p><i>By signing below, I agree that I will continue to follow the Independent Care Waiver program’s policies and procedures. I agree that if I have concerns or questions about the program, I will seek clarification from my case manager. If I am utilizing the Consumer Directed care option, I agree to follow the CDC’s policy and procedures as well as maintain my budget as approved. </i></p>
          </div>
          <div class="signSec">
            <div class="row">
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="heading">
                  <h4>Client/Representative Signature</h4>
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="putData">
                  <input type="text" name="Signature" value="{{$form->Signature}}">
                </div>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-12">
              </div>
              <div class="col-md-1 col-sm-1 col-xs-12">
                <div class="heading">
                  <h4>Date:</h4>
                </div>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-12">
                <div class="putData">
                  <input type="text" name="Date" value="{{$form->Date}}">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="pages">
      <div class="container">
        <ul class="list-inline">
            <input type="Submit" value="Next" class="submitbtnsave">
        <li><a href="{{route('careform_form19index',$careform_id)}}">Back</a></li>

        </ul>
      </div>
    </div>
  </div>
            </form>
            @endforeach

          </body>


@endsection


<script>
    $('#datatable_tabletools').DataTable({
        dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete : function(oSettings, json) {
            $(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
                $(this).addClass('btn-sm btn-default');
            });
        },
        "language": {
            "search": "Search: ",
            "searchPlaceholder": "Search records"
        },
        "pageLength": 50,
        "bDestroy": true,

        "order": []
    });
    /* END TABLE TOOLS */

    function ReportError(res)
    {
    }// Filter placeholder dataTable
    $(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>

<script src="{{ asset('js/all.js') }}""></script>
<!-- jquery.slicknav JS -->
<script src="{{ asset('js/jquery.slicknav.js') }}""></script>
<!-- Custom JS -->
<script src="{{ asset('js/custom.js') }}""></script>
