


@extends('layouts.masterlayout')

@section('content')
    {{-- <a href="{{route('careform.create_form',$clientId)}}"" class="btn btn-success fa-pull-right" style="margin-right: 20px;">Create Form</a> --}}
    <br>
    <br>


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>New Care form</title>
        <!-- All CSS -->
        {{-- <link href="{{ asset('css/all.css') }}" rel="stylesheet"> --}}
        <!-- slicknav CSS -->
        <link href="{{ asset('css/slicknav.css') }}" rel="stylesheet">
        <!-- Style CSS -->
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <!-- responsive css -->
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
        <!-- Google Fonts CSS -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

        </head>
<style>
    .topbar .navbar-search input {
    /* font-size: .85rem; */
    height: 62px;
    margin-top: 10px;
}
</style>

        <body class="responsive">




            @foreach ($forms as $form)




           @php
$safety_hazards = $form->safety_hazards;
$safety_hazards2 = explode(',', $safety_hazards);
@endphp

@php
$sanitation_hazards2 = $form->sanitation_hazards;
$sanitation_hazards = explode(',', $sanitation_hazards2);
@endphp

@php
$social_functioning2 = $form->social_functioning;
$social_functioning = explode(',', $social_functioning2);
@endphp

@php
$isolation2 = $form->isolation;
$isolation = explode(',', $isolation2);
@endphp









            <form method="POST" action="{{route('careform_form19Update',$careform_id)}}">
                @csrf
                @method('get')
                <input type="hidden" name="careform_id" value=" {{ $careform_id }}">
                <div class="allPadding newCareForm">
                    <div class="topSecform">
                      <div class="container">
                        <div class="formHead ">
                          <div class="row">
                            <div class="col-md-9 col-sm-9 col-xs-12 centerCol">
                              <h2>Appendix D-4 </h2>
                              <h2>RENEWAL PARTICIPANT ASSESSMENT FORM</h2>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-2 col-sm-2 col-xs-12">
                              <div class="heading">
                                <h4>Member name:</h4>
                              </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                              <div class="putData">
                                <input type="text" name="member_name">
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="appenCMain statusSec">
                          <div class="saftySec">
                            <h6>ENVIRONMENTAL:</h6>
                            <h6>SAFETY HAZARDS:</h6>
                            <div class="chkBxsList incontinenceSec">
                              <ul class="list-inline checkboxes">
                                <li>
                                  <input type="checkbox" name="safety_hazards[]" value="1" @php echo (in_array("1", $safety_hazards2)?'checked':''); @endphp>
                                  <label for="safety_hazards">None </label>
                                </li>
                                <li>
                                  <input type="checkbox" name="safety_hazards[]" value="2" @php echo (in_array("2", $safety_hazards2)?'checked':''); @endphp>
                                  <label for="safety_hazards">Inadequate floor/ roof/ windows</label>
                                </li>
                                <li>
                                  <input type="checkbox" name="safety_hazards[]" value="3" @php echo (in_array("3", $safety_hazards2)?'checked':''); @endphp>
                                  <label for="safety_hazards">Unsafe gas/electric appliance </label>
                                </li>
                                <li>
                                  <input type="checkbox" name="safety_hazards[]" value="4" @php echo (in_array("4", $safety_hazards2)?'checked':''); @endphp>
                                  <label for="safety_hazards"> lack of safety devices</label>
                                </li>
                                <li>
                                  <input type="checkbox" name="safety_hazards[]" value="5" @php echo (in_array("5", $safety_hazards2)?'checked':''); @endphp>
                                  <label for="safety_hazards">Inadequate heating  </label>
                                </li>
                                <li>
                                  <input type="checkbox" name="safety_hazards[]" value="6" @php echo (in_array("6", $safety_hazards2)?'checked':''); @endphp>
                                  <label for="safety_hazards">Inadequate cooling</label>
                                </li>
                                <li>
                                  <input type="checkbox" name="safety_hazards[]" value="7" @php echo (in_array("7", $safety_hazards2)?'checked':''); @endphp>
                                  <label for="safety_hazards">Unsafe floor coverings</label>
                                </li>
                                <li>
                                  <input type="checkbox" name="safety_hazards[]" value="8" @php echo (in_array("8", $safety_hazards2)?'checked':''); @endphp>
                                  <label for="safety_hazards">Inadequate stair railings</label>
                                </li>
                                <li>
                                  <input type="checkbox" name="safety_hazards[]" value="9" @php echo (in_array("9", $safety_hazards2)?'checked':''); @endphp>
                                  <label for="safety_hazards">Other</label>
                                </li>
                              </ul>
                            </div>
                            <h6>Comments:</h6>
                            <textarea rows="2" name="comment1" value="{{ $form->comment1 }}">{{ $form->comment1 }}</textarea>

                          </div>
                          <h6>SANITATION HAZARDS:</h6>
                          <div class="chkBxsList incontinenceSec">
                            <ul class="list-inline checkboxes">
                              <li>
                                <input type="checkbox" name="sanitation_hazards[]" value="1" @php echo (in_array("1", $sanitation_hazards)?'checked':''); @endphp>
                                <label for="sanitation_hazards">None </label>
                              </li>
                              <li>
                                <input type="checkbox" name="sanitation_hazards[]" value="2" @php echo (in_array("2", $sanitation_hazards)?'checked':''); @endphp>
                                <label for="sanitation_hazards">No running water </label>
                              </li>
                              <li>
                                <input type="checkbox" name="sanitation_hazards[]" value="3" @php echo (in_array("3", $sanitation_hazards)?'checked':''); @endphp>
                                <label for="sanitation_hazards">Inadequate sewage disposal</label>
                              </li>
                              <li>
                                <input type="checkbox" name="sanitation_hazards[]" value="4" @php echo (in_array("4", $sanitation_hazards)?'checked':''); @endphp>
                                <label for="sanitation_hazards">No schedule trash pick-up</label>
                              </li>
                              <li>
                                <input type="checkbox" name="sanitation_hazards[]" value="5" @php echo (in_array("5", $sanitation_hazards)?'checked':''); @endphp>
                                <label for="sanitation_hazards">Contaminated water</label>
                              </li>
                              <li>
                                <input type="checkbox" name="sanitation_hazards[]" value="6" @php echo (in_array("6", $sanitation_hazards)?'checked':''); @endphp>
                                <label for="sanitation_hazards">Inadequate/improper food storage </label>
                              </li>
                              <li>
                                <input type="checkbox" name="sanitation_hazards[]" value="7" @php echo (in_array("7", $sanitation_hazards)?'checked':''); @endphp>
                                <label for="sanitation_hazards">Cluttered/soiled living area</label>
                              </li>
                              <li>
                                <input type="checkbox" name="sanitation_hazards[]" value="8" @php echo (in_array("8", $sanitation_hazards)?'checked':''); @endphp>
                                <label for="sanitation_hazards">No toileting facility</label>
                              </li>
                              <li>
                                <input type="checkbox" name="sanitation_hazards[]" value="9" @php echo (in_array("9", $sanitation_hazards)?'checked':''); @endphp>
                                <label for="sanitation_hazards">No cooking facility </label>
                              </li>
                              <li>
                                <input type="checkbox" name="sanitation_hazards[]" value="10" @php echo (in_array("10", $sanitation_hazards)?'checked':''); @endphp>
                                <label for="sanitation_hazards">Insects/ rodents present</label>
                              </li>
                              <li>
                                <input type="checkbox" name="sanitation_hazards[]" value="11" @php echo (in_array("11", $sanitation_hazards)?'checked':''); @endphp>
                                <label for="sanitation_hazards">Other</label>
                              </li>
                            </ul>
                          </div>
                          <h6>Comments:</h6>
                          <textarea rows="2" name="comment2" value="{{ $form->comment2 }}">{{ $form->comment2 }}</textarea>
                          <div class="socialFun">
                            <h6>Social functioning:</h6>
                            <p>In the past year has there been a decline in the member’s level of participation in social, religious, occupational or other preferred activities? </p>
                            <div class="chkBxsList incontinenceSec">
                              <ul class="list-inline checkboxes">
                                <li>
                                  <input type="checkbox" name="social_functioning[]" value="1" @php echo (in_array("1", $social_functioning)?'checked':''); @endphp>
                                  <label for="social_functioning">No </label>
                                </li>
                                <li>
                                  <input type="checkbox" name="social_functioning[]" value="2"  @php echo (in_array("2", $social_functioning)?'checked':''); @endphp>
                                  <label for="social_functioning">Decline, but individual is not distressed</label>
                                </li>
                                <li>
                                  <input type="checkbox" name="social_functioning[]" value="3"  @php echo (in_array("3", $social_functioning)?'checked':''); @endphp>
                                  <label for="social_functioning">Decline, and individual is distressed</label>
                                </li>
                                <li>
                                  <input type="checkbox" name="social_functioning[]" value="4" @php echo (in_array("4", $social_functioning)?'checked':''); @endphp>
                                  <label for="social_functioning">Other</label>
                                </li>
                              </ul>
                            </div>
                          </div>
                          <div class=" socialFun isolation">
                            <h6>Isolation:</h6>
                            <p>How often is the member alone during the day?</p>
                            <div class="chkBxsList incontinenceSec">
                              <ul class="list-inline checkboxes">
                                <li>
                                  <input type="checkbox" name="isolation[]" value="1" @php echo (in_array("1", $isolation)?'checked':''); @endphp>
                                  <label for="isolation">Never or Seldom </label>
                                </li>
                                <li>
                                  <input type="checkbox" name="isolation[]" value="2"  @php echo (in_array("2", $isolation)?'checked':''); @endphp>
                                  <label for="isolation">About 1 hour </label>
                                </li>
                                <li>
                                  <input type="checkbox" name="isolation[]" value="3"  @php echo (in_array("3", $isolation)?'checked':''); @endphp>
                                  <label for="isolation">Long periods of time</label>
                                </li>
                                <li>
                                  <input type="checkbox" name="isolation[]" value="4" @php echo (in_array("4", $isolation)?'checked':''); @endphp>
                                  <label for="isolation">All the time</label>
                                </li>
                              </ul>
                            </div>
                            <p>Is the member involved in community activities (ex: church, shopping, social activities)?</p>
                            <div class="chkBxsList incontinenceSec">
                              <ul class="list-inline checkboxes">
                                <li>
                                  <input type="radio" name="community_activities" value="Yes" @php echo ($form->community_activities == 'Yes'?'checked':''); @endphp>
                                  <label for="community_activities">Yes </label>
                                </li>
                                <li>
                                  <input type="radio" name="community_activities" value="No" @php echo ($form->community_activities == 'No'?'checked':''); @endphp>
                                  <label for="community_activities">No </label>
                                </li>
                              </ul>
                            </div>
                          </div>
                          <h6>Comments:</h6>
                          <textarea rows="5" name="comment3" value="{{ $form->comment3 }}">{{ $form->comment3 }}</textarea>
                        </div>
                      </div>
                    </div>
                    <div class="pages">
                      <div class="container">
                        <ul class="list-inline">
                            <input type="Submit" value="Next" class="submitbtnsave">
                            <li><a href="{{route('careform_form18index',$careform_id)}}">Back</a></li>

                        </ul>
                      </div>
                    </div>
                  </div>

            </form>
            @endforeach

          </body>


@endsection


<script>
    $('#datatable_tabletools').DataTable({
        dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete : function(oSettings, json) {
            $(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
                $(this).addClass('btn-sm btn-default');
            });
        },
        "language": {
            "search": "Search: ",
            "searchPlaceholder": "Search records"
        },
        "pageLength": 50,
        "bDestroy": true,

        "order": []
    });
    /* END TABLE TOOLS */

    function ReportError(res)
    {
    }// Filter placeholder dataTable
    $(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>

<script src="{{ asset('js/all.js') }}""></script>
<!-- jquery.slicknav JS -->
<script src="{{ asset('js/jquery.slicknav.js') }}""></script>
<!-- Custom JS -->
<script src="{{ asset('js/custom.js') }}""></script>
