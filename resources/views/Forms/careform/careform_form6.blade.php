


@extends('layouts.masterlayout')

@section('content')
    {{-- <a href="{{route('careform.create_form',$clientId)}}"" class="btn btn-success fa-pull-right" style="margin-right: 20px;">Create Form</a> --}}
    <br>
    <br>


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>New Care form</title>
        <!-- All CSS -->
        {{-- <link href="{{ asset('css/all.css') }}" rel="stylesheet"> --}}
        <!-- slicknav CSS -->
        <link href="{{ asset('css/slicknav.css') }}" rel="stylesheet">
        <!-- Style CSS -->
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <!-- responsive css -->
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
        <!-- Google Fonts CSS -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

        </head>
<style>
    .topbar .navbar-search input {
    /* font-size: .85rem; */
    height: 62px;
    margin-top: 10px;
}
</style>

        <body class="responsive">




            @foreach ($forms as $form)




            <form method="POST" action="{{route('careform_form6Update',$careform_id)}}">
                @csrf
                @method('get')
                <input type="hidden" name="careform_id" value=" {{ $careform_id }}">
                <div class="allPadding newCareForm">
    <div class="topSecform">
      <div class="container">
        <div class="formHead ">
          <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12 centerCol">
              <h2>Independent Care Waiver Program <br>Community Care Path Signature Sheet</h2>
            </div>
          </div>
        </div>
        <div class="sigSheetSec">
          <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="heading">
                <h4>Member Name:</h4>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="putData">
                <input type="text" name="name_of_member" value="{{ $form->name_of_member}}">
              </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12"></div>
            <div class="col-md-1 col-sm-1 col-xs-12">
              <div class="heading">
                <h4>D.O.B:</h4>
              </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="putData">
                <input type="text" name="dob" value="{{ $form->dob}}">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="heading">
                <h4>Medicaid #:</h4>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="putData">
                <input type="text" name="medicaid" value="{{ $form->medicaid}}">
              </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12"></div>
            <div class="col-md-1 col-sm-1 col-xs-12">
              <div class="heading">
                <h4>Date:</h4>
              </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="putData">
                <input type="text" name="date" value="{{ $form->date}}">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="heading">
                <h4>Case Manager Name:</h4>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="putData">
                <input type="text" name="case_manager_name" value="{{ $form->case_manager_name}}">
              </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12"></div>
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="heading">
                <h4>Signature:</h4>
              </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="putData">
                <input type="text" name="signature" value="{{ $form->signature}}">
              </div>
            </div>
          </div>
          <p><i>By signing and initialing the lines below we are acknowledging that my plan of care and goals have been discussed between the member/caregiver and case manager during the quarterly visits conducted per DCH/ICWP policy and we are in agreement. </i></p>
          <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="heading">
                <h4>First Quarter:</h4>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="putData">
                <input type="text" name="first_quarter" value="{{ $form->first_quarter}}">
              </div>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-12">
              <div class="heading">
                <h4>Date:</h4>
              </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="putData">
                <input type="text" name="first_quarter_date" value="{{ $form->first_quarter_date}}">
              </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="heading">
                <h4>CM Initials:</h4>
              </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="putData">
                <input type="text" name="first_quarter_cm" value="{{ $form->first_quarter_cm}}">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="heading">
                <h4>Second Quarter:</h4>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="putData">
                <input type="text" name="second_quarter" value="{{ $form->second_quarter}}">
              </div>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-12">
              <div class="heading">
                <h4>Date:</h4>
              </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="putData">
                <input type="text" name="second_quarter_date" value="{{ $form->second_quarter_date}}">
              </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="heading">
                <h4>CM Initials:</h4>
              </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="putData">
                <input type="text" name="second_quarter_cm" value="{{ $form->second_quarter_cm}}">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="heading">
                <h4>Third Quarter:</h4>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="putData">
                <input type="text" name="third_quarter" value="{{ $form->third_quarter}}">
              </div>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-12">
              <div class="heading">
                <h4>Date:</h4>
              </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="putData">
                <input type="text" name="third_quarter_date" value="{{ $form->third_quarter_date}}">
              </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="heading">
                <h4>CM Initials:</h4>
              </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="putData">
                <input type="text" name="third_quarter_cm" value="{{ $form->third_quarter_cm}}">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="heading">
                <h4>Fourth Quarter:</h4>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="putData">
                <input type="text" name="forth_quarter" value="{{ $form->forth_quarter}}">
              </div>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-12">
              <div class="heading">
                <h4>Date:</h4>
              </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="putData">
                <input type="text" name="forth_quarter_date" value="{{ $form->forth_quarter_date}}">
              </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="heading">
                <h4>CM Initials:</h4>
              </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="putData">
                <input type="text" name="forth_quarter_cm" value="{{ $form->forth_quarter_cm}}">
              </div>
            </div>
          </div>
          <h6><b><i>***Obtain signatures every quarter and forward to GMCF with Annual Renewal. Enter the Carepath and Quarterly information into the web portal.</i></b></h6>
        </div>
      </div>
    </div>
    <div class="pages">
      <div class="container">
        <ul class="list-inline">
            <input type="Submit" value="Next" class="submitbtnsave">
        <li><a href="{{route('careform_form5index',$careform_id)}}">Back</a></li>

        </ul>
      </div>
    </div>
  </div>
            </form>
            @endforeach

          </body>


@endsection


<script>
    $('#datatable_tabletools').DataTable({
        dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete : function(oSettings, json) {
            $(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
                $(this).addClass('btn-sm btn-default');
            });
        },
        "language": {
            "search": "Search: ",
            "searchPlaceholder": "Search records"
        },
        "pageLength": 50,
        "bDestroy": true,

        "order": []
    });
    /* END TABLE TOOLS */

    function ReportError(res)
    {
    }// Filter placeholder dataTable
    $(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>

<script src="{{ asset('js/all.js') }}""></script>
<!-- jquery.slicknav JS -->
<script src="{{ asset('js/jquery.slicknav.js') }}""></script>
<!-- Custom JS -->
<script src="{{ asset('js/custom.js') }}""></script>
