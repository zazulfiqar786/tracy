


@extends('layouts.masterlayout')

@section('content')
    {{-- <a href="{{route('careform.create_form',$clientId)}}"" class="btn btn-success fa-pull-right" style="margin-right: 20px;">Create Form</a> --}}
    <br>
    <br>


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>New Care form</title>
        <!-- All CSS -->
        {{-- <link href="{{ asset('css/all.css') }}" rel="stylesheet"> --}}
        <!-- slicknav CSS -->
        <link href="{{ asset('css/slicknav.css') }}" rel="stylesheet">
        <!-- Style CSS -->
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <!-- responsive css -->
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
        <!-- Google Fonts CSS -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

        </head>
<style>
    .topbar .navbar-search input {
    /* font-size: .85rem; */
    height: 62px;
    margin-top: 10px;
}
</style>

        <body class="responsive">




            @foreach ($forms as $form)





            <form method="POST" action="{{route('careform_form21Update',$careform_id)}}">
                @csrf
                @method('get')
                <input type="hidden" name="careform_id" value=" {{ $careform_id }}">
                <div class="allPadding newCareForm">


                  <div class="topSecform">
      <div class="container">
        <div class="formHead ">
          <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12 centerCol">
              <h2>Appendix D-4 </h2>
              <h2>RENEWAL PARTICIPANT ASSESSMENT FORM</h2>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="heading">
                <h4>Member name:</h4>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="putData">
                <input type="text" name="member_name" value="{{$form->member_name}}">
              </div>
            </div>
          </div>
        </div>
        <div class="appenCMain statusSec">
          <h6>Narrative Summary:</h6>
          <textarea rows="30" name="Summary">{{$form->Summary}}</textarea>
          <div class="signSec">
            <div class="row">
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="heading">
                  <h4>Case Manager Signature: </h4>
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="putData">
                  <input type="text" name="Signature" value="{{$form->Signature}}">
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12">
              </div>
              <div class="col-md-1 col-sm-1 col-xs-12">
                <div class="heading">
                  <h4>Date:</h4>
                </div>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-12">
                <div class="putData">
                  <input type="text" name="Date" value="{{$form->Date}}">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="pages">
      <div class="container">
        <ul class="list-inline">
            <input type="Submit" value="Next" class="submitbtnsave">
        <li><a href="{{route('careform_form20index',$careform_id)}}">Back</a></li>

        </ul>
      </div>
    </div>
  </div>
            </form>
            @endforeach

          </body>


@endsection


<script>
    $('#datatable_tabletools').DataTable({
        dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete : function(oSettings, json) {
            $(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
                $(this).addClass('btn-sm btn-default');
            });
        },
        "language": {
            "search": "Search: ",
            "searchPlaceholder": "Search records"
        },
        "pageLength": 50,
        "bDestroy": true,

        "order": []
    });
    /* END TABLE TOOLS */

    function ReportError(res)
    {
    }// Filter placeholder dataTable
    $(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>

<script src="{{ asset('js/all.js') }}""></script>
<!-- jquery.slicknav JS -->
<script src="{{ asset('js/jquery.slicknav.js') }}""></script>
<!-- Custom JS -->
<script src="{{ asset('js/custom.js') }}""></script>
