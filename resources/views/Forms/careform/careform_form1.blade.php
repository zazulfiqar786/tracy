


@extends('layouts.masterlayout')

@section('content')
    {{-- <a href="{{route('careform.create_form',$clientId)}}" class="btn btn-success fa-pull-right" style="margin-right: 20px;">Create Form</a> --}}
    <br>
    <br>


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>New Care form</title>
        <!-- All CSS -->
        {{-- <link href="{{ asset('css/all.css') }}" rel="stylesheet"> --}}
        <!-- slicknav CSS -->
        <link href="{{ asset('css/slicknav.css') }}" rel="stylesheet">
        <!-- Style CSS -->
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <!-- responsive css -->
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
        <!-- Google Fonts CSS -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

        </head>

    <body class="responsive">
        @foreach ($forms as $form)

        @php
        $careform_form1_miscellaneous = $form->careform_form1_miscellaneous;
        $careform_form1_miscellaneous2 = explode(',', $careform_form1_miscellaneous);
        @endphp

@php
$careform_form1_wheelchair = $form->careform_form1_wheelchair;
$careform_form1_wheelchair2 = explode(',', $careform_form1_wheelchair);
@endphp

@php
$careform_form1_assistive = $form->careform_form1_assistive;
$careform_form1_assistive2 = explode(',', $careform_form1_assistive);
@endphp

@php
$careform_form1_respiratory = $form->careform_form1_respiratory;
$careform_form1_respiratory2 = explode(',', $careform_form1_respiratory);
@endphp

@php
$careform_form1_mobility = $form->careform_form1_mobility;
$careform_form1_mobility2 = explode(',', $careform_form1_mobility);
@endphp

@php
$careform_form1_bathroom = $form->careform_form1_bathroom;
$careform_form1_bathroom2 = explode(',', $careform_form1_bathroom);
@endphp

@php
$careform_form1_beds = $form->careform_form1_beds;
$careform_form1_beds2 = explode(',', $careform_form1_beds);
@endphp


@php
$careform_form1_transfer = $form->careform_form1_transfer;
$careform_form1_transfer2 = explode(',', $careform_form1_transfer);
@endphp




            <form method="POST" action="{{route('careform_form1Update',$careform_id)}}">
            @csrf
            @method('get')
            <input type="hidden" name="careform_id" value=" {{ $careform_id }}">
        <div class="allPadding newCareForm">
          <div class="topSecform">
            <div class="container">
              <div class="formHead ">
                <div class="row">
                  <div class="col-md-8 col-sm-8 col-xs-12 centerCol">
                    <h2>Appendix H-2</h2>
                    <h2>EQUIPMENT AND SUPPLY SHEET</h2>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="heading">
                      <h4><b>Equipment LIST FOR:</b></h4>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="putData">
                      <input type="text" name="careform_form1_equipment" value=" {{ $careform_form1_equipment=$form->careform_form1_equipment }}">
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12"></div>
                  <div class="col-md-1 col-sm-1 col-xs-12">
                    <div class="heading">
                      <h4><b>DATE:</b></h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12">
                    <div class="putData">
                      <input type="text" name="careform_form1_equipment_date" value="{{ $careform_form1_equipment=$form->careform_form1_equipment_date }}">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="newCareMain">
                    <h4>Miscellaneous:</h4>
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox" name="careform_form1_miscellaneous[]" @php echo (in_array("1", $careform_form1_miscellaneous2)?'checked':''); @endphp value="1">
                        <label for="vehicle1">Electrical stimulation unit</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_miscellaneous[]" @php echo (in_array("2", $careform_form1_miscellaneous2)?'checked':''); @endphp  value="2">
                        <label for="vehicle2">Portable Ramps</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_miscellaneous[]" @php echo (in_array("3", $careform_form1_miscellaneous2)?'checked':''); @endphp  value="3">
                        <label for="vehicle2">Augmented communication devices</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_miscellaneous[]" @php echo (in_array("4", $careform_form1_miscellaneous2)?'checked':''); @endphp  value="4">
                        <label for="vehicle2">Reacher</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_miscellaneous[]" @php echo (in_array("5", $careform_form1_miscellaneous2)?'checked':''); @endphp  value="5">
                        <label for="vehicle2">Shoe horn</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_miscellaneous[]" @php echo (in_array("6", $careform_form1_miscellaneous2)?'checked':''); @endphp  value="6">
                        <label for="vehicle2">Sponge</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_miscellaneous[]" @php echo (in_array("7", $careform_form1_miscellaneous2)?'checked':''); @endphp  value="7">
                        <label for="vehicle2">Dressing stick</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_miscellaneous[]" @php echo (in_array("8", $careform_form1_miscellaneous2)?'checked':''); @endphp  value="8">
                        <label for="vehicle2">Sock aid</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="newCareMain">
                    <h4>Wheelchair Accessories</h4>
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox" name="careform_form1_wheelchair[]" @php echo (in_array("1", $careform_form1_wheelchair2)?'checked':''); @endphp value="1">
                        <label for="vehicle1">Manual Wheelchair</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_wheelchair[]"  @php echo (in_array("2", $careform_form1_wheelchair2)?'checked':''); @endphp  value="2">
                        <label for="vehicle2">Power Wheelchair</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_wheelchair[]"  @php echo (in_array("3", $careform_form1_wheelchair2)?'checked':''); @endphp  value="3">
                        <label for="vehicle2">Tilt in space wheelchair</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_wheelchair[]"   @php echo (in_array("4", $careform_form1_wheelchair2)?'checked':''); @endphp value="4">
                        <label for="vehicle2">Wheelchair Cushion</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_wheelchair[]"  @php echo (in_array("5", $careform_form1_wheelchair2)?'checked':''); @endphp  value="5">
                        <label for="vehicle2">Wheelchair Lap Tray</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_wheelchair[]"  @php echo (in_array("6", $careform_form1_wheelchair2)?'checked':''); @endphp  value="6">
                        <label for="vehicle2">Lift Chair</label>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="newCareMain">
                    <h4>Assistive Devices for Increased Function:</h4>
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox" name="careform_form1_assistive[]"  @php echo (in_array("1", $careform_form1_assistive2)?'checked':''); @endphp  value="1">
                        <label for="vehicle1">Specialty Phone</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_assistive[]" @php echo (in_array("2", $careform_form1_assistive2)?'checked':''); @endphp  value="2">
                        <label for="vehicle2">Mouth stick </label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_assistive[]" @php echo (in_array("3", $careform_form1_assistive2)?'checked':''); @endphp  value="3">
                        <label for="vehicle2">Plate Guard </label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_assistive[]" @php echo (in_array("4", $careform_form1_assistive2)?'checked':''); @endphp value="4">
                        <label for="vehicle2">Specialty Utensils </label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_assistive[]" @php echo (in_array("5", $careform_form1_assistive2)?'checked':''); @endphp  value="5">
                        <label for="vehicle2">Over the Stove Mirror</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="newCareMain">
                    <h4>Respiratory Equipment</h4>
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox" name="careform_form1_respiratory[]" @php echo (in_array("1", $careform_form1_respiratory2)?'checked':''); @endphp  value="1">
                        <label for="vehicle1">Ventilator</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_respiratory[]" @php echo (in_array("2", $careform_form1_respiratory2)?'checked':''); @endphp  value="2">
                        <label for="vehicle2">In-exsufflator (cough assist)</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_respiratory[]" @php echo (in_array("3", $careform_form1_respiratory2)?'checked':''); @endphp  value="3">
                        <label for="vehicle2">Suction machine</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_respiratory[]" @php echo (in_array("4", $careform_form1_respiratory2)?'checked':''); @endphp  value="4">
                        <label for="vehicle2">Ambu- bag</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_respiratory[]" @php echo (in_array("5", $careform_form1_respiratory2)?'checked':''); @endphp  value="5">
                        <label for="vehicle2">Other</label>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="newCareMain">
                    <h4>Mobility Assistance Devices:</h4>
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox" name="careform_form1_mobility[]" @php echo (in_array("1", $careform_form1_mobility2)?'checked':''); @endphp   value="1">
                        <label for="vehicle1">Quad cane </label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_mobility[]" @php echo (in_array("2", $careform_form1_mobility2)?'checked':''); @endphp  value="2">
                        <label for="vehicle2">Standard cane </label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_mobility[]" @php echo (in_array("3", $careform_form1_mobility2)?'checked':''); @endphp  value="3">
                        <label for="vehicle2">Side Walker/ hemi walker</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_mobility[]" @php echo (in_array("4", $careform_form1_mobility2)?'checked':''); @endphp  value="4">
                        <label for="vehicle2">Lofstrand / forearm crutches</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_mobility[]" @php echo (in_array("5", $careform_form1_mobility2)?'checked':''); @endphp  value="5">
                        <label for="vehicle2">Regular Crutches </label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_mobility[]" @php echo (in_array("6", $careform_form1_mobility2)?'checked':''); @endphp  value="6">
                        <label for="vehicle2">Rolling walker </label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_mobility[]" @php echo (in_array("7", $careform_form1_mobility2)?'checked':''); @endphp  value="7">
                        <label for="vehicle2">Walker without wheels</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_mobility[]" @php echo (in_array("8", $careform_form1_mobility2)?'checked':''); @endphp  value="8">
                        <label for="vehicle2">Walker platform attachment</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_mobility[]" @php echo (in_array("9", $careform_form1_mobility2)?'checked':''); @endphp  value="9">
                        <label for="vehicle2">Scooter</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="newCareMain">
                    <h4>Bathroom Equipment:</h4>
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox" name="careform_form1_bathroom[]" @php echo (in_array("1", $careform_form1_bathroom2)?'checked':''); @endphp  value="1">
                        <label for="vehicle1">Tub grab bars</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_bathroom[]" @php echo (in_array("2", $careform_form1_bathroom2)?'checked':''); @endphp value="2">
                        <label for="vehicle2">Raised toilet seat </label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_bathroom[]" @php echo (in_array("3", $careform_form1_bathroom2)?'checked':''); @endphp value="3">
                        <label for="vehicle2">Bedside commode chair</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_bathroom[]" @php echo (in_array("4", $careform_form1_bathroom2)?'checked':''); @endphp value="4">
                        <label for="vehicle2">Drop arm commode</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_bathroom[]" @php echo (in_array("5", $careform_form1_bathroom2)?'checked':''); @endphp value="5">
                        <label for="vehicle2">Upright shower chair</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_bathroom[]" @php echo (in_array("6", $careform_form1_bathroom2)?'checked':''); @endphp value="6">
                        <label for="vehicle2">Shower stool </label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_bathroom[]" @php echo (in_array("7", $careform_form1_bathroom2)?'checked':''); @endphp value="7">
                        <label for="vehicle2">Reclining shower chair</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_bathroom[]" @php echo (in_array("8", $careform_form1_bathroom2)?'checked':''); @endphp value="8">
                        <label for="vehicle2">Bath transfer bench</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_bathroom[]" @php echo (in_array("9", $careform_form1_bathroom2)?'checked':''); @endphp value="9">
                        <label for="vehicle2">Bath tub safety rail </label>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="newCareMain">
                    <h4>Beds/ mattresses:</h4>
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox" name="careform_form1_beds[]" @php echo (in_array("1", $careform_form1_beds2)?'checked':''); @endphp value="1">
                        <label for="vehicle1">Hospital Bed</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_beds[]"  @php echo (in_array("2", $careform_form1_beds2)?'checked':''); @endphp value="2">
                        <label for="vehicle2"> Specialty mattress</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_beds[]" @php echo (in_array("3", $careform_form1_beds2)?'checked':''); @endphp value="3">
                        <label for="vehicle2">Safety bed rails</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_beds[]" @php echo (in_array("4", $careform_form1_beds2)?'checked':''); @endphp value="4">
                        <label for="vehicle2">Over the bed table</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_beds[]" @php echo (in_array("5", $careform_form1_beds2)?'checked':''); @endphp value="5">
                        <label for="vehicle2">Other</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="newCareMain">
                    <h4>Transfer Devices:</h4>
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox" name="careform_form1_transfer[]" @php echo (in_array("1", $careform_form1_transfer2)?'checked':''); @endphp value="1">
                        <label for="vehicle1">Sliding board</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_transfer[]"  @php echo (in_array("2", $careform_form1_transfer2)?'checked':''); @endphp  value="2">
                        <label for="vehicle2">Hoyer lift</label>
                      </li>
                      <li>
                        <input type="checkbox" name="careform_form1_transfer[]"  @php echo (in_array("3", $careform_form1_transfer2)?'checked':''); @endphp  value="3">
                        <label for="vehicle2">Gait belt</label>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="pages">
            <div class="container">
              <ul class="list-inline">
                <input type="Submit" value="Next" class="submitbtnsave">
              </ul>
            </div>
          </div>
        </div>

        </form>
        @endforeach
      </body>

@endsection


<script>
    $('#datatable_tabletools').DataTable({
        dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete : function(oSettings, json) {
            $(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
                $(this).addClass('btn-sm btn-default');
            });
        },
        "language": {
            "search": "Search: ",
            "searchPlaceholder": "Search records"
        },
        "pageLength": 50,
        "bDestroy": true,

        "order": []
    });
    /* END TABLE TOOLS */

    function ReportError(res)
    {
    }// Filter placeholder dataTable
    $(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>

<script src="{{ asset('js/all.js') }}"></script>
<!-- jquery.slicknav JS -->
<script src="{{ asset('js/jquery.slicknav.js') }}"></script>
<!-- Custom JS -->
<script src="{{ asset('js/custom.js') }}"></script>
