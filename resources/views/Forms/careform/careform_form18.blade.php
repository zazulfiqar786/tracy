


@extends('layouts.masterlayout')

@section('content')
    {{-- <a href="{{route('careform.create_form',$clientId)}}"" class="btn btn-success fa-pull-right" style="margin-right: 20px;">Create Form</a> --}}
    <br>
    <br>


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>New Care form</title>
        <!-- All CSS -->
        {{-- <link href="{{ asset('css/all.css') }}" rel="stylesheet"> --}}
        <!-- slicknav CSS -->
        <link href="{{ asset('css/slicknav.css') }}" rel="stylesheet">
        <!-- Style CSS -->
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <!-- responsive css -->
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
        <!-- Google Fonts CSS -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

        </head>
<style>
    .topbar .navbar-search input {
    /* font-size: .85rem; */
    height: 62px;
    margin-top: 10px;
}
</style>

        <body class="responsive">




            @foreach ($forms as $form)

            @php
                $based_on_your_personal = $form->based_on_your_personal;
                $based_on_your_personal2 = explode(',', $based_on_your_personal);
            @endphp

            @php
                $classify_memory = $form->classify_memory;
                $classify_memory2 = explode(',', $classify_memory);
            @endphp

            @php
                $personal_assistance = $form->personal_assistance;
                $personal_assistance2 = explode(',', $personal_assistance);
            @endphp


            <form method="POST" action="{{route('careform_form18Update',$careform_id)}}">
                @csrf
                @method('get')
                <input type="hidden" name="careform_id" value=" {{ $careform_id }}">

                <div class="allPadding newCareForm">
                    <div class="topSecform">
                      <div class="container">
                        <div class="formHead ">
                          <div class="row">
                            <div class="col-md-9 col-sm-9 col-xs-12 centerCol">
                              <h2>Appendix D-4 </h2>
                              <h2>RENEWAL PARTICIPANT ASSESSMENT FORM</h2>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-2 col-sm-2 col-xs-12">
                              <div class="heading">
                                <h4>Member name:</h4>
                              </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                              <div class="putData">
                                <input type="text"name="member_name" value="{{ $form->member_name }}">
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="appenCMain statusSec">
                          <h6>Memory Issues:</h6>
                          <p>Based on your personal knowledge, does the member appear to have difficulties in remembering things?</p>
                          <div class="chkBxsList incontinenceSec">
                            <ul class="list-inline checkboxes">
                              <li>
                                <input type="checkbox" name="based_on_your_personal[]" @php echo (in_array("1", $based_on_your_personal2)?'checked':''); @endphp  value="1">
                                <label for="">No issues </label>
                              </li>
                              <li>
                                <input type="checkbox" name="based_on_your_personal[]" @php echo (in_array("2", $based_on_your_personal2)?'checked':''); @endphp  value="2">
                                <label for="">Yes, there are issues</label>
                              </li>
                              <li>
                                <input type="checkbox" name="based_on_your_personal[]" @php echo (in_array("3", $based_on_your_personal2)?'checked':''); @endphp value="3">
                                <label for="">No personal knowledge </label>
                              </li>
                              <li>
                                <input type="checkbox" name="based_on_your_personal[]" @php echo (in_array("4", $based_on_your_personal2)?'checked':''); @endphp  value="4">
                                <label for="">Member is non-verbal</label>
                              </li>
                            </ul>
                          </div>
                          <p>If yes, would you classify memory difficulty as (choose one):</p>
                          <div class="chkBxsList difficultySec">
                            <ul class="list-inline checkboxes">
                              <li>
                                <input type="checkbox" name="classify_memory[]" @php echo (in_array("1", $classify_memory2)?'checked':''); @endphp  value="1">
                                <label for="">Short-term memory problems that, with occasional reminders, do no cause difficulty in the person performing self-care tasks.</label>
                              </li>
                              <li>
                                <input type="checkbox" name="classify_memory[]"  @php echo (in_array("2", $classify_memory2)?'checked':''); @endphp   value="2">
                                <label for="">Memory lapses that result in the person frequently not performing self-care tasks without reminders?</label>
                              </li>
                              <li>
                                <input type="checkbox" name="classify_memory[]"  @php echo (in_array("3", $classify_memory2)?'checked':''); @endphp   value="3">
                                <label for="">Memory lapses resulting in the inability to perform routine tasks on a daily basis?</label>
                              </li>
                              <li>
                                <label>Comments:</label>
                                <textarea rows="2" name="comments1" value="{{ $form->comments1 }}">{{ $form->comments1 }}</textarea>
                              </li>
                            </ul>
                          </div>
                          <h6>Behavior Issues: </h6>
                          <div class="row">
                            <div class="col-md-7 col-sm-7 col-xs-12">
                              <div class="heading">
                                <h4>Are there any behavior issues that may impact their ability to continue living in the community? </h4>
                              </div>
                            </div>
                            <div class="col-md-5 col-sm-5 col-xs-12">
                              <ul class="list-inline checkboxes">
                                <li>
                                  <input type="radio" name="behavior_issues_yes_no" value="Yes" @php echo ($form->behavior_issues_yes_no == 'Yes'?'checked':''); @endphp>
                                  <label for="">Yes</label>
                                </li>
                                <li>
                                  <input type="radio" name="behavior_issues_yes_no" value="No" @php echo ($form->behavior_issues_yes_no == 'No'?'checked':''); @endphp>
                                  <label for="">No</label>
                                </li>
                              </ul>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-2 col-sm-2 col-xs-12">
                              <div class="heading">
                                <h4>If yes, describe: </h4>
                              </div>
                            </div>
                            <div class="col-md-10 col-sm-10 col-xs-12">
                              <div class="putData">
                                <input type="text" name="behavior_issues_yes" value="{{ $form->behavior_issues_yes }}">
                              </div>
                            </div>
                          </div>
                          <h6>Is the member:</h6>
                          <div class="row">
                            <div class="col-md-7 col-sm-7 col-xs-12">
                              <div class="heading">
                                <h4>Smoking without supervision?</h4>
                              </div>
                            </div>
                            <div class="col-md-5 col-sm-5 col-xs-12">
                              <ul class="list-inline checkboxes">
                                <li>
                                  <input type="radio" name="smoking_without" value="Yes" @php echo ($form->smoking_without == 'Yes'?'checked':''); @endphp>
                                  <label for="">Yes</label>
                                </li>
                                <li>
                                  <input type="radio" name="smoking_without" value="No" @php echo ($form->smoking_without == 'No'?'checked':''); @endphp>
                                  <label for="">No</label>
                                </li>
                              </ul>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-12">
                              <div class="heading">
                                <h4>If yes, interventions: </h4>
                              </div>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <div class="putData">
                                <input type="text"  name="smoking_without_yes" value="{{ $form->smoking_without_yes }}">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-7 col-sm-7 col-xs-12">
                              <div class="heading">
                                <h4>Suspected alcohol abuse?</h4>
                              </div>
                            </div>
                            <div class="col-md-5 col-sm-5 col-xs-12">
                              <ul class="list-inline checkboxes">
                                <li>
                                  <input type="radio" name="suspected_alcohol" value="Yes" @php echo ($form->suspected_alcohol == 'Yes'?'checked':''); @endphp>
                                  <label for="">Yes</label>
                                </li>
                                <li>
                                  <input type="radio" name="suspected_alcohol" value="No" @php echo ($form->suspected_alcohol == 'No'?'checked':''); @endphp>
                                  <label for="">No</label>
                                </li>
                              </ul>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-12">
                              <div class="heading">
                                <h4>If yes, interventions: </h4>
                              </div>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <div class="putData">
                                <input type="text" name="suspected_alcohol_yes" value="{{ $form->suspected_alcohol_yes }}">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-7 col-sm-7 col-xs-12">
                              <div class="heading">
                                <h4>Suspected drug abuse?</h4>
                              </div>
                            </div>
                            <div class="col-md-5 col-sm-5 col-xs-12">
                              <ul class="list-inline checkboxes">
                                <li>
                                  <input type="Radio" name="suspected_drug" value="Yes" @php echo ($form->suspected_drug == 'Yes'?'checked':''); @endphp>
                                  <label for="">Yes</label>
                                </li>
                                <li>
                                  <input type="Radio" name="suspected_drug" value="No" @php echo ($form->suspected_drug == 'No'?'checked':''); @endphp>
                                  <label for="">No</label>
                                </li>
                              </ul>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-12">
                              <div class="heading">
                                <h4>If yes, interventions: </h4>
                              </div>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <div class="putData">
                                <input type="text" name="suspected_drug_yes" value="{{ $form->suspected_drug_yes }}">
                              </div>
                            </div>
                          </div>
                            <h6>Comments:</h6>
                            <textarea rows="3" name="comments2" value="{{ $form->comments2 }}">{{ $form->comments2 }}</textarea>
                          <div class="chkBxsList incontinenceSec">
                            <h6>Personal Assistance Services Needed: </h6>
                            <ul class="list-inline checkboxes">
                              <li>
                                <input type="checkbox" name="personal_assistance[]"  @php echo (in_array("1", $personal_assistance2)?'checked':''); @endphp   value="1">
                                <label for="">Meal prep   </label>
                              </li>
                              <li>
                                <input type="checkbox" name="personal_assistance[]" @php echo (in_array("2", $personal_assistance2)?'checked':''); @endphp  value="2">
                                <label for="">Bathing</label>
                              </li>
                              <li>
                                <input type="checkbox" name="personal_assistance[]" @php echo (in_array("3", $personal_assistance2)?'checked':''); @endphp   value="3">
                                <label for="">Routine hair care</label>
                              </li>
                              <li>
                                <input type="checkbox" name="personal_assistance[]" @php echo (in_array("4", $personal_assistance2)?'checked':''); @endphp   value="4">
                                <label for="">Exercise</label>
                              </li>
                              <li>
                                <input type="checkbox" name="personal_assistance[]" @php echo (in_array("5", $personal_assistance2)?'checked':''); @endphp   value="5">
                                <label for="">Feeding/eating</label>
                              </li>
                              <li>
                                <input type="checkbox" name="personal_assistance[]" @php echo (in_array("6", $personal_assistance2)?'checked':''); @endphp   value="6">
                                <label for="">Shaving</label>
                              </li>
                              <li>
                                <input type="checkbox"  name="personal_assistance[]" @php echo (in_array("7", $personal_assistance2)?'checked':''); @endphp   value="7">
                                <label for="">Routine skin care</label>
                              </li>
                              <li>
                                <input type="checkbox"  name="personal_assistance[]" @php echo (in_array("8", $personal_assistance2)?'checked':''); @endphp   value="8">
                                <label for="">Laundry</label>
                              </li>
                              <li>
                                <input type="checkbox"  name="personal_assistance[]" @php echo (in_array("9", $personal_assistance2)?'checked':''); @endphp   value="9">
                                <label for="">Oral care</label>
                              </li>
                              <li>
                                <input type="checkbox"  name="personal_assistance[]" @php echo (in_array("10", $personal_assistance2)?'checked':''); @endphp   value="10">
                                <label for="">Grooming</label>
                              </li>
                              <li>
                                <input type="checkbox"  name="personal_assistance[]" @php echo (in_array("11", $personal_assistance2)?'checked':''); @endphp   value="11">
                                <label for="">Shopping</label>
                              </li>
                              <li>
                                <input type="checkbox"  name="personal_assistance[]" @php echo (in_array("12", $personal_assistance2)?'checked':''); @endphp   value="12">
                                <label for="">Community outings</label>
                              </li>
                              <li>
                                <input type="checkbox"  name="personal_assistance[]" @php echo (in_array("13", $personal_assistance2)?'checked':''); @endphp   value="13">
                                <label for="">Transfers</label>
                              </li>
                              <li>
                                <input type="checkbox"  name="personal_assistance[]"  @php echo (in_array("14", $personal_assistance2)?'checked':''); @endphp value="14">
                                <label for="">Dressing</label>
                              </li>
                              <li>
                                <input type="checkbox"  name="personal_assistance[]"  @php echo (in_array("15", $personal_assistance2)?'checked':''); @endphp value="15">
                                <label for="">Cleaning</label>
                              </li>
                              <li>
                                <input type="checkbox"  name="personal_assistance[]"  @php echo (in_array("16", $personal_assistance2)?'checked':''); @endphp value="16">
                                <label for="">Assist with self-administered medication</label>
                              </li>
                              <li>
                                <input type="checkbox" name="personal_assistance[]"  @php echo (in_array("17", $personal_assistance2)?'checked':''); @endphp value="17">
                                <label for="">Other:</label>
                              </li>
                            </ul>
                          </div>
                          <h6>Comments:</h6>
                          <textarea rows="3" name="comments3" value="{{ $form->comments3 }}">{{ $form->comments3 }}</textarea>
                        </div>
                      </div>
                    </div>
                    <div class="pages">
                      <div class="container">
                        <ul class="list-inline">
                            <input type="Submit" value="Next" class="submitbtnsave">
                            <li><a href="{{route('careform_form17index',$careform_id)}}">Back</a></li>

                        </ul>
                      </div>
                    </div>
                  </div>


            </form>
            @endforeach

          </body>


@endsection


<script>
    $('#datatable_tabletools').DataTable({
        dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete : function(oSettings, json) {
            $(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
                $(this).addClass('btn-sm btn-default');
            });
        },
        "language": {
            "search": "Search: ",
            "searchPlaceholder": "Search records"
        },
        "pageLength": 50,
        "bDestroy": true,

        "order": []
    });
    /* END TABLE TOOLS */

    function ReportError(res)
    {
    }// Filter placeholder dataTable
    $(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>

<script src="{{ asset('js/all.js') }}""></script>
<!-- jquery.slicknav JS -->
<script src="{{ asset('js/jquery.slicknav.js') }}""></script>
<!-- Custom JS -->
<script src="{{ asset('js/custom.js') }}""></script>
