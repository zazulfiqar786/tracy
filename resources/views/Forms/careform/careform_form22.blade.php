


@extends('layouts.masterlayout')

@section('content')
    {{-- <a href="{{route('careform.create_form',$clientId)}}"" class="btn btn-success fa-pull-right" style="margin-right: 20px;">Create Form</a> --}}
    <br>
    <br>


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>New Care form</title>
        <!-- All CSS -->
        {{-- <link href="{{ asset('css/all.css') }}" rel="stylesheet"> --}}
        <!-- slicknav CSS -->
        <link href="{{ asset('css/slicknav.css') }}" rel="stylesheet">
        <!-- Style CSS -->
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <!-- responsive css -->
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
        <!-- Google Fonts CSS -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

        </head>
<style>
    .topbar .navbar-search input {
    /* font-size: .85rem; */
    height: 62px;
    margin-top: 10px;
}
</style>

        <body class="responsive">

            @foreach ($forms as $form)

            @php
            $skills2 = $form->skills;
            $skills = explode(',', $skills2);
        @endphp


            <form method="POST" action="{{route('careform_form22Update',$careform_id)}}">
                @csrf
                @method('get')
                <input type="hidden" name="careform_id" value=" {{ $careform_id }}">

                <div class="allPadding newCareForm">
                    <div class="topSecform">
                      <div class="container">
                        <div class="formHead ">
                          <div class="row">
                            <div class="col-md-9 col-sm-9 col-xs-12 centerCol">
                              <h2>Appendix D-4 </h2>
                              <h2>RENEWAL PARTICIPANT ASSESSMENT FORM</h2>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-2 col-sm-2 col-xs-12">
                              <div class="heading">
                                <h4>Member name:</h4>
                              </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                              <div class="putData">
                                <input type="text" name="member_name" value="{{ $form->member_name }}">
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="appenCMain statusSec">
                          <h6>Member/Family/Circle of Support Attestation Statement:</h6>
                          <div class="row">
                            <div class="col-md-5 col-sm-5 col-xs-12">
                              <div class="heading">
                                <h4>I/we agree to provide the following care to</h4>
                              </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                              <div class="putData">
                                <input type="text" name="i_agree_to_provide" value="{{$form->i_agree_to_provide}}">
                              </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                              <div class="heading">
                                <h4>when ICWP personnel are not present.</h4>
                              </div>
                            </div>
                          </div>
                          <div class="">
                            <p><b><i>By initialing the lines below I/we are acknowledging they have been discussed with me by the case manager:</i></b></p>
                            <ul>
                              <li>I understand that I will oversee, and when needed, I will provide general care duties such as: bathing, grooming;  assistance with transferring, maintaining continence and skin integrity, housekeeping; meal prep and general safety oversight.</li>
                              <li>I understand that ICWP services are not 24 hour a day care and I will need to be available for the applicant as needed.</li>
                              <li>I understand that ICWP services are a supplement to care provided by natural or identified circle of support</li>
                              <li>I understand that ICWP does not provide services to members living in a personal care home. I have been made aware that in order to receive ICWP services, members must live in a residence with a support system or an ICWP approved Alternative Living Services facility.</li>
                              <li>I am responsible for the following skilled care duties (list specifics): </li>
                              <div class="resList">
                                <ul class="list-inline checkboxes">
                                  <li>
                                    <input type="checkbox" name="skills[]" value="1" @php echo (in_array("1", $skills)?'checked':''); @endphp>
                                    <label for="skills">Tracheostomy care</label>
                                  </li>
                                  <li>
                                    <input type="checkbox" name="skills[]" value="2" @php echo (in_array("2", $skills)?'checked':''); @endphp>
                                    <label for="skills">Suctioning</label>
                                  </li>
                                  <li>
                                    <input type="checkbox" name="skills[]" value="3" @php echo (in_array("3", $skills)?'checked':''); @endphp>
                                    <label for="skills">Medication administration</label>
                                  </li>
                                  <li>
                                    <input type="checkbox" name="skills[]" value="4" @php echo (in_array("4", $skills)?'checked':''); @endphp>
                                    <label for="skills">Catheter care </label>
                                  </li>
                                  <li>
                                    <input type="checkbox" name="skills[]" value="5" @php echo (in_array("5", $skills)?'checked':''); @endphp>
                                    <label for="skills">Accuchecks injections</label>
                                  </li>
                                  <li>
                                    <input type="checkbox" name="skills[]" value="6" @php echo (in_array("6", $skills)?'checked':''); @endphp>
                                    <label for="skills">Wound care</label>
                                  </li>
                                  <li>
                                    <input type="checkbox" name="skills[]" value="7" @php echo (in_array("7", $skills)?'checked':''); @endphp>
                                    <label for="skills">Tube feeding set up and administration</label>
                                  </li>
                                  <li>
                                    <input type="checkbox" name="skills[]" value="8" @php echo (in_array("8", $skills)?'checked':''); @endphp>
                                    <label for="skills">Other</label>
                                  </li>
                                </ul>
                              </div>
                            </ul>
                          </div>
                          <div class="row">
                            <div class="col-md-5 col-sm-5 col-xs-12">
                              <div class="putData">
                                <input type="text" name="sign1" value="{{ $form->sign1 }}">
                                <h6>Signature of family/circle of support designee</h6>
                              </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                              <div class="putData">
                                <input type="text" name="relationship1" value="{{ $form->relationship1 }}">
                                <h6>Relationship</h6>
                              </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                              <div class="putData">
                                <input type="date"  name="date1" value="{{ $form->date1 }}">
                                <h6>Date</h6>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-5 col-sm-5 col-xs-12">
                              <div class="putData">
                                <input type="text"  name="sign2" value="{{ $form->sign2 }}">
                                <h6>Signature of family/circle of support designee</h6>
                              </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                              <div class="putData">
                                <input type="text" name="relationship2" value="{{ $form->relationship2 }}">
                                <h6>Relationship</h6>
                              </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                              <div class="putData">
                                <input type="date" name="date2" value="{{ $form->date2 }}">
                                <h6>Date</h6>
                              </div>
                            </div>
                          </div>
                          <p> hereby authorize the Department of Community Health (DCH) or its designee to contact my family/circle of support listed above.  DCH may share any information concerning my medical, mental or physical condition and treatment for the purpose of service  provision or maintaining my health, safety and welfare. </p>
                          <p>I understand that unless otherwise limited by state or federal regulations, and except to the extent that action has been taken based on  my consent, I may withdraw this consent at any time. My revocation must be in writing signed and dated by me or on my behalf, and  will be effective upon receipt of notice</p>
                          <p>I understand that I may revoke or change the individuals listed above at any time. </p>
                          <div class="signSec">
                            <div class="row">
                              <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="heading">
                                  <h4>Signature of Member/Applicant or Authorized Representative:</h4>
                                </div>
                              </div>
                              <div class="col-md-3 col-sm-3 col-xs-12">
                                <div class="putData">
                                  <input type="text" name="sign3" value="{{ $form->sign3 }}">
                                </div>
                              </div>
                              <div class="col-md-2 col-sm-2 col-xs-12">
                              </div>
                              <div class="col-md-1 col-sm-1 col-xs-12">
                                <div class="heading">
                                  <h4>Date:</h4>
                                </div>
                              </div>
                              <div class="col-md-2 col-sm-2 col-xs-12">
                                <div class="putData">
                                  <input type="text" name="date3" value="{{ $form->date3 }}">
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="signSec">
                            <div class="row">
                              <div class="col-md-2 col-sm-2 col-xs-12">
                                <div class="heading">
                                  <h4>Witness:</h4>
                                </div>
                              </div>
                              <div class="col-md-5 col-sm-5 col-xs-12">
                                <div class="putData">
                                  <input type="text" name="witness" value="{{ $form->witness}}">
                                </div>
                              </div>
                              <div class="col-md-2 col-sm-2 col-xs-12">
                              </div>
                              <div class="col-md-1 col-sm-1 col-xs-12">
                                <div class="heading">
                                  <h4>Date:</h4>
                                </div>
                              </div>
                              <div class="col-md-2 col-sm-2 col-xs-12">
                                <div class="putData">
                                  <input type="text" name="date4" value="{{ $form->date4}}">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="pages">
                      <div class="container">
                        <ul class="list-inline">
                            <input type="Submit" value="Next" class="submitbtnsave">
                            <li><a href="{{route('careform_form21index',$careform_id)}}">Back</a></li>

                        </ul>
                      </div>
                    </div>
                  </div>


            </form>
            @endforeach

          </body>


@endsection


<script>
    $('#datatable_tabletools').DataTable({
        dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete : function(oSettings, json) {
            $(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
                $(this).addClass('btn-sm btn-default');
            });
        },
        "language": {
            "search": "Search: ",
            "searchPlaceholder": "Search records"
        },
        "pageLength": 50,
        "bDestroy": true,

        "order": []
    });
    /* END TABLE TOOLS */

    function ReportError(res)
    {
    }// Filter placeholder dataTable
    $(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>

<script src="{{ asset('js/all.js') }}""></script>
<!-- jquery.slicknav JS -->
<script src="{{ asset('js/jquery.slicknav.js') }}""></script>
<!-- Custom JS -->
<script src="{{ asset('js/custom.js') }}""></script>
