


@extends('layouts.masterlayout')

@section('content')
    {{-- <a href="{{route('careform.create_form',$clientId)}}"" class="btn btn-success fa-pull-right" style="margin-right: 20px;">Create Form</a> --}}
    <br>
    <br>


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>New Care form</title>
        <!-- All CSS -->
        {{-- <link href="{{ asset('css/all.css') }}" rel="stylesheet"> --}}
        <!-- slicknav CSS -->
        <link href="{{ asset('css/slicknav.css') }}" rel="stylesheet">
        <!-- Style CSS -->
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <!-- responsive css -->
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
        <!-- Google Fonts CSS -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

        </head>
<style>
    .topbar .navbar-search input {
    /* font-size: .85rem; */
    height: 62px;
    margin-top: 10px;
}
</style>

        <body class="responsive">




            @foreach ($forms as $form)




            <form method="POST" action="{{route('careform_form13Update',$careform_id)}}">
                @csrf
                @method('get')
                <input type="hidden" name="careform_id" value=" {{ $careform_id }}">
                <div class="allPadding newCareForm">
    <div class="topSecform">
      <div class="container">
        <div class="formHead ">
          <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12 centerCol">
              <h2>APPENDIX I</h2>
              <h2>Independent Care Program<br> FREEDOM OF CHOICE (Statement of<br> Informed Consent)</h2>
            </div>
          </div>
        </div>
        <div class="appenCMain">
          <p>It is the policy of the State of Georgia that participants have the option to receive appropriate services in the setting of choice. Further, it is the policy of the State to recognize the participant’s individual dignity; providing safeguards to protect rights, health, and the welfare of recipients. <b> Rev. 10/2015</b></p>
          <p>Based on these beliefs the State of Georgia assures that potential participants and their authorized representative(s) will be afforded an opportunity to make an informed choice concerning services. Once an individual is determined to be likely to require the level of care provided in a nursing facility or hospital the individual or his/her authorized representative will be (1) informed of any feasible alternative available under the waiver, and (2) given the choice of either institutional or home and community-based services, and (3) that the substance of the information provided will make one reasonable familiar with service options, their alternatives, and possible benefits and hazards, and (4) the disclosure of said information is designed to be fully understood and appears to be fully understood. </p>
          <h6>Verification :</h6>
          <p>I have verified that the participant or his/her authorized representative have been informed about their choices in the manner outlined above.</p>
          <div class="appenCMainCntnt">
            <div class="row">
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="putData">
                  <input type="text" name="case_manager" value="{{ $form->case_manager }}">
                  <h5><i>Case Manager</i></h5>
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
              </div>
              <div class="col-md-2 col-sm-2 col-xs-12">
                <div class="putData">
                  <input type="text" name="case_manager_date"  value="{{ $form->case_manager_date }}">
                  <h5><i>Date</i></h5>
                </div>
              </div>
            </div>
          </div>
          <h6>Acceptance :</h6>
          <p>I and/or my authorized representative have been informed of my choices and have chosen to participate in the Home and Community Based Services Program.</p>
          <div class="appenCMainCntnt">
            <div class="row">
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="putData">
                  <input type="text" name="participant" value="{{ $form->participant }}">
                  <h5><i>Participant</i></h5>
                </div>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-12">
                <div class="putData">
                  <input type="text" name="participant_date"  value="{{ $form->participant_date }}">
                  <h5><i>Date</i></h5>
                </div>
              </div>
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="putData">
                  <input type="text" name="authorized_representative"  value="{{ $form->authorized_representative }}">
                  <h5><i>Authorized Representative</i></h5>
                </div>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-12">
                <div class="putData">
                  <input type="text" name="authorized_representative_date" value="{{ $form->authorized_representative_date }}">
                  <h5><i>Date</i></h5>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="putData">
                  <input type="text" name="witness" value="{{ $form->witness }}">
                  <h5><i>Witness</i></h5>
                </div>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-12">
                <div class="putData">
                  <input type="text" name="witness_date" value="{{ $form->witness_date }}">
                  <h5><i>Date</i></h5>
                </div>
              </div>
            </div>
          </div>
          <h6>Refusal :</h6>
          <p>I and/or my authorized representative have been informed of my choices and have chosen to refuse waiver services.</p>
          <div class="appenCMainCntnt">
            <div class="row">
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="putData">
                  <input type="text" name="refusal_participant" value="{{ $form->refusal_participant }}">
                  <h5><i>Participant</i></h5>
                </div>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-12">
                <div class="putData">
                  <input type="text" name="refusal_participant_date" value="{{ $form->refusal_participant_date }}">
                  <h5><i>Date</i></h5>
                </div>
              </div>
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="putData">
                  <input type="text" name="refusal_authorized_representative" value="{{ $form->refusal_authorized_representative }}">
                  <h5><i>Authorized Representative</i></h5>
                </div>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-12">
                <div class="putData">
                  <input type="text" name="refusal_authorized_representative_date" value="{{ $form->refusal_authorized_representative_date }}">
                  <h5><i>Date</i></h5>
                </div>a
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="putData">
                  <input type="text" name="refusal_witness" value="{{ $form->refusal_witness }}">
                  <h5><i>Witness</i></h5>
                </div>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-12">
                <div class="putData">
                  <input type="text" name="refusal_witness_date" value="{{ $form->refusal_witness_date }}">
                  <h5><i>Date</i></h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="pages">
      <div class="container">
        <ul class="list-inline">
            <input type="Submit" value="Next" class="submitbtnsave">
        <li><a href="{{route('careform_form12index',$careform_id)}}">Back</a></li>
        </ul>
      </div>
    </div>
  </div>
            </form>
            @endforeach

          </body>


@endsection


<script>
    $('#datatable_tabletools').DataTable({
        dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete : function(oSettings, json) {
            $(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
                $(this).addClass('btn-sm btn-default');
            });
        },
        "language": {
            "search": "Search: ",
            "searchPlaceholder": "Search records"
        },
        "pageLength": 50,
        "bDestroy": true,

        "order": []
    });
    /* END TABLE TOOLS */

    function ReportError(res)
    {
    }// Filter placeholder dataTable
    $(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>

<script src="{{ asset('js/all.js') }}""></script>
<!-- jquery.slicknav JS -->
<script src="{{ asset('js/jquery.slicknav.js') }}""></script>
<!-- Custom JS -->
<script src="{{ asset('js/custom.js') }}""></script>
