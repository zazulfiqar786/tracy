


@extends('layouts.masterlayout')

@section('content')
    {{-- <a href="{{route('careform.create_form',$clientId)}}"" class="btn btn-success fa-pull-right" style="margin-right: 20px;">Create Form</a> --}}
    <br>
    <br>


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>New Care form</title>
        <!-- All CSS -->
        {{-- <link href="{{ asset('css/all.css') }}" rel="stylesheet"> --}}
        <!-- slicknav CSS -->
        <link href="{{ asset('css/slicknav.css') }}" rel="stylesheet">
        <!-- Style CSS -->
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <!-- responsive css -->
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
        <!-- Google Fonts CSS -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

        </head>
<style>
    .topbar .navbar-search input {
    /* font-size: .85rem; */
    height: 62px;
    margin-top: 10px;
}
</style>

        <body class="responsive">




            @foreach ($forms as $form)




            <form method="POST" action="{{route('careform_form5Update',$careform_id)}}">
                @csrf
                @method('get')
                <input type="hidden" name="careform_id" value=" {{ $careform_id }}">

                <div class="allPadding newCareForm">
    <div class="topSecform">
      <div class="container">
        <div class="formHead ">
          <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12 centerCol">
              <h2>APPENDIX F</h2>
            </div>
          </div>
        </div>
        <div class="appFHead">
          <div class="row flexRow">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="logo">

              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="appenHead">
                <div class="head">
                  <h4>Name of Member/Patient/Applicant:</h4>
                  <input type="text" name="name_of_member" value="{{ $form->name_of_member}}">
                </div>
                <div class="head">
                  <h4>Date of Birth:</h4>
                  <input type="text" name="dob" value="{{ $form->dob}}">
                </div>
                <div class="head">
                  <h4>IF AVAILABLE:</h4>
                  <input type="text" name="if_available" value="{{ $form->if_available}}">
                  <h6>ID Number Used by Requesting Agency</h6>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="appFMain">
          <h2>AUTHORIZATION FOR RELEASE OF INFORMATION</h2>
          <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="heading">
                <p>I hearby request and authorize:</p>
              </div>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <div class="putData">
                <input type="text" name="request_and_authoriz" value="{{ $form->request_and_authoriz}}" placeholder="(Name of Person or Agency Requesting Information)">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="heading">
                <p>to obtain from:</p>
              </div>
            </div>
            <div class="col-md-10 col-sm-10 col-xs-12">
              <div class="putData">
                <input type="text" name="to_obtain_from" value="{{ $form->to_obtain_from}}" placeholder="(Name of Person or Agency Holding the Information)">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-10 col-sm-10 col-xs-12">
              <div class="heading">
                <p>the following type(s) of information from my records (and any specific portion thereof):</p>
              </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="putData">
               <input type="text" name="following_types1" value="{{ $form->following_types1}}"  placeholder="">
              </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="putData">
               <input type="text" name="following_types2" value="{{ $form->following_types2}}"  placeholder="">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="heading">
                <p>for the purpose of:</p>
              </div>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <div class="putData">
               <input type="text" name="for_the_perpose1" value="{{ $form->for_the_perpose1}}"  placeholder="">
              </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="putData">
               <input type="text" name="for_the_perpose2" value="{{ $form->for_the_perpose2}}"  placeholder="">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="heading">
                <p>All information I hereby authorize to be obtained form this agency will be held strictly confidential and cannot be released by the recipient without my written consent. I understand that this authorization will remain in effect for:</p>
                <ul class="list-inline checkboxes">
                  <li>
                    <input type="checkbox" name="all_information_authorize" value="1">
                    <label for="vehicle1">Ninety (90) days unless I specify an earlier expiration date here:</label>
                  </li>
                  <li>
                    <input type="checkbox" name="all_information_authorize" value="2">
                    <label for="vehicle1"> One (1) year.</label>
                  </li>
                  <li>
                    <input type="checkbox" name="all_information_authorize" value="3">
                    <label for="vehicle1"> The period necessary to complete all transactions on accounts related to services provided to me</label>
                  </li>
                </ul>
                <p><i><b>I understand that unless otherwise limited by state or federal regulations, and except to the extent that action has been taken which was based on my consent. I may withdraw this consent at any time.</b></i></p>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="putData">
                <input type="text" name="i_understand_date" value="{{ $form->i_understand_date}}">
                <h6>Date</h6>
              </div>
            </div>
            <div class="col-md-5 col-sm-5 col-xs-12"></div>
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="putData">
                <input type="text" name="signature_of_member" value="{{ $form->signature_of_member}}">
                <h6>(Signature of Member/Parent/Applicant)</h6>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="putData">
                <input type="text" name="signature_of_witness" value="{{ $form->signature_of_witness}}">
                <h6>(Signature of Witness)</h6>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="putData">
                <input type="text" name="title_relation_member" value="{{ $form->title_relation_member}}">
                <h6>(Title or Relationship to Member)</h6>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="putData">
                <input type="text" name="sign_parent" value="{{ $form->sign_parent}}">
                <h6>(Signature of Parent or Authorized) <br>Representative, where applicable)</h6>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="putData">
                <input type="text" name="sign_parent_date" value="{{ $form->sign_parent_date}}">
                <h6>Date</h6>
              </div>
            </div>
          </div>
          <hr>
          <div class="withdrawSec">
            <div class="row">
              <p><i><b>USE THIS SPACE ONLY IF MEMBER WITHDRAWS CONSENT</b></i></p>
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="putData">
                  <input type="text" name="withdrawn_member" value="{{ $form->withdrawn_member}}">
                  <h6>(Date this consent is withdrawn by member) </h6>
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
              </div>
              <div class="col-md-2 col-sm-2 col-xs-12">
                <div class="putData">
                  <input type="text" name="signature_member_2" value="{{ $form->signature_member_2}}">
                  <h6>(Signature of Member)</h6>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="pages">
      <div class="container">
        <ul class="list-inline">
            <input type="Submit" value="Next" class="submitbtnsave">
        <li><a href="{{route('careform_form4index',$careform_id)}}">Back</a></li>

        </ul>
      </div>
    </div>
  </div>
            </form>
            @endforeach

          </body>


@endsection


<script>
    $('#datatable_tabletools').DataTable({
        dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete : function(oSettings, json) {
            $(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
                $(this).addClass('btn-sm btn-default');
            });
        },
        "language": {
            "search": "Search: ",
            "searchPlaceholder": "Search records"
        },
        "pageLength": 50,
        "bDestroy": true,

        "order": []
    });
    /* END TABLE TOOLS */

    function ReportError(res)
    {
    }// Filter placeholder dataTable
    $(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>

<script src="{{ asset('js/all.js') }}""></script>
<!-- jquery.slicknav JS -->
<script src="{{ asset('js/jquery.slicknav.js') }}""></script>
<!-- Custom JS -->
<script src="{{ asset('js/custom.js') }}""></script>
