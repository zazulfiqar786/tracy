


@extends('layouts.masterlayout')

@section('content')
    {{-- <a href="{{route('careform.create_form',$clientId)}}"" class="btn btn-success fa-pull-right" style="margin-right: 20px;">Create Form</a> --}}
    <br>
    <br>


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>New Care form</title>
        <!-- All CSS -->
        {{-- <link href="{{ asset('css/all.css') }}" rel="stylesheet"> --}}
        <!-- slicknav CSS -->
        <link href="{{ asset('css/slicknav.css') }}" rel="stylesheet">
        <!-- Style CSS -->
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <!-- responsive css -->
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
        <!-- Google Fonts CSS -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

        </head>
<style>
    .topbar .navbar-search input {
    /* font-size: .85rem; */
    height: 62px;
    margin-top: 10px;
}
</style>
        <body class="responsive">

            @foreach ($forms as $form)
            <form method="POST" action="{{route('careform_form3Update',$careform_id)}}">
                @csrf
                @method('get')
                <input type="hidden" name="careform_id" value=" {{ $careform_id }}">
                <div class="allPadding newCareForm">
                    <div class="topSecform">
                      <div class="container">
                        <div class="formHead ">
                          <div class="row">
                            <div class="col-md-8 col-sm-8 col-xs-12 centerCol">
                              <h2>Appendix H-3</h2>
                              <h2>ICWP Financial Summary</h2>
                            </div>
                          </div>
                          <div class="row flexRow">
                            <div class="col-md-1 col-sm-1 col-xs-12">
                              <div class="heading">
                                <h4><b>Name:</b></h4>
                              </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                              <div class="putData">
                                <input type="text"  name="icwp_financial_summary" value="{{ $form->icwp_financial_summary }}">
                              </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12"></div>
                            <div class="col-md-1 col-sm-1 col-xs-12">
                              <div class="heading">
                                <h4><b>DATE:</b></h4>
                              </div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                              <div class="putData">
                                <input type="text"  name="date" value="{{ $form->date }}">
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="table-responsive">
                          <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th scope="col">Service Description</th>
                                <th scope="col">Frequency Intensity</th>
                                <th scope="col">Responsible Provider</th>
                                <th scope="col">Per Unit Cost</th>
                                <th scope="col">Yearly Cost</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>
                                  <input type="text" name="service_description_1"  value="{{ $form->service_description_1 }}">
                                </td>
                                <td>
                                  <input type="text" name="frequency_intensity_1" value="{{ $form->frequency_intensity_1 }}">
                                </td>
                                <td>
                                  <input type="text" name="responsible_provider_1" value="{{ $form->responsible_provider_1 }}">
                                </td>
                                <td>
                                  <input type="text" name="per_unit_cost_1" value="{{ $form->per_unit_cost_1 }}">
                                </td>
                                <td>
                                  <input type="text" name="yearly_cast_1" value="{{ $form->yearly_cast_1 }}">
                                </td>
                              </tr>
                              <tr>
                                <td>
                                    <input type="text" name="service_description_2"  value="{{ $form->service_description_2 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="frequency_intensity_2" value="{{ $form->frequency_intensity_2}}">
                                  </td>
                                  <td>
                                    <input type="text" name="responsible_provider_2" value="{{ $form->responsible_provider_2 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="per_unit_cost_2" value="{{ $form->per_unit_cost_2 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="yearly_cast_2" value="{{ $form->yearly_cast_2 }}">
                                  </td>
                              </tr>
                              <tr>
                                <td>
                                    <input type="text" name="service_description_3"  value="{{ $form->service_description_3 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="frequency_intensity_3" value="{{ $form->frequency_intensity_3 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="responsible_provider_3" value="{{ $form->responsible_provider_3 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="per_unit_cost_3" value="{{ $form->per_unit_cost_3 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="yearly_cast_3" value="{{ $form->yearly_cast_3 }}">
                                  </td>
                              </tr>
                              <tr>
                                <td>
                                    <input type="text" name="service_description_4"  value="{{ $form->service_description_4 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="frequency_intensity_4" value="{{ $form->frequency_intensity_4 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="responsible_provider_4" value="{{ $form->responsible_provider_4 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="per_unit_cost_4" value="{{ $form->per_unit_cost_4 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="yearly_cast_4" value="{{ $form->yearly_cast_4 }}">
                                  </td>
                              </tr>
                              <tr>
                                <td>
                                    <input type="text" name="service_description_5"  value="{{ $form->service_description_5 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="frequency_intensity_5" value="{{ $form->frequency_intensity_5 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="responsible_provider_5" value="{{ $form->responsible_provider_5 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="per_unit_cost_5" value="{{ $form->per_unit_cost_5 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="yearly_cast_5" value="{{ $form->yearly_cast_5 }}">
                                  </td>
                              </tr>
                              <tr>
                                <td>
                                    <input type="text" name="service_description_6"  value="{{ $form->service_description_6 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="frequency_intensity_6" value="{{ $form->frequency_intensity_6 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="responsible_provider_6" value="{{ $form->responsible_provider_6 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="per_unit_cost_6" value="{{ $form->per_unit_cost_6 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="yearly_cast_6" value="{{ $form->yearly_cast_6 }}">
                                  </td>
                              </tr>
                              <tr>
                                <td>
                                    <input type="text" name="service_description_7"  value="{{ $form->service_description_7 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="frequency_intensity_7" value="{{ $form->frequency_intensity_7 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="responsible_provider_7" value="{{ $form->responsible_provider_7 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="per_unit_cost_7" value="{{ $form->per_unit_cost_7 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="yearly_cast_7" value="{{ $form->yearly_cast_7 }}">
                                  </td>
                              </tr>
                              <tr>
                                <td>
                                    <input type="text" name="service_description_8"  value="{{ $form->service_description_8 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="frequency_intensity_8" value="{{ $form->frequency_intensity_8 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="responsible_provider_8" value="{{ $form->responsible_provider_8 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="per_unit_cost_8" value="{{ $form->per_unit_cost_8 }}">
                                  </td>
                                  <td>
                                    <input type="text" name="yearly_cast_8" value="{{ $form->yearly_cast_8 }}">
                                  </td>
                              </tr>
                              <tr>
                                <td>
                                  <input type="text" name="">
                                </td>
                                <td colspan="3" class="grandTotal">
                                  <h6>Grand Total:</h6>
                                </td>
                                <td>
                                  <h2>$0.00</h2>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                    <div class="pages">
                      <div class="container">
                        <ul class="list-inline">
                            <input type="Submit" value="Next" class="submitbtnsave">
                            <li><a href="{{route('careform_form2index',$careform_id)}}">Back</a></li>

                        </ul>
                      </div>
                    </div>
                  </div>
            </form>
            @endforeach

          </body>


@endsection


<script>
    $('#datatable_tabletools').DataTable({
        dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete : function(oSettings, json) {
            $(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
                $(this).addClass('btn-sm btn-default');
            });
        },
        "language": {
            "search": "Search: ",
            "searchPlaceholder": "Search records"
        },
        "pageLength": 50,
        "bDestroy": true,

        "order": []
    });
    /* END TABLE TOOLS */

    function ReportError(res)
    {
    }// Filter placeholder dataTable
    $(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>

<script src="{{ asset('js/all.js') }}""></script>
<!-- jquery.slicknav JS -->
<script src="{{ asset('js/jquery.slicknav.js') }}""></script>
<!-- Custom JS -->
<script src="{{ asset('js/custom.js') }}""></script>
