


@extends('layouts.masterlayout')

@section('content')
    {{-- <a href="{{route('careform.create_form',$clientId)}}"" class="btn btn-success fa-pull-right" style="margin-right: 20px;">Create Form</a> --}}
    <br>
    <br>


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>New Care form</title>
        <!-- All CSS -->
        {{-- <link href="{{ asset('css/all.css') }}" rel="stylesheet"> --}}
        <!-- slicknav CSS -->
        <link href="{{ asset('css/slicknav.css') }}" rel="stylesheet">
        <!-- Style CSS -->
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <!-- responsive css -->
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
        <!-- Google Fonts CSS -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

        </head>
<style>
    .topbar .navbar-search input {
    /* font-size: .85rem; */
    height: 62px;
    margin-top: 10px;
}
</style>

        <body class="responsive">




            @foreach ($forms as $form)




@php
$devices = $form->devices;
$devices2 = explode(',', $devices);
@endphp




            <form method="POST" action="{{route('careform_form16Update',$careform_id)}}">
                @csrf
                @method('get')
                <input type="hidden" name="careform_id" value=" {{ $careform_id }}">
                <div class="allPadding newCareForm">
           <div class="topSecform">
      <div class="container">
        <div class="formHead ">
          <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12 centerCol">
              <h2>Appendix D-4 </h2>
              <h2>RENEWAL PARTICIPANT ASSESSMENT FORM</h2>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="heading">
                <h4>Member name:</h4>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="putData">
                <input type="text" name="member_name" value="{{$form->member_name}}">
              </div>
            </div>
          </div>
        </div>
        <div class="appenCMain statusSec">
          <h6>Status:</h6>
          <div class="row">
            <div class="col-md-5 col-sm-5 col-xs-12">
              <div class="heading">
                <h4> What was the date of the last physician visit? </h4>
              </div>
            </div>
            <div class="col-md-7 col-sm-7 col-xs-12">
              <div class="putData">
                <input type="text" name="physician_visit" value="{{$form->physician_visit}}">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-7 col-sm-7 col-xs-12">
              <div class="heading">
                <h4>Were any new physician’s orders received this annual period? </h4>
              </div>
            </div>
            <div class="col-md-5 col-sm-5 col-xs-12">
              <ul class="list-inline checkboxes">
                <li>
                  <input type="radio" name="annual_period" value="Yes" @php echo ($form->annual_period == 'Yes'?'checked':''); @endphp>
                  <label for="vehicle1">Yes</label>
                </li>
                <li>
                  <input type="radio" name="annual_period" value="No" @php echo ($form->annual_period == 'No'?'checked':''); @endphp>
                  <label for="vehicle2">No</label>
                </li>
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="heading">
                <h4> If yes, please describe:  </h4>
              </div>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <div class="putData">
                <input type="text" name="describe" value="{{$form->describe}}">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12">
              <div class="heading">
                <h4> Were there any emergency room visits or hospitalizations during this annual period?  </h4>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <ul class="list-inline checkboxes">
                <li>
                  <input type="radio" name="hospitalizations_visit" value="Yes" @php echo ($form->hospitalizations_visit == 'Yes'?'checked':''); @endphp>
                  <label for="vehicle1">Yes</label>
                </li>
                <li>
                  <input type="radio" name="hospitalizations_visit" value="No" @php echo ($form->hospitalizations_visit == 'No'?'checked':''); @endphp>
                  <label for="vehicle2">No</label>
                </li>
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-5 col-sm-5 col-xs-12">
              <div class="heading">
                <h4> If yes, please provide the dates and diagnosis: </h4>
              </div>
            </div>
            <div class="col-md-7 col-sm-7 col-xs-12">
              <div class="putData">
                <input type="text" name="diagnosis_date" value="{{$form->diagnosis_date}}">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-5 col-sm-5 col-xs-12">
              <div class="heading">
                <h4>Did you, the Case manager, submit a variance?</h4>
              </div>
            </div>
            <div class="col-md-7 col-sm-7 col-xs-12">
              <ul class="list-inline checkboxes">
                <li>
                  <input type="radio" name="variance" value="Yes" @php echo ($form->variance == 'Yes'?'checked':''); @endphp>
                  <label for="vehicle1">Yes</label>
                </li>
                <li>
                  <input type="radio" name="variance" value="No" @php echo ($form->variance == 'No'?'checked':''); @endphp>
                  <label for="vehicle2">No</label>
                </li>
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12">
              <div class="heading">
                <h4>Is the individual in jeopardy of moving from their home to a facility to get the care they need?</h4>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <ul class="list-inline checkboxes">
                <li>
                  <input type="radio" name="facility_car" value="Yes" @php echo ($form->facility_car == 'Yes'?'checked':''); @endphp>
                  <label for="vehicle1">Yes</label>
                </li>
                <li>
                  <input type="radio" name="facility_car" value="No" @php echo ($form->facility_car == 'No'?'checked':''); @endphp>
                  <label for="vehicle2">No</label>
                </li>
              </ul>
            </div>
          </div>


          <h6>Risk factors:</h6>
          <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12">
              <div class="heading">
                <h4> Does member have a history of nursing facility placement in the past year? </h4>
              </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
              <ul class="list-inline checkboxes">
                <li>
                  <input type="radio" name="facility_placement" value="Yes" @php echo ($form->facility_placement == 'Yes'?'checked':''); @endphp>
                  <label for="vehicle1">Yes</label>
                </li>
                <li>
                  <input type="radio" name="facility_placement" value="No" @php echo ($form->facility_placement == 'No'?'checked':''); @endphp>
                  <label for="vehicle2">No</label>
                </li>
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="heading">
                <h4> If yes, what were the reasons?  </h4>
              </div>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <div class="putData">
                <input type="text" name="facility_placement_reasons" value="{{$form->facility_placement_reasons}}">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-5 col-sm-5 col-xs-12">
              <div class="heading">
                <h4>Does the member have a progressive disease?</h4>
              </div>
            </div>
            <div class="col-md-7 col-sm-7 col-xs-12">
              <ul class="list-inline checkboxes">
                <li>
                  <input type="radio" name="progressive_disease" value="Yes" @php echo ($form->progressive_disease == 'Yes'?'checked':''); @endphp>
                  <label for="vehicle1">Yes</label>
                </li>
                <li>
                  <input type="radio" name="progressive_disease" value="No" @php echo ($form->progressive_disease == 'No'?'checked':''); @endphp>
                  <label for="vehicle2">No</label>
                </li>
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="heading">
                <h4> If so, state the disease: </h4>
              </div>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <div class="putData">
                <input type="text" name="disease_state" value="{{$form->disease_state}}">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="heading">
                <h4>Has the member fell 2 or more time in the past year?</h4>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <ul class="list-inline checkboxes">
                <li>
                  <input type="radio" name="member_past_year" value="Yes" @php echo ($form->member_past_year == 'Yes'?'checked':''); @endphp>
                  <label for="vehicle1">Yes</label>
                </li>
                <li>
                  <input type="radio" name="member_past_year" value="No" @php echo ($form->member_past_year == 'No'?'checked':''); @endphp>
                  <label for="vehicle2">No</label>
                </li>
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="heading">
                <h4>If so, has a fall safety program been implemented? </h4>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <ul class="list-inline checkboxes">
                <li>
                  <input type="radio" name="safety_implemented" value="Yes" @php echo ($form->safety_implemented == 'Yes'?'checked':''); @endphp>
                  <label for="vehicle1">Yes</label>
                </li>
                <li>
                  <input type="radio" name="safety_implemented" value="No" @php echo ($form->safety_implemented == 'No'?'checked':''); @endphp>
                  <label for="vehicle2">No</label>
                </li>
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="heading">
                <h4>If the member has fallen, what were the contributing factors? </h4>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="putData">
                <input type="text" name="contributing_factors" value="{{$form->contributing_factors}}">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="heading">
                <h4>Does the member have urinary incontinence?</h4>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <ul class="list-inline checkboxes">
                <li>
                  <input type="radio" name="urinary_incontinence" value="Yes" @php echo ($form->urinary_incontinence == 'Yes'?'checked':''); @endphp>
                  <label for="vehicle1">Yes</label>
                </li>
                <li>
                  <input type="radio" name="urinary_incontinence" value="No" @php echo ($form->urinary_incontinence == 'No'?'checked':''); @endphp>
                  <label for="vehicle2">No</label>
                </li>
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="heading">
                <h4>Does the member have bowel incontinence?</h4>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <ul class="list-inline checkboxes">
                <li>
                  <input type="radio" name="bowel_incontinence" value="Yes" @php echo ($form->bowel_incontinence == 'Yes'?'checked':''); @endphp>
                  <label for="vehicle1">Yes</label>
                </li>
                <li>
                  <input type="radio" name="bowel_incontinence" value="No" @php echo ($form->bowel_incontinence == 'No'?'checked':''); @endphp>
                  <label for="vehicle2">No</label>
                </li>
              </ul>
            </div>
          </div>

          <div class="chkBxsList incontinenceSec">
            <h6>If Incontinent, what devices does member use?:</h6>
            <ul class="list-inline checkboxes">
              <li>
                <input type="checkbox" name="devices[]" value="1" @php echo (in_array("1", $devices2)?'checked':''); @endphp>
                <label for="vehicle1">Diaper/ pull-up/ brief</label>
              </li>
              <li>
                <input type="checkbox" name="devices[]" value="2" @php echo (in_array("2", $devices2)?'checked':''); @endphp>
                <label for="vehicle2">Indwelling cath</label>
              </li>
              <li>
                <input type="checkbox" name="devices[]" value="3" @php echo (in_array("3", $devices2)?'checked':''); @endphp>
                <label for="vehicle2">Bowel program</label>
              </li>
              <li>
                <input type="checkbox" name="devices[]" value="4" @php echo (in_array("4", $devices2)?'checked':''); @endphp>
                <label for="vehicle2">Chux</label>
              </li>
              <li>
                <input type="checkbox" name="devices[]" value="5" @php echo (in_array("5", $devices2)?'checked':''); @endphp>
                <label for="vehicle2">Supra pubic cath </label>
              </li>
              <li>
                <input type="checkbox" name="devices[]" value="6" @php echo (in_array("6", $devices2)?'checked':''); @endphp>
                <label for="vehicle2">Ostomy</label>
              </li>
              <li>
                <input type="checkbox" name="devices[]" value="7" @php echo (in_array("7", $devices2)?'checked':''); @endphp>
                <label for="vehicle2">Urinal</label>
              </li>
              <li>
                <input type="checkbox" name="devices[]" value="8" @php echo (in_array("8", $devices2)?'checked':''); @endphp>
                <label for="vehicle2">Self-cath</label>
              </li>
              <li>
                <input type="checkbox" name="devices[]" value="9" @php echo (in_array("9", $devices2)?'checked':''); @endphp>
                <label for="vehicle2">Dialysis</label>
              </li>
              <li>
                <input type="checkbox" name="devices[]" value="10" @php echo (in_array("10", $devices2)?'checked':''); @endphp>
                <label for="vehicle2">Condom cath</label>
              </li>
              <li>
                <input type="checkbox" name="devices[]" value="11" @php echo (in_array("11", $devices2)?'checked':''); @endphp>
                <label for="vehicle2">Ileostomy/ ileoconduit </label>
              </li>
              <li>
                <input type="checkbox" name="devices[]" value="12" @php echo (in_array("12", $devices2)?'checked':''); @endphp>
                <label for="vehicle2">Other </label>
              </li>
            </ul>
          </div>
          <h6>Comments:</h6>
          <textarea rows="8"  name="Comments"></textarea>
        </div>
      </div>
    </div>
    <div class="pages">
      <div class="container">
        <ul class="list-inline">
            <input type="Submit" value="Next" class="submitbtnsave">
        <li><a href="{{route('careform_form15index',$careform_id)}}">Back</a></li>
        </ul>
      </div>
    </div>
  </div>
            </form>
            @endforeach

          </body>


@endsection


<script>
    $('#datatable_tabletools').DataTable({
        dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete : function(oSettings, json) {
            $(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
                $(this).addClass('btn-sm btn-default');
            });
        },
        "language": {
            "search": "Search: ",
            "searchPlaceholder": "Search records"
        },
        "pageLength": 50,
        "bDestroy": true,

        "order": []
    });
    /* END TABLE TOOLS */

    function ReportError(res)
    {
    }// Filter placeholder dataTable
    $(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>

<script src="{{ asset('js/all.js') }}""></script>
<!-- jquery.slicknav JS -->
<script src="{{ asset('js/jquery.slicknav.js') }}""></script>
<!-- Custom JS -->
<script src="{{ asset('js/custom.js') }}""></script>
