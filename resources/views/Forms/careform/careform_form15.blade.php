


@extends('layouts.masterlayout')

@section('content')
    {{-- <a href="{{route('careform.create_form',$clientId)}}"" class="btn btn-success fa-pull-right" style="margin-right: 20px;">Create Form</a> --}}
    <br>
    <br>


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>New Care form</title>
        <!-- All CSS -->
        {{-- <link href="{{ asset('css/all.css') }}" rel="stylesheet"> --}}
        <!-- slicknav CSS -->
        <link href="{{ asset('css/slicknav.css') }}" rel="stylesheet">
        <!-- Style CSS -->
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <!-- responsive css -->
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
        <!-- Google Fonts CSS -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

        </head>
<style>
    .topbar .navbar-search input {
    /* font-size: .85rem; */
    height: 62px;
    margin-top: 10px;
}
</style>

        <body class="responsive">




            @foreach ($forms as $form)


            @php
$Skilled = $form->Skilled;
$Skilled2 = explode(',', $Skilled);
@endphp

@php
$Evaluation = $form->Evaluation;
$Evaluation2 = explode(',', $Evaluation);
@endphp

@php
$Potential = $form->Potential;
$Potential2 = explode(',', $Potential);
@endphp

@php
$ICWP_member = $form->ICWP_member;
$ICWP_member2 = explode(',', $ICWP_member);
@endphp


            <form method="POST" action="{{route('careform_form15Update',$careform_id)}}">
                @csrf
                @method('get')
                <input type="hidden" name="careform_id" value=" {{ $careform_id }}">
                <div class="allPadding newCareForm">
                <div class="topSecform">
      <div class="container">
        <div class="formHead ">
          <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12 centerCol">
              <h2>Appendix D-4 </h2>
              <h2>RENEWAL PARTICIPANT ASSESSMENT FORM</h2>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="heading">
                <h4>Member name:</h4>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <div class="putData">
                <input type="text" name="member_name" value="{{$form->member_name}}">
              </div>
            </div>
          </div>
        </div>
        <div class="appenCMain">
          <h6>Medical List:</h6>
          <table class="table table-bordered">
            <tbody>
              <tr>
                <td>
                  <input type="text" name="Medical_List_1" value="{{$form->Medical_List_1}}">
                </td>
                <td>
                  <input type="text" name="Medical_List_2" value="{{$form->Medical_List_2}}">
                </td>
              </tr>
              <tr>
                <td>
                  <input type="text" name="Medical_List_3" value="{{$form->Medical_List_3}}">
                </td>
                <td>
                  <input type="text" name="Medical_Lis5_4" value="{{$form->Medical_Lis5_4}}">
                </td>
              </tr>
              <tr>
                <td>
                  <input type="text" name="Medical_List_5" value="{{$form->Medical_List_5}}">
                </td>
                <td>
                  <input type="text" name="Medical_List_6" value="{{$form->Medical_List_6}}">
                </td>
              </tr>
              <tr>
                <td>
                  <input type="text" name="Medical_List_7" value="{{$form->Medical_List_7}}">
                </td>
                <td>
                  <input type="text" name="Medical_List_8" value="{{$form->Medical_List_8}}">
                </td>
              </tr>
              <tr>
                <td>
                  <input type="text" name="Medical_List_9" value="{{$form->Medical_List_9}}">
                </td>
                <td>
                  <input type="text" name="Medical_List_10" value="{{$form->Medical_List_10}}">
                </td>
              </tr>
              <tr>
                <td>
                  <input type="text" name="Medical_List_11" value="{{$form->Medical_List_11}}">
                </td>
                <td>
                  <input type="text" name="Medical_List_12" value="{{$form->Medical_List_12}}">
                </td>
              </tr>
            </tbody>
          </table>
          <div class="chkBxsList">
            <h6>Skilled care duties required: </h6>
            <ul class="list-inline checkboxes">
              <li>
                <input type="checkbox" name="Skilled[]" value="1" @php echo (in_array("1", $Skilled2)?'checked':''); @endphp>
                <label for="vehicle1">Tracheostomy Care</label>
              </li>
              <li>
                <input type="checkbox" name="Skilled[]" value="2" @php echo (in_array("2", $Skilled2)?'checked':''); @endphp>
                <label for="vehicle2">Suctioning</label>
              </li>
              <li>
                <input type="checkbox" name="Skilled[]" value="3" @php echo (in_array("3", $Skilled2)?'checked':''); @endphp>
                <label for="vehicle2">Medication administration</label>
              </li>
              <li>
                <input type="checkbox" name="Skilled[]" value="4" @php echo (in_array("4", $Skilled2)?'checked':''); @endphp>
                <label for="vehicle2">Catheter Care</label>
              </li>
              <li>
                <input type="checkbox" name="Skilled[]" value="5" @php echo (in_array("5", $Skilled2)?'checked':''); @endphp>
                <label for="vehicle2">Vent</label>
              </li>
              <li>
                <input type="checkbox" name="Skilled[]" value="6" @php echo (in_array("6", $Skilled2)?'checked':''); @endphp>
                <label for="vehicle2">Wound Care</label>
              </li>
              <li>
                <input type="checkbox" name="Skilled[]" value="7" @php echo (in_array("7", $Skilled2)?'checked':''); @endphp
                <label for="vehicle2">Tube feeding set up/ administration</label>
              </li>
              <li>
                <input type="checkbox" name="Skilled[]" value="8" @php echo (in_array("8", $Skilled2)?'checked':''); @endphp>
                <label for="vehicle2">Accucheck/ injections</label>
              </li>
              <li>
                <input type="checkbox" name="Skilled[]" value="9" @php echo (in_array("9", $Skilled2)?'checked':''); @endphp>
                <label for="vehicle2">Other</label>
              </li>
            </ul>
          </div>
          <div class="chkBxsList">
            <h6>Evaluation:</h6>
            <h4> What is the Member’s condition compared to the last annual plan care year? </h4>
            <ul class="list-inline checkboxes">
              <li>
                <input type="checkbox" name="Evaluation[]" value="1" @php echo (in_array("1", $Evaluation2)?'checked':''); @endphp>
                <label for="vehicle1">Stable</label>
              </li>
              <li>
                <input type="checkbox" name="Evaluation[]" value="2" @php echo (in_array("2", $Evaluation2)?'checked':''); @endphp>
                <label for="vehicle2">Improved</label>
              </li>
              <li>
                <input type="checkbox" name="Evaluation[]" value="3" @php echo (in_array("3", $Evaluation2)?'checked':''); @endphp>
                <label for="vehicle2">Declining</label>
              </li>
            </ul>
          </div>
          <div class="chkBxsList functional">
            <h6>Functional Potential:</h6>
            <ul class="list-inline checkboxes">
              <li>
                <input type="checkbox" name="Potential[]" value="1" @php echo (in_array("1", $Potential2)?'checked':''); @endphp>
                <label for="vehicle1">Member believes he/she is capable of increased functional independence</label>
              </li>
              <li>
                <input type="checkbox" name="Potential[]" value="2" @php echo (in_array("2", $Potential2)?'checked':''); @endphp>
                <label for="vehicle2">Caregivers believe Member is capable increased functional independence</label>
              </li>
              <li>
                <input type="checkbox" name="Potential[]" value="3" @php echo (in_array("3", $Potential2)?'checked':''); @endphp>
                <label for="vehicle2">Good prospects of recovery for current disease of conditions, improved health status expected</label>
              </li>
              <li>
                <input type="checkbox" name="Potential[]" value="4" @php echo (in_array("4", $Potential2)?'checked':''); @endphp>
                <label for="vehicle2">None of the above </label>
              </li>
            </ul>
          </div>
          <div class="chkBxsList significant">
            <h6>Significant Changes in Last Year as reported by the ICWP member: </h6>
            <ul class="list-inline checkboxes">
              <li>
                <input type="checkbox" name="ICWP_member[]" value="1" @php echo (in_array("1", $ICWP_member2)?'checked':''); @endphp>
                <label for="vehicle1">Hospitalizations</label>
              </li>
              <li>
                <input type="checkbox" name="ICWP_member[]" value="2" @php echo (in_array("2", $ICWP_member2)?'checked':''); @endphp>
                <label for="vehicle2">Functional Decline</label>
              </li>
              <li>
                <input type="checkbox" name="ICWP_member[]" value="3" @php echo (in_array("3", $ICWP_member2)?'checked':''); @endphp>
                <label for="vehicle2">Caregiver Status change</label>
              </li>
              <li>
                <input type="checkbox" name="ICWP_member[]" value="4" @php echo (in_array("4", $ICWP_member2)?'checked':''); @endphp>
                <label for="vehicle2">New Diagnosis </label>
              </li>
              <li>
                <input type="checkbox" name="ICWP_member[]" value="5" @php echo (in_array("5", $ICWP_member2)?'checked':''); @endphp>
                <label for="vehicle2">Loss of Loved One </label>
              </li>
              <li>
                <input type="checkbox" name="ICWP_member[]" value="6" @php echo (in_array("6", $ICWP_member2)?'checked':''); @endphp>
                <label for="vehicle2">Any Sentinel Event</label>
              </li>
            </ul>
          </div>
          <h6>Comments:</h6>
          <textarea rows="8" name="Comments"> </textarea>
        </div>
      </div>
    </div>
    <div class="pages">
      <div class="container">
        <ul class="list-inline">
            <input type="Submit" value="Next" class="submitbtnsave">
        <li><a href="{{route('careform_form14index',$careform_id)}}">Back</a></li>

        </ul>
      </div>
    </div>
  </div>
            </form>
            @endforeach

          </body>


@endsection


<script>
    $('#datatable_tabletools').DataTable({
        dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete : function(oSettings, json) {
            $(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
                $(this).addClass('btn-sm btn-default');
            });
        },
        "language": {
            "search": "Search: ",
            "searchPlaceholder": "Search records"
        },
        "pageLength": 50,
        "bDestroy": true,

        "order": []
    });
    /* END TABLE TOOLS */

    function ReportError(res)
    {
    }// Filter placeholder dataTable
    $(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>

<script src="{{ asset('js/all.js') }}""></script>
<!-- jquery.slicknav JS -->
<script src="{{ asset('js/jquery.slicknav.js') }}""></script>
<!-- Custom JS -->
<script src="{{ asset('js/custom.js') }}""></script>
