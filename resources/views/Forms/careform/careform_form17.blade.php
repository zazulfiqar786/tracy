


@extends('layouts.masterlayout')

@section('content')
    {{-- <a href="{{route('careform.create_form',$clientId)}}"" class="btn btn-success fa-pull-right" style="margin-right: 20px;">Create Form</a> --}}
    <br>
    <br>


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>New Care form</title>
        <!-- All CSS -->
        {{-- <link href="{{ asset('css/all.css') }}" rel="stylesheet"> --}}
        <!-- slicknav CSS -->
        <link href="{{ asset('css/slicknav.css') }}" rel="stylesheet">
        <!-- Style CSS -->
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <!-- responsive css -->
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
        <!-- Google Fonts CSS -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

        </head>
<style>
    .topbar .navbar-search input {
    /* font-size: .85rem; */
    height: 62px;
    margin-top: 10px;
}
</style>

        <body class="responsive">




            @foreach ($forms as $form)


            @php
            $where_the_wound = $form->where_the_wound;
            $where_the_wound2 = explode(',', $where_the_wound);
            @endphp

            @php
            $primary_modes = $form->primary_modes;
            $primary_modes2 = explode(',', $primary_modes);
            @endphp


            <form method="POST" action="{{route('careform_form17Update',$careform_id)}}">
                @csrf
                @method('get')
                <input type="hidden" name="careform_id" value=" {{ $careform_id }}">


                <div class="allPadding newCareForm">
                    <div class="topSecform">
                      <div class="container">
                        <div class="formHead ">
                          <div class="row">
                            <div class="col-md-9 col-sm-9 col-xs-12 centerCol">
                              <h2>Appendix D-4 </h2>
                              <h2>RENEWAL PARTICIPANT ASSESSMENT FORM</h2>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-2 col-sm-2 col-xs-12">
                              <div class="heading">
                                <h4>Member name:</h4>
                              </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                              <div class="putData">
                                <input type="text" name="member_name" value="{{ $form->member_name }}">
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="appenCMain statusSec">
                          <h6>SKIN:</h6>
                          <div class="row">
                            <div class="col-md-7 col-sm-7 col-xs-12">
                              <div class="heading">
                                <h4>Does member have a wound?</h4>
                              </div>
                            </div>
                            <div class="col-md-5 col-sm-5 col-xs-12">
                              <ul class="list-inline checkboxes">
                                <li>
                                  <input type="radio" name="member_wound"  value="Yes" @php echo ($form->member_wound == 'Yes'?'checked':''); @endphp>
                                  <label for="vehicle1">Yes</label>
                                </li>
                                <li>
                                  <input type="radio" name="member_wound" value="No" @php echo ($form->member_wound == 'No'?'checked':''); @endphp>
                                  <label for="vehicle2">No</label>
                                </li>
                              </ul>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-5 col-sm-5 col-xs-12">
                              <div class="heading">
                                <h4>If yes, where is it located and what stage is it? </h4>
                              </div>
                            </div>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                              <div class="putData">
                                <input type="text" name="where_is_located" value="{{ $form->where_is_located }}">
                              </div>
                            </div>
                          </div>
                          <div class="chkBxsList incontinenceSec">
                            <h6>If yes, where is the wound being treated:</h6>
                            <ul class="list-inline checkboxes">
                              <li>
                                <input type="checkbox" name="where_the_wound[]" @php echo (in_array("1", $where_the_wound2)?'checked':''); @endphp value="1">
                                <label for="vehicle1">Home Health</label>
                              </li>
                              <li>
                                <input type="checkbox" name="where_the_wound[]" @php echo (in_array("2", $where_the_wound2)?'checked':''); @endphp value="2">
                                <label for="vehicle2">Wound Clinic</label>
                              </li>
                              <li>
                                <input type="checkbox" name="where_the_wound[]" @php echo (in_array("3", $where_the_wound2)?'checked':''); @endphp  value="3">
                                <label for="vehicle2">Doctor’s office</label>
                              </li>
                              <li>
                                <input type="checkbox" name="where_the_wound[]" @php echo (in_array("4", $where_the_wound2)?'checked':''); @endphp  value="4">
                                <label for="vehicle2">Informal Caregiver</label>
                              </li>
                              <li>
                                <input type="checkbox" name="where_the_wound[]" @php echo (in_array("5", $where_the_wound2)?'checked':''); @endphp  value="5">
                                <label for="vehicle2">Other</label>
                              </li>
                            </ul>
                          </div>
                          <p><b><i>Please attach wound care notes. The notes should include location, stage of wound, current treatment plan, progress being made on treatment. If skilled PSS care for wounds has been approved, then submit notes from the PSS provider detailing the additional care aides are providing</i></b></p>
                          <div class="row">
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <div class="heading">
                                <h4> Have you included these notes in a separate attachment?</h4>
                              </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                              <ul class="list-inline checkboxes">
                                <li>
                                  <input type="radio" name="separate_attachment" value="Yes" @php echo ($form->separate_attachment == 'Yes'?'checked':''); @endphp>
                                  <label for="vehicle1">Yes</label>
                                </li>
                                <li>
                                  <input type="radio" name="separate_attachment" value="No" @php echo ($form->separate_attachment == 'No'?'checked':''); @endphp>
                                  <label for="vehicle2">No</label>
                                </li>
                              </ul>
                            </div>
                          </div>
                          <h6>Comments:</h6>
                          <textarea rows="8" name="comments1" value="{{ $form->comments1 }}">{{ $form->comments1 }}</textarea>
                          <div class="chkBxsList incontinenceSec">
                            <h6>Primary Modes of Locomotion:</h6>
                            <ul class="list-inline checkboxes">
                              <li>
                                <input type="checkbox" name="primary_modes[]" @php echo (in_array("1", $primary_modes2)?'checked':''); @endphp  value="1">
                                <label for="vehicle1">No assistive device</label>
                              </li>
                              <li>
                                <input type="checkbox" name="primary_modes[]"@php echo (in_array("2", $primary_modes2)?'checked':''); @endphp  value="2">
                                <label for="vehicle2">Wheelchair </label>
                              </li>
                              <li>
                                <input type="checkbox" name="primary_modes[]" @php echo (in_array("3", $primary_modes2)?'checked':''); @endphp  value="3">
                                <label for="vehicle2">Cane</label>
                              </li>
                              <li>
                                <input type="checkbox" name="primary_modes[]" @php echo (in_array("4", $primary_modes2)?'checked':''); @endphp  value="4">
                                <label for="vehicle2">Activity does not occur</label>
                              </li>
                              <li>
                                <input type="checkbox" name="primary_modes[]" @php echo (in_array("5", $primary_modes2)?'checked':''); @endphp  value="5">
                                <label for="vehicle2">Walker/crutch scooter </label>
                              </li>
                              <li>
                                <input type="checkbox" name="primary_modes[]" @php echo (in_array("6", $primary_modes2)?'checked':''); @endphp  value="6">
                                <label for="vehicle2">Other</label>
                              </li>
                            </ul>
                          </div>
                          <h6>Comments:</h6>
                          <textarea rows="8" name="comments2" value="{{ $form->comments2 }}">{{ $form->comments2 }}</textarea>
                        </div>
                      </div>
                    </div>
                    <div class="pages">
                      <div class="container">
                        <ul class="list-inline">
                            <input type="Submit" value="Next" class="submitbtnsave">
                        <li><a href="{{route('careform_form16index',$careform_id)}}">Back</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>

            </form>
            @endforeach

          </body>


@endsection


<script>
    $('#datatable_tabletools').DataTable({
        dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete : function(oSettings, json) {
            $(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
                $(this).addClass('btn-sm btn-default');
            });
        },
        "language": {
            "search": "Search: ",
            "searchPlaceholder": "Search records"
        },
        "pageLength": 50,
        "bDestroy": true,

        "order": []
    });
    /* END TABLE TOOLS */

    function ReportError(res)
    {
    }// Filter placeholder dataTable
    $(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>

<script src="{{ asset('js/all.js') }}""></script>
<!-- jquery.slicknav JS -->
<script src="{{ asset('js/jquery.slicknav.js') }}""></script>
<!-- Custom JS -->
<script src="{{ asset('js/custom.js') }}""></script>
