


@extends('layouts.masterlayout')

@section('content')
    {{-- <a href="{{route('careform.create_form',$clientId)}}"" class="btn btn-success fa-pull-right" style="margin-right: 20px;">Create Form</a> --}}
    <br>
    <br>


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>New Care form</title>
        <!-- All CSS -->
        {{-- <link href="{{ asset('css/all.css') }}" rel="stylesheet"> --}}
        <!-- slicknav CSS -->
        <link href="{{ asset('css/slicknav.css') }}" rel="stylesheet">
        <!-- Style CSS -->
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <!-- responsive css -->
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
        <!-- Google Fonts CSS -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

        </head>
<style>
    .topbar .navbar-search input {
    /* font-size: .85rem; */
    height: 62px;
    margin-top: 10px;
}
</style>

        <body class="responsive">

        <div class="allPadding newCareForm">
    <div class="topSecform">
      <div class="container">
        <div class="formHead ">
          <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12 centerCol">
              <h2>APPENDIX C-1</h2>
              <h2>CONSUMER DIRECETD CARE OPTION<br> MEMORANDUM OF UNDERSTANDING</h2>
            </div>
          </div>
        </div>
        <div class="appenCMain">
          <h6>Discarge of Members:</h6>
          <p>Discharge may take place when any of the following occurs:</p>
          <ul>
            <li>GMCF in consultation with the case manager and other providers, determines that the member is no longer appropriate or eligible for ICWP</li>
            <li>The Utilization Review (UR) staff recommends discharge (10 day notice is required)</li>
            <li>The member has not received ICWP personal support services for sixty (60) consecutive days.</li>
            <li>The member has behavior that is disruptive, illegal, threatening, and /or dangerous to self or others.</li>
            <li> The member refuses to comply with treatment/agreed upon plan of care.</li>
            <li> The member chooses to be discharged or enters long-term facility.</li>
            <li>The cost to serve member exceeds the allocated budgeted amount.</li>
            <li> The member knowingly and freely commits fraudulent activities.</li>
            <li> Case Manager notifies GMCF review nurses of member’s failure to follow the following polices:</li>
              <ul class="furtherList">
                <li> Failure to meet the following critical carepath goals for two consecutive quarters:</li>
                  <ul class="morefurtherList">
                    <li>Failure to maintain maximum control over daily schedule and decisions</li>
                    <li>Failure to assume responsibility for cost effective use of medical services and supplies</li>
                    <li> Exhibition of problem or symptomatic behavior which places the ICWP participant at risk of social isolation, neglect, or physical injury to themselves or others </li>
                  </ul>
                <li> Failure to stay within budget for two consecutive months</li>
                <li> Use of the State Backup plan for at least two occasions for two consecutive months</li>
                <li> Preventable decline in health outcomes for two consecutive quarters.</li>
              </ul>
            <li>GMCF review nurse will send a denial for continued placement in Consumer-Directed Services and return the member to Traditional Services.<br>
            <b>Members May reapply for the Consumer Directed Option:</b><br>When a consumer is moved back into the traditional option there is no loss of services. After one year the consumer will be eligible to reapply for Consumer-Directed option. 
            </li>
            <li>Additional terms</li>
              <ul class="furtherList">
                <li>As an ICWP consumer/ representative, it is your responsibility to be actively involved in achieving goals related to good health and community living. Goals identified for all participants in ICWP are:</li>
                  <ul class="morefurtherList">
                    <li>Maintain maximum control over daily schedules and decisions.</li>
                    <li>Participate socially and be connected and involved in community activities of your choice.</li>
                    <li>Assume responsibility of cost effective medical services and supplies.</li>
                    <li>Assume responsibility of cost effective medical services and supplies.</li>
                    <li> Maintain a diet that is balanced and appropriate for decreasing risk of further disability.</li>
                    <li>Participate in interventions that maintain skin in a healthy condition, avoiding breakdowns.</li>
                    <li> Understand and observe medication regimen.</li>
                    <li>Participate in interventions of daily living without interruptions due to cognitive or physical impairments (self or informal caregiver report, observation by case manager, provider reports, etc).</li>
                    <li> Maintain bowel and bladder program that promotes skin integrity, cleanliness and positive health status.</li>
                    <li> Performs transfers and mobility safely and when needed.</li>
                  </ul>
                <li>It is the responsibility as ICWP case managers to:</li>
                  <ul class="morefurtherList">
                    <li> Respond to phone calls and requests in a timely manner</li>
                    <li>Respect your choices throughout the development and maintenance of your individual community plan.</li>
                    <li> Provide you with options and information in order for you to make informed decisions.</li>
                    <li>Meet with you and your significant other and notify you of any changes in your service.</li>
                    <li> Take responsibility to initiate termination of ICWP services after a 30 day notice is given if any of the following activities are taking place; use of drugs or other harmful substances, consumer or household involvement in any type of illegal activity, lack of effort or concern in maintaining you physical health or emotional health, lack of effort or concern in actively participation in your life, or abuse of any services provided by ICWP</li>
                  </ul>
              </ul>
          </ul>
        </div>
      </div>
    </div>
    <div class="pages">
      <div class="container">
        <ul class="list-inline">
        <li><a href="{{route('careform_form8index',$careform_id)}}">Back</a></li>
        <li><a href="{{route('careform_form10index',$careform_id)}}">Next</a></li>
        </ul>
      </div>
    </div>
  </div>

          </body>


@endsection


<script>
    $('#datatable_tabletools').DataTable({
        dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete : function(oSettings, json) {
            $(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
                $(this).addClass('btn-sm btn-default');
            });
        },
        "language": {
            "search": "Search: ",
            "searchPlaceholder": "Search records"
        },
        "pageLength": 50,
        "bDestroy": true,

        "order": []
    });
    /* END TABLE TOOLS */

    function ReportError(res)
    {
    }// Filter placeholder dataTable
    $(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>

<script src="{{ asset('js/all.js') }}""></script>
<!-- jquery.slicknav JS -->
<script src="{{ asset('js/jquery.slicknav.js') }}""></script>
<!-- Custom JS -->
<script src="{{ asset('js/custom.js') }}""></script>
