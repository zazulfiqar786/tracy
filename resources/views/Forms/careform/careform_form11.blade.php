


@extends('layouts.masterlayout')

@section('content')
    {{-- <a href="{{route('careform.create_form',$clientId)}}"" class="btn btn-success fa-pull-right" style="margin-right: 20px;">Create Form</a> --}}
    <br>
    <br>


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>New Care form</title>
        <!-- All CSS -->
        {{-- <link href="{{ asset('css/all.css') }}" rel="stylesheet"> --}}
        <!-- slicknav CSS -->
        <link href="{{ asset('css/slicknav.css') }}" rel="stylesheet">
        <!-- Style CSS -->
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <!-- responsive css -->
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
        <!-- Google Fonts CSS -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

        </head>
<style>
    .topbar .navbar-search input {
    /* font-size: .85rem; */
    height: 62px;
    margin-top: 10px;
}
</style>
Form 11
        <body class="responsive">



      


            <form method="POST" action="{{route('careform_form8Update',$careform_id)}}">
                @csrf
                @method('get')
                <input type="hidden" name="careform_id" value=" {{ $careform_id }}">
                <div class="allPadding newCareForm">
    <div class="topSecform">
      <div class="container">
        <div class="formHead ">
          <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12 centerCol">
              <h2>APPENDIX B</h2>
              <h2>MEMBER RIGHTS AND RESPONSIBILITIES</h2>
            </div>
          </div>
        </div>
        <div class="appenCMain">
          <h6>Member’s rights include:</h6>
          <ul>
            <li>The right of access to accurate and easy-to-understand information.</li>
            <li>The right to be treated with respect and to maintain one’s dignity and individuality.</li>
            <li> The right to voice grievances and complaints regarding treatment or care that is furnished, without fear of retaliation, discrimination, coercion, or reprisal.</li>
            <li> The right of choice of an approved provider.</li>
            <li>The right to accept or refuse services</li>
            <li>The right to be informed of and participate in preparing the care plan and any changes in the plan.</li>
            <li>The right to be advised in advance of the provider(s) who will furnish care and the frequency and duration of visits ordered.</li>
            <li>The right to confidential treatment of all information, including information in the member record.</li>
            <li> The right to receive services in accordance with the current plan of care.</li>
            <li>The right to be informed of the name, business telephone number and business address of the person/agency supervising the services and how to contact that person/agency.</li>
            <li>The right to have property and place of residence treated with respect.</li>
            <li>The right to review member’s records on request.</li>
            <li> The rights to receive care and services without discrimination.</li>
          </ul>
        </div>
      </div>
    </div>
    <div class="pages">
      <div class="container">
        <ul class="list-inline">
        <li><a href="{{route('careform_form10index',$careform_id)}}">Back</a></li>
        <li><a href="{{route('careform_form12index',$careform_id)}}">Next</a></li>
        </ul>
      </div>
    </div>
  </div>
            </form>
        
          </body>


@endsection


<script>
    $('#datatable_tabletools').DataTable({
        dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete : function(oSettings, json) {
            $(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
                $(this).addClass('btn-sm btn-default');
            });
        },
        "language": {
            "search": "Search: ",
            "searchPlaceholder": "Search records"
        },
        "pageLength": 50,
        "bDestroy": true,

        "order": []
    });
    /* END TABLE TOOLS */

    function ReportError(res)
    {
    }// Filter placeholder dataTable
    $(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>

<script src="{{ asset('js/all.js') }}""></script>
<!-- jquery.slicknav JS -->
<script src="{{ asset('js/jquery.slicknav.js') }}""></script>
<!-- Custom JS -->
<script src="{{ asset('js/custom.js') }}""></script>
