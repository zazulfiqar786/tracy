


@extends('layouts.masterlayout')

@section('content')

    <br>
    <br>


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>New Care form</title>
        <!-- All CSS -->
        {{-- <link href="{{ asset('css/all.css') }}" rel="stylesheet"> --}}
        <!-- slicknav CSS -->
        <link href="{{ asset('css/slicknav.css') }}" rel="stylesheet">
        <!-- Style CSS -->
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <!-- responsive css -->
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
        <!-- Google Fonts CSS -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

        </head>
<style>
    .topbar .navbar-search input {
    /* font-size: .85rem; */
    height: 62px;
    margin-top: 10px;
}
</style>

        <body class="responsive">

        <div class="allPadding newCareForm">
    <div class="topSecform">
      <div class="container">
        <div class="formHead ">
          <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12 centerCol">
              <h2>APPENDIX C-1</h2>
              <h2>CONSUMER DIRECETD CARE OPTION<br> MEMORANDUM OF UNDERSTANDING</h2>
            </div>
          </div>
        </div>
        <div class="appenCMain">
          <h6>General Understanding:</h6>
          <ul>
            <li> Georgia Medical Care Foundation (GMCF) or Shepherd Care develops the Initial Plan of Care.</li>
            <li>Approval is needed in writing from GMCF for all services rendered under this program.</li>
            <li>The member or his/her representative is responsible for hiring, training, supervising, disciplining, terminating and paying their attendant through the FI.</li>
            <li>All services must be rendered according to the Individual Plan of Care signed by the member, case manager, FI and the planning team and approved by GMCF and/or ShepherdCare</li>
            <li> Cost of care must be provided within the allocated budgeted amount. The monies for salaries, taxes and benefits of personal support service provider and the FI agency fee will be funded from the budgeted personal supportservice hours.</li>
            <li> The member or his/her representative will have the freedom of choice to choose his or her Fiscal Intermediary, an enrolled Medicaid Provider.</li>
            <li>The member or his/her representative will be responsible for staffing/schedule attendants (s) as well as developing a back up plan should the primary attendant not be available</li>
            <li> A member may not participate in more than one Medicaid Waiver Programs at the same time.</li>
            <li> The Utilization Review team will make in-home visits as needed and required.</li>
            <li> The member is aware that if he/she is admitted to a hospital he/she cannot continue to receive PSS hours and will not be paid during the hospital stay. Caregivers will not be paid during the period an ICWP member is hospitalized.</li>
            <li>The member must be responsible for maintenance, proper use and replacement of the fax machine.</li>
            <li>The <b> member is responsible</b> for any cost associated with backgrounds checks that <b> exceed 5</b> per Plan of Care year.</li>
            <li>The <b>member will be responsible </b>for paying any employee, if the background check has not been confirmed by the FI.</li>
          </ul>
        </div>
      </div>
    </div>
    <div class="pages">
      <div class="container">
        <ul class="list-inline">
        <li><a href="{{route('careform_form7index',$careform_id)}}">Back</a></li>
        <li><a href="{{route('careform_form9index',$careform_id)}}">Next</a></li>
      </div>
    </div>
  </div>


            <!-- @foreach ($forms as $form) -->




            <!-- <form method="POST" action="{{route('careform_form8Update',$careform_id)}}">
                @csrf
                @method('get')
                <input type="hidden" name="careform_id" value=" {{ $careform_id }}">

            </form> -->
            <!-- @endforeach -->

          </body>


@endsection


<script>
    $('#datatable_tabletools').DataTable({
        dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete : function(oSettings, json) {
            $(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
                $(this).addClass('btn-sm btn-default');
            });
        },
        "language": {
            "search": "Search: ",
            "searchPlaceholder": "Search records"
        },
        "pageLength": 50,
        "bDestroy": true,

        "order": []
    });
    /* END TABLE TOOLS */

    function ReportError(res)
    {
    }// Filter placeholder dataTable
    $(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>

<script src="{{ asset('js/all.js') }}""></script>
<!-- jquery.slicknav JS -->
<script src="{{ asset('js/jquery.slicknav.js') }}""></script>
<!-- Custom JS -->
<script src="{{ asset('js/custom.js') }}""></script>
