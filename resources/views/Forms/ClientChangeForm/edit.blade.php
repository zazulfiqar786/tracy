@extends('layouts.masterlayout')

@section('content')

    <!-- All CSS -->
    {{--    <link href="{{asset('css/all.css')}}" rel="stylesheet">--}}
    {{--    <!-- slicknav CSS -->--}}
    {{--    <link href="{{asset('css/slicknav.css')}}" rel="stylesheet">--}}
    <!-- Style CSS -->
    <link href="{{asset('css/custom.css')}}" rel="stylesheet">
    <!-- responsive css -->
    <link href="{{asset('css/responsive.css')}}" rel="stylesheet">
    <!-- Google Fonts CSS -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

    <body class="responsive">
@php
    $myString = $form->change_type;
    $myArray = explode(',', $myString);
@endphp
<form method="POST" action="{{route('ClientChangeForm.update',$form->id)}}">
    @csrf
    @method('put')

    <input type="hidden" name="client_id" value="{{$form->client_id}}">
        <div class="allPadding">
          <div class="topSecform">
            <div class="container">
              <div class="formHead">
                <div class="row">
                  <div class="col-md-8 col-sm-8 col-xs-12 centerCol">
                    <h2>Client Change Form</h2>
                  </div>
                </div>
              </div>
              <div class="formTable ">
                <div class="row flexRow">
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Member Name:</h4>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="member_name" value="{{$form->member_name}}" >
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Date of Change:</h4>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="date_of_change" value="{{$form->date_of_change}}" >
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Change Type:</h4>
                    </div>
                  </div>
                  <div class="col-md-9 col-sm-9 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox" name="change_type[]" @php echo (in_array("1", $myArray)?'checked':''); @endphp value="1" >
                        <label for=""> Case Manager</label>
                      </li>
                      <li>
                        <input type="checkbox" name="change_type[]" @php echo (in_array("2", $myArray)?'checked':''); @endphp  value="2" >
                        <label for=""> Hospitalization</label>
                      </li>
                      <li>
                        <input type="checkbox" name="change_type[]" @php echo (in_array("3", $myArray)?'checked':''); @endphp value="3" >
                        <label for="">Personal Support Provider</label>
                      </li>
                      <li>
                        <input type="checkbox" name="change_type[]" @php echo (in_array("4", $myArray)?'checked':''); @endphp value="4" >
                        <label for="">Respite</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Name of new Case Manager/Personal Support Provider:</h4>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="name_new_case_manager" value="{{$form->name_new_case_manager}}" >
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Respite:</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Approved Hours:</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="approved_hours" value="{{$form->approved_hours}}" >
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Remaining Hours:</h4>
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="remaining_hours" value="{{$form->remaining_hours}}" >
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="additionSec">
            <div class="container">
              <h4>Additional Notes/Documentation:</h4>
              <textarea rows="10" name="additionals_notes_documentation" >{{$form->additionals_notes_documentation }}</textarea>
            </div>
          </div>
          <div class="signSec">
            <div class="container">
              <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <div class="heading">
                    <h4>CASE MANAGER SIGNATURE:</h4>
                  </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <div class="putData">
                    <input type="text" name="case_manager_signature" value="{{$form->case_manager_signature}}" >
                  </div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12">
                </div>
                <div class="col-md-1 col-sm-1 col-xs-12">
                  <div class="heading">
                    <h4>DATE:</h4>
                  </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <div class="putData">
                    <input type="text" name="case_manager_date" value="{{$form->case_manager_date}}" >
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="signSec">
            <div class="container">
              <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <div class="heading">
                    <h4>OWNER SIGNATURE:</h4>
                  </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <div class="putData">
                    <input type="text" name="owner_signature" value="{{$form->owner_signature}}" >
                  </div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12">
                </div>
                <div class="col-md-1 col-sm-1 col-xs-12">
                  <div class="heading">
                    <h4>DATE:</h4>
                  </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <div class="putData">
                    <input type="text" name="owner_date" value="{{$form->owner_date}}" >
                  </div>
                </div>
              </div>
              <center> <input type="submit" value="Save" name="btnSubmit" class="btn btn-success"></center>
            </div>
          </div>
        </div>
</form>
      </body>

    <!-- All JS -->
    <script src="{{asset('js/all.js')}}"></script>
    <!-- jquery.slicknav JS -->
    <script src="{{asset('js/jquery.slicknav.js')}}"></script>
    <!-- Custom JS -->
    <script src="{{asset('js/custom.js')}}"></script>

@endsection
