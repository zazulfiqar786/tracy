@extends('layouts.masterlayout')

@section('content')

    <!-- All CSS -->
    {{--    <link href="{{asset('css/all.css')}}" rel="stylesheet">--}}
    {{--    <!-- slicknav CSS -->--}}
    {{--    <link href="{{asset('css/slicknav.css')}}" rel="stylesheet">--}}
    <!-- Style CSS -->
    <link href="{{asset('css/custom.css')}}" rel="stylesheet">
    <!-- responsive css -->
    <link href="{{asset('css/responsive.css')}}" rel="stylesheet">
    <!-- Google Fonts CSS -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

    <body class="responsive">


             {{-- <input type="hidden" name="client_id" value="{{$clientId}}"> --}}
            <div class="allPadding">
                <div class="topSecform">
                  <div class="container">
                    <div class="formHead">
                      <div class="row">
                        <div class="col-md-8 col-sm-8 col-xs-12 centerCol">
                          <h2>Follow Up Form</h2>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="followTopSec">
                  <div class="container">
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                      <div class="heading">
                        <h4>Member Name:</h4>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                      <div class="putData">
                        <input type="text" name="member_name" value="{{ $form->member_name }}" disabled>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="followTable ">
                  <div class="container">
                    <div class="followUpTable table-bordered">
                      <div class="followTableHead">
                        <div class="row flexRow">
                          <div class="col-md-2 col-sm-2 col-xs-12 col noPadding">
                            <div class="heading">
                              <h4>Time</h4>
                            </div>
                          </div>
                          <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                            <div class="heading">
                              <h4>Plan/FU #</h4>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                            <div class="heading">
                              <h4>Notes</h4>
                            </div>
                          </div>
                          <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                            <div class="heading">
                              <h4>Doc. Hrs</h4>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row flexRow">
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                          <ul class="list-inline dateData">
                            <li class="heading">
                              <h4>Date:</h4>
                            </li>
                            <li class="putData">
                              <input type="text" name="date_1" value="{{ $form->date_1 }}" disabled>
                            </li>
                            <div class="clearfix"></div>
                            <li class="heading">
                              <h4>ST:</h4>
                            </li>
                            <li class="putData">
                              <input type="text" name="st_1" value="{{ $form->st_1 }}" disabled>
                            </li>
                            <div class="clearfix"></div>
                            <li class="heading">
                              <h4>ET:</h4>
                            </li>
                            <li class="putData">
                              <input type="text" name="et_1" value="{{ $form->et_1 }}" disabled>
                            </li>
                          </ul>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                          <ul class="list-inline plan">
                            <li class="putData">
                              <input type="text" name="plan_fu1"  value="{{ $form->plan_fu1 }}" disabled>
                            </li>
                          </ul>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                          <ul class="list-inline">
                            <li class="putData">
                              <input type="text" name="notes1"  value="{{ $form->notes1 }}" disabled>
                              {{-- <input type="text" name="">
                              <input type="text" name="">
                              <input type="text" name="">
                              <input type="text" name=""> --}}
                            </li>
                          </ul>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                          <ul class="list-inline">
                            <li class="putData">
                              <input type="text" name="doc_hrs1" class="dcrHr"  value="{{ $form->doc_hrs1 }}" disabled>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="row flexRow">
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                          <ul class="list-inline dateData">
                            <li class="heading">
                              <h4>Date:</h4>
                            </li>
                            <li class="putData">
                              <input type="text" name="date_2"   value="{{ $form->date_2 }}" disabled>
                            </li>
                            <div class="clearfix"></div>
                            <li class="heading">
                              <h4>ST:</h4>
                            </li>
                            <li class="putData">
                              <input type="text" name="st_2"  value="{{ $form->st_2 }}" disabled>
                            </li>
                            <div class="clearfix"></div>
                            <li class="heading">
                              <h4>ET:</h4>
                            </li>
                            <li class="putData">
                              <input type="text" name="et_2"  value="{{ $form->et_2 }}" disabled>
                            </li>
                          </ul>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                          <ul class="list-inline plan">
                            <li class="putData">
                              <input type="text" name="plan_fu2"  value="{{ $form->plan_fu2 }}" disabled>
                            </li>
                          </ul>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                          <ul class="list-inline">
                            <li class="putData">
                              <input type="text" name="notes2"  value="{{ $form->notes2 }}" disabled>
                              {{-- <input type="text" name="">
                              <input type="text" name="">
                              <input type="text" name="">
                              <input type="text" name=""> --}}
                            </li>
                          </ul>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                          <ul class="list-inline">
                            <li class="putData">
                              <input type="text" name="doc_hrs2" class="dcrHr"  value="{{ $form->doc_hrs2 }}" disabled>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="row flexRow">
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                          <ul class="list-inline dateData">
                            <li class="heading">
                              <h4>Date:</h4>
                            </li>
                            <li class="putData">
                              <input type="text" name="date_3"  value="{{ $form->date_3 }}" disabled>
                            </li>
                            <div class="clearfix"></div>
                            <li class="heading">
                              <h4>ST:</h4>
                            </li>
                            <li class="putData">
                              <input type="text" name="st_3"  value="{{ $form->st_3 }}" disabled>
                            </li>
                            <div class="clearfix"></div>
                            <li class="heading">
                              <h4>ET:</h4>
                            </li>
                            <li class="putData">
                              <input type="text" name="et_3" value="{{ $form->et_3 }}" disabled>
                            </li>
                          </ul>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                          <ul class="list-inline plan">
                            <li class="putData">
                              <input type="text" name="plan_fu3"  value="{{ $form->plan_fu3 }}" disabled>
                            </li>
                          </ul>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                          <ul class="list-inline">
                            <li class="putData">
                              <input type="text" name="notes3"  value="{{ $form->notes3 }}" disabled>
                              {{-- <input type="text" name="">
                              <input type="text" name="">
                              <input type="text" name="">
                              <input type="text" name=""> --}}
                            </li>
                          </ul>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                          <ul class="list-inline">
                            <li class="putData">
                              <input type="text" name="doc_hrs3" class="dcrHr"  value="{{ $form->doc_hrs3 }}" disabled>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="row flexRow">
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                          <ul class="list-inline dateData">
                            <li class="heading">
                              <h4>Date:</h4>
                            </li>
                            <li class="putData">
                              <input type="text" name="date_4" value="{{ $form->date_4 }}" disabled>
                            </li>
                            <div class="clearfix"></div>
                            <li class="heading">
                              <h4>ST:</h4>
                            </li>
                            <li class="putData">
                              <input type="text" name="st_4" value="{{ $form->st_4 }}" disabled>
                            </li>
                            <div class="clearfix"></div>
                            <li class="heading">
                              <h4>ET:</h4>
                            </li>
                            <li class="putData">
                              <input type="text" name="et_4"  value="{{ $form->et_4 }}" disabled>
                            </li>
                          </ul>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                          <ul class="list-inline plan">
                            <li class="putData">
                              <input type="text" name="plan_fu4"   value="{{ $form->plan_fu4 }}" disabled>
                            </li>
                          </ul>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                          <ul class="list-inline">
                            <li class="putData">
                              <input type="text" name="notes4"   value="{{ $form->notes4 }}" disabled>
                              {{-- <input type="text" name="">
                              <input type="text" name="">
                              <input type="text" name="">
                              <input type="text" name=""> --}}
                            </li>
                          </ul>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                          <ul class="list-inline">
                            <li class="putData">
                              <input type="text" name="doc_hrs4" class="dcrHr"  value="{{ $form->doc_hrs4 }}" disabled>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="signSec">
                  <div class="container">
                    <div class="row">
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="heading">
                          <h4>CASE MANAGER SIGNATURE:</h4>
                        </div>
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="putData">
                          <input type="text" name="case_manager_signature"  value="{{ $form->case_manager_signature }}" disabled>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                      </div>
                      <div class="col-md-1 col-sm-1 col-xs-12">
                        <div class="heading">
                          <h4>DATE:</h4>
                        </div>
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="putData">
                          <input type="text" name="case_manager_date"  value="{{ $form->case_manager_date }}" disabled>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="signSec">
                  <div class="container">
                    <div class="row">
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="heading">
                          <h4>OWNER SIGNATURE:</h4>
                        </div>
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="putData">
                          <input type="text" name="owner_signature" value="{{ $form->owner_signature }}" disabled>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                      </div>
                      <div class="col-md-1 col-sm-1 col-xs-12">
                        <div class="heading">
                          <h4>DATE:</h4>
                        </div>
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="putData">
                          <input type="text" name="owner_date"  value="{{ $form->owner_date }}" disabled>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

      </body>

    <!-- All JS -->
    <script src="{{asset('js/all.js')}}"></script>
    <!-- jquery.slicknav JS -->
    <script src="{{asset('js/jquery.slicknav.js')}}"></script>
    <!-- Custom JS -->
    <script src="{{asset('js/custom.js')}}"></script>

@endsection
