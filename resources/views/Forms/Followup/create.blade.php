@extends('layouts.masterlayout')

@section('content')

    <!-- All CSS -->
    {{--    <link href="{{asset('css/all.css')}}" rel="stylesheet">--}}
    {{--    <!-- slicknav CSS -->--}}
    {{--    <link href="{{asset('css/slicknav.css')}}" rel="stylesheet">--}}
    <!-- Style CSS -->
    <link href="{{asset('css/custom.css')}}" rel="stylesheet">
    <!-- responsive css -->
    <link href="{{asset('css/responsive.css')}}" rel="stylesheet">
    <!-- Google Fonts CSS -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

    <body class="responsive">
        <form method="POST" action="{{route('followup.store')}}">
            @csrf
            @method('post')

             <input type="hidden" name="client_id" value="{{$clientId}}">
            <div class="allPadding">
                <div class="topSecform">
                  <div class="container">
                    <div class="formHead">
                      <div class="row">
                        <div class="col-md-8 col-sm-8 col-xs-12 centerCol">
                          <h2>Follow Up Form</h2>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="followTopSec">
                  <div class="container">
                    <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                      <div class="heading">
                        <h4>Member Name:</h4>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                      <div class="putData">
                        <input type="text" name="member_name">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="followTable ">
                  <div class="container">
                    <div class="followUpTable table-bordered">
                      <div class="followTableHead">
                        <div class="row flexRow">
                          <div class="col-md-2 col-sm-2 col-xs-12 col noPadding">
                            <div class="heading">
                              <h4>Time</h4>
                            </div>
                          </div>
                          <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                            <div class="heading">
                              <h4>Plan/FU #</h4>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                            <div class="heading">
                              <h4>Notes</h4>
                            </div>
                          </div>
                          <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                            <div class="heading">
                              <h4>Doc. Hrs</h4>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row flexRow">
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                          <ul class="list-inline dateData">
                            <li class="heading">
                              <h4>Date:</h4>
                            </li>
                            <li class="putData">
                              <input type="text" name="date_1">
                            </li>
                            <div class="clearfix"></div>
                            <li class="heading">
                              <h4>ST:</h4>
                            </li>
                            <li class="putData">
                              <input type="text" name="st_1">
                            </li>
                            <div class="clearfix"></div>
                            <li class="heading">
                              <h4>ET:</h4>
                            </li>
                            <li class="putData">
                              <input type="text" name="et_1">
                            </li>
                          </ul>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                          <ul class="list-inline plan">
                            <li class="putData">
                              <input type="text" name="plan_fu1">
                            </li>
                          </ul>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                          <ul class="list-inline">
                            <li class="putData">
                              <input type="text" name="notes1">
                              {{-- <input type="text" name="">
                              <input type="text" name="">
                              <input type="text" name="">
                              <input type="text" name=""> --}}
                            </li>
                          </ul>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                          <ul class="list-inline">
                            <li class="putData">
                              <input type="text" name="doc_hrs1" class="dcrHr">
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="row flexRow">
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                          <ul class="list-inline dateData">
                            <li class="heading">
                              <h4>Date:</h4>
                            </li>
                            <li class="putData">
                              <input type="text" name="date_2">
                            </li>
                            <div class="clearfix"></div>
                            <li class="heading">
                              <h4>ST:</h4>
                            </li>
                            <li class="putData">
                              <input type="text" name="st_2">
                            </li>
                            <div class="clearfix"></div>
                            <li class="heading">
                              <h4>ET:</h4>
                            </li>
                            <li class="putData">
                              <input type="text" name="et_2">
                            </li>
                          </ul>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                          <ul class="list-inline plan">
                            <li class="putData">
                              <input type="text" name="plan_fu2">
                            </li>
                          </ul>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                          <ul class="list-inline">
                            <li class="putData">
                              <input type="text" name="notes2">
                              {{-- <input type="text" name="">
                              <input type="text" name="">
                              <input type="text" name="">
                              <input type="text" name=""> --}}
                            </li>
                          </ul>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                          <ul class="list-inline">
                            <li class="putData">
                              <input type="text" name="doc_hrs2" class="dcrHr">
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="row flexRow">
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                          <ul class="list-inline dateData">
                            <li class="heading">
                              <h4>Date:</h4>
                            </li>
                            <li class="putData">
                              <input type="text" name="date_3">
                            </li>
                            <div class="clearfix"></div>
                            <li class="heading">
                              <h4>ST:</h4>
                            </li>
                            <li class="putData">
                              <input type="text" name="st_3">
                            </li>
                            <div class="clearfix"></div>
                            <li class="heading">
                              <h4>ET:</h4>
                            </li>
                            <li class="putData">
                              <input type="text" name="et_3">
                            </li>
                          </ul>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                          <ul class="list-inline plan">
                            <li class="putData">
                              <input type="text" name="plan_fu3">
                            </li>
                          </ul>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                          <ul class="list-inline">
                            <li class="putData">
                              <input type="text" name="notes3">
                              {{-- <input type="text" name="">
                              <input type="text" name="">
                              <input type="text" name="">
                              <input type="text" name=""> --}}
                            </li>
                          </ul>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                          <ul class="list-inline">
                            <li class="putData">
                              <input type="text" name="doc_hrs3" class="dcrHr">
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="row flexRow">
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                          <ul class="list-inline dateData">
                            <li class="heading">
                              <h4>Date:</h4>
                            </li>
                            <li class="putData">
                              <input type="text" name="date_4">
                            </li>
                            <div class="clearfix"></div>
                            <li class="heading">
                              <h4>ST:</h4>
                            </li>
                            <li class="putData">
                              <input type="text" name="st_4">
                            </li>
                            <div class="clearfix"></div>
                            <li class="heading">
                              <h4>ET:</h4>
                            </li>
                            <li class="putData">
                              <input type="text" name="et_4">
                            </li>
                          </ul>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                          <ul class="list-inline plan">
                            <li class="putData">
                              <input type="text" name="plan_fu4">
                            </li>
                          </ul>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 noPadding">
                          <ul class="list-inline">
                            <li class="putData">
                              <input type="text" name="notes4">
                              {{-- <input type="text" name="">
                              <input type="text" name="">
                              <input type="text" name="">
                              <input type="text" name=""> --}}
                            </li>
                          </ul>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                          <ul class="list-inline">
                            <li class="putData">
                              <input type="text" name="doc_hrs4" class="dcrHr">
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="signSec">
                  <div class="container">
                    <div class="row">
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="heading">
                          <h4>CASE MANAGER SIGNATURE:</h4>
                        </div>
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="putData">
                          <input type="text" name="case_manager_signature">
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                      </div>
                      <div class="col-md-1 col-sm-1 col-xs-12">
                        <div class="heading">
                          <h4>DATE:</h4>
                        </div>
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="putData">
                          <input type="text" name="case_manager_date">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="signSec">
                  <div class="container">
                    <div class="row">
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="heading">
                          <h4>OWNER SIGNATURE:</h4>
                        </div>
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="putData">
                          <input type="text" name="owner_signature">
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                      </div>
                      <div class="col-md-1 col-sm-1 col-xs-12">
                        <div class="heading">
                          <h4>DATE:</h4>
                        </div>
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="putData">
                          <input type="text" name="owner_date">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
        <center> <input type="submit" value="Save" name="btnSubmit" class="btn btn-success"></center>
        </form>
      </body>

    <!-- All JS -->
    <script src="{{asset('js/all.js')}}"></script>
    <!-- jquery.slicknav JS -->
    <script src="{{asset('js/jquery.slicknav.js')}}"></script>
    <!-- Custom JS -->
    <script src="{{asset('js/custom.js')}}"></script>

@endsection
