@extends('layouts.masterlayout')
@section('content')

    <!-- All CSS -->
{{--    <link href="{{asset('css/all.css')}}" rel="stylesheet">--}}
{{--    <!-- slicknav CSS -->--}}
{{--    <link href="{{asset('css/slicknav.css')}}" rel="stylesheet">--}}
    <!-- Style CSS -->
    <link href="{{asset('css/custom.css')}}" rel="stylesheet">
    <!-- responsive css -->
    <link href="{{asset('css/responsive.css')}}" rel="stylesheet">
    <!-- Google Fonts CSS -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

    @php
    $cal_type = $form->cal_type;
    $cal_type2 = explode(',', $cal_type);

    $topic_of_discussion = $form->topic_of_discussion;
    $topic_of_discussion2 = explode(',', $topic_of_discussion);

    $curent_source = $form->curent_source;
    $curent_source2 = explode(',', $curent_source);

    $curent_icwp = $form->curent_icwp;
    $curent_icwp2 = explode(',', $curent_icwp);

    $reviewed_population = $form->reviewed_population;
    $reviewed_population2 = explode(',', $reviewed_population);


    $varified = $form->varified;
    $varified2 = explode(',', $varified);

    @endphp


    <body class="responsive">
        <div class="allPadding">
          <div class="topSecform">
            <div class="container">
              <div class="formHead">
                <div class="row">
                  <div class="col-md-8 col-sm-8 col-xs-12 centerCol">
                    <h2>Monthly Contact Form</h2>
                  </div>
                </div>
              </div>
              <div class="formTable ">
                <div class="row flexRow">
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Cal Date:</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text"  name="cal_date" disabled value="{{ $form->cal_date}}">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Time of Day :</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="time_of_date"  disabled value="{{ $form->time_of_date}}">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Worker</h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="worker"  disabled value="{{ $form->worker}}">
                    </div>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Call Type:</h4>
                    </div>
                  </div>
                  <div class="col-md-9 col-sm-9 col-xs-12 noPadding">
                    <ul class="list-inline checkboxes">
                      <li>
                        <input type="checkbox" name="cal_type[]" @php echo (in_array("1", $cal_type2)?'checked':''); @endphp disabled value="1" >
                        <label for="vehicle1">To member/caregiver</label>
                      </li>
                      <li>
                        <input type="checkbox" name="cal_type[]"  @php echo (in_array("2", $cal_type2)?'checked':''); @endphp  value="2" disabled>
                        <label for="vehicle2"> From member/caregiver</label>
                      </li>
                      <li>
                        <input type="checkbox" name="cal_type[]"  @php echo (in_array("3", $cal_type2)?'checked':''); @endphp  value="3" disabled>
                        <label for="vehicle2">To/From provider</label>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row flexRow">
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Contact Name </h4>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="contact_name"  disabled value="{{ $form->contact_name}}">
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Relation (if other than member) </h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="relation"  disabled value="{{ $form->relation}}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="discussionSec">
            <div class="container">
              <h4> <b>Topic of Discussion-</b> choose all that apply * </h4>
              <div class="row flexRow">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <ul class="list-inline checkboxes">
                    <li>
                      <input type="checkbox" name="topic_of_discussion[]" @php echo (in_array("1", $topic_of_discussion2)?'checked':''); @endphp disabled  value="1">
                      <label for="vehicle1">Caregiver issue</label>
                    </li>
                    <li>
                      <input type="checkbox"  name="topic_of_discussion[]"  @php echo (in_array("2", $topic_of_discussion2)?'checked':''); @endphp disabled    value="2">
                      <label for="vehicle2">ERS transmitter monthly test requirement</label>
                    </li>
                    <li>
                      <input type="checkbox"  name="topic_of_discussion[]" @php echo (in_array("3", $topic_of_discussion2)?'checked':''); @endphp disabled   value="3">
                      <label for="vehicle2">Hospitalization</label>
                    </li>
                    <li>
                      <input type="checkbox"  name="topic_of_discussion[]"  @php echo (in_array("4", $topic_of_discussion2)?'checked':''); @endphp disabled   value="4">
                      <label for="vehicle2">Client complaint/concern</label>
                    </li>
                    <li>
                      <input type="checkbox"  name="topic_of_discussion[]"  @php echo (in_array("5", $topic_of_discussion2)?'checked':''); @endphp disabled   value="5">
                      <label for="vehicle1">Fall(s)</label>
                    </li>
                    <li>
                      <input type="checkbox"  name="topic_of_discussion[]"  @php echo (in_array("6", $topic_of_discussion2)?'checked':''); @endphp disabled   value="6">
                      <label for="vehicle2">ER no admit</label>
                    </li>
                    <li>
                      <input type="checkbox"  name="topic_of_discussion[]"  @php echo (in_array("7", $topic_of_discussion2)?'checked':''); @endphp disabled   value="7">
                      <label for="vehicle2">Cost share issue </label>
                    </li>
                    <li>
                      <input type="checkbox"  name="topic_of_discussion[]"  @php echo (in_array("8", $topic_of_discussion2)?'checked':''); @endphp disabled   value="8">
                      <label for="vehicle2">Family issue</label>
                    </li>
                    <li>
                      <input type="checkbox"  name="topic_of_discussion[]"  @php echo (in_array("9", $topic_of_discussion2)?'checked':''); @endphp disabled   value="9">
                      <label for="vehicle2">Medical Appt Compliance</label>
                    </li>
                    <li>
                      <input type="checkbox" name="topic_of_discussion[]"  @php echo (in_array("10", $topic_of_discussion2)?'checked':''); @endphp disabled   value="10">
                      <label for="vehicle2">DME issue</label>
                    </li>
                    <li>
                      <input type="checkbox"  name="topic_of_discussion[]"  @php echo (in_array("11", $topic_of_discussion2)?'checked':''); @endphp disabled   value="11">
                      <label for="vehicle2">Home Environment issue</label>
                    </li>
                    <li>
                      <input type="checkbox" name="topic_of_discussion[]" @php echo (in_array("12", $topic_of_discussion2)?'checked':''); @endphp disabled   value="12">
                      <label for="vehicle2">Non waivered service issue</label>
                    </li>
                    <li>
                      <input type="checkbox" name="topic_of_discussion[]" @php echo (in_array("3", $topic_of_discussion2)?'checked':''); @endphp disabled   value="13">
                      <label for="vehicle2">Provider complaint/concern</label>
                    </li>
                    <li>
                      <input type="checkbox" name="topic_of_discussion[]" @php echo (in_array("14", $topic_of_discussion2)?'checked':''); @endphp disabled   value="14">
                      <label for="vehicle2">Service Issue/Service Change</label>
                    </li>
                    <li>
                      <input type="checkbox" name="topic_of_discussion[]"  @php echo (in_array("15", $topic_of_discussion2)?'checked':''); @endphp disabled   value="15">
                      <label for="vehicle2">Support System Change(s)</label>
                    </li>
                    <li>
                      <input type="checkbox" name="topic_of_discussion[]"  @php echo (in_array("16", $topic_of_discussion2)?'checked':''); @endphp disabled   value="16">
                      <label for="vehicle2">Other</label>
                    </li>
                    <li>
                      <input type="checkbox"  name="topic_of_discussion[]" @php echo (in_array("17", $topic_of_discussion2)?'checked':''); @endphp disabled   value="17">
                      <label for="vehicle2">ICWP- Supply status</label>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="additionSec">
            <div class="container">
              <h4>*Narrative requirement for items marked above….. </h4>
              <textarea rows="5" name="narraitive_requirement">{{ $form->narraitive_requirement }}</textarea>
            </div>
          </div>
          <div class="discussionSec servicesSec">
            <div class="container">
              <h4><b>Current SOURCE/CCSP service(s) for member:</b> </h4>
              <div class="row flexRow">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <ul class="list-inline checkboxes">
                    <li>
                      <input type="checkbox" name="curent_source[]" @php echo (in_array("1", $curent_source2)?'checked':''); @endphp disabled   value="1">
                      <label for="vehicle1">PSS/X</label>
                    </li>
                    <li>
                      <input type="checkbox"  name="curent_source[]"  @php echo (in_array("2", $curent_source2)?'checked':''); @endphp disabled   value="2">
                      <label for="vehicle2">ERS</label>
                    </li>
                    <li>
                      <input type="checkbox" name="curent_source[]"  @php echo (in_array("3", $curent_source2)?'checked':''); @endphp disabled   value="3">
                      <label for="vehicle2">ADH</label>
                    </li>
                    <li>
                      <input type="checkbox" name="curent_source[]"  @php echo (in_array("4", $curent_source2)?'checked':''); @endphp disabled   value="4">
                      <label for="vehicle2">ALS</label>
                    </li>
                    <li>
                      <input type="checkbox" name="curent_source[]"  @php echo (in_array("5", $curent_source2)?'checked':''); @endphp disabled   value="5">
                      <label for="vehicle1">SFC</label>
                    </li>
                    <li>
                      <input type="checkbox" name="curent_source[]"  @php echo (in_array("6", $curent_source2)?'checked':''); @endphp disabled   value="6">
                      <label for="vehicle2">Cd PSS</label>
                    </li>
                    <li>
                      <input type="checkbox" name="curent_source[]"  @php echo (in_array("7", $curent_source2)?'checked':''); @endphp disabled   value="7">
                      <label for="vehicle2">OHR</label>
                    </li>
                    <li>
                      <input type="checkbox" name="curent_source[]"  @php echo (in_array("8", $curent_source2)?'checked':''); @endphp disabled   value="8">
                      <label for="vehicle2">HDM</label>
                    </li>
                    <li>
                      <input type="checkbox" name="curent_source[]"  @php echo (in_array("9", $curent_source2)?'checked':''); @endphp disabled   value="9">
                      <label for="vehicle2">ECM/TCM</label>
                    </li>
                    <li>
                      <input type="checkbox" name="curent_source[]"  @php echo (in_array("10", $curent_source2)?'checked':''); @endphp disabled   value="10">
                      <label for="vehicle2">HDS/SNS</label>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="discussionSec icwpservicesSec">
            <div class="container">
              <h4><b>Current ICWP service(s) for member: </b> </h4>
              <div class="row flexRow">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <ul class="list-inline checkboxes">
                    <li>
                      <input type="checkbox" name="curent_icwp[]"  @php echo (in_array("1", $curent_icwp2)?'checked':''); @endphp disabled   value="1">
                      <label for="vehicle1">ALS</label>
                    </li>
                    <li>
                      <input type="checkbox" name="curent_icwp[]" @php echo (in_array("2", $curent_icwp2)?'checked':''); @endphp disabled  value="2">
                      <label for="vehicle2">PSS/Respite</label>
                    </li>
                    <li>
                      <input type="checkbox"  name="curent_icwp[]" @php echo (in_array("3", $curent_icwp2)?'checked':''); @endphp disabled  value="3">
                      <label for="vehicle2">SNS</label>
                    </li>
                    <li>
                      <input type="checkbox"  name="curent_icwp[]" @php echo (in_array("4", $curent_icwp2)?'checked':''); @endphp disabled  value="4">
                      <label for="vehicle2">ADH</label>
                    </li>
                    <li>
                      <input type="checkbox" name="curent_icwp[]" @php echo (in_array("5", $curent_icwp2)?'checked':''); @endphp disabled  value="5">
                      <label for="vehicle1">ERS</label>
                    </li>
                    <li>
                      <input type="checkbox"  name="curent_icwp[]" @php echo (in_array("6", $curent_icwp2)?'checked':''); @endphp disabled  value="6">
                      <label for="vehicle2">CD PSS</label>
                    </li>
                    <li>
                      <input type="checkbox"  name="curent_icwp[]" @php echo (in_array("7", $curent_icwp2)?'checked':''); @endphp disabled  value="7">
                      <label for="vehicle2">Enhanced/Traditional Case Management</label>
                    </li>
                    <li>
                      <input type="checkbox"  name="curent_icwp[]" @php echo (in_array("8", $curent_icwp2)?'checked':''); @endphp disabled  value="8">
                      <label for="vehicle2">Other</label>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="kinSec">
            <div class="container">
              <h4><b>Next of Kin on file:</b> </h4>
              <div class="kinSecCntnt">
                <div class="row flexRow">
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Name </h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="file_name"  disabled value="{{ $form->file_name}}">
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Address </h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="file_address"  disabled value="{{ $form->file_address}}">
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Phone </h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="file_phone"  disabled value="{{ $form->file_phone}}">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Relationship </h4>
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="file_relationship"  disabled value="{{ $form->file_relationship}}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="kinSec">
            <div class="container">
              <h4><b>Additional contacts:</b> </h4>
              <div class="kinSecCntnt">
                <div class="row flexRow">
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Name </h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="contacts_name"  disabled value="{{ $form->contacts_name}}">
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Address </h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="contacts_address"  disabled value="{{ $form->contacts_address}}">
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Phone </h4>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="contacts_phone"  disabled value="{{ $form->contacts_phone}}">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12 noPadding">
                    <div class="heading">
                      <h4>Relationship </h4>
                    </div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-12 noPadding">
                    <div class="putData">
                      <input type="text" name="contacts_relationship"  disabled value="{{ $form->contacts_relationship}}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="reviewdSec">
            <div class="container">
              <div class="reviewdSecCntnt">
                <div class="reviewdCntnt">
                  <h4><b>Reviewed population-specific CDC guidelines with the member/caregiver:</b><a href="https://www.cdc.gov/">https://www.cdc.gov/</a></h4>
                  <div class="row flexRow">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <ul class="list-inline checkboxes">
                        <li>
                          <input type="checkbox" name="reviewed_population[]"   @php echo (in_array("1", $reviewed_population2)?'checked':''); @endphp disabled value="1">
                          <label for="vehicle1">Yes</label>
                        </li>
                        <li>
                          <input type="checkbox" name="reviewed_population[]"  @php echo (in_array("2", $reviewed_population2)?'checked':''); @endphp disabled  value="2">
                          <label for="vehicle2">No</label>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="reviewdCntnt">
                  <h4><b>Verified that member and/or caregiver is able to contact healthcare provider by phone if symptomatic:</b></h4>
                  <div class="row flexRow">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <ul class="list-inline checkboxes">
                        <li>
                          <input type="checkbox" name="varified[]"   @php echo (in_array("1", $varified2)?'checked':''); @endphp disabled  value="1">
                          <label for="vehicle1">Yes</label>
                        </li>
                        <li>
                          <input type="checkbox" name="varified[]"  @php echo (in_array("2", $varified2)?'checked':''); @endphp  disabled value="2">
                          <label for="vehicle2">No</label>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="signSec">
            <div class="container">
              <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <div class="heading">
                    <h4>CASE MANAGER SIGNATURE:</h4>
                  </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <div class="putData">
                    <input type="text" name="case_manager_signature"  disabled value="{{ $form->case_manager_signature}}">
                  </div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12">
                </div>
                <div class="col-md-1 col-sm-1 col-xs-12">
                  <div class="heading">
                    <h4>DATE:</h4>
                  </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <div class="putData">
                    <input type="text" name="case_manager_date"  disabled value="{{ $form->case_manager_date}}">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="signSec">
            <div class="container">
              <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <div class="heading">
                    <h4>OWNER SIGNATURE:</h4>
                  </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <div class="putData">
                    <input type="text" name="owner_signature"  disabled value="{{ $form->owner_signature}}">
                  </div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12">
                </div>
                <div class="col-md-1 col-sm-1 col-xs-12">
                  <div class="heading">
                    <h4>DATE:</h4>
                  </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <div class="putData">
                    <input type="text" name="owner_date"  disabled value="{{ $form->owner_date}}">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </form>
      </body>


<!-- All JS -->
<script src="{{asset('js/all.js')}}"></script>
<!-- jquery.slicknav JS -->
<script src="{{asset('js/jquery.slicknav.js')}}"></script>
<!-- Custom JS -->
<script src="{{asset('js/custom.js')}}"></script>

@endsection

