@extends('layouts.masterlayout')

@section('content')

@php
$roleuser=Illuminate\Support\Facades\Auth::user()->roles;
@endphp
@if($roleuser=="staff")
    <a href="{{route('monthlyContactReport.create_form',$clientId)}}" class="btn btn-success fa-pull-right" style="margin-right: 20px;">Create Form</a>
  @endif
    <br>
    <br>

    <section id="widget-grid" class="" style="border-top:2px solid #CCC; margin-left: 10px; margin-right: 10px">
        <!-- row -->
        <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">

                    <!-- widget div-->
                    <div>
                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                        </div>
                        <!-- end widget edit box -->
                        <!-- widget content -->
                        <div class="widget-body no-padding">
                            <div class="widget-body-toolbar" style="overflow-x:auto;">
                            </div >
                            <table id="datatable_tabletools" class="table table-striped table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th nowrap><center>S#</center></th>
                                    <th nowrap><center>Contact Name:</center></th>
                                    <th nowrap><center>Relation</center></th>
                                    <th nowrap><center>File name</center></th>
                                    <th nowrap><center>File phone</center></th>

                                    <th nowrap><center>Action</center></th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($forms as $form)
                                    <tr>
                                        <td nowrap>{{$loop->iteration}}</td>
                                        <td nowrap>{{$form->contact_name}}</td>
                                        <td nowrap>{{$form->relation}}</td>
                                        <td nowrap>{{$form->file_name}}</td>
                                        <td nowrap>{{$form->file_phone}}</td>


                                        <td nowrap> <center>
                                                @if(isset($form->is_manager) && $form->is_manager == 0)
                                                    <form method="POST" action="{{ route('staff.caseManager') }}">
                                                        @csrf
                                                        <input type="hidden" name="staffId" value="{{ $form->id }}">
                                                        <input type="submit" value="Mark Case Manager">
                                                    </form>
                                                @endif
                                                <a href="{{route('monthlyContactReport_show',$form->id)}}" class="btn btn-sm btn-primary"  title="View" >
                                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                                </a>
                                                @if($roleuser=="staff")
                                                <a href="{{route('monthlyContactReport_edit',$form->id)}}" class="btn btn-sm btn-primary"  title="Edit">
                                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                                </a>
                                                    <form method="POST" action="{{route('monthlyContactReport.destroy',$form->id)}}">
                                                        @csrf
                                                        @method('delete')
                                                        <input type="submit" class="btn btn-sm btn-danger" value="Delete">
                                                    </form >
                                                @endif
                                            </center>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>

                                </tfoot>
                            </table>
                        </div>
                        <!-- end widget content -->
                    </div>
                    <!-- end widget div -->
                </div>
                <!-- end widget -->
            </article>
        </div>
    </section>

@endsection


<script>
    $('#datatable_tabletools').DataTable({
        dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete : function(oSettings, json) {
            $(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
                $(this).addClass('btn-sm btn-default');
            });
        },
        "language": {
            "search": "Search: ",
            "searchPlaceholder": "Search records"
        },
        "pageLength": 50,
        "bDestroy": true,

        "order": []
    });
    /* END TABLE TOOLS */

    function ReportError(res)
    {
    }// Filter placeholder dataTable
    $(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>
