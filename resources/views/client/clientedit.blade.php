
@extends('layouts.masterlayout')

@section('content')
    <h2>Update Record</h2>
    <hr>
    <br>
    <form method="post" action="{{route('client.update')}}">
            @csrf
        <div class="form-group">
            <label class="control-label col-sm-2" for="name">User Name</label>
            <input type="hidden" class="form-control" value="@if(isset($userData['id'])) {{$userData['id']}} @else 0 @endif"  name="client_id" >

            <div class="col-sm-10">
            <input type="text" class="form-control"  name="name" value="@if(isset($userData['name'])){{$userData['name']}} @else 'client name' @endif" >
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="email">Email</label>
            <div class="col-sm-10">
            <input type="text" class="form-control"  name="email" value="@if(isset($userData['email'])){{$userData['email']}}@else clientdemo@gmail.com @endif" >
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="Password">Password</label>
            <div class="col-sm-10">
                <input type="password" class="form-control"  name="password" value="@if(isset($userData['password'])){{$userData['password']}}@else '******' @endif" >
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="country">Country</label>
            <div class="col-sm-10">
            <input type="text" class="form-control"  name="country" value="@if(isset($userData['country'])){{$userData['country']}}@else country @endif" >
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="city">City</label>
            <div class="col-sm-10">
            <input type="text" class="form-control"  name="city" value="@if(isset($userData['city'])){{$userData['city']}} @else city @endif" >
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="region">Region</label>
            <div class="col-sm-10">
            <input type="text" class="form-control"  name="region" value="@if(isset($userData['region'])) {{$userData['region']}} @else region  @endif" >
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="contact">Contact</label>
            <div class="col-sm-10">
            <input type="number" class="form-control"  name="contact" value="@if(isset($userData['contact'])){{$userData['contact']}}  @endif" >
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success">Update</button>
            </div>
        </div>

    </form>
@endsection
