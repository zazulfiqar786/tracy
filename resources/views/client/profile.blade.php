@extends('layouts.masterlayout')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4 ">Profile</h1>
                                    </div>
                                    <form method="POST" action="{{ route('client.update') }}">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ $clientData[0]->id }}">
                                        <input type="hidden" name="user_id" value="{{ $clientData[0]->user_id }}">
                                        <div class="form-group row">
                                            <label for="name"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                                            <div class="col-md-6">
                                                <input id="name" type="text"
                                                       class="form-control @error('name') is-invalid @enderror"
                                                       name="name" value="{{$clientData[0]->name}}" required
                                                       autocomplete="name" autofocus>
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="country"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Country') }}</label>
                                            <div class="col-md-6">
                                                <input id="country" type="text" name="country"
                                                       class="form-control @error('country') is-invalid @enderror"
                                                       value="{{ $clientData[0]->country }}" required
                                                       autocomplete="country" autofocus>
                                                @error('country')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="state"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('State') }}</label>
                                            <div class="col-md-6">
                                                <input id="state" type="text" name="state"
                                                       class="form-control @error('state') is-invalid @enderror"
                                                       value="{{ $clientData[0]->state }}" required
                                                       autocomplete="state" autofocus>
                                                @error('state')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="city"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('City') }}</label>
                                            <div class="col-md-6">
                                                <input id="city" type="text" name="city"
                                                       class="form-control @error('city') is-invalid @enderror"
                                                       value="{{ $clientData[0]->city }}" required
                                                       autocomplete="city" autofocus>
                                                @error('city')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="county"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('County') }}</label>
                                            <div class="col-md-6">
                                                <input id="county" name="county" type="text"
                                                       class="form-control @error('county') is-invalid @enderror"
                                                       value="{{ $clientData[0]->county }}" required
                                                       autocomplete="county" autofocus>
                                                @error('county')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                 </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="area"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Area') }}</label>
                                            <div class="col-md-6">
                                                <input id="area" type="text" name="area"
                                                       class="form-control @error('area') is-invalid @enderror"
                                                       value="{{ $clientData[0]->area }}" required
                                                       autocomplete="area" autofocus>
                                                @error('area')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="zip_code"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Zip Code') }}</label>
                                            <div class="col-md-6">
                                                <input id="zip_code" type="text" name="zip_code"
                                                       class="form-control @error('zip_code') is-invalid @enderror"
                                                       value="{{ $clientData[0]->zip_code }}" required
                                                       autocomplete="zip_code" autofocus>
                                                @error('zip_code')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="contact"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Contact') }}</label>
                                            <div class="col-md-6">
                                                <input id="contact" name="contact" type="text"
                                                       class="form-control @error('contact') is-invalid @enderror"
                                                       value="{{ $clientData[0]->contact }}" required
                                                       autocomplete="contact" autofocus>
                                                @error('contact')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
{{--                                        <div class="form-group row">--}}
{{--                                            <label for="password"--}}
{{--                                                   class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}
{{--                                            <div class="col-md-6">--}}
{{--                                                <input id="password" name="password" type="text"--}}
{{--                                                       class="form-control @error('password') is-invalid @enderror"--}}
{{--                                                       autocomplete="contact" autofocus>--}}
{{--                                                @error('password')--}}
{{--                                                <span class="invalid-feedback" role="alert">--}}
{{--                                    <strong>{{ $message }}</strong>--}}
{{--                                </span>--}}
{{--                                                @enderror--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="form-group row mb-0">
                                            <div class="col-md-6 offset-md-4">
                                                <button type="submit" class="btn btn-primary">
                                                    {{ __('Update') }}
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
