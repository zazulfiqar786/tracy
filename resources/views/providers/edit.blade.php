@extends('layouts.masterlayout')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4 ">Edit Provider</h1>
                                    </div>

                                    {{--<h1>number of candidates registered here {{$events}} </h1>--}}
                                    <form method="POST" action="{{ route('provider.update',$providerData->id) }}">
                                        @csrf
{{--                                        <input type="hidden" name="providerId" value="{{$providerData->id}}">--}}
                                        <div class="form-group row">
                                            <label for="name"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                            <div class="col-md-6">
                                                <input id="name" type="text"
                                                       class="form-control @error('name') is-invalid @enderror"
                                                       name="name" value="{{ $providerData->name }}" required
                                                       autocomplete="name" autofocus>

                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="country"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Country') }}</label>

                                            <div class="col-md-6">
                                                <input id="country" type="text"
                                                       class="form-control @error('country') is-invalid @enderror"
                                                       name="country" value="{{ $providerData->country }}" required
                                                       autocomplete="name" autofocus>

                                                @error('country')
                                                <span class="invalid-feedback" role="alert">
                                                     <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="state"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('State') }}</label>

                                            <div class="col-md-6">
                                                <input id="state" type="text"
                                                       class="form-control @error('state') is-invalid @enderror"
                                                       name="state" value="{{ $providerData->state }}" required
                                                       autocomplete="name" autofocus>

                                                @error('state')
                                                <span class="invalid-feedback" role="alert">
                                                     <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="city"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('City') }}</label>

                                            <div class="col-md-6">
                                                <input id="city" type="text"
                                                       class="form-control @error('city') is-invalid @enderror"
                                                       name="city" value="{{$providerData->city}}" required
                                                       autocomplete="name" autofocus>

                                                @error('city')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="county"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('County') }}</label>

                                            <div class="col-md-6">
                                                <input id="county" type="text"
                                                       class="form-control @error('county') is-invalid @enderror"
                                                       name="county" value="{{$providerData->county}}" required
                                                       autocomplete="name" autofocus>

                                                @error('county')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="area"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Area') }}</label>

                                            <div class="col-md-6">
                                                <input id="area" type="text"
                                                       class="form-control @error('area') is-invalid @enderror"
                                                       name="area" value="{{ $providerData->area }}" required
                                                       autocomplete="name" autofocus>

                                                @error('area')
                                                <span class="invalid-feedback" role="alert">
                                                     <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="zip_code"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Zip Code') }}</label>

                                            <div class="col-md-6">
                                                <input id="zip_code" type="text"
                                                       class="form-control @error('zip_code') is-invalid @enderror"
                                                       name="zip_code" value="{{ $providerData->zip_code }}" required
                                                       autocomplete="name" autofocus>

                                                @error('zip_code')
                                                <span class="invalid-feedback" role="alert">
                                                     <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="contact"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('contact') }}</label>

                                            <div class="col-md-6">
                                                <input id="contact" type="text"
                                                       class="form-control @error('contact') is-invalid @enderror"
                                                       name="contact" value="{{$providerData->contact}}" required
                                                       autocomplete="name" autofocus>

                                                @error('contact')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                        </div>


                                        <div class="form-group row mb-0">
                                            <div class="col-md-6 offset-md-4">
                                                <button type="submit" class="btn btn-primary">
                                                    {{ __('Update') }}
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
