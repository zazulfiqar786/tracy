@extends('layouts.masterlayout')
@section('content')
{{--    <a href="{{route('staff.create')}}" class="btn btn-success fa-pull-right" style="margin-right: 20px;">Create staff</a>--}}
{{--    <br>--}}
{{--    <br>--}}

    <section id="widget-grid" class="" style="border-top:2px solid #CCC; margin-left: 10px; margin-right: 10px">
        <!-- row -->
        <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">

                    <!-- widget div-->
                    <div>
                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                        </div>
                        <!-- end widget edit box -->
                        <!-- widget content -->
                        <div class="widget-body no-padding">
                            <div class="widget-body-toolbar" style="overflow-x:auto;">
                            </div >
                            <table id="datatable_tabletools" class="table table-striped table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th nowrap><center>S#</center></th>
                                    <th nowrap><center>Staff Name</center></th>
                                    <th nowrap><center>No of<br>Cases</center></th>
                                    <th nowrap><center>Date</center></th>
                                    <th nowrap><center>State</center></th>
                                    <th nowrap><center>Action</center></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($allCaseManagers as $caseManager)
                                    <tr>
                                        <td nowrap><center>{{$loop->iteration}}</center></td>
                                        @if(isset($caseManager->staffs->name))
                                            <td nowrap><center>{{$caseManager->staffs->name}}</center></td>
                                        @else
                                            <td nowrap><center>Name NA</center></td>
                                        @endif
                                        @if(isset($caseManager->no_of_cases))
                                            <td nowrap>{{$caseManager->no_of_cases}}</td>
                                        @else
                                            <td nowrap><center>Case Manager NA</center></td>
                                        @endif
                                        @if(isset($caseManager->date))
                                            <td nowrap>{{$caseManager->date}}</td>
                                        @else
                                            <td nowrap><center>Date NA</center></td>
                                        @endif
                                        @if(isset($caseManager->status))
                                            @if($caseManager->status == 0)
                                                <td nowrap>INACTIVE</td>
                                            @elseif($caseManager->status == 0)
                                                <td nowrap>ACTIVE</td>
                                            @else
                                                <td nowrap>Other</td>
                                            @endif
                                            @else
                                                <td nowrap>Status NA</td>
                                        @endif
                                            <td nowrap> <center>

                                                    <form method="POST" action="{{ route('staff.unMarkCaseManager') }}">
                                                        @csrf
                                                        <input type="hidden" name="managerId" value="{{$caseManager->id}}">
                                                        <input type="submit" value="Unmark Case Manager">
                                                    </form>
                                            </center>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>

                                </tfoot>
                            </table>
                        </div>
                        <!-- end widget content -->
                    </div>
                    <!-- end widget div -->
                </div>
                <!-- end widget -->
            </article>
        </div>
    </section>

@endsection


<script>
    $('#datatable_tabletools').DataTable({
        dom: "<'dt-top-row'lf>" + "B" +"<'row'<'col-sm-12'tr>>" + "<'dt-row dt-bottom-row'<'col-sm-5'i><'col-sm-7 text-right'p>>",
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        initComplete : function(oSettings, json) {
            $(this).closest('#dt_table_tools_wrapper').find('.dt-button').addClass('table_tools_group').children('button.dt-button').each(function() {
                $(this).addClass('btn-sm btn-default');
            });
        },
        "language": {
            "search": "Search: ",
            "searchPlaceholder": "Search records"
        },
        "pageLength": 50,
        "bDestroy": true,

        "order": []
    });
    /* END TABLE TOOLS */

    function ReportError(res)
    {
    }// Filter placeholder dataTable
    $(".dataTables_filter .input-group input").attr("placeholder", "Search records");

</script>
