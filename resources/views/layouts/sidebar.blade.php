
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/duotone.css" integrity="sha384-R3QzTxyukP03CMqKFe0ssp5wUvBPEyy9ZspCB+Y01fEjhMwcXixTyeot+S40+AjZ" crossorigin="anonymous"/>
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/fontawesome.css" integrity="sha384-eHoocPgXsiuZh+Yy6+7DsKAerLXyJmu2Hadh4QYyt+8v86geixVYwFqUvMU8X90l" crossorigin="anonymous"/>
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="home">
            <div class="sidebar-brand-icon rotate-n-15">
                <i class="fas fa-laugh-wink"></i>
            </div>
            @if(Auth::user()->roles=='client')
            <div class="sidebar-brand-text mx-3">Client <br> <sup></sup></div>
            @elseif(Auth::user()->roles=='staff')
            <div class="sidebar-brand-text mx-3">Staff<br> <sup></sup></div>
            @elseif(Auth::user()->roles=='provider')
            <div class="sidebar-brand-text mx-3">Provider<br> <sup></sup></div>
            @else
            <div class="sidebar-brand-text mx-3">Admin <br> <sup></sup></div>
        @endif
        </a>
        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="{{route('home')}}">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Home</span></a>
        </li>
        @if(Auth::user()->roles=='admin')
        <li class="nav-item">
            <a class="nav-link collapsed" data-toggle="collapse" data-target="#collapseOne"
               aria-expanded="true" aria-controls="collapseOne">
                <i class="fas fa-fw fa-cog"></i>
                <span>All Staff</span>
            </a>
            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Profile:</h6>

                    <a href="{{ route('staff.showAll')}}" class="collapse-item">View</a>
                </div>
            </div>
             <!-- Divider  -->
        <hr class="sidebar-divider">
            <a class="nav-link collapsed" data-toggle="collapse" data-target="#collapseCaseManagers"
               aria-expanded="true" aria-controls="collapseOne">
                <i class="fas fa-fw fa-cog"></i>
                <span>Case Managers</span>
            </a>
            <div id="collapseCaseManagers" class="collapse" aria-labelledby="headingCaseManagers" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Profile:</h6>

                    <a href="{{ route('casemanager.index')}}" class="collapse-item">View</a>
                </div>
            </div>
            <!-- Divider  -->



            <!-- Divider  -->
            <hr class="sidebar-divider">



            @else
        @endif
        </li>

        {{-- @if(Auth::user()->roles=='admin' || Auth::user()->roles=='staff')

        <li class="nav-item">
            <a href="" class="nav-link collapsed" data-toggle="collapse" data-target="#collapseTwo"
               aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-cog"></i>
                <span>Disease Management</span>
            </a>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Diseases:</h6>
                    @if(Auth::user()->roles=='admin' || Auth::user()->roles=='staff')
                    <a href="{{route('disease.show')}}" class="collapse-item">View</a>
                    @else
                    @endif
                </div>
            </div>
        </li>
        @else
        @endif

        <hr class="sidebar-divider"> --}}


        @if(Auth::user()->roles=='admin'|| Auth::user()->roles=='staff')
        <li class="nav-item">
            <a href="" class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseThree"
               aria-expanded="true" aria-controls="collapseThree">
                <i class="fas fa-fw fa-cog"></i>
                <span>Client Management</span>
            </a>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Clients:</h6>
                    @if(Auth::user()->roles=='admin')
                        <a href="{{ route('client.showAll')}}" class="collapse-item">View</a>
                        <a href="{{ route('assigned.managers')}}" class="collapse-item">Client and Managers</a>
                    @endif
                    @if(Auth::user()->roles=='staff')
                        <a href="{{ route('staff.client')}}" class="collapse-item">My Client</a>
                    @endif
                </div>
            </div>


        </li>



        @else
        @endif

        @if(Auth::user()->roles=='staff')
        <li class="nav-item">
            <a href="" class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFour"
               aria-expanded="true" aria-controls="collapseFour">
                <i class="fas fa-fw fa-cog"></i>
                <span>Staff Profile</span>
            </a>
            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Clients:</h6>
                    @if(Auth::user()->roles=='staff')
                    <a href="{{ route('staff.show')}}" class="collapse-item">View</a>
                    @else
                    @endif
                </div>
            </div>

            {{-- <hr class="sidebar-divider">
            <a class="nav-link collapsed" data-toggle="collapse" data-target="#collapseForms"
               aria-expanded="true" aria-controls="collapseOne">
                <i class="fas fa-fw fa-cog"></i>
                <span>Forms</span>
            </a>
            <div id="collapseForms" class="collapse" aria-labelledby="headingCaseManagers" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">All Forms:</h6>

                    <a href="{{ route('AppendixD5.index')}}" class="collapse-item">Appendix-D-5</a>
                </div>
            </div> --}}
        </li>
        <!-- Divider  -->

        @else
        @endif

        {{-- @if(Auth::user()->roles=='staff' || Auth::user()->roles=='admin' || Auth::user()->roles=='client' ) --}}
        @if( Auth::user()->roles=='client' )

        <li class="nav-item">
            <a href="" class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFive"
               aria-expanded="true" aria-controls="collapseFive">
                <i class="fas fa-fw fa-cog"></i>
                <span>My Cases</span>
            </a>
            <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    {{-- <h6 class="collapse-header">Clients:</h6> --}}
                    @if(Auth::user()->roles=='client')
                    <a href="{{ route('cases.clientcases', Auth::user()->id)}}" class="collapse-item">My Case Manager</a>
                    @else
                    {{-- <a href="{{ route('cases.allcases')}}" class="collapse-item">All Cases</a> --}}
                    @endif
                </div>
            </div>
        </li>
            <hr class="sidebar-divider">
            <li class="nav-item">
                @if(Auth::user()->roles=='client')
                <a href="{{ route('client.profile')}}" class="nav-link collapsed" data-target="#collapseProfile"
                   aria-expanded="true" aria-controls="collapseFive">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Profile</span>
                </a>
{{--                @elseif(Auth::user()->roles=='admin')--}}
{{--                    <a href="#" class="nav-link collapsed" data-target="#collapseProfile"--}}
{{--                       aria-expanded="true" aria-controls="collapseFive">--}}
{{--                        <i class="fas fa-fw fa-cog"></i>--}}
{{--                        <span>Profile</span>--}}
{{--                    </a>--}}
                {{-- @elseif(Auth::user()->roles=='staff')
                    <a href="#" class="nav-link collapsed" data-target="#collapseProfile"
                       aria-expanded="true" aria-controls="collapseFive">
                        <i class="fas fa-fw fa-cog"></i>
                        <span>Profile</span>
                    </a> --}}
                @endif
            </li>

        <!-- Divider  -->

        @else
        @endif

        @if(Auth::user()->roles=='admin')
        <hr class="sidebar-divider d-none d-md-block">
        <li class="nav-item">
            <a href="" class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSix"
               aria-expanded="true" aria-controls="collapseSix">
                <i class="fas fa-fw fa-cog"></i>
                <span>Providers</span>
            </a>
            <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Provider:</h6>
                    @if(Auth::user()->roles=='admin')
                    <a href="{{ route('provider.showAll')}}" class="collapse-item">All Providers</a>
                    {{-- <a href="{{ route('provider.show', Auth::user()->id)}}" class="collapse-item">ABC Provider Detail</a> --}}
                    @else

                    @endif
                </div>
            </div>
        </li>
        <!-- Divider  -->
        <hr class="sidebar-divider">
        @else
        @endif

        <!-- Divider -->
        <!-- <hr class="sidebar-divider d-none d-md-block"> -->

        <!-- Sidebar Toggler (Sidebar) -->




        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>
    </ul>

