<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class careform_form17 extends Model
{
    protected $fillable = [
        'client_id',
        'case_manager_id',
        'careform_id',
        'member_wound',
        'member_name',
        'where_is_located',
        'where_the_wound',
        'separate_attachment',
        'comments1',
        'primary_modes',
        'comments2',

    ];
}
