<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class careform_form5 extends Model
{
    protected $fillable = [
        'client_id',
        'case_manager_id',
        'careform_id',
        'name_of_member',
        'dob',
        'request_and_authoriz',
        'to_obtain_from',
        'following_types1',
        'following_types2',
        'for_the_perpose1',
        'for_the_perpose2',
        'all_information_authorize',
        'i_understand_date',
        'signature_of_member',
        'signature_of_witness',
        'title_relation_member',
        'sign_parent',
        'sign_parent_date',
        'withdrawn_member',
        ];
}
