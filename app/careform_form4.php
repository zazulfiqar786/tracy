<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class careform_form4 extends Model
{
    protected $fillable = [
        'client_id',
        'case_manager_id',
        'careform_id',
        'name',
        'plan_of_care_date',
        'your_icwp',
        'your_icwp_phone',
        'your_emergency_contact_name',
        'your_emergency_phone',
        'p_a_provider',
        'p_a_provider_phone',
        'your_primary_care_physician',
        'your_primary_care_physician_phone',
        's_e',
        's_e_sp',
        's_e_phone',
        's_e_equipment_provider',
        's_e_equipment_provider_phone',
        'hospital_for_emergencies',
        'h_f_e_phone_number',
        'h_f_e_address1',
        'h_f_e_address2',
        'behaviour_managment',
        'note_1',
        'note_2',
        'note_3',
        'note_4',
        'note_5',
        'note_6',
        'created_at',
        'updated_at'];
}
