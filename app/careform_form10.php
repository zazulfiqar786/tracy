<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class careform_form10 extends Model
{
    protected $fillable = [
        'client_id',
        'case_manager_id',
        'careform_id',
        'this',
        'day_of',
        'year20',
        'print_name',
        'signature_member',
        'signature_member_date',
        'print_name_cm',
        'signature_cm',
        'signature_date',
    ];
}
