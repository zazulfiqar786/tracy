<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class careform_form2 extends Model
{
    protected $fillable = [
        'client_id',
        'case_manager_id',
        'careform_id',
        'icwp_supply_list',
        'date',
        'gloves_permonth',
        'gloves_peryear',
        'gloves_perunit',
        'gloves_totalyear',

        'adult_briefs_permonth',
        'adult_briefs_peryear',
        'adult_briefs_perunit',
        'adult_briefs_totalyear',


        'bladder_permonth',
        'bladder_peryear',
        'bladder_perunit',
        'bladder_totalyear',

        'chux_permonth',
        'chux_peryear',
        'chux_perunit',
        'chux_totalyear',

        'wipes_permonth',
        'wipes_peryear',
        'wipes_perunit',
        'wipes_totalyear',

        'skin_permonth',
        'skin_peryear',
        'skin_perunit',
        'skin_totalyear',

        'reusable_permonth',
        'reusable_peryear',
        'reusable_perunit',
        'reusable_totalyear',

        'urinal_permonth',
        'urinal_peryear',
        'urinal_perunit',
        'urinal_totalyear',

        'bisacodyl_permonth',
        'bisacodyl_peryear',
        'bisacodyl_perunit',
        'bisacodyl_totalyear',

        'glycerin_permonth',
        'glycerin_peryear',
        'glycerin_perunit',
        'glycerin_totalyear',

        'uagic_permonth',
        'uagic_peryear',
        'uagic_perunit',
        'uagic_totalyear',

        'fleet_permonth',
        'fleet_peryear',
        'fleet_perunit',
        'fleet_totalyear',

        'lubricant_permonth',
        'lubricant_peryear',
        'lubricant_perunit',
        'lubricant_totalyear',

        'urinary_permonth',
        'urinary_peryear',
        'urinary_perunit',
        'urinary_totalyear',

        'tf_permonth',
        'tf_peryear',
        'tf_perunit',
        'tf_totalyear',

        'glycerin_swabs_permonth',
        'glycerin_swabs_peryear',
        'glycerin_swabs_perunit',
        'glycerin_swabs_totalyear',

        'toothettes_permonth',
        'toothettes_peryear',
        'toothettes_perunit',
        'toothettes_totalyear',

        'slipAndpuff_permonth',
        'slipAndpuff_peryear',
        'slipAndpuff_perunit',
        'slipAndpuff_totalyear',

        'bibs_disposable_permonth',
        'bibs_disposable_peryear',
        'bibs_disposable_perunit',
        'bibs_disposable_totalyear',

        'bibs_reusable_permonth',
        'bibs_reusable_peryear',
        'bibs_reusable_perunit',
        'bibs_reusable_totalyear',

        'inner_cannula_permonth',
        'inner_cannula_peryear',
        'inner_cannula_perunit',
        'inner_cannula_totalyear',

        'trach_care_permonth',
        'trach_care_peryear',
        'trach_care_perunit',
        'trach_care_totalyear',

        'trach_collar_permonth',
        'trach_collar_peryear',
        'trach_collar_perunit',
        'trach_collar_totalyear',

        'drain_sponge_permonth',
        'drain_sponge_peryear',
        'drain_sponge_perunit',
        'drain_sponge_totalyear',

        'multidose_permonth',
        'multidose_peryear',
        'multidose_perunit',
        'multidose_totalyear',

        'barrier_spray_permonth',
        'barrier_spray_peryear',
        'barrier_spray_perunit',
        'barrier_spray_totalyear',

        'periwash_permonth',
        'periwash_peryear',
        'periwash_perunit',
        'periwash_totalyear',

        'heel_protector_permonth',
        'heel_protector_peryear',
        'heel_protector_perunit',
        'heel_protector_totalyear',

        'elbow_protectors_permonth',
        'elbow_protectors_peryear',
        'elbow_protectors_perunit',
        'elbow_protectors_totalyear',

        'skin_barrier_permonth',
        'skin_barrier_peryear',
        'skin_barrier_perunit',
        'skin_barrier_totalyear',

        'geomat_permonth',
        'geomat_peryear',
        'geomat_perunit',
        'geomat_totalyear',

        'reacher_permonth',
        'reacher_peryear',
        'reacher_perunit',
        'reacher_totalyear',


    ];
}
