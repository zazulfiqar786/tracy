<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaseManagers extends Model
{
    protected $fillable = ['staff_id','no_of_cases','status','date'];

    public function staffs()
    {
        return $this->hasOne(Staff::class,'id','staff_id');
    }

}
