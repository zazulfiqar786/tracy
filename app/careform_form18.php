<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class careform_form18 extends Model
{
    protected $fillable = [
        'client_id',
        'case_manager_id',
        'careform_id',
        'member_name',
        
        'based_on_your_personal',
        'classify_memory',
        'comments1',
        'behavior_issues_yes_no',
        'behavior_issues_yes',
        'smoking_without',
        'smoking_without_yes',
        'suspected_alcohol',
        'suspected_alcohol_yes',
        'suspected_drug',
        'suspected_drug_yes',
        'comments2',
        'personal_assistance',
        'comments3',
    ];
}
