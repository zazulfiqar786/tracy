<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['user_id','name','email','contact','country','state','city','county','area',
        'zip_code','status'];

    public function users()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
    public function cases()
    {
        return $this->hasMany(ClientCase::class,'id','client_id');
    }
}
