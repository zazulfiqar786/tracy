<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class followup extends Model
{
    protected $fillable = [
        'provider_id',
        'client_id',
        'case_manager_id',
        'member_name',
        'date_1',
        'st_1',
        'et_1',
        'plan_fu1',
        'notes1',
        'doc_hrs1',
        'date_2',
        'st_2',
        'et_2',
        'plan_fu2',
        'notes2',
        'doc_hrs2',
        'date_3',
        'st_3',
        'et_3',
        'plan_fu3',
        'notes3',
        'doc_hrs3',
        'date_4',
        'st_4',
        'et_4',
        'plan_fu4',
        'notes4',
        'doc_hrs4',
        'case_manager_signature',
        'case_manager_date',
        'owner_signature',
        'owner_date',
        'created_at',
        'updated_at',
];
    //
}
