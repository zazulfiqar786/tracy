<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientCase extends Model
{
    protected $fillable = [
        'client_id',
        'client_name',
        'case_name',
        'description',
        'documents',
        'case_no',
        'case_date',
        'client_height',
        'client_weight',
        'country',
        'state',
        'city',
        'county',
        'area',
        'zip_code',
        'client_case_status',
        'assigned_status',
    ];
}
