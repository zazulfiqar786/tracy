<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $fillable = ['user_id','name','contact','country','state','city','county','area',
        'zip_code','status','is_manager'];
}
