<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CasesAssignment extends Model
{
    protected $fillable = ['manager_id','client_id','client_name','status','staff_id'];
    public function manager()
    {
        return $this->hasOne(CaseManagers::class,'id','manager_id');
    }
    public function staff()
    {

        return $this->hasOne(Staff::class,'id','staff_id');
    }
    public function client()
    {
        return $this->hasOne(Client::class,'id','client_id');
    }
}
