<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class admin
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

//                return (Auth::check() && Qs::userIsAdmin()) ? $next($request) : redirect()->route('login');
        if (Auth::check()) {

            if (Auth::user()->roles == 'admin')
            {
                return $next($request);
            }
            else
            {
               return redirect()->route('login');
            }
        }
    }
}
