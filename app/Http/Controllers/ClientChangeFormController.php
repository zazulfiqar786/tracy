<?php

namespace App\Http\Controllers;

use App\ClientChangeForm;
use Illuminate\Http\Request;

class ClientChangeFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $clientId = $id;
        return view('Forms.ClientChangeForm.index',$clientId);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Forms.ClientChangeForm.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge([
            'change_type' => implode(',', (array) $request->get('change_type'))
        ]);

        $data = $request->all();

        $isCreated = ClientChangeForm::create($data);
        return redirect()->route('client.change.form',$request->client_id);

        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClientChangeForm  $clientChangeForm
     * @return \Illuminate\Http\Response
     */
    public function show(ClientChangeForm $clientChangeForm ,$id)
    {
        // dd('Show by Id');
        $form = ClientChangeForm::find($id);
        return view('Forms.ClientChangeForm.show',compact('form'));
    }
    public function editForm(ClientChangeForm $clientChangeForm ,$id)
    {
        $clientId = $id;
        // dd('Show by Id');
        $form = ClientChangeForm::find($id);
        return view('Forms.ClientChangeForm.edit',compact('form','clientId'));
    }

    public function myClientForms($id)
    {
        $data['clientId'] = $id;
        $data['forms'] = ClientChangeForm::where('client_id', $id)->get();
        return view('Forms.ClientChangeForm.index',$data);
    }
    public function createForm($id)
    {
        $clientId = $id;
        return view('Forms.ClientChangeForm.create',compact('clientId'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ClientChangeForm  $clientChangeForm
     * @return \Illuminate\Http\Response
     */
    public function edit(ClientChangeForm $clientChangeForm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClientChangeForm  $clientChangeForm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClientChangeForm $clientChangeForm,$id)
    {
        $request->merge([
            'change_type' => implode(',', (array) $request->get('change_type'))
        ]);
        $data = $request->all();
        $isUpdated = ClientChangeForm::find($id)->update($data);

        return redirect()->route('client.change.form',$request->client_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ClientChangeForm  $clientChangeForm
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClientChangeForm $clientChangeForm,$id)
    {

        $isDeleted =  ClientChangeForm::where('id', $id)->delete();
        if($isDeleted)
        {
            return back();
        }
    }
}
