<?php

namespace App\Http\Controllers;

use App\careform_form17;
use Illuminate\Http\Request;

class CareformForm17Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $careform_id=$id;
        $forms = careform_form17::where('careform_id',$id)->get();
        return view('Forms.careform.careform_form17',compact('forms','careform_id'));
    }

    public function careform_form17Update(Request $request,careform_form17 $careform_form17, $id){

        $request->merge([
            'where_the_wound' => implode(',', (array) $request->get('where_the_wound'))
        ]);

        $request->merge([
            'primary_modes' => implode(',', (array) $request->get('primary_modes'))
        ]);

        $data = $request->except(['_token', '_method' ]);

        unset($data['_token']);
                $isUpdated = careform_form17::where('careform_id',$id)->update($data);
                $careformid=$request->careform_id;
        return \Redirect::route('careform_form18index', $careformid);
                // return redirect()->route('my.client.forms',$request->client_id);
            }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\careform_form17  $careform_form17
     * @return \Illuminate\Http\Response
     */
    public function show(careform_form17 $careform_form17)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\careform_form17  $careform_form17
     * @return \Illuminate\Http\Response
     */
    public function edit(careform_form17 $careform_form17)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\careform_form17  $careform_form17
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, careform_form17 $careform_form17)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\careform_form17  $careform_form17
     * @return \Illuminate\Http\Response
     */
    public function destroy(careform_form17 $careform_form17)
    {
        //
    }
}
