<?php

namespace App\Http\Controllers;

use App\careform_form15;
use Illuminate\Http\Request;
use App\careform_form14;


class CareformForm15Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {

        $careform_id=$id;
        $forms = careform_form15::where('careform_id',$id)->get();
        return view('Forms.careform.careform_form15',compact('forms','careform_id'));
        //
    }


    public function careform_form15Update(Request $request,careform_form15 $careform_form15, $id){


        $request->merge([
            'Skilled' => implode(',', (array) $request->get('Skilled'))
        ]);

        $request->merge([
            'Evaluation' => implode(',', (array) $request->get('Evaluation'))
        ]);

        $request->merge([
            'Potential' => implode(',', (array) $request->get('Potential'))
        ]);


        $request->merge([
            'ICWP_member' => implode(',', (array) $request->get('ICWP_member'))
        ]);



        $data = $request->except(['_token', '_method' ]);

        unset($data['_token']);
                $isUpdated = careform_form15::where('careform_id',$id)->update($data);
                $careformid=$request->careform_id;
                return \Redirect::route('careform_form16index', $careformid);





    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\careform_form15  $careform_form15
     * @return \Illuminate\Http\Response
     */
    public function show(careform_form15 $careform_form15)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\careform_form15  $careform_form15
     * @return \Illuminate\Http\Response
     */
    public function edit(careform_form15 $careform_form15)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\careform_form15  $careform_form15
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, careform_form15 $careform_form15)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\careform_form15  $careform_form15
     * @return \Illuminate\Http\Response
     */
    public function destroy(careform_form15 $careform_form15)
    {
        //
    }
}
