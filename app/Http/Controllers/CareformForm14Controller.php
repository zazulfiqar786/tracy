<?php

namespace App\Http\Controllers;

use App\careform_form14;
use Illuminate\Http\Request;

class CareformForm14Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {



        $careform_id=$id;
        $forms = careform_form14::where('careform_id',$id)->get();
        return view('Forms.careform.careform_form14',compact('forms','careform_id'));
    }

    public function careform_form14Update(Request $request,careform_form14 $careform_form14, $id){





        // $request->merge([
        //     'gender' => implode(',', (array) $request->get('gender'))
        // ]);


        // $request->merge([
        //     'past_year' => implode(',', (array) $request->get('past_year'))
        // ]);

        $request->merge([
            'Living_Arrangements' => implode(',', (array) $request->get('Living_Arrangements'))
        ]);

        $request->merge([
            'compared_to_1_year_ago' => implode(',', (array) $request->get('compared_to_1_year_ago'))
        ]);







        $data = $request->except(['_token', '_method' ]);

        unset($data['_token']);
                $isUpdated = careform_form14::where('careform_id',$id)->update($data);
                $careformid=$request->careform_id;
        return \Redirect::route('careform_form15index', $careformid);
                // return redirect()->route('my.client.forms',$request->client_id);
            }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\careform_form14  $careform_form14
     * @return \Illuminate\Http\Response
     */
    public function show(careform_form14 $careform_form14)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\careform_form14  $careform_form14
     * @return \Illuminate\Http\Response
     */
    public function edit(careform_form14 $careform_form14)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\careform_form14  $careform_form14
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, careform_form14 $careform_form14)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\careform_form14  $careform_form14
     * @return \Illuminate\Http\Response
     */
    public function destroy(careform_form14 $careform_form14)
    {
        //
    }
}
