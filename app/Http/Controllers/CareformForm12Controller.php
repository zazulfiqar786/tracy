<?php

namespace App\Http\Controllers;

use App\careform_form12;
use Illuminate\Http\Request;

class CareformForm12Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $careform_id=$id;
        $forms = careform_form12::where('careform_id',$id)->get();
        return view('Forms.careform.careform_form12',compact('forms','careform_id'));
    }

    public function careform_form12Update(Request $request,careform_form12 $careform_form12, $id){

        $data = $request->except(['_token', '_method' ]);

        unset($data['_token']);
                $isUpdated = careform_form12::where('careform_id',$id)->update($data);
                $careformid=$request->careform_id;
        return \Redirect::route('careform_form13index', $careformid);
                // return redirect()->route('my.client.forms',$request->client_id);
            }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\careform_form12  $careform_form12
     * @return \Illuminate\Http\Response
     */
    public function show(careform_form12 $careform_form12)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\careform_form12  $careform_form12
     * @return \Illuminate\Http\Response
     */
    public function edit(careform_form12 $careform_form12)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\careform_form12  $careform_form12
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, careform_form12 $careform_form12)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\careform_form12  $careform_form12
     * @return \Illuminate\Http\Response
     */
    public function destroy(careform_form12 $careform_form12)
    {
        //
    }
}
