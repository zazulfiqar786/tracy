<?php

namespace App\Http\Controllers;

use App\Disease;
use Illuminate\Http\Request;

class DiseaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/disease/create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $getInsertedDisease = Disease::create([
            'name' => $request->diseaseName
        ]);
        // return redirect()->back()->with('diseaseCreate', 'Disease Added!');
        return redirect()->route('disease.show');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Disease  $disease
     * @return \Illuminate\Http\Response
     */
    public function show(Disease $disease)
    {
        $diseaseData = Disease::all();
        return view('/disease/show',['diseases'=>$diseaseData]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Disease  $disease
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $diseaseData = Disease::find($id);
        return view('/disease/edit',['diseases'=>$diseaseData]);

    }

    public function update(Request $request)
    {

        if($disease = Disease::find($request->diseaseId))
        {
            $disease->name = $request->diseaseName;
            if($disease->save())
            {
                return redirect()->route('disease.show')->with('diseaseEditSuccess', 'Disease Updated successfully');
            }
            else
            {
                return redirect()->back();
            }
        }
        else
        {
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Disease  $disease
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $disease = Disease::where('id', $id)->first(); // File::find($id)
        if($disease)
        {
            $disease->delete();
            return redirect()->back()->with('deleteDiseaseSuccess', 'Disease deleted successfully');
        }
    }
}
