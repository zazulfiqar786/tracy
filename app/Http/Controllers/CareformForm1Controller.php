<?php

namespace App\Http\Controllers;

use App\careform_form1;
use Illuminate\Http\Request;
use DB;
class CareformForm1Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {

        $careform_id=$id;
        $forms = careform_form1::where('careform_id',$id)->get();
        return view('Forms.careform.careform_form1',compact('forms','careform_id'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\careform_form1  $careform_form1
     * @return \Illuminate\Http\Response
     */
    public function show(careform_form1 $careform_form1)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\careform_form1  $careform_form1
     * @return \Illuminate\Http\Response
     */
    public function edit(careform_form1 $careform_form1)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\careform_form1  $careform_form1
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, careform_form1 $careform_form1)
    {
        dd($request);
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\careform_form1  $careform_form1
     * @return \Illuminate\Http\Response
     */
    public function destroy(careform_form1 $careform_form1)
    {
        //
    }
    public function careform_form1Update(Request $request,careform_form1 $careform_form1, $id){




$request->merge([
    'careform_form1_miscellaneous' => implode(',', (array) $request->get('careform_form1_miscellaneous'))
]);

$request->merge([
    'careform_form1_wheelchair' => implode(',', (array) $request->get('careform_form1_wheelchair'))
]);

$request->merge([
    'careform_form1_assistive' => implode(',', (array) $request->get('careform_form1_assistive'))
]);

$request->merge([
    'careform_form1_respiratory' => implode(',', (array) $request->get('careform_form1_respiratory'))
]);

$request->merge([
    'careform_form1_mobility' => implode(',', (array) $request->get('careform_form1_mobility'))
]);

$request->merge([
    'careform_form1_bathroom' => implode(',', (array) $request->get('careform_form1_bathroom'))
]);

$request->merge([
    'careform_form1_beds' => implode(',', (array) $request->get('careform_form1_beds'))
]);

$request->merge([
    'careform_form1_transfer' => implode(',', (array) $request->get('careform_form1_transfer'))
]);




$data = $request->except(['_token', '_method' ]);


unset($data['_token']);
        $isUpdated = careform_form1::where('careform_id',$id)->update($data);

        $careformid=$request->careform_id;
        return \Redirect::route('careform_form2index', $careformid);
        // return back();
        // return redirect()->route('my.client.forms',$request->client_id);

    }
}
