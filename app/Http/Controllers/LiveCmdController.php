<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class LiveCmdController extends Controller
{
    public  function  clear_cache()
    {
        Artisan::call('cache:clear');
        dd("Cache is cleared");

    }
    public  function view_clear()
    {
        Artisan::call('view:clear');
        dd("View is cleared");
    }

    public  function optimize()
    {
        Artisan::call('optimize');
        dd("optimize Done");

    }

    public  function storage_link()
    {
        Artisan::call('storage:link');
        dd("Storage Link done");
    }

    public  function route_clear()
    {
        Artisan::call('route:clear');
        dd("route clear done");
    }

    public  function route_cache()
    {
        Artisan::call('route:cache');
        dd("route cache done");
    }


}
