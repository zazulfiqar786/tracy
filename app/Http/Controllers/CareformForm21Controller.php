<?php

namespace App\Http\Controllers;

use App\careform_form21;
use Illuminate\Http\Request;

class CareformForm21Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {

          $careform_id=$id;
        $forms = careform_form21::where('careform_id',$id)->get();
        return view('Forms.careform.careform_form21',compact('forms','careform_id'));
        //
    }

       public function careform_form21Update(Request $request,careform_form21 $careform_form21, $id){


        //   $request->merge([
        //     'safety_hazards' => implode(',', (array) $request->get('safety_hazards'))
        // ]);





               $data = $request->except(['_token', '_method' ]);

        unset($data['_token']);
                $isUpdated = careform_form21::where('careform_id',$id)->update($data);
                $careformid=$request->careform_id;
        return \Redirect::route('careform_form22index', $careformid);





    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\careform_form21  $careform_form21
     * @return \Illuminate\Http\Response
     */
    public function show(careform_form21 $careform_form21)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\careform_form21  $careform_form21
     * @return \Illuminate\Http\Response
     */
    public function edit(careform_form21 $careform_form21)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\careform_form21  $careform_form21
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, careform_form21 $careform_form21)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\careform_form21  $careform_form21
     * @return \Illuminate\Http\Response
     */
    public function destroy(careform_form21 $careform_form21)
    {
        //
    }
}
