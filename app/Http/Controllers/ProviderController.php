<?php

namespace App\Http\Controllers;

use App\Provider;
use Illuminate\Database\Eloquent\Concerns\HasAttributes;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class ProviderController extends Controller
{
    public function index()
    {

    }

    public function create()
    {
        return view('providers.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['roles'] = 'provider';
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($request->password),
            'roles' => $data['roles'],
        ]);

        $data['user_id'] = $user->id;
        $data['status'] = 1;
        $isInserted = Provider::create($data);
        if ($isInserted)
        {
            return redirect()->route('provider.showAll');
        }
        else
        {
            return redirect()->back();
        }
    }

    public function show(Provider $provider)
    {
        $userId = auth()->user()->id;
        \DB::connection()->enableQueryLog();
        $userData = \DB::table('users')
            ->where(['users.roles' => 'provider'])->
            where(['users.id' => $userId])->get();
        // dd($queries = \DB::getQueryLog());

        return view('providers.show', ['userData' => $userData]);

    }

    public function showAll()
    {
//        \DB::connection()->enableQueryLog();
//        $allProviders = \DB::table('users')
//            ->where(['users.roles' => 'provider'])->get();
        $allProviders = Provider::latest()->get();
        return view('providers.showAll', ['userData' => $allProviders]);
    }
    // bit 2 for all provider
    // bit 1 for individual provider

    public function edit($id)
    {
        $providerData = Provider::find($id);
        return view('providers.edit', ['providerData' => $providerData]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $isUpdated = Provider::find($id)->update($data);
        if ($isUpdated)
        {
            return redirect()->route('provider.showAll')->with('success', 'Record Updated successfully');
        }
        else
        {
            return redirect()->back();
        }
    }


    public function destroy($id)
    {
        $provider = Provider::where('id', $id)->first();
        if($provider)
        {
            $provider->delete();
            return redirect()->back();
        }
        else
        {
            return redirect()->back();
        }

    }
}

