<?php

namespace App\Http\Controllers;

use App\careform_form16;
use App\careform_form20;
use Illuminate\Http\Request;

class CareformForm20Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {

        $careform_id=$id;
        $forms = careform_form20::where('careform_id',$id)->get();
        return view('Forms.careform.careform_form20',compact('forms','careform_id'));
        //
    }


        public function careform_form20Update(Request $request,careform_form20 $careform_form20, $id){


          $request->merge([
            'Caregiver' => implode(',', (array) $request->get('Caregiver'))
        ]);

        $request->merge([
            'assistance' => implode(',', (array) $request->get('assistance'))
        ]);

         $request->merge([
            'Relationship_to_Member' => implode(',', (array) $request->get('Relationship_to_Member'))
        ]);


          $request->merge([
            'Relationship_to_memberd' => implode(',', (array) $request->get('Relationship_to_memberd'))
        ]);



               $data = $request->except(['_token', '_method' ]);

        unset($data['_token']);
                $isUpdated = careform_form20::where('careform_id',$id)->update($data);
                $careformid=$request->careform_id;
        return \Redirect::route('careform_form21index', $careformid);





    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\careform_form20  $careform_form20
     * @return \Illuminate\Http\Response
     */
    public function show(careform_form20 $careform_form20)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\careform_form20  $careform_form20
     * @return \Illuminate\Http\Response
     */
    public function edit(careform_form20 $careform_form20)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\careform_form20  $careform_form20
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, careform_form20 $careform_form20)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\careform_form20  $careform_form20
     * @return \Illuminate\Http\Response
     */
    public function destroy(careform_form20 $careform_form20)
    {
        //
    }
}
