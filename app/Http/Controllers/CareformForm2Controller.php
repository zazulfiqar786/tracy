<?php

namespace App\Http\Controllers;

use App\careform_form2;
use Illuminate\Http\Request;

class CareformForm2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $careform_id=$id;
        $forms = careform_form2::where('careform_id',$id)->get();
        return view('Forms.careform.careform_form2',compact('forms','careform_id'));



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\careform_form2  $careform_form2
     * @return \Illuminate\Http\Response
     */
    public function show(careform_form2 $careform_form2)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\careform_form2  $careform_form2
     * @return \Illuminate\Http\Response
     */
    public function edit(careform_form2 $careform_form2)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\careform_form2  $careform_form2
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, careform_form2 $careform_form2)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\careform_form2  $careform_form2
     * @return \Illuminate\Http\Response
     */
    public function destroy(careform_form2 $careform_form2)
    {
        //
    }

    public function careform_form2Update(Request $request,careform_form2 $careform_form2, $id){
        $data = $request->except(['_token', '_method' ]);

        unset($data['_token']);
                $isUpdated = careform_form2::where('careform_id',$id)->update($data);
                $careformid=$request->careform_id;
        return \Redirect::route('careform_form3index', $careformid);
                // return redirect()->route('my.client.forms',$request->client_id);
            }
}
