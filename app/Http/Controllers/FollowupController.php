<?php

namespace App\Http\Controllers;

use App\followup;
use Illuminate\Http\Request;

class FollowupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->all();
        $create=followup::create($data);
        return redirect()->route('followup.forms',$request->client_id);
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\followup  $followup
     * @return \Illuminate\Http\Response
     */
    public function show(followup $followup, $id)
    {
        $form = followup::find($id);
        return view('Forms.Followup.show',compact('form'));
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\followup  $followup
     * @return \Illuminate\Http\Response
     */
    public function edit(followup $followup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\followup  $followup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, followup $followup)

    {
        $id=$request->id;
        $data = $request->all();
        $isUpdated = followup::find($id)->update($data);

        return redirect()->route('followup.forms',$request->client_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\followup  $followup
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, followup $followup)
    {

        $id=$request->id;

        $isDeleted =  followup::where('id', $id)->delete();
        if($isDeleted)
        {
            return back();
        }
    }

    public function myClientForms($id)
    {

        $data['clientId'] = $id;
        $data['forms'] = followup::where('client_id', $id)->get();
        return view('Forms.Followup.index',$data);

        // return view('Forms.Followup.index');
    }
    public function createForm($id)
    {

        $clientId = $id;
        return view('Forms.Followup.create',compact('clientId'));
    }
    public function editForm(followup $MonthlyVisitForm ,$id)
    {
        $clientId = $id;
        // dd('Show by Id');
        $form = followup::find($id);
        return view('Forms.Followup.edit',compact('form','clientId'));
    }
}
