<?php


namespace App\Http\Controllers;

use App\CasesAssignment;
use App\Disease;
use App\Staff;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\CaseManagers;
use Illuminate\Support\Facades\DB;


class StaffController extends Controller
{

    public function index()
    {
        //
    }


    public function create()
    {
        $allDiseases = Disease::all();
        return view('staff.create', ['diseases' => $allDiseases]);
    }


    public function store(Request $request)
    {
        $staff = new User();
        $getInsertedStaff = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'roles' => 'staff',
            'assigned_disease' => $request->assignedDisease,
        ]);
        if ($staff) {
            $getInsertedStaff = Staff::create([
                'user_id' => $getInsertedStaff->id,
                'name' => $request->name,
                'roles' => 'staff',
                'country' => $request->country,
                'state' => $request->state,
                'city' => $request->city,
                'county' => $request->county,
                'area' => $request->area,
                'zip_code' => $request->zip_code,
                'contact' => $request->contact,
            ]);
        }
        return redirect()->route('staff.showAll')->with('success', 'staff Created successfully');
    }


    public function show(Staff $staff)
    {
        $userId = auth()->user()->id;
        //$userData = User::find($userId);
        \DB::connection()->enableQueryLog();
        $userData = \DB::table('users')
            ->where(['users.roles' => 'staff'])->
            where(['users.id' => $userId])->get();
        // dd($queries = \DB::getQueryLog());

        return view('staff.show', ['userData' => $userData]);
    }
    public function staffClient()
    {

        $staffId = Staff::where('user_id',Auth::user()->id)->firstOrFail();
        $data['staffClients'] = CasesAssignment::where('staff_id',$staffId->id)->get();
        return view('staff.staffclients',$data);
    }

    public function showAllStaff()
    {
        \DB::connection()->enableQueryLog();
        $allStaff = Staff::latest('id','DESC')->get();
        return view('staff.showAllStaff', ['allUsers' => $allStaff]);
    }

    public function edit($id)
    {
        $staffData = Staff::find($id);
        return view('/staff/edit', ['staffData' => $staffData]);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $staff = Staff::find($request->staffId)->update($data);
        if($staff)
        {
            return redirect()->route('staff.showAll')->with('staffEditSuccess', 'staff Updated successfully');
        }
        else
        {
            return redirect()->back();
        }
    }
    public function caseManager(Request $request)
    {
        $toUpdate['is_manager'] = 1;
        $page2update =  Staff::find($request->staffId)->update($toUpdate);
         $user = new CaseManagers();
         $user->staff_id = $request->staffId;
         $user->status = 1;
         $user->save();
        return redirect()->route('staff.showAll')->with('success', 'staff Created successfully');
    }
    public function unMarkaseManager(Request $request)
    {
        $toUpdate['is_manager'] = 0;
        $caseManager =  CaseManagers::where('id',$request->managerId)->firstOrFail();
        $page2update =  Staff::find($caseManager->staff_id)->update($toUpdate);
        CaseManagers::destroy($caseManager->id);
        return redirect()->back();
    }
    public function destroy($id)
    {
        $staff = User::where('id', $id)->first();
        if ($staff) {
            $staff->delete();
            return redirect()->back()->with('staffDeleteSuccess', 'staff deleted successfully');
        }
    }
}
