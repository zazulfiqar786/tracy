<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $userId = auth()->user()->id;
        $userData = User::find($userId);
        return view('home',['userData'=>$userData]);
    }

    public function allForms($id)
    {
        $clientId = $id;
        return view('Forms.allForms',compact('clientId'));
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('login');
    }

    public function profile()
    {
        $userId = auth()->user()->id;
        $profile= User::find($userId);
        return view('profile',['userData'=>$profile]);
    }

    public function profileupdate(Request $request)
    {

        $data = $request;
        $data['pass']=$data['password'];
        if(empty($data['pass']) || $data['pass'] === null || $data['pass'] === '')
        {
            $data = $request->all();
            $data['password']= bcrypt($data['password']);
             User::find($request->id)->update($data);

            return redirect()->back();

        }
        else {

            $data = $request->all();
            $data['password']= bcrypt($data['password']);
             User::find($request->id)->update($data);
            return redirect()->back();
        }
    }



}
