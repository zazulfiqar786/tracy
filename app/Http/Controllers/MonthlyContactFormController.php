<?php

namespace App\Http\Controllers;

use App\MonthlyContactForm;
use Illuminate\Http\Request;

class MonthlyContactFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->merge([
            'cal_type' => implode(',', (array) $request->get('cal_type'))
        ]);
        $request->merge([
            'topic_of_discussion' => implode(',', (array) $request->get('topic_of_discussion'))
        ]);
        $request->merge([
            'curent_source' => implode(',', (array) $request->get('curent_source'))
        ]);
        $request->merge([
            'curent_icwp' => implode(',', (array) $request->get('curent_icwp'))
        ]);
        $request->merge([
            'reviewed_population' => implode(',', (array) $request->get('reviewed_population'))
        ]);
        $request->merge([
            'varified' => implode(',', (array) $request->get('varified'))
        ]);




        $data = $request->all();

        $isCreated = MonthlyContactForm::create($data);
        return redirect()->route('monthlyContactReport.forms',$request->client_id);
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MonthlyContactForm  $monthlyContactForm
     * @return \Illuminate\Http\Response
     */
    public function show(MonthlyContactForm $monthlyContactForm, $id)
    {
        $form = MonthlyContactForm::find($id);
        return view('Forms.MonthlyContactForm.show',compact('form'));
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MonthlyContactForm  $monthlyContactForm
     * @return \Illuminate\Http\Response
     */
    public function edit(MonthlyContactForm $monthlyContactForm)
    {

        return "Edit";
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MonthlyContactForm  $monthlyContactForm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MonthlyContactForm $monthlyContactForm,$id)
    {
        $request->merge([
            'cal_type' => implode(',', (array) $request->get('cal_type'))
        ]);
        $request->merge([
            'topic_of_discussion' => implode(',', (array) $request->get('topic_of_discussion'))
        ]);
        $request->merge([
            'curent_source' => implode(',', (array) $request->get('curent_source'))
        ]);
        $request->merge([
            'curent_icwp' => implode(',', (array) $request->get('curent_icwp'))
        ]);
        $request->merge([
            'reviewed_population' => implode(',', (array) $request->get('reviewed_population'))
        ]);
        $request->merge([
            'varified' => implode(',', (array) $request->get('varified'))
        ]);
        $data = $request->all();
        $isUpdated = MonthlyContactForm::find($id)->update($data);
        return redirect()->route('monthlyContactReport.forms',$request->client_id);
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MonthlyContactForm  $monthlyContactForm
     * @return \Illuminate\Http\Response
     */
    public function destroy(MonthlyContactForm $monthlyContactForm, $id)
    {
        $isDeleted =  MonthlyContactForm::where('id', $id)->delete();
        if($isDeleted)
        {
            return back();
        }
        //
    }

    public function myClientForms($id)
    {

        $data['clientId'] = $id;
        $data['forms'] = MonthlyContactForm::where('client_id', $id)->get();
        return view('Forms.MonthlyContactForm.index',$data);

        return view('Forms.MonthlyContactForm.index');
    }
    public function createForm($id)
    {

        $clientId = $id;
        return view('Forms.MonthlyContactForm.create',compact('clientId'));
    }
    public function editForm(MonthlyContactForm $MonthlyVisitForm ,$id)
    {
        $clientId = $id;
        // dd('Show by Id');
        $form = MonthlyContactForm::find($id);
        return view('Forms.MonthlyContactForm.edit',compact('form','clientId'));
    }
}
