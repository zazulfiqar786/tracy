<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use App\User;

class ClientController extends Controller
{

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }

    public function show()
    {
        \DB::connection()->enableQueryLog();
        $userData = \DB::table('users')
            ->where(['users.roles' => 'staff'])
            ->orWhere(['users.roles' => 'admin'])->get();
        return view('/client/show',['userData'=>$userData]);
    }
    public function showAllClient()
    {
        $allClients = Client::latest()->get();
        return view('client.show',compact('allClients'));
    }


    public function edit($id)
    {
        $userData = Client::find($id);
            return view('client.clientedit',['userData'=>$userData]);
    }

    public function update(Request $request, Client $client)
    {
        $data = $request->all();
        $client = Client::find($request->id)->update($data);
        if($client)
        {
            $userData['name'] = $request->name;
            $user = User::find($request->user_id)->update($userData);
            if($user)
            {
                return redirect()->route('home')->with('success', 'Record Updated successfully');
            }
            else
            {
                return redirect()->back();
            }
        }
        else
        {
            return redirect()->back();
        }
    }

    public function clientProfile()
    {
        $userId = auth()->user()->id;
        $profile= Client::where('user_id',$userId)->get();
        return view('client.profile',['clientData'=>$profile]);
    }

    public function destroy($id)
    {
        $user = User::where('id', $id)->first(); // File::find($id)
        if($user)
        {
            $user->delete();
            return redirect()->back()->with('success', 'Record deleted successfully');
        }
    }
}
