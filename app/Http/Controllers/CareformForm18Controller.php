<?php

namespace App\Http\Controllers;

use App\careform_form18;
use Illuminate\Http\Request;

class CareformForm18Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $careform_id=$id;
        $forms = careform_form18::where('careform_id',$id)->get();
        return view('Forms.careform.careform_form18',compact('forms','careform_id'));
    }

    public function careform_form18Update(Request $request,careform_form18 $careform_form18, $id){

        $request->merge([
            'based_on_your_personal' => implode(',', (array) $request->get('based_on_your_personal'))
        ]);

        $request->merge([
            'classify_memory' => implode(',', (array) $request->get('classify_memory'))
        ]);

        $request->merge([
            'personal_assistance' => implode(',', (array) $request->get('personal_assistance'))
        ]);

        $data = $request->except(['_token', '_method' ]);

        unset($data['_token']);
                $isUpdated = careform_form18::where('careform_id',$id)->update($data);
                $careformid=$request->careform_id;
        return \Redirect::route('careform_form19index', $careformid);
                // return redirect()->route('my.client.forms',$request->client_id);
            }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\careform_form18  $careform_form18
     * @return \Illuminate\Http\Response
     */
    public function show(careform_form18 $careform_form18)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\careform_form18  $careform_form18
     * @return \Illuminate\Http\Response
     */
    public function edit(careform_form18 $careform_form18)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\careform_form18  $careform_form18
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, careform_form18 $careform_form18)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\careform_form18  $careform_form18
     * @return \Illuminate\Http\Response
     */
    public function destroy(careform_form18 $careform_form18)
    {
        //
    }
}
