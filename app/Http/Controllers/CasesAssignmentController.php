<?php

namespace App\Http\Controllers;

use App\CaseManagers;
use App\CasesAssignment;
use App\Client;
use App\ClientCase;
use App\Http\Requests\CaseAssignment;
use App\Staff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CasesAssignmentController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(CaseAssignment $request)
    {
        $data = $request->all();
        $data['staff_id'] = $data['manager_id'];
//        if(isset($request->manager_zip_code))
//        {
//            $data['manager_id'] = $request->manager_zip_code;
//            $data['staff_id'] = $data['manager_id'];
//
//        }
//        elseif(isset($request->manager_area))
//        {
//            $data['manager_id'] = $request->manager_area;
//            $data['staff_id'] = $data['manager_id'];
//
//        }
//        elseif(isset($request->manager_county))
//        {
//            $data['manager_id'] = $request->manager_county;
//            $data['staff_id'] = $data['manager_id'];
//
//        }
//        elseif(isset($request->manager_city))
//        {
//            $data['manager_id'] = $request->manager_city;
//            $data['staff_id'] = $data['manager_id'];
//
//        }
//        elseif(isset($request->manager_state))
//        {
//            $data['manager_id'] = $request->manager_state;
//            $data['staff_id'] = $data['manager_id'];
//
//        }
//        elseif(isset($request->manager_country))
//        {
//            $data['manager_id'] = $request->manager_country;
//            $data['staff_id'] = $data['manager_id'];
//
//        }
//        else
//        {
//            $data['manager_id'] = null;
//            $data['staff_id'] = null;
//        }
        $isInserted =  CasesAssignment::create($data);
        if($isInserted)
        {
            return redirect()->route('assigned.managers');
        }
        else
        {
            return redirect()->back();
        }

    }

    public function viewAll()
    {
        $data['clientManagers'] = CasesAssignment::all();
        return view('casesassignment.assigned_managers',$data);
    }


    public function show($id)
    {
//        $data['caseDetail'] = ClientCase::find($id);
        $data['clientDetail'] = Client::find($id);
        $data['caseManagers'] = CaseManagers::all();
        return view('casesassignment.show',$data);
    }

    public function edit(CasesAssignment $casesAssignment)
    {
        //
    }

    public function update(Request $request, CasesAssignment $casesAssignment)
    {
        //
    }

    public function destroy(CasesAssignment $casesAssignment)
    {
        //
    }
}
