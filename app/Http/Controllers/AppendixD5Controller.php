<?php

namespace App\Http\Controllers;

use App\AppendixD5;
use Illuminate\Http\Request;

class AppendixD5Controller extends Controller
{

    public function index()
    {
        $forms = AppendixD5::all();
        return view('Forms.AppendixD5.index',compact('forms'));
    }


    public function create()
    {
        return view('Forms.AppendixD5.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $isCreated = AppendixD5::create($data);
        return redirect()->route('my.client.forms',$request->client_id);
    }


    public function show(AppendixD5 $appendixD5,$id)
    {
        $form = AppendixD5::find($id);
        return view('Forms.AppendixD5.show',compact('form'));
    }

    public function edit(AppendixD5 $appendixD5,$id)
    {
        $form = AppendixD5::find($id);
        return view('Forms.AppendixD5.edit',compact('form'));
    }

    public function update(Request $request, AppendixD5 $appendixD5,$id)
    {
        $data = $request->all();
        $isUpdated = AppendixD5::find($id)->update($data);
        return redirect()->route('my.client.forms',$request->client_id);
    }


    public function destroy(AppendixD5 $appendixD5,$id)
    {
        $isDeleted =  AppendixD5::where('id', $id)->delete();
        if($isDeleted)
        {
            return back();
        }
    }

    public function myClientForms($id)
    {
        $data['clientId'] = $id;
        $data['forms'] = AppendixD5::where('client_id', $id)->get();
        return view('Forms.AppendixD5.index',$data);
    }
    public function createForm($id)
    {
        $clientId = $id;
        return view('Forms.AppendixD5.create',compact('clientId'));
    }
//    public function formsIndex($id)
//    {
//        $clientId = $id;
//        return view('AppendixD5.index',compact('clientId'));
//
//    }
}
