<?php

namespace App\Http\Controllers;

use App\CaseManagers;
use App\Staff;
use Illuminate\Http\Request;

class CaseManagersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allCaseManagers = CaseManagers::all();
        return view('casemanagers.index',['allCaseManagers' => $allCaseManagers]);
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show(CaseManagers $caseManagers)
    {
        //
    }


    public function edit(CaseManagers $caseManagers)
    {
        //
    }


    public function update(Request $request, CaseManagers $caseManagers)
    {
        //
    }


    public function destroy(CaseManagers $caseManagers)
    {
        //
    }
}
