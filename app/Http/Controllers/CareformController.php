<?php

namespace App\Http\Controllers;

use App\careform;
use App\careform_form1;
use App\careform_form2;
use App\careform_form3;
use App\careform_form4;
use App\careform_form5;
use App\careform_form6;
use App\careform_form7;
use App\careform_form10;
use App\careform_form12;
use App\careform_form13;
use App\careform_form14;
use App\careform_form15;
use App\careform_form16;
use App\careform_form17;
use App\careform_form18;
use App\careform_form19;
use App\careform_form20;
use App\careform_form21;
use App\careform_form22;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CareformController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $forms = careform::all();
        return view('Forms.careform.index',compact('forms'));
    }





    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Forms.careform.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->all();
        $isCreated = careform::create($data);

        $careformid=$isCreated->id;

        $values = array('careform_id' => $careformid);
        DB::table('careform_form1s')->insert($values);

        $values = array('careform_id' => $careformid);
        DB::table('careform_form2s')->insert($values);

        $values = array('careform_id' => $careformid);
        DB::table('careform_form3s')->insert($values);

        $values = array('careform_id' => $careformid);
        DB::table('careform_form4s')->insert($values);

        $values = array('careform_id' => $careformid);
        DB::table('careform_form5s')->insert($values);

        $values = array('careform_id' => $careformid);
        DB::table('careform_form6s')->insert($values);

        $values = array('careform_id' => $careformid);
        DB::table('careform_form7s')->insert($values);

        $values = array('careform_id' => $careformid);
        DB::table('careform_form10s')->insert($values);

        $values = array('careform_id' => $careformid);
        DB::table('careform_form12s')->insert($values);

        $values = array('careform_id' => $careformid);
        DB::table('careform_form13s')->insert($values);

        $values = array('careform_id' => $careformid);
        DB::table('careform_form14s')->insert($values);

        $values = array('careform_id' => $careformid);
        DB::table('careform_form15s')->insert($values);

        $values = array('careform_id' => $careformid);
        DB::table('careform_form16s')->insert($values);

        $values = array('careform_id' => $careformid);
        DB::table('careform_form17s')->insert($values);

        $values = array('careform_id' => $careformid);
        DB::table('careform_form18s')->insert($values);

        $values = array('careform_id' => $careformid);
        DB::table('careform_form19s')->insert($values);

        $values = array('careform_id' => $careformid);
        DB::table('careform_form20s')->insert($values);

        $values = array('careform_id' => $careformid);
        DB::table('careform_form21s')->insert($values);

        $values = array('careform_id' => $careformid);
        DB::table('careform_form22s')->insert($values);



        return \Redirect::route('careform_form1index', $careformid);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\careform  $careform
     * @return \Illuminate\Http\Response
     */
    public function show(careform $careform,$id)
    {

        $form = careform::find($id);
        return view('Forms.careform.show',compact('form'));


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\careform  $careform
     * @return \Illuminate\Http\Response
     */
    public function edit(careform $careform, $id)
    {
        // dd('Data Set');
        dd($id);
        // $form = careform::find($id);
        // return view('Forms.careform.edit',compact('form'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\careform  $careform
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, careform $careform)
    {

        $id=$request->id;
        // dd($id);
        $data = $request->all();
        $isUpdated = careform::find($id)->update($data);
        return redirect()->route('careform.forms',$request->client_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\careform  $careform
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,careform $careform)
    {

        $id=$request->id;
        $isDeleted =  careform::where('id', $id)->delete();
        $isDeleted =  careform_form1::where('careform_id', $id)->delete();
        $isDeleted =  careform_form2::where('careform_id', $id)->delete();
        $isDeleted =  careform_form3::where('careform_id', $id)->delete();
        $isDeleted =  careform_form4::where('careform_id', $id)->delete();
        $isDeleted =  careform_form5::where('careform_id', $id)->delete();
        $isDeleted =  careform_form6::where('careform_id', $id)->delete();
        $isDeleted =  careform_form7::where('careform_id', $id)->delete();
        $isDeleted =  careform_form10::where('careform_id', $id)->delete();
        $isDeleted =  careform_form12::where('careform_id', $id)->delete();
        $isDeleted =  careform_form13::where('careform_id', $id)->delete();
        $isDeleted =  careform_form14::where('careform_id', $id)->delete();
        $isDeleted =  careform_form15::where('careform_id', $id)->delete();
        $isDeleted =  careform_form16::where('careform_id', $id)->delete();
        $isDeleted =  careform_form17::where('careform_id', $id)->delete();
        $isDeleted =  careform_form18::where('careform_id', $id)->delete();
        $isDeleted =  careform_form19::where('careform_id', $id)->delete();
        $isDeleted =  careform_form20::where('careform_id', $id)->delete();
        $isDeleted =  careform_form21::where('careform_id', $id)->delete();
        $isDeleted =  careform_form22::where('careform_id', $id)->delete();



        if($isDeleted)
        {
            return back();
        }
    }
    public function myClientForms($id)
    {

        // dd($id);
        $data['clientId'] = $id;
        $data['forms'] = careform::where('client_id', $id)->get();
        return view('Forms.careform.index',$data);
    }
    public function createForm($id)
    {

        $clientId = $id;
        return view('Forms.careform.create',compact('clientId'));
    }
    public function editForm(careform $careform ,$id)
        {
            $clientId = $id;
            // dd('Show by Id');
            $form = careform::find($id);
            return view('Forms.careform.edit',compact('form','clientId'));
        }

        public function careform_forms(Request $request, careform $careform ,$id)
        {

            $careform_id=$id;
            return view('Forms.careform.careform_show',compact('careform_id'));

        }


}
