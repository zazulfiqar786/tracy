<?php

namespace App\Http\Controllers;

use App\Client;
use App\Staff;
use App\ClientCase;
use App\CasesAssignment;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class ClientCaseController extends Controller
{
    public function index()
    {

    }

    public function create($id)
    {
//        $clientData = User::find($id);
        $userId = Auth::user()->id;
        $clientData = Client::where('user_id',$userId)->firstOrFail();
        return view('/clientcases/create',['clientData' => $clientData]);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['client_case_status'] = 1;
        $data['assigned_status'] = 0;
        ClientCase::create($data);
        //client_case_status 1 for PENDING
        return redirect()->route('cases.clientcases',$request->client_id)->with('clientCaseCreated', 'Case Created successfully');

    }

    public function ShowClientCases($id)
    {

        $staffIdGet = Client::where('user_id',Auth::user()->id)->firstOrFail();
        $clientidOnly=$staffIdGet->id;
        $staffId = CasesAssignment::where('client_id',$staffIdGet->id)->firstOrFail();
        $staffIdfetch=$staffId->staff_id;
        $staffId_stafftable = Staff::where('id',$staffIdfetch)->firstOrFail();

        $data['staffClients'] = CasesAssignment::where('staff_id',$staffIdfetch)->where('client_id',$clientidOnly)->get();
        return view('clientcases.showclientcases',$data);





    }
    public function ShowAllCases()
    {
        $allCases = ClientCase::orderBy('created_at', 'desc')->get();
        return view('/clientcases/showAllCases',['cases' => $allCases]);
    }

    public function details($id)
    {
        return "hey client details $id";
    }

    public function edit($id)
    {

        $caseData = ClientCase::find($id);
        return view('/clientcases/edit',['caseData'=>$caseData]);
    }

    public function update(Request $request)
    {
        if($staff = ClientCase::find($request->caseId))
        {
            $staff->name = $request->caseName;
            $staff->description = $request->caseDescription;
            $staff->documents = $request->caseDocuments;
            $staff->case_no = $request->caseNo;
            $staff->case_date = $request->caseDate;
            $staff->client_height = $request->clientHeight;
            $staff->client_weight = $request->clientWeight;
            if($staff->save())
            {
                if(Auth::user()->roles == 'admin')
                {
                    return redirect()->route('cases.allcases')->with('caseEditSuccess', 'Case Updated successfully');
                }
                else
                {
                    return redirect()->route('cases.clientcases',$request->clientId)->with('caseEditSuccess', 'Case Updated successfully');
                }

            }
            else
            {
                return redirect()->back();
            }
        }
        else
        {
        }
    }

    public function destroy($id)
    {
        $case = ClientCase::where('id', $id)->first();
        if($case)
        {
            $case->delete();
            return redirect()->back()->with('caseDeleteSuccess', 'case   deleted successfully');
        }
    }
}
