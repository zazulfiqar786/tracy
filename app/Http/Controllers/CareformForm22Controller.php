<?php

namespace App\Http\Controllers;

use App\careform_form22;
use App\careform;

use Illuminate\Http\Request;

class CareformForm22Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $careform_id=$id;
        $forms = careform_form22::where('careform_id',$id)->get();
        return view('Forms.careform.careform_form22',compact('forms','careform_id'));
    }

    public function careform_form22Update(Request $request,careform_form22 $careform_form22, $id){

        $request->merge([
            'skills' => implode(',', (array) $request->get('skills'))
        ]);



        $data = $request->except(['_token', '_method' ]);

        unset($data['_token']);
                $isUpdated = careform_form22::where('careform_id',$id)->update($data);
                $careformid=$request->careform_id;
$dataquery= careform::where('id',$careformid)->get();



     return \Redirect::route('careform.forms', $dataquery[0]['client_id']);



            }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\careform_form22  $careform_form22
     * @return \Illuminate\Http\Response
     */
    public function show(careform_form22 $careform_form22)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\careform_form22  $careform_form22
     * @return \Illuminate\Http\Response
     */
    public function edit(careform_form22 $careform_form22)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\careform_form22  $careform_form22
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, careform_form22 $careform_form22)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\careform_form22  $careform_form22
     * @return \Illuminate\Http\Response
     */
    public function destroy(careform_form22 $careform_form22)
    {
        //
    }
}
