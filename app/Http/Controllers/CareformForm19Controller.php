<?php

namespace App\Http\Controllers;

use App\careform_form19;
use Illuminate\Http\Request;

class CareformForm19Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {

          $careform_id=$id;
        $forms = careform_form19::where('careform_id',$id)->get();
        return view('Forms.careform.careform_form19',compact('forms','careform_id'));
        //
    }

         public function careform_form19Update(Request $request,careform_form19 $careform_form19, $id){


          $request->merge([
            'safety_hazards' => implode(',', (array) $request->get('safety_hazards'))
        ]);

        $request->merge([
            'sanitation_hazards' => implode(',', (array) $request->get('sanitation_hazards'))
        ]);

         $request->merge([
            'social_functioning' => implode(',', (array) $request->get('social_functioning'))
        ]);


          $request->merge([
            'isolation' => implode(',', (array) $request->get('isolation'))
        ]);



               $data = $request->except(['_token', '_method' ]);

        unset($data['_token']);
                $isUpdated = careform_form19::where('careform_id',$id)->update($data);
                $careformid=$request->careform_id;
        return \Redirect::route('careform_form20index', $careformid);





    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\careform_form19  $careform_form19
     * @return \Illuminate\Http\Response
     */
    public function show(careform_form19 $careform_form19)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\careform_form19  $careform_form19
     * @return \Illuminate\Http\Response
     */
    public function edit(careform_form19 $careform_form19)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\careform_form19  $careform_form19
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, careform_form19 $careform_form19)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\careform_form19  $careform_form19
     * @return \Illuminate\Http\Response
     */
    public function destroy(careform_form19 $careform_form19)
    {
        //
    }
}
