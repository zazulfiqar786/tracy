<?php

namespace App\Http\Controllers;

use App\AppendixK1;
use Illuminate\Http\Request;

class AppendixK1Controller extends Controller
{

    public function index($id)
    {
        $clientId = $id;
        $forms = AppendixK1::all();
        return view('Forms.AppendixK1.index',$clientId->compact('forms'));
    }


    public function create()
    {
        return view('Forms.AppendixK1.create');
    }


    public function store(Request $request)
    {

        $request->merge([
            'status' => implode(',', (array) $request->get('status'))
        ]);

        $request->merge([
            'discharge_reasons' => implode(',', (array) $request->get('discharge_reasons'))
        ]);

        $request->merge([
            'transfer_reasons' => implode(',', (array) $request->get('transfer_reasons'))
        ]);

        $data = $request->all();

        $isCreated = AppendixK1::create($data);
        return redirect()->route('Appendixmy.client.forms',$request->client_id);
    }


    public function show(AppendixK1 $appendixK1,$id)
    {

        $form = AppendixK1::find($id);
        // dd($form);
        return view('Forms.AppendixK1.show',compact('form'));
        //
    }


    public function edit(AppendixK1 $appendixK1,$id)
    {
        $form = AppendixK1::find($id);
        // dd($form);
        return view('Forms.AppendixK1.edit',compact('form'));

        //
    }


    public function update(Request $request, AppendixK1 $appendixK1,$id)
    {
        $request->merge([
            'status' => implode(',', (array) $request->get('status'))
        ]);

        $request->merge([
            'discharge_reasons' => implode(',', (array) $request->get('discharge_reasons'))
        ]);

        $request->merge([
            'transfer_reasons' => implode(',', (array) $request->get('transfer_reasons'))
        ]);

        $data = $request->all();
        $isUpdated = AppendixK1::find($id)->update($data);

        // return redirect()->route('Forms.AppendixK1.index',$request->client_id);
        return redirect()->route('Appendixmy.client.forms',$request->client_id);
    }

    public function myClientForms($id)
    {

        $data['clientId'] = $id;
        $data['forms'] = AppendixK1::where('client_id', $id)->get();
        return view('Forms.AppendixK1.index',$data);
    }
    public function createForm($id)
    {

        $clientId = $id;
        return view('Forms.AppendixK1.create',compact('clientId'));
    }

    public function destroy(AppendixK1 $appendixK1, $id)
    {
        $isDeleted =  AppendixK1::where('id', $id)->delete();
        if($isDeleted)
        {
            return back();
        }
    }
}
