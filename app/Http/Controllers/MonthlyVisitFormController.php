<?php

namespace App\Http\Controllers;

use App\MonthlyVisitForm;
use Illuminate\Http\Request;

class MonthlyVisitFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->merge([
            'monthly_contact' => implode(',', (array) $request->get('monthly_contact'))
        ]);

        $request->merge([
            'permision_for_tele' => implode(',', (array) $request->get('permision_for_tele'))
        ]);

        $request->merge([
            'appearence_of_emotional' => implode(',', (array) $request->get('appearence_of_emotional'))
        ]);
        $request->merge([
            'monthly_health_provider' => implode(',', (array) $request->get('monthly_health_provider'))
        ]);
        $request->merge([
            'referral_needed' => implode(',', (array) $request->get('referral_needed'))
        ]);
        $request->merge([
            'services_report' => implode(',', (array) $request->get('services_report'))
        ]);
        $request->merge([
            'medical' => implode(',', (array) $request->get('medical'))
        ]);
        $request->merge([
            'skin_integrity' => implode(',', (array) $request->get('skin_integrity'))
        ]);

        $request->merge([
            'active_wound_care' => implode(',', (array) $request->get('active_wound_care'))
        ]);

        $request->merge([
            'decubitus_stage' => implode(',', (array) $request->get('decubitus_stage'))
        ]);

        $request->merge([
            'uti' => implode(',', (array) $request->get('uti'))
        ]);
        $request->merge([
            'bowel' => implode(',', (array) $request->get('bowel'))
        ]);

        $request->merge([
            'bladder' => implode(',', (array) $request->get('bladder'))
        ]);
        $request->merge([
            'ambulation' => implode(',', (array) $request->get('ambulation'))
        ]);
        $request->merge([
            'transfer_assistance' => implode(',', (array) $request->get('transfer_assistance'))
        ]);

        $request->merge([
            'fall_past' => implode(',', (array) $request->get('fall_past'))
        ]);
        $request->merge([
            'meal_plan' => implode(',', (array) $request->get('meal_plan'))
        ]);





        $data = $request->all();

        $isCreated = MonthlyVisitForm::create($data);
        return redirect()->route('monthlyVisitReport.forms',$request->client_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MonthlyVisitForm  $monthlyVisitForm
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MonthlyVisitForm  $monthlyVisitForm
     * @return \Illuminate\Http\Response
     */
    public function edit(MonthlyVisitForm $monthlyVisitForm)
    {
        dd('edit');
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MonthlyVisitForm  $monthlyVisitForm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MonthlyVisitForm $monthlyVisitForm,$id)
    {

        $request->merge([
            'monthly_contact' => implode(',', (array) $request->get('monthly_contact'))
        ]);

        $request->merge([
            'permision_for_tele' => implode(',', (array) $request->get('permision_for_tele'))
        ]);

        $request->merge([
            'appearence_of_emotional' => implode(',', (array) $request->get('appearence_of_emotional'))
        ]);
        $request->merge([
            'monthly_health_provider' => implode(',', (array) $request->get('monthly_health_provider'))
        ]);
        $request->merge([
            'referral_needed' => implode(',', (array) $request->get('referral_needed'))
        ]);
        $request->merge([
            'services_report' => implode(',', (array) $request->get('services_report'))
        ]);
        $request->merge([
            'medical' => implode(',', (array) $request->get('medical'))
        ]);
        $request->merge([
            'skin_integrity' => implode(',', (array) $request->get('skin_integrity'))
        ]);

        $request->merge([
            'active_wound_care' => implode(',', (array) $request->get('active_wound_care'))
        ]);

        $request->merge([
            'decubitus_stage' => implode(',', (array) $request->get('decubitus_stage'))
        ]);

        $request->merge([
            'uti' => implode(',', (array) $request->get('uti'))
        ]);
        $request->merge([
            'bowel' => implode(',', (array) $request->get('bowel'))
        ]);

        $request->merge([
            'bladder' => implode(',', (array) $request->get('bladder'))
        ]);
        $request->merge([
            'ambulation' => implode(',', (array) $request->get('ambulation'))
        ]);
        $request->merge([
            'transfer_assistance' => implode(',', (array) $request->get('transfer_assistance'))
        ]);

        $request->merge([
            'fall_past' => implode(',', (array) $request->get('fall_past'))
        ]);
        $request->merge([
            'meal_plan' => implode(',', (array) $request->get('meal_plan'))
        ]);


        $data = $request->all();
        $isUpdated = MonthlyVisitForm::find($id)->update($data);
        return redirect()->route('monthlyVisitReport.forms',$request->client_id);
        //
    }

    public function myClientForms($id)
    {

        // dd($id);
        $data['clientId'] = $id;
        $data['forms'] = MonthlyVisitForm::where('client_id', $id)->get();
        return view('Forms.MonthlyVisitForm.index',$data);
    }
    public function createForm($id)
    {
        $clientId = $id;
        return view('Forms.MonthlyVisitForm.create',compact('clientId'));
    }
    public function editForm(MonthlyVisitForm $MonthlyVisitForm ,$id)
    {

        $clientId = $id;
        // dd('Show by Id');
        $form = MonthlyVisitForm::find($id);
        return view('Forms.MonthlyVisitForm.edit',compact('form','clientId'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MonthlyVisitForm  $monthlyVisitForm
     * @return \Illuminate\Http\Response
     */
    public function destroy(MonthlyVisitForm $monthlyVisitForm,$id)
    {

        $isDeleted =  MonthlyVisitForm::where('id', $id)->delete();
        if($isDeleted)
        {
            return back();
        }
        //
    }

    public function show(MonthlyVisitForm $MonthlyVisitForm ,$id)
    {

        // dd('Show by Id');
        $form = MonthlyVisitForm::find($id);
        return view('Forms.MonthlyVisitForm.show',compact('form'));
    }




}
