<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppendixK1 extends Model
{
    protected $fillable = [
        'provider_id',
        'client_id',
        'case_manager_id',
        'provider_name',
        'provider_number',
        'provider_address',
        'member_name',
        'medicaid_number',
        'status',
        'effective_date',
        'discharge_reasons',
        'transfer_reasons',
        'provider_signature',
        'date',
    ];
}
