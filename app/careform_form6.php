<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class careform_form6 extends Model
{
    protected $fillable = [
        'client_id',
        'case_manager_id',
        'careform_id',
        'name_of_member',
        'dob',
        'medicaid',
        'date',
        'case_manager_name',
        'signature',
        'first_quarter',
        'first_quarter_date',
        'first_quarter_cm',
        'second_quarter',
        'second_quarter_date',
        'second_quarter_cm',
        'third_quarter',
        'third_quarter_date',
        'third_quarter_cm',
        'forth_quarter',
        'forth_quarter_date',
        'forth_quarter_cm',
        
    ];
}
