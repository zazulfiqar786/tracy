<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientChangeForm extends Model
{
    protected $fillable = [
        'provider_id',
        'client_id',
        'case_manager_id',
        'member_name',
        'date_of_change',
        'change_type',
        'name_new_case_manager',
        'approved_hours',
        'remaining_hours',
        'additionals_notes_documentation',
        'case_manager_signature',
        'case_manager_date',
        'owner_signature',
        'owner_date',

    ];
}
