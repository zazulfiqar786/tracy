<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MonthlyContactForm extends Model
{
    protected $fillable = [
        'provider_id',
        'client_id',
        'case_manager_id',
        'cal_date',
        'time_of_date',
        'worker',
        'cal_type',
        'contact_name',
        'relation',
        'topic_of_discussion',
        'narraitive_requirement',
        'curent_source',
        'curent_icwp',
        'file_name',
        'file_address',
        'file_phone',
        'file_relationship',
        'contacts_name',
        'contacts_address',
        'contacts_phone',
        'contacts_relationship',
        'reviewed_population',
        'varified',
        'case_manager_signature',
        'case_manager_date',
        'owner_signature',
        'owner_date',
        'created_at',
        'updated_at'
    ];
}
