<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class careform_form13 extends Model
{
    protected $fillable = [
        'client_id',
        'case_manager_id',
        'careform_id',
        'case_manager',
        'case_manager_date',
        'participant',
        'participant_date',
        'authorized_representative',
        'authorized_representative_date',
        'witness',
        'witness_date',
        'refusal_participant',
        'refusal_participant_date',
        'refusal_authorized_representative',
        'refusal_authorized_representative_date',
        'refusal_witness',
        'refusal_witness_date',
    ];


}
