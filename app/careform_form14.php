<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class careform_form14 extends Model
{
    protected $fillable = [
        'client_id',
        'case_manager_id',
        'careform_id',
        'member_name_last'];
}
