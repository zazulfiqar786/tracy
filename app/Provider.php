<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $fillable = ['user_id','name','email','password','contact','country','state','city','county','area',
        'zip_code','status'];
}
