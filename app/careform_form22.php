<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class careform_form22 extends Model
{
    protected $fillable = [
        'client_id',
        'case_manager_id',
        'careform_id',
        'member_name',
        'i_agree_to_provide',
        'skills',
        'sign1',
        'relationship1',
        'date1',
        'sign2',
        'relationship2',
        'date2',
        'sign3',
        'date3',
        'witness',
        'date4'];
}
