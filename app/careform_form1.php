<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class careform_form1 extends Model
{
    protected $fillable = [
        'client_id',
        'case_manager_id',
        'careform_id',
        'careform_form1_equipment',
        'careform_form1_equipment_date',
        'careform_form1_miscellaneous',
        'careform_form1_wheelchair',
        'careform_form1_assistive',
        'careform_form1_respiratory',
        'careform_form1_mobility',
        'careform_form1_bathroom',
        'careform_form1_beds',
        'careform_form1_transfer',
        'created_at',
        'updated_at',



    ];
}
