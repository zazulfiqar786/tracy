<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class careform_form3 extends Model
{
    protected $fillable = [
        'client_id',
        'case_manager_id',
        'careform_id',
        'icwp_financial_summary',
        'date',

        'service_description_1',
        'frequency_intensity_1',
        'responsible_provider_1',
        'per_unit_cost_1',
        'yearly_cast_1',


        'service_description_2',
        'frequency_intensity_2',
        'responsible_provider_2',
        'per_unit_cost_2',
        'yearly_cast_2',


        'service_description_3',
        'frequency_intensity_3',
        'responsible_provider_3',
        'per_unit_cost_3',
        'yearly_cast_3',


        'service_description_4',
        'frequency_intensity_4',
        'responsible_provider_4',
        'per_unit_cost_4',
        'yearly_cast_4',


        'service_description_5',
        'frequency_intensity_5',
        'responsible_provider_5',
        'per_unit_cost_5',
        'yearly_cast_5',


        'service_description_6',
        'frequency_intensity_6',
        'responsible_provider_6',
        'per_unit_cost_6',
        'yearly_cast_6',


        'service_description_7',
        'frequency_intensity_7',
        'responsible_provider_7',
        'per_unit_cost_7',
        'yearly_cast_7',


        'service_description_8',
        'frequency_intensity_8',
        'responsible_provider_8',
        'per_unit_cost_8',
        'yearly_cast_8'
    ];

}
