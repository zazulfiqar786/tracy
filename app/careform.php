<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class careform extends Model
{
    protected $fillable = [
        'client_id',
        'case_manager_id',
        'provider_id',
        'member_name',
        'medicaid_id',
        'date_completed',
        'current_street_address',
        'current_appointment_no',
        'current_city',
        'current_state',
        'current_zip',
        'current_country',
        'previous_street_address',
        'previous_appointment_no',
        'previous_city',
        'previous_state',
        'previous_zip',
        'previous_country',
        'living_setting',
        'relative_name',
        'relative_relationship_to_member',
        'relative_phone',
        'relative_alternate_phone',
        'relative_street_address',
        'relative_city',
        'relative_zip',
        'relative_country',
    ];
}
